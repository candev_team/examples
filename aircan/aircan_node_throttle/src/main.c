/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2009 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#include <bsp.h>
#include <shell.h>

#define CK_TASK_PRIO    3
#define CK_TASK_STACK   (2 * 1024)

extern void ckappMain(void);

static void ck_task (void * arg)
{
   (void)arg;

   ckappMain();
}

int main(void)
{
   shell_prompt_set ("throttle>");

   /* Change values in bsp setup when calling set_i2c_pot_vcc_servo(wra_value)
    * and set_i2c_pot_vcc_sensor(wrb_value) to get correct voltage
    * (depends on input voltage) for the peripherals to be connected.
    * Or call from application if more than one application use the same bsp
    * and need to have different voltages.
    */

   task_spawn (
         "CAN Kingdom",
         ck_task,
         CK_TASK_PRIO,
         CK_TASK_STACK,
         NULL);

   return 0;
}
