/*
 * throttle.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include "motor.h"

#include <bsp.h>

#define ANGLE_MAX 511
#define ANGLE_MIN -512

#define PWM_SERVO_MIN      400      /* 4% = 0.8ms */
#define PWM_SERVO_CENTER   750      /* 7.5% = 1.5ms */
#define PWM_SERVO_MAX      1100     /* 11% = 2.2 ms */

extern process_data_t * process_data;

void ckappOnIdle(void)
{
}

bool checkShutdown(void)
{
   return false;
}

static uint16_t angle_to_pwm (int16_t angle_max, int16_t angle_min, int16_t angle, uint16_t pwm_max, uint16_t pwm_min)
{
   float angle_range = angle_max - angle_min;
   float pwm_range = pwm_max - pwm_min;

   float range_mult = pwm_range / angle_range;

   return ((float)(angle - angle_min) * range_mult) + pwm_min;
}

void handleMotor_ctrl(ckd_motor_ctrl* doc)
{
   uint16_t duty_cycle;

   duty_cycle = angle_to_pwm (ANGLE_MAX, ANGLE_MIN, doc->motor_speed, PWM_SERVO_MAX, PWM_SERVO_MIN);
   duty_cycle = PWM_SERVO_MIN + (PWM_SERVO_MAX - duty_cycle);

   mtx_lock(process_data->pd_lock);
   process_data->pwm[PWM_SERVO] = duty_cycle;
   mtx_unlock(process_data->pd_lock);
}
