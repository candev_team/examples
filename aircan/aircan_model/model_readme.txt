Aircan Model
============

The model project contains the Kingdom Founder model files. They specify the
structure of the system and its parts, most importantly the messages that each
city (embedded unit in CAN Kingdom terminology) can send and receive.

Kingdom Founder can generate source code for sending and receiving the messages
that are specified in the model.

When the source code has been generated it should be placed in the module projects
(joystick module, servo module and motor module projects).
