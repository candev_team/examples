/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PA9
#define GPIO_UART1_RX   GPIO_PA10

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_ADC1_IN1   GPIO_PA0
#define GPIO_ADC1_IN2   GPIO_PA1
#define GPIO_ADC1_IN3   GPIO_PA2
#define GPIO_ADC1_IN4   GPIO_PA3

#define GPIO_SW1        GPIO_PB10
#define GPIO_SW2        GPIO_PB11
#define GPIO_SW3        GPIO_PC15
#define GPIO_SW4        GPIO_PC6
#define GPIO_SW5        GPIO_PA4
#define GPIO_SW6        GPIO_PA5
#define GPIO_SW7        GPIO_PA6
#define GPIO_SW8        GPIO_PC13

#define GPIO_IO1        GPIO_PC0
#define GPIO_IO2        GPIO_PC1
#define GPIO_IO3        GPIO_PC2
#define GPIO_IO4        GPIO_PC3

#define GPIO_PWRON_1    GPIO_PC4
#define GPIO_PWRON_2    GPIO_PC5
#define GPIO_PWRON_3    GPIO_PB0
#define GPIO_PWRON_4    GPIO_PB1

#define GPIO_SPI3_MOSI  GPIO_PC12
#define GPIO_SPI3_MISO  GPIO_PC11
#define GPIO_SPI3_SCK   GPIO_PC10
#define GPIO_SPI3_CS    GPIO_PD2

#define GPIO_I2C_SDA    GPIO_PB7
#define GPIO_I2C_SCL    GPIO_PB6

#define GPIO_VDD_IO     GPIO_PB2


#define VALUE_NOW       0
#define VALUE_MIN       1
#define VALUE_MAX       2

#define ADC_JOYSTICK_Y  0           /* ~1900 top,  ~240 bottom */
#define ADC_JOYSTICK_X  1           /* ~1980 left, ~320 right */
#define ADC_KNOB        2
#define ADC_SLIDER      3

#define GPIO_TRIM_X_DEC_INV   0
#define GPIO_TRIM_X_INC_INV   1
#define GPIO_TRIM_Y_INC_INV   2
#define GPIO_TRIM_Y_DEC_INV   3
#define GPIO_DIR_SW_UP        4

#define ADC_VALUES            4

/** process_data data structure, used to represent process data e.g.  */
typedef struct process_data
{
   mtx_t *  pd_lock;       /**< Process data mutex */
   uint16_t adc[3][ADC_VALUES];     /**< Sampled adc values [NOW, MIN, MAX] */
   bool     gpio[8];      /**< GPIO switch inputs */
} process_data_t;

void bsp_reset_peripherals (void);

#endif /* BSP_H */
