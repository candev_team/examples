/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

/**
 * Lua module for accessing i2c slaves.
 *
 * The module represents the i2c slaves as instances of a class.
 * We do it this way instead of the simpler approach where the i2c slaves are
 * file descriptors acted on by functions because:
 * - it allows us to close the files descriptors even in case where exception
 *   is thrown (using destructors).
 * - it allows us to keep state for each slave, enabling stronger type checking.
 */

#include "lua_i2c.h"
#include <lauxlib.h>
#include <i2c/i2c.h>
#include <string.h>
#include <fcntl.h>   /* for open() */
#include <unistd.h>  /* for read()/write() */

#undef RTK_DEBUG

#ifdef RTK_DEBUG
#define DPRINT(...) rprintp ("lua_i2c: "__VA_ARGS__)
#else
#define DPRINT(...)
#endif  /* RTK_DEBUG */

#define LUA_I2C_SLAVE LUA_I2C_LIBNAME".slave"

/* i2c slave type. Exposed to Lua as a "userdata" type. */
typedef struct lua_i2c_slave
{
   int fd;
   bool is_opened;
} lua_i2c_slave_t;

static void lua_i2c_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Lua: self = i2c.open('/i2c_bus/slave') */
static int lua_i2c_open (lua_State * L)
{
   /* Get one argument from stack */
   lua_i2c_check_number_of_arguments (L, 1);
   const char * slave_name = luaL_checkstring (L, 1);

   /* Push new class instance at top of stack */
   lua_i2c_slave_t * self = lua_newuserdata(L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable(L, LUA_I2C_SLAVE); /* Set class for instance */

   self->fd = open (slave_name, O_RDWR, 0);
   if (self->fd < 0)
   {
      return luaL_error (L, "could not open slave '%s'", slave_name);
   }
   self->is_opened = true;
   DPRINT ("Opened\n");

   /* One result is already on stack */
   return 1;
}

/* Lua: self.close(self) OR self:close() */
static int lua_i2c_close (lua_State * L)
{
   /* Get one argument from stack */
   lua_i2c_check_number_of_arguments (L, 1);
   lua_i2c_slave_t * self = luaL_checkudata (L, 1, LUA_I2C_SLAVE);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "slave already closed");
   ASSERT (self->fd >= 0);

   int error = close (self->fd);
   if (error)
   {
      return luaL_error (L, "error closing slave");
   }

   /* Mark as closed */
   self->is_opened = false;
   DPRINT ("Closed\n");

   /* Push no results on stack */
   return 0;
}

/* Destructor. Called by garbage collector. */
static int lua_i2c_collect_garbage (lua_State * L)
{
   /* Get one argument from stack */
   lua_i2c_slave_t * self = luaL_checkudata (L, 1, LUA_I2C_SLAVE);
   ASSERT (self != NULL);

   if (self->is_opened == false)
   {
      /* Already closed, so nothing to clean up */
      DPRINT ("GC: nothing to do\n");
      return 0; /* Push no results on stack */
   }

   int error = close (self->fd);
   if (error)
   {
      return luaL_error (L, "error closing slave");
   }

   /* Mark as closed */
   self->is_opened = false;
   DPRINT ("GC: closed\n");

   /* Push no results on stack */
   return 0;
}

/* Lua: data = self.read(self, size) OR data = self:read(size) */
static int lua_i2c_read (lua_State * L)
{
   /* Get two arguments from stack */
   lua_i2c_check_number_of_arguments (L, 2);
   lua_i2c_slave_t * self = luaL_checkudata (L, 1, LUA_I2C_SLAVE);
   int size = luaL_checkinteger (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "slave not opened");
   luaL_argcheck (L, size > 0, 2, "size needs to be positive");
   ASSERT (self->fd >= 0);

   luaL_Buffer buffer;
   char * raw_buffer = luaL_buffinitsize (L, &buffer, size);
   (void)read (self->fd, raw_buffer, size);

   /* Push one result on stack */
   luaL_pushresultsize (&buffer, size);
   return 1;
}

/* Lua: self.write(self, data) OR self:write(data) */
static int lua_i2c_write (lua_State * L)
{
   /* Get two arguments from stack */
   size_t size;
   lua_i2c_check_number_of_arguments (L, 2);
   lua_i2c_slave_t * self = luaL_checkudata (L, 1, LUA_I2C_SLAVE);
   luaL_argcheck (L, lua_type (L, 2) != LUA_TNUMBER, 2, "string expected"); /* Disallow implicit conversion */
   const char * string = luaL_checklstring (L, 2, &size);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "slave not opened");
   luaL_argcheck (L, size > 0, 2, "size needs to be positive");
   ASSERT (self->fd >= 0);

   (void)write (self->fd, string, size);

   /* Push no results on stack */
   return 0;
}

static void lua_i2c_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
        {"close", lua_i2c_close},
        {"read", lua_i2c_read},
        {"write", lua_i2c_write},
        {"__gc", lua_i2c_collect_garbage},
        {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for i2c slave class in the global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_I2C_SLAVE)
    *      metatable.__index = metatable
    *      metatable.write = ..
    *      metatable.read = .. (and so on)
    */
   luaL_newmetatable (L, LUA_I2C_SLAVE); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);                /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");      /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);        /* Add methods to metatable */
   lua_pop (L, 1);                       /* Pop metatable reference from stack */
}

int lua_i2c_openlib (lua_State * L)
{
   lua_i2c_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
       {"open", lua_i2c_open},
       {NULL, NULL}  /* Sentinel */
   };
   luaL_newlib (L, functions);
   return 1;
}
