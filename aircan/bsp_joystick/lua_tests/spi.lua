-- Test Lua-spi integration

slavel_name = '/spi0/slave1'
invalid_slavel_name = '/spi0/slave99'

-- Send/receive single byte buffer (no errors)
slave = spi.open(slavel_name)
slave:select()
sent = '1'
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:unselect()
slave:close()

-- Send/receive buffer with special characters (no errors)
slave = spi.open(slavel_name)
slave:select()
sent = '\x00\a\b\f\n\r\t\v\\\x00\xff'
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:unselect()
slave:close()

-- Send/receive large buffer (no errors)
slave = spi.open(slavel_name)
slave:select()
sent = string.rep('\x42', 6000)
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:unselect()
slave:close()

-- Test open() error handling
assert(pcall(spi.open, invalid_slavel_name) == false) -- 'could not open slave'
assert(pcall(spi.open, slavel_name, 'extra') == false) -- 'wrong number of arguments'
assert(pcall(spi.open) == false) -- 'wrong number of arguments'

-- Test close() error handling
slave = spi.open(slavel_name)
assert(pcall(spi.close, slave, 'extra') == false) -- 'wrong number of arguments'
assert(pcall(spi.close) == false) -- 'wrong number of arguments'
slave:select()
assert(pcall(spi.close, slave) == false) -- 'wrong number of arguments'
slave:unselect()
slave:close()

-- Test select()/unselect() error handling
slave = spi.open(slavel_name)
assert(pcall(slave.unselect, slave) == false)  -- 'slave not selected'
slave:select()
assert(pcall(slave.select, slave) == false)  -- 'slave already selected'
slave:unselect()
slave:close()

-- Test read() error handling
slave = spi.open(slavel_name)
assert(pcall(slave.read, slave, 1) == false)  -- 'slave not selected'
slave:select()
assert(pcall(slave.read, slave, 0) == false)  -- 'size needs to be positive'
assert(pcall(slave.read, slave, -1) == false)  -- 'size needs to be positive'
slave:unselect()
slave:close()

-- Test write() error handling
slave = spi.open(slavel_name)
assert(pcall(slave.write, slave, 'data') == false)  -- 'slave not selected'
slave:select()
assert(pcall(slave.write, slave, '') == false)  -- 'size needs to be positive'
assert(pcall(slave.write, slave, 5) == false)  -- 'string expected'
assert(pcall(slave.write, slave, 5.0) == false)  -- 'string expected'
slave:unselect()
slave:close()

