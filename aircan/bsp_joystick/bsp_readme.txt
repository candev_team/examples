BSP Read-me
===========

The BSP (Board Support Package) projects contain functionality for interacting
with, configuring and booting some specific kind of hardware.
