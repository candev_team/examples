/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PA9
#define GPIO_UART1_RX   GPIO_PA10

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_SERVO_PWM  GPIO_PC3

#define GPIO_ADC1_IN2   GPIO_PA1
#define GPIO_ADC1_IN3   GPIO_PA2
#define GPIO_ADC1_IN4   GPIO_PA3
#define GPIO_ADC2_IN1   GPIO_PA4

#define GPIO_FREQ_1     GPIO_PA6

#define GPIO_I2C_SDA    GPIO_PB7
#define GPIO_I2C_SCL    GPIO_PB6

#define GPIO_DEBUG_LED  GPIO_PA8


#define ADC_SENSOR          0
#define ADC_SERVO_CURRENT   1
#define ADC_BAT_VOLTAGE     2
#define ADC_SERVO_VOLTAGE   3
#define PWM_SERVO           0

/** process_data data structure, used to represent process data e.g.  */
typedef struct process_data
{
   mtx_t *  pd_lock;       /**< Process data mutex */
   uint16_t adc[4];        /**< Sampled adc values */
   uint16_t pwm[1];        /**< Output PWM duty cycles */
} process_data_t;

void bsp_reset_peripherals (void);

#endif /* BSP_H */
