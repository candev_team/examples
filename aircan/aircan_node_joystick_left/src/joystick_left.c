/*
 * joystick_left.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include "joystick.h"

#include <bsp.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

extern process_data_t * process_data;

#define ANGLE_MAX                   511
#define ANGLE_MIN                   -512
#define JOYSTICK_SEND_INTERVAL_MS   10
#define MAX_MIN_SAVE_DELAY_TIME_MS  (10 * 1000)
#define MAX_MIN_FILE                "/disk1/adc_max.min"

static uint16_t adc_min[ADC_VALUES];
static uint16_t adc_max[ADC_VALUES];

static bool send = false;
static bool save = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

static void save_tmr_callback (tmr_t * timer, void * arg)
{
   save = true;
   tmr_stop (timer);
}

static int16_t adc_to_angle (uint16_t adc_max, uint16_t adc_min, uint16_t adc_now, int16_t angle_max, int16_t angle_min)
{
   float angle_range = angle_max - angle_min;
   float adc_range = adc_max - adc_min;

   float range_mult = angle_range / adc_range;

   return ((float)(adc_now - adc_min) * range_mult) + angle_min;
}

static void read_max_min (void)
{
   struct stat file_status;

   if (stat (MAX_MIN_FILE, &file_status) == 0)
   {
      int file = open (MAX_MIN_FILE, O_RDONLY, 0);

      read (file, adc_min, sizeof(adc_min));
      read (file, adc_max, sizeof(adc_max));

      close (file);
   }
   else
   {
      uint32_t i;

      for (i = 0; i < ADC_VALUES; i++)
      {
         adc_min[i] = 0xFFFF;
         adc_max[i] = 0;
      }
   }

   mtx_lock (process_data->pd_lock);
   memcpy (process_data->adc[VALUE_MIN], adc_min, sizeof(process_data->adc[VALUE_MIN]));
   memcpy (process_data->adc[VALUE_MAX], adc_max, sizeof(process_data->adc[VALUE_MAX]));
   mtx_unlock (process_data->pd_lock);
}

static void save_max_min (void)
{
   int file = open (MAX_MIN_FILE, O_CREAT | O_WRONLY, 0);

   write (file, adc_min, sizeof(adc_min));
   write (file, adc_max, sizeof(adc_max));

   close (file);
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static tmr_t * save_timer;
   static bool setup = false;

   static ckd_xout xout;
   static ckd_yout yout;
   static ckd_dout dout;
   static ckd_joyMotCtrl mctrl;

   static bool direction = false;
   static bool old_direction = false;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (JOYSTICK_SEND_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      save_timer = tmr_create (tick_from_ms (MAX_MIN_SAVE_DELAY_TIME_MS), save_tmr_callback, NULL, TMR_ONCE);

      read_max_min ();

      setup = true;
   }
   else
   {
      mtx_lock(process_data->pd_lock);
      if ((memcmp(adc_min, process_data->adc[VALUE_MIN], sizeof(adc_min)) != 0) ||
            (memcmp(adc_max, process_data->adc[VALUE_MAX], sizeof(adc_max)) != 0))
      {
         tmr_stop (save_timer);
         tmr_set (save_timer, tick_from_ms (MAX_MIN_SAVE_DELAY_TIME_MS));
         tmr_start (save_timer);
      }

      memcpy (adc_min, process_data->adc[VALUE_MIN], sizeof(adc_min));
      memcpy (adc_max, process_data->adc[VALUE_MAX], sizeof(adc_max));
      mtx_unlock(process_data->pd_lock);

      if (save)
      {
         save = false;
         save_max_min ();
      }

      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);

         xout.joyx = adc_to_angle (process_data->adc[VALUE_MAX][ADC_JOYSTICK_X], process_data->adc[VALUE_MIN][ADC_JOYSTICK_X], process_data->adc[VALUE_NOW][ADC_JOYSTICK_X], ANGLE_MAX, ANGLE_MIN);
         yout.joyy = adc_to_angle (process_data->adc[VALUE_MAX][ADC_JOYSTICK_Y], process_data->adc[VALUE_MIN][ADC_JOYSTICK_Y], process_data->adc[VALUE_NOW][ADC_JOYSTICK_Y], ANGLE_MAX, ANGLE_MIN);
         mctrl.joyy = yout.joyy;

         if (process_data->gpio[GPIO_TRIM_X_INC_INV] == false)
         {
            xout.jtx = 1;
         }
         else if (process_data->gpio[GPIO_TRIM_X_DEC_INV] == false)
         {
            xout.jtx = -1;
         }
         else
         {
            xout.jtx = 0;
         }

         if (process_data->gpio[GPIO_TRIM_Y_INC_INV] == false)
         {
            yout.jty = 1;
         }
         else if (process_data->gpio[GPIO_TRIM_Y_DEC_INV] == false)
         {
            yout.jty = -1;
         }
         else
         {
            yout.jty = 0;
         }

         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_xout(&xout);
         ckappSend_CKDN_yout(&yout);
         ckappSend_CKDN_joyMotCtrl(&mctrl);

         mtx_lock(process_data->pd_lock);
         direction = process_data->gpio[GPIO_DIR_SW_UP];
         mtx_unlock(process_data->pd_lock);

         if ((direction == true) && (old_direction == false))
         {
            dout.jd = 1;
            ckappSend_CKDN_dout(&dout);
         }
         else if ((direction == false) && (old_direction == true))
         {
            dout.jd = 0;
            ckappSend_CKDN_dout(&dout);
         }

         old_direction = direction;
      }
   }
}

bool checkShutdown(void)
{
   return false;
}
