

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick.h
 * 
 * Joystick for controlling RC aircrafts.
 * 
 * Generated from: /aircan_model/joystick.kfm
 */

#ifndef SAJ
#define SAJ

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Joystick X Output. Record number: 11, List number: 1
#define CKDN_xout 11
 
typedef struct {
  int16_t joyx:10; // Joystick Angle X
  int8_t jtx:2; // Joystick Trim X
  
    
    
    
  
} ckd_xout;


// Joystick Y Output. Record number: 12, List number: 1
#define CKDN_yout 12
 
typedef struct {
  int16_t joyy:10; // Joystick Angle Y
  int8_t jty:2; // Joystick Trim Y
  
    
    
    
  
} ckd_yout;


// Joystick Direction Output. Record number: 13, List number: 1
#define CKDN_dout 13
 
typedef struct {
  uint8_t jd:1; // Direction
  
    
    
    
  
} ckd_dout;


// Joystick Motor Control. Record number: 14, List number: 1
#define CKDN_joyMotCtrl 14
 
typedef struct {
  int16_t joyy:10; // Joystick Angle Y
  
    
    
    
  
} ckd_joyMotCtrl;



#pragma pack()

// *** Transmit functions

void ckappSend_CKDN_xout(ckd_xout* doc);
void ckappSend_CKDN_yout(ckd_yout* doc);
void ckappSend_CKDN_dout(ckd_dout* doc);
void ckappSend_CKDN_joyMotCtrl(ckd_joyMotCtrl* doc);

#endif /* SAJ */
