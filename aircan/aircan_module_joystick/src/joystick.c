

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick.h
 * 
 * Joystick for controlling RC aircrafts.
 * 
 * Generated from: /aircan_model/joystick.kfm
 */

#include "ck.h"
#include "joystick.h"


// *** Receive handle functions


// *** Transmit functions
void ckappSend_CKDN_xout(ckd_xout* doc) {
  ckSend(CKDN_xout, (uint8_t*) doc, sizeof(ckd_xout), 0);
}
void ckappSend_CKDN_yout(ckd_yout* doc) {
  ckSend(CKDN_yout, (uint8_t*) doc, sizeof(ckd_yout), 0);
}
void ckappSend_CKDN_dout(ckd_dout* doc) {
  ckSend(CKDN_dout, (uint8_t*) doc, sizeof(ckd_dout), 0);
}
void ckappSend_CKDN_joyMotCtrl(ckd_joyMotCtrl* doc) {
  ckSend(CKDN_joyMotCtrl, (uint8_t*) doc, sizeof(ckd_joyMotCtrl), 0);
}

extern void ckappOnIdle(void);
extern bool checkShutdown(void);

void ckappMain(void) {
  ckInit(ckStartListenForDefaultLetter);
  for (;;) {
    ckappOnIdle();
    ckMain();
    if (checkShutdown()) break;
  }
}

void ckappSetActionMode(ckActMode am) {
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
}

void ckappDispatchRTR(uint16_t doc, bool txF) {
}

int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  return 1; // Page not supported
}

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
  } // End of document switch
} // End of ckappDispatch

