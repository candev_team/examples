Aircan Node Read-me
===================

The node projects contains functionality that is specific to each individual
embedded unit. In Aircan all application code lives here. 

The nodes are called *cities* in  CAN Kingdom terminology.

The node projects also contain configuration files which can be written to
units to assign their node ID:s and CAN message ID:s (called *Envelopes* and
*City Numbers* in CAN Kingdom). The configurations files contain the same
information as the cities would save if they were configured by sending King's
documents to them. 
