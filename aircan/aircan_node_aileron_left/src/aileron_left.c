/*
 * aileron_left.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include "servo.h"

#include <bsp.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define ANGLE_MAX 511
#define ANGLE_MIN -512

#define PWM_SERVO_MIN         400      /* 4% = 0.8ms */
#define PWM_SERVO_CENTER      750      /* 7.5% = 1.5ms */
#define PWM_SERVO_MAX         1100     /* 11% = 2.2 ms */
#define SAVE_DELAY_TIME_MS    (10 * 1000)
#define SAVE_FILE             "/disk1/trim.inv"

extern process_data_t * process_data;

static bool save = false;
static int16_t trim = 0;
static bool invert_pwm_output = false;
static tmr_t * save_timer;

static void save_tmr_callback (tmr_t * timer, void * arg)
{
   save = true;
   tmr_stop (timer);
}

static uint16_t angle_to_pwm (int16_t angle_max, int16_t angle_min, int16_t angle, uint16_t pwm_max, uint16_t pwm_min)
{
   float angle_range = angle_max - angle_min;
   float pwm_range = pwm_max - pwm_min;

   float range_mult = pwm_range / angle_range;

   return ((float)(angle - angle_min) * range_mult) + pwm_min;
}

static void read_save_file (void)
{
   struct stat file_status;

   if (stat (SAVE_FILE, &file_status) == 0)
   {
      int file = open (SAVE_FILE, O_RDONLY, 0);

      read (file, &trim, sizeof(trim));
      read (file, &invert_pwm_output, sizeof(invert_pwm_output));

      close (file);
   }
}

static void save_save_file (void)
{
   int file = open (SAVE_FILE, O_CREAT | O_WRONLY, 0);

   write (file, &trim, sizeof(trim));
   write (file, &invert_pwm_output, sizeof(invert_pwm_output));

   close (file);
}

static void start_save_timer (void)
{
   tmr_stop (save_timer);
   tmr_set (save_timer, tick_from_ms(SAVE_DELAY_TIME_MS));
   tmr_start (save_timer);
}

void ckappOnIdle(void)
{
   static bool setup = false;

   if (!setup)
   {
      save_timer = tmr_create (tick_from_ms (SAVE_DELAY_TIME_MS), save_tmr_callback, NULL, TMR_ONCE);

      read_save_file ();

      setup = true;
   }
   else
   {
      if (save)
      {
         save = false;
         save_save_file ();
      }
   }
}

bool checkShutdown(void)
{
   return false;
}

void handleSi(ckd_si* doc)
{
   uint16_t duty_cycle;

   if ((doc->trim == -1) || (doc->trim == 1))
   {
      trim += doc->trim;

      start_save_timer ();
   }

   duty_cycle = angle_to_pwm (ANGLE_MAX, ANGLE_MIN, doc->aspv + trim, PWM_SERVO_MAX, PWM_SERVO_MIN);

   if (invert_pwm_output)
   {
      duty_cycle = PWM_SERVO_MIN + (PWM_SERVO_MAX - duty_cycle);
   }

   mtx_lock(process_data->pd_lock);
   process_data->pwm[PWM_SERVO] = duty_cycle;
   mtx_unlock(process_data->pd_lock);
}

void handleDir(ckd_dir* doc)
{
   invert_pwm_output = doc->dir;
   start_save_timer ();
}
