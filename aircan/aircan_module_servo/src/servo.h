

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * servo.h
 * 
 * 
 * 
 * Generated from: /aircan_model/servo.kfm
 */

#ifndef SAS
#define SAS

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Servo Input. Record number: 10, List number: 2
#define CKDN_si 10
 
typedef struct {
  int16_t aspv:10; // Angle
  int8_t trim:2; // Trim
  
    
    
    
  
} ckd_si;


// Direction. Record number: 11, List number: 2
#define CKDN_dir 11
 
typedef struct {
  uint8_t dir:1; // Direction
  
    
    
    
  
} ckd_dir;



#pragma pack()

// *** Transmit functions


#endif /* SAS */
