

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * motor.h
 * 
 * Motor for the Aircan example system.
 * 
 * Generated from: /aircan_model/motor.kfm
 */

#ifndef MOTOR
#define MOTOR

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Motor control. Record number: 10, List number: 2
#define CKDN_motor_ctrl 10
 
typedef struct {
  int16_t motor_speed:10; // Motor speed
  
    
    
    
  
} ckd_motor_ctrl;



#pragma pack()

// *** Transmit functions


#endif /* MOTOR */
