#include <kern.h>
#include <gpio.h>
#include <pwm/pwm.h>
#include <counter/counter.h>
#include <bsp.h>
#include <spi/spi.h>
#include <adc/adc.h>
#include <can/can.h>
#include <can/bxcan.h>
#include <nor/nor.h>

#include <string.h>
#include <stdlib.h>

#define TEST_SPI           1
#define TEST_ADC           0
#define TEST_GPIO          0
#define TEST_PWM_IN        0
#define TEST_FREQ_IN       0
#define TEST_SERVO         0
#define TEST_CAN           0
#define TEST_NOR           0
#define TEST_FUEL_VALVE    0
#define TEST_ULEVEL        0


#if TEST_CAN

#define CAN_MSG_RECEIVED         BIT(0)

typedef struct can_struct
{
   int can_fd;
   flags_t * can_flag;
} can_struct_t;

static void can_rx_callback (void * arg, can_event_t event )
{
   can_struct_t * can_struct = (can_struct_t *) arg;

   flags_set(can_struct->can_flag, CAN_MSG_RECEIVED);
}


static void can_handler_task (void * arg)
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   can_frame_t rx_frame;
   can_frame_t tx_frame;
   flags_value_t flags;

   while (1)
   {
      if (flags_wait_any_tmo(can_struct->can_flag, CAN_MSG_RECEIVED, 1000, &flags))
      {
         tx_frame.id = 0x555;
         tx_frame.dlc = 0;
         can_transmit(can_struct->can_fd, &tx_frame);
      }
      else
      {
         flags_clr(can_struct->can_flag, flags);
         can_receive(can_struct->can_fd, &rx_frame);

         tx_frame.id = rx_frame.id + 1;
         tx_frame.dlc = rx_frame.dlc;

         memcpy(tx_frame.data, rx_frame.data, rx_frame.dlc);
         can_transmit(can_struct->can_fd, &tx_frame);
      }
   }
}
#endif


const char *byte_to_binary(int x, int bits);

int main(void)
{
#if TEST_PWM_IN || TEST_SERVO || TEST_SPI || TEST_ADC || TEST_FUEL_VALVE || TEST_ULEVEL || TEST_FREQ_IN
   size_t i;
#endif
#if TEST_SPI || TEST_PWM_IN
   size_t j;
#endif
#if TEST_ADC
   int adc1;
   int adc2;
   int adc3;
   int adc4;
   int adc5;
   int adc_ref;
   uint32_t value;
#endif
#if TEST_SPI
   int spi_temp;
   uint8_t temp_count = 1;
   char * spi_path[] = {"/spi3/k-temp0", "/spi3/k-temp1", "/spi3/k-temp2"};
   uint32_t spi_value;
#endif
#if TEST_GPIO
   int pin_value;
#endif
#if TEST_PWM_IN
   int32_t values[2][3];
   int pwm_out;
   int pwm_in_per;
   int pwm_in_pul;
#endif /* TEST_PWM_IN */
#if TEST_SERVO
   int pwm0;
   int pwm1;
   int pwm2;
   int pwm3;
   int adc3;
   uint32_t value;
#endif /* TEST_SERVO */


   rprintf ("Hello world\n");

#if TEST_SPI
   // TODO implement test...
   /* NOTE: type-K thermocouple should be connected to X707, X708 and X710 */
   for (i = 0; i<temp_count; i++)
   {
      spi_value = 0;
      spi_temp = 0;
      rprintf ("Testing SPI device %s\n", spi_path[i]);
      spi_temp = open (spi_path[i], O_RDONLY, 0);
      ASSERT(spi_temp > 0);

      for (j = 0; j<1000; j++)
      {
         spi_select (spi_temp);
         read (spi_temp, &spi_value, 1);
         spi_unselect(spi_temp);

         rprintf ("  spi value\n%d\n", spi_value);
         rprintf ("  0b%s\n", byte_to_binary(spi_value, 16));
         rprintf ("  0b%s\n", byte_to_binary(spi_value & 3, 3));
         rprintf ("  temp value\n0x%02X\n", spi_value >> 3);
         rprintf ("  value %d\n", spi_value >> 3);
         rprintf ("  0b%s\n\n", byte_to_binary(spi_value >> 3, 13));
         usleep(1 * 1000 * 1000);
      }
      rprintf ("\n");
      close(spi_temp);

      usleep(5 * 1000 * 1000);
   }

#endif /* TEST_SPI */

#if TEST_ADC
   // TODO implement test...
   rprintf ("Starting ADC test\n");
   adc1 = adc2 = adc3 = adc4 = adc5 = adc_ref = 0;

   adc5 = adc_open ("/adc1/5");
   ASSERT(adc5 > 0);
   adc1 = adc_open ("/adc1/1");
   ASSERT(adc1 > 0);
   adc2 = adc_open ("/adc1/2");
   ASSERT(adc2 > 0);
   adc3 = adc_open ("/adc1/3");
   ASSERT(adc3 > 0);
   adc4 = adc_open ("/adc1/4");
   ASSERT(adc4 > 0);
   adc_ref = adc_open ("/adc1/18");
   ASSERT(adc_ref > 0);

   adc_start(adc5);
   adc_start(adc1);
   adc_start(adc2);
   adc_start(adc3);
   adc_start(adc4);
   adc_start(adc_ref);

   for (i=0; i<1000; i++)
   {
      gpio_set(GPIO_IO_2, i%2);
      value = adc_sample_get(adc5);
      rprintf ("adc5 %d\n", value);
      value = adc_sample_get(adc1);
      rprintf ("adc1 %d\n", value);
      value = adc_sample_get(adc2);
      rprintf ("adc2 %d\n", value);
      value = adc_sample_get(adc3);
      rprintf ("adc3 %d\n", value);
      value = adc_sample_get(adc4);
      rprintf ("adc4 %d\n", value);
      value = adc_sample_get(adc_ref);
      rprintf ("adc_ref %d\n", value);
      usleep(1 * 1000 * 1000);
   }


   rprintf ("ADC test done\n\n\n");
#endif /* TEST_ADC */

#if TEST_GPIO
   /* NOTE: IO_1 should be connected with  IO_2 */
   /* GPIO test
    * - Test IO_1 as input and IO_2 as output
    * - Test IO_2 as input and IO_1 as output
    * - Test IO_1 and IO_2 as input with pull-down
    * - Test IO_1 and IO_2 as input with pull-up
    * - Reconfigure IO_1 and IO_2 as output
    */
   static const gpio_cfg_t gpio_cfg_io_1_in_np[] =
      {{ GPIO_IO_1, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL }};
   static const gpio_cfg_t gpio_cfg_io_2_in_np[] =
      {{ GPIO_IO_2, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL }};
   static const gpio_cfg_t gpio_cfg_io_1_in_pd[] =
      {{ GPIO_IO_1, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_PULLDOWN }};
   static const gpio_cfg_t gpio_cfg_io_2_in_pd[] =
      {{ GPIO_IO_2, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_PULLDOWN }};
   static const gpio_cfg_t gpio_cfg_io_1_in_pu[] =
      {{ GPIO_IO_1, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_PULLUP }};
   static const gpio_cfg_t gpio_cfg_io_2_in_pu[] =
      {{ GPIO_IO_2, GPIO_INPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_PULLUP }};
   static const gpio_cfg_t gpio_cfg_io_1_out[] =
      {{ GPIO_IO_1, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL }};
   static const gpio_cfg_t gpio_cfg_io_2_out[] =
      {{ GPIO_IO_2, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL }};
   rprintf ("Starting GPIO test\n");
   pin_value = -1;

   rprintf ("Test IO_1 as input and IO_2 as output\n");
   gpio_configure_pin (gpio_cfg_io_1_in_np);
   gpio_configure_pin (gpio_cfg_io_2_out);
   gpio_set(GPIO_IO_2, 0);
   pin_value = gpio_get(GPIO_IO_1);
   rprintf ("pin_value should be 0: %d\n", pin_value);
   ASSERT(pin_value == 0);
   gpio_set(GPIO_IO_2, 1);
   pin_value = gpio_get(GPIO_IO_1);
   rprintf ("pin_value should be non-zero: %d\n\n", pin_value);
   ASSERT(pin_value != 0);

   rprintf ("Test IO_2 as input and IO_1 as output\n");
   gpio_configure_pin (gpio_cfg_io_2_in_np);
   gpio_configure_pin (gpio_cfg_io_1_out);
   gpio_set(GPIO_IO_1, 0);
   pin_value = gpio_get(GPIO_IO_2);
   rprintf ("pin_value should be 0: %d\n", pin_value);
   ASSERT(pin_value == 0);
   gpio_set(GPIO_IO_1, 1);
   pin_value = gpio_get(GPIO_IO_2);
   rprintf ("pin_value should be non-zero: %d\n\n", pin_value);
   ASSERT(pin_value != 0);

   rprintf ("Test IO_1 and IO_2 as input with pull-down\n");
   gpio_configure_pin (gpio_cfg_io_1_in_pd);
   gpio_configure_pin (gpio_cfg_io_2_in_pd);
   pin_value = gpio_get(GPIO_IO_1);
   rprintf ("IO_1 pin_value should be 0: %d\n", pin_value);
   ASSERT(pin_value == 0);
   pin_value = -1;
   pin_value = gpio_get(GPIO_IO_2);
   rprintf ("IO_2 pin_value should be 0: %d\n\n", pin_value);
   ASSERT(pin_value == 0);

   rprintf ("Test IO_1 and IO_2 as input with pull-up\n");
   gpio_configure_pin (gpio_cfg_io_1_in_pu);
   gpio_configure_pin (gpio_cfg_io_2_in_pu);
   pin_value = gpio_get(GPIO_IO_1);
   rprintf ("IO_1 pin_value should be non-zero: %d\n", pin_value);
   ASSERT(pin_value != 0);
   pin_value = -1;
   pin_value = gpio_get(GPIO_IO_2);
   rprintf ("IO_2 pin_value should be non-zero: %d\n\n", pin_value);
   ASSERT(pin_value != 0);

   /* Configure IO_1 and IO_2 as output */
   gpio_configure_pin (gpio_cfg_io_1_out);
   gpio_configure_pin (gpio_cfg_io_2_out);
   rprintf ("GPIO test done\n\n\n");
#endif /* TEST_GPIO */

#if TEST_PWM_IN
   /* NOTE: PWM_IN (PA5) should be connected to PWM1 */
   /* PWM in test
    * - Open and start all counter channels
    * - Open the first PWM out channel
    * - Set 50.00% duty cycle
    * - Start PWM out
    * - Test start/stop
    * - Test the two supported counter modes
    * - Test duty cycle change
    * - Test period change
    * - Stop and close all counter channels and the PWM out channel
    */
   rprintf ("Starting PWM in test\n");
   pwm_out = 0;
   pwm_in_per = pwm_in_pul = 0;

   pwm_in_per = counter_open("/counter0/0");
   ASSERT(pwm_in_per > 0);
   pwm_in_pul = counter_open("/counter0/1");
   ASSERT(pwm_in_pul > 0);

   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   counter_start(pwm_in_pul, COUNTER_DUAL_EDGE_MEASURE_PULSE);

   for (i = 0; i<2; i++)
   {
      for (j = 0; j<3; j++)
      {
         values[i][j] = 0;
      }
   }
   pwm_out = pwm_open("/pwm0/0");
   ASSERT(pwm_out > 0);
   pwm_duty_cycle_set(pwm_out, 5000);
   pwm_start(pwm_out);
   usleep(40 * 1000);

   /* Test start stop */
   rprintf ("Test start/stop\n");
   counter_read_time(pwm_in_per);
   usleep(25 * 1000);
   values[0][0] = counter_read_time(pwm_in_per);
   values[1][0] = counter_read_time(pwm_in_pul);
   usleep(25 * 1000);
   counter_stop(pwm_in_per);
   values[0][1] = counter_read_time(pwm_in_per);
   values[1][1] = counter_read_time(pwm_in_pul);
   rprintf ("1. Values\n2. -1 on channel 1 and value on channel 2\nFirst:  %d > 0 , %d > 0\n"
         "Second: %d == -2 , %d > 0\n", values[0][0], values[1][0], values[0][1] , values[1][1]);
   ASSERT(values[0][0] > 0 && values[1][0] > 0);
   ASSERT(values[0][1] == -1);
   ASSERT(values[1][1] > 0);
   usleep(25 * 1000);
   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   counter_stop(pwm_in_pul);
   values[0][0] = counter_read_time(pwm_in_per);
   values[1][0] = counter_read_time(pwm_in_pul);
   usleep(25 * 1000);
   counter_start(pwm_in_pul, COUNTER_DUAL_EDGE_MEASURE_PULSE);
   values[0][1] = counter_read_time(pwm_in_per);
   values[1][1] = counter_read_time(pwm_in_pul);
   rprintf ("1. Value on channel 1 and -1 channel 2\n2. Values on both channels\n"
         "First:  %d > 0 , %d == -2\nSecond: %d > 0 , %d > 0\n\n",
         values[0][0], values[1][0], values[0][1] , values[1][1]);
   usleep(25 * 1000);
   ASSERT(values[0][0] > 0);
   ASSERT(values[1][0] == -1);
   ASSERT(values[0][1] > 0 && values[1][1] > 0);

   /* Test the different counter modes... */
   rprintf ("Test different counter modes\n");
   counter_read_time(pwm_in_per);
   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   counter_start(pwm_in_pul, COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   usleep(25 * 1000);
   values[0][0] = counter_read_time(pwm_in_per);
   values[1][0] = counter_read_time(pwm_in_pul); /* Note: pwm_in_pul is temporary reconfigured to measure period */
   rprintf ("Both channels should return same period: %d == %d\n", values[0][0], values[1][0]);
   ASSERT(values[0][0] > 0 && values[0][0] == values[1][0]);
   usleep(25 * 1000);
   counter_read_time(pwm_in_per);
   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PULSE);
   counter_start(pwm_in_pul, COUNTER_DUAL_EDGE_MEASURE_PULSE);
   usleep(25 * 1000);
   values[0][0] = counter_read_time(pwm_in_per); /* Note: pwm_in_per is temporary reconfigured to measure pulse */
   values[1][0] = counter_read_time(pwm_in_pul);
   rprintf ("Both channels should return same pulse: %d == %d\n\n", values[0][0], values[1][0]);
   ASSERT(values[0][0] > 0 && values[0][0] == values[1][0]);
   /* Reconfigure pwm_in_per to COUNTER_DUAL_EDGE_MEASURE_PERIOD */
   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PERIOD);

   /* Change duty cycle */
   rprintf ("Test 75.00%% duty cycle\n");
   usleep(40 * 1000);
   values[0][0] = counter_read_time(pwm_in_pul);
   pwm_duty_cycle_set(pwm_out, 7500);
   usleep(5 * 1000);
   values[0][1] = counter_read_time(pwm_in_pul);
   usleep(30 * 1000);
   values[0][2] = counter_read_time(pwm_in_pul);
   rprintf ("1. Values\n2. Same values\n3. New values\nFirst:  %8d\nSecond: %8d\nThird:  %8d\n\n",
         values[0][0], values[0][1] ,values[0][2]);
   ASSERT(values[0][0] == values[0][1]);
   ASSERT(values[0][1] < values[0][2]);

   /* Change period */
   rprintf ("Test different period\n");
   usleep(40 * 1000);
   values[0][0] = counter_read_time(pwm_in_per);
   pwm_frequency_set(pwm_out, 60);
   usleep(5 * 1000);
   values[0][1] = counter_read_time(pwm_in_per);
   usleep(30 * 1000);
   values[0][2] = counter_read_time(pwm_in_per);
   rprintf ("1. Values\n2. Same values\n3. New values\nFirst:  %8d\nSecond: %8d\nThird:  %8d\n\n",
         values[0][0], values[0][1] , values[0][2]);
   ASSERT(values[0][0] == values[0][1]);
   ASSERT(values[0][1] > values[0][2]);

   /* Revert period change */
   pwm_frequency_set(pwm_out, 50);

   rprintf ("Stop and close all counter channels, and PWM out channel\n");
   counter_stop(pwm_in_per);
   counter_stop(pwm_in_pul);
   pwm_stop(pwm_out);

   counter_close(pwm_in_per);
   counter_close(pwm_in_pul);
   pwm_close(pwm_out);

   rprintf ("PWM in test done\n\n\n");
#endif /* TEST_PWM_IN */


#if TEST_FREQ_IN
   /* NOTE: FREQ_IN (PA6 and PA7) should be connected to PWM2 PWM3 */
   /* FREQ in test
    * - Open and start all counter channels
    * - Open the PWM out channels
    * - Set 20.00% duty cycle
    * - Start PWM out
    * - Increase from 50 hz up to 4000 hz two times
    * - Stop and close all counter channels and the PWM out channels
    */
   {
      int32_t freq_values[2];
      int freq_pwm_out[2];
      int freq_in[4];

      rprintf ("Starting FREQ in test\n");

      freq_pwm_out[0] = 0;
      freq_pwm_out[1] = 0;

      freq_in[2] = counter_open("/counter0/0");
      ASSERT(freq_in[2] > 0);
      freq_in[3] = counter_open("/counter0/1");
      ASSERT(freq_in[3] > 0);



      freq_in[0] = counter_open("/freq0/0");
      ASSERT(freq_in[0] > 0);
      freq_in[1] = counter_open("/freq1/0");
      ASSERT(freq_in[1] > 0);

      counter_start(freq_in[0], COUNTER_DUAL_EDGE_MEASURE_PERIOD);
      counter_start(freq_in[1], COUNTER_DUAL_EDGE_MEASURE_PERIOD);

      for (i = 0; i<2; i++)
      {
         freq_values[i] = 0;
      }

      freq_pwm_out[0] = pwm_open("/pwm0/1");
      ASSERT(freq_pwm_out[0] > 0);

      freq_pwm_out[1] = pwm_open("/pwm0/2");
      ASSERT(freq_pwm_out[1] > 0);

      pwm_duty_cycle_set(freq_pwm_out[0], 2000);

      usleep(40 * 1000);

      /* Test start stop */
      rprintf ("Test start/stop\n");

      for (i = 0; i<500; i++)
      {
         int set_freq = (50 + (i*16)%4000);

         pwm_start(freq_pwm_out[0]);
         pwm_start(freq_pwm_out[1]);

         pwm_frequency_set(freq_pwm_out[0], set_freq);
         pwm_duty_cycle_set(freq_pwm_out[0], 2000);

         pwm_frequency_set(freq_pwm_out[1], set_freq);
         pwm_duty_cycle_set(freq_pwm_out[1], 2000);

         usleep(10 * (1000000 / (set_freq)));
         counter_read_time(freq_in[0]);
         counter_read_time(freq_in[1]);
         usleep(2 * (1000000 / (set_freq)));

         freq_values[0] = counter_read_time(freq_in[0]);
         freq_values[1] = counter_read_time(freq_in[1]);

         if (((100 * abs(set_freq - ((1000000000) / freq_values[0]))) / (set_freq)) > 2 ||
                        ((100 * abs(set_freq - ((1000000000) / freq_values[1]))) / (set_freq)) > 2)
         {
            rprintf ("f: %d, f_0: %d, f_1: %d, d_0: %dns, d_1: %dns\n", set_freq, ((1000000000) / freq_values[0]), ((1000000000) / freq_values[1]), ((1000000000) / set_freq) - freq_values[0], ((1000000000) / set_freq) - freq_values[1]);
         }
         ASSERT(((100 * abs(set_freq - ((1000000000) / freq_values[0]))) / (set_freq)) <= 2);
         ASSERT(((100 * abs(set_freq - ((1000000000) / freq_values[1]))) / (set_freq)) <= 2);

         pwm_stop(freq_pwm_out[0]);
         pwm_stop(freq_pwm_out[1]);

         usleep(2 * (1000000 / (set_freq)));

         counter_read_time(freq_in[0]);
         counter_read_time(freq_in[1]);

         /* wait for the time out to kick in */
         usleep(100000);

         freq_values[0] = counter_read_time(freq_in[0]);
         freq_values[1] = counter_read_time(freq_in[1]);

         ASSERT(freq_values[0] == -1);
         ASSERT(freq_values[1] == -1);


      }

      /* Revert period change */
      pwm_frequency_set(freq_pwm_out[0], 50);
      pwm_frequency_set(freq_pwm_out[1], 50);

      pwm_stop(freq_pwm_out[0]);
      pwm_stop(freq_pwm_out[1]);

      counter_close(freq_in[0]);
      counter_close(freq_in[1]);

      pwm_close(freq_pwm_out[0]);
      pwm_close(freq_pwm_out[1]);

      rprintf ("FREQ in test done\n\n\n");
   }
#endif /* TEST_FREQ_IN */

#if TEST_SERVO
   /* Servo test
    * - Open and start all PWM channels
    * - All servos should move from their minimum to maximum position
    * - Stop and close all PWM channels
    */
   rprintf ("Starting servo test\n");
   pwm0 = pwm_open ("/pwm0/0");
   ASSERT(pwm0 > 0);
   pwm1 = pwm_open ("/pwm0/1");
   ASSERT(pwm1 > 0);
   pwm2 = pwm_open ("/pwm0/2");
   ASSERT(pwm2 > 0);
   pwm3 = pwm_open ("/pwm0/3");
   ASSERT(pwm3 > 0);

   pwm_duty_cycle_set(pwm0, 0);
   pwm_duty_cycle_set(pwm1, 0);
   pwm_duty_cycle_set(pwm2, 0);
   pwm_duty_cycle_set(pwm3, 0);

   pwm_start(pwm0);
   usleep(1 * 1000);
   pwm_start(pwm1);
   usleep(1 * 1000);
   pwm_start(pwm2);
   usleep(1 * 1000);
   pwm_start(pwm3);
   usleep(40 * 1000);

   rprintf ("Full right...\n");
   for (i=300; i<350; i++)
   {
      pwm_duty_cycle_set(pwm0, i);
      pwm_duty_cycle_set(pwm1, i);
      pwm_duty_cycle_set(pwm2, i);
      pwm_duty_cycle_set(pwm3, i);
      usleep(120 * 1000);
      if(i%50 == 0)
         rprintf ("i=%u\n", i);
   }
   rprintf ("Sleep 10s...\n");
   usleep(10 * 1000 * 1000);

   rprintf ("Right->Middle\n");
   for (i=350; i<750; i++)
   {
      pwm_duty_cycle_set(pwm0, i);
      pwm_duty_cycle_set(pwm1, i);
      pwm_duty_cycle_set(pwm2, i);
      pwm_duty_cycle_set(pwm3, i);
      usleep(120 * 1000);
      if(i%50 == 0)
         rprintf ("i=%u\n", i);
   }
   rprintf ("Sleep 10s...\n");
   usleep(10 * 1000 * 1000);

   rprintf ("Middle->Left\n");
   for (i=750; i<1150; i++)
   {
      pwm_duty_cycle_set(pwm0, i);
      pwm_duty_cycle_set(pwm1, i);
      pwm_duty_cycle_set(pwm2, i);
      pwm_duty_cycle_set(pwm3, i);
      usleep(120 * 1000);
      if(i%50 == 0)
         rprintf ("i=%u\n", i);
   }
   rprintf ("Sleep 10s...\n");
   usleep(10 * 1000 * 1000);

   rprintf ("Full left...\n");
   for (i=1150; i<1200; i++)
   {
      pwm_duty_cycle_set(pwm0, i);
      pwm_duty_cycle_set(pwm1, i);
      pwm_duty_cycle_set(pwm2, i);
      pwm_duty_cycle_set(pwm3, i);
      usleep(120 * 1000);
      if(i%50 == 0)
         rprintf ("i=%u\n", i);
   }
   rprintf ("Sleep 10s...\n");
   usleep(10 * 1000 * 1000);

   rprintf ("Stop and close all PWM channels\n");
   usleep(40 * 1000);
   pwm_stop(pwm1);
   usleep(1 * 1000);
   pwm_stop(pwm2);
   usleep(1 * 1000);
   pwm_stop(pwm3);
   usleep(1 * 1000);
   pwm_stop(pwm0);


   rprintf ("Start PWM4 channels\n");
   pwm_start(pwm3);
   usleep(40 * 1000);
   rprintf ("Middle\n");
   pwm_duty_cycle_set(pwm3, 1000);

   adc3 = adc_open ("/adc1/4");
   ASSERT(adc3 > 0);
   adc_start(adc3);

   for (i=1000; i>500; i--)
   {
      pwm_duty_cycle_set(pwm3, i);
      value = adc_sample_get(adc3);
      rprintf ("adc3 %d\n", value);
      usleep(300 * 1000);
   }

   rprintf ("Servo test done\n\n\n");
#endif /* TEST_SERVO */

#if TEST_CAN
   /* CAN test
    * - Goes CAN bus on at 250 Kbit.
    * - Sends CAN frame id 0x555, dlc = 0 every second.
    * - Echos ever received CAN frame with incremented id.
    */

   {
      const uint8_t fifo_mode = 1;
      can_cfg_t can_cfg[] = {
            {125000,  13, 2 ,1, 0},
            {250000,   6, 1 ,1, 0},
            {500000,   6, 1 ,1, 0},
            {1000000,  7, 1 ,1, 0},
         };
      can_filter_t filter = {
           .id   = 0x0,
           .mask = 0x0,
         };
      can_struct_t * can_struct;

      can_struct = malloc(sizeof(can_struct_t));
      UASSERT (can_struct != NULL, EMEM);

      can_struct->can_flag = flags_create(0);
      can_struct->can_fd = open("/can0", O_RDWR, 0);
      UASSERT(can_struct->can_fd != -1, EARG);

      can_set_callback (can_struct->can_fd, can_rx_callback, CAN_EVENT_MSG_RECEIVED, can_struct);
      can_filter (can_struct->can_fd, &filter);
      ioctl (can_struct->can_fd, IOCTL_BXCAN_TXFP, &fifo_mode);

      can_bus_off(can_struct->can_fd);
      can_set_cfg(can_struct->can_fd, &can_cfg[1]);
      can_bus_on(can_struct->can_fd);

      task_spawn ("can_handler_task", can_handler_task, 10, 1024, can_struct);

   }
#endif /* TEST_CAN */


#if TEST_NOR
   {
      /* NOR test
       * - Opens NOR and runs erase
       * - Tests write/read/compare on the flash memory
       */

      static drv_t * efmi;
      static nor_media_info_t media_info;
      static char buffer[0x1000];
      static char read_buffer[0x1000];

      rprintf ("Starting NOR test\n");

      efmi = dev_find_driver ("/nor0");

      ASSERT(nor_get_media_info(efmi, &media_info) == 0);
      ASSERT(nor_erase(efmi, 0x00000000, media_info.memory_size) == 0);

      rprintf ("NOR erase ok\n");

      memset(buffer, 0xFF, 0x1000);

      buffer[1] = 0x00;

      ASSERT(nor_write(efmi, 0x00000000, 8, buffer) == 0);

      ASSERT(nor_read(efmi, 0x00000000, 0x1000, read_buffer) == 0);

      ASSERT(memcmp(buffer, read_buffer, 0x1000) == 0);

      rprintf ("NOR simple 1 write/read/compare ok\n");

      ASSERT(nor_erase(efmi, 0x00000000, media_info.memory_size) == 0);

      buffer[0x42] = 0x42;

      ASSERT(nor_write(efmi, 0x00000000, 0x1000, buffer) == 0);

      ASSERT(nor_read(efmi, 0x00000000, 0x1000, read_buffer) == 0);

      ASSERT(memcmp(buffer, read_buffer, 0x1000) == 0);

      ASSERT(nor_erase(efmi, 0x00000000, media_info.memory_size) == 0);

      buffer[0] = 0x08;
      buffer[1] = 0xFF;
      buffer[2] = 0x08;
      buffer[3] = 0xFF;
      buffer[4] = 0x00;
      buffer[5] = 0x10;
      buffer[6] = 0x00;
      buffer[7] = 0x00;
      buffer[8] = 0x00;
      buffer[9] = 0x20;

      ASSERT(nor_write(efmi, 0x00000000, 1, buffer) == 0);
      ASSERT(nor_write(efmi, 0x00000002, 1, &buffer[2]) == 0);
      ASSERT(nor_write(efmi, 0x00000004, 2, &buffer[4]) == 0);
      ASSERT(nor_write(efmi, 0x00000006, 4, &buffer[6]) == 0);

      ASSERT(nor_read(efmi, 0x00000000, 10, read_buffer) == 0);

      ASSERT(memcmp(buffer, read_buffer, 10) == 0);

      rprintf ("NOR simple 2 write/read/compare ok\n");

      {
         uint32_t total_written_data = 0;
         size_t i;

         for (i = 0; i < sizeof(buffer); i++)
         {
            buffer[i] = rand();
         }

         ASSERT(nor_erase(efmi, 0x00000000, media_info.memory_size) == 0);

         while (total_written_data < media_info.memory_size)
         {
            if ((total_written_data + sizeof(buffer)) > media_info.memory_size)
            {
               ASSERT(nor_write(efmi, (0x00000000 + total_written_data),
                     (media_info.sector_size - total_written_data), buffer) == 0);

               ASSERT(nor_read(efmi, 0x00000000,
                     (media_info.sector_size - total_written_data), read_buffer) == 0);

               ASSERT(memcmp(buffer, read_buffer,
                     (media_info.sector_size - total_written_data)) == 0);

               total_written_data += (media_info.sector_size - total_written_data);
            }
            else
            {
               ASSERT(nor_write(efmi, (0x00000000 + total_written_data),
                     sizeof(buffer), buffer) == 0);

               ASSERT(nor_read(efmi, 0x00000000, sizeof(buffer), read_buffer) == 0);

               ASSERT(memcmp(buffer, read_buffer, sizeof(buffer)) == 0);

               total_written_data += sizeof(buffer);
            }
         }
      }
      rprintf ("NOR whole flash write/read/compare ok\n");

      rprintf ("NOR test done\n\n\n");
   }
#endif /* TEST_NOR */


#if TEST_FUEL_VALVE
   {
      /* FUEL_VALVE test
       * - Toggle FUEL_VALVE_1 and FUEL_VALVE_2
       * - Tests write/read/compare on the flash memory
       */

      rprintf ("Starting FUEL_VALVE test\n");

      gpio_set(GPIO_FULE_VALVE_1, 0);
      gpio_set(GPIO_FULE_VALVE_2, 0);
      for ( i = 0; i < 100; i++)
      {
         task_delay(tick_from_ms(1000));
         gpio_set(GPIO_FULE_VALVE_1, i%2);
         gpio_set(GPIO_FULE_VALVE_2, i%2);
      }

      rprintf ("FUEL_VALVE test done\n\n\n");
   }
#endif /* TESTFUEL_VALVE */

#if TEST_ULEVEL
   {
      /* ULEVEL test
       * - Toggle U_LEVEL0 and U_LEVEL1
       */

      rprintf ("Starting ULEVEL test\n");

      gpio_set(GPIO_U_LEVEL0, 0);
      gpio_set(GPIO_U_LEVEL1, 0);
      for ( i = 0; i < 100; i++)
      {
         gpio_set(GPIO_U_LEVEL0, 1);
         gpio_set(GPIO_U_LEVEL1, 0);
         rprintf ("U_LEVEL0 = %d, U_LEVEL1 = %d\n", gpio_get(GPIO_U_LEVEL0), gpio_get(GPIO_U_LEVEL1));
         task_delay(tick_from_ms(10000));
         gpio_set(GPIO_U_LEVEL0, 0);
         gpio_set(GPIO_U_LEVEL1, 1);
         rprintf ("U_LEVEL0 = %d, U_LEVEL1 = %d\n", gpio_get(GPIO_U_LEVEL0), gpio_get(GPIO_U_LEVEL1));
         task_delay(tick_from_ms(10000));
         gpio_set(GPIO_U_LEVEL0, 1);
         rprintf ("U_LEVEL0 = %d, U_LEVEL1 = %d\n", gpio_get(GPIO_U_LEVEL0), gpio_get(GPIO_U_LEVEL1));
         task_delay(tick_from_ms(10000));
         gpio_set(GPIO_U_LEVEL0, 0);
         gpio_set(GPIO_U_LEVEL1, 0);
         rprintf ("U_LEVEL0 = %d, U_LEVEL1 = %d\n", gpio_get(GPIO_U_LEVEL0), gpio_get(GPIO_U_LEVEL1));
         task_delay(tick_from_ms(10000));
      }

      rprintf ("ULEVEL test done\n\n\n");
   }
#endif /* TESTFUEL_VALVE */

   rprintf ("Exit main...\n");

   return 0;
}

const char *byte_to_binary(int x, int bits)
{
    static char b[17];
    b[0] = '\0';

    int z;
    for (z = 1 << (bits-1); z > 0; z >>= 1)
    {
        strcat(b, ((x & z) == z) ? "1" : "0");
    }
    return b;
}
