/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id$
 *------------------------------------------------------------------------------
 */
#include <bsp.h>
#include <shell.h>

#include "servo_example.h"

#if IO_NODE == 1
#include "joystick1.h"
#elif IO_NODE == 2
#include "joystick2.h"
#endif

#define CK_TASK_PRIO    3
#define CK_TASK_STACK   (2 * 1024)

extern void ckappMain(void);

static void ck_task (void * arg)
{
   (void)arg;

   ckappMain();
}

int main(void)
{
#if SERVO_NODE == 1
   shell_init ("s01>");
#elif SERVO_NODE == 2
   shell_init ("s02>");
#elif SERVO_NODE == 3
   shell_init ("s03>");
#elif SERVO_NODE == 4
   shell_init ("s04>");
#elif SERVO_NODE == 5
   shell_init ("sl_h_5>");
#elif SERVO_NODE == 6
   shell_init ("sl_v_6>");
#elif SERVO_NODE == 7
   shell_init ("sl_l_7>");
#elif SERVO_NODE == 9
   shell_init ("fan>");
#endif

   process_data_t * process_data;

   /* Allocate the process data structure */
   process_data = malloc(sizeof(process_data_t));
   UASSERT (process_data != NULL, EMEM);

   /* Create the process data lock */
   process_data->pd_lock = mtx_create();
   UASSERT(process_data->pd_lock != NULL, EMEM);

   /* Spawn sample tasks */
   task_spawn("sample_adc", sample_adc, ADC_SAMPLE_PRIORITY, ADC_SAMPLE_STACK_SIZE, process_data);

   /* Spawn write tasks */
   task_spawn("write_pwm_out", write_pwm_out, PWM_OUT_WRITE_PRIORITY, PWM_OUT_WRITE_STACK_SIZE, process_data);

   task_spawn (
         "CAN Kingdom",
         ck_task,
         CK_TASK_PRIO,
         CK_TASK_STACK,
         NULL);

   return 0;
}
