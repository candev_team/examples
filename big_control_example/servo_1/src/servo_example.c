/*
 * servo_example.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include <config.h>
#include <adc/adc.h>
#include <pwm/pwm.h>
#include <can/bxcan.h>
#include <bsp.h>

#include <string.h>
#include "servo_example.h"


#define CAN_MSG_RECEIVED         BIT(0)

typedef struct can_struct
{
   int can_fd;
   flags_t * can_flag;
   process_data_t * process_data;
} can_struct_t;

static process_data_t * process_data;

bool checkShutdown(void)
{
   return false;
}

#define servo_power_INTERVAL_MS   500

#if SERVO_NODE == 1

#include "servo.h"

static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_servo_power power;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (servo_power_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);
         power.s_voltage =  process_data->adc_value[0];
         power.s_current =  process_data->adc_value[1];
         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_servo_power(&power);
      }
   }
}

void handleS01s04_pwm(ckd_s01s04_pwm* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s01_angle;
   mtx_unlock(process_data->pd_lock);
}

#elif SERVO_NODE == 2
#include "servo.h"
static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_servo_power power;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (servo_power_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);
         power.s_voltage =  process_data->adc_value[0];
         power.s_current =  process_data->adc_value[1];
         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_servo_power(&power);
      }
   }
}
void handleS01s04_pwm(ckd_s01s04_pwm* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s02_angle;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 3
#include "servo.h"
static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_servo_power power;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (servo_power_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);
         power.s_voltage =  process_data->adc_value[0];
         power.s_current =  process_data->adc_value[1];
         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_servo_power(&power);
      }
   }
}
void handleS01s04_pwm(ckd_s01s04_pwm* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s03_angle;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 4
#include "servo.h"
static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_servo_power power;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (servo_power_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);
         power.s_voltage =  process_data->adc_value[0];
         power.s_current =  process_data->adc_value[1];
         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_servo_power(&power);
      }
   }
}
void handleS01s04_pwm(ckd_s01s04_pwm* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s04_angle;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 5
#include "searchlight.h"
void ckappOnIdle(void)
{

}

void handleLight(ckd_light* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s_light_h;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 6
#include <searchlight.h>
void ckappOnIdle(void)
{

}

void handleLight(ckd_light* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s_light_v;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 7
#include <searchlight.h>
void ckappOnIdle(void)
{

}

void handleLight(ckd_light* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->s_light_lamp;
   mtx_unlock(process_data->pd_lock);
}
#elif SERVO_NODE == 8
#elif SERVO_NODE == 9
#include "motor.h"

void ckappOnIdle(void)
{

}

void handleMotor_ctrl(ckd_motor_ctrl* doc)
{
   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_pul[0] = doc->motor_speed;
   mtx_unlock(process_data->pd_lock);
}

#endif

void sample_adc(void * arg)
{
   process_data = (process_data_t *) arg;
   int adc[6];
   uint32_t tmp_value[6];

   mtx_lock(process_data->pd_lock);
   process_data->adc_value[0] = 0;
   process_data->adc_value[1] = 0;
   mtx_unlock(process_data->pd_lock);

   adc[0] = adc_open ("/adc1/1");
   ASSERT(adc[0] > 0);
   adc[1] = adc_open ("/adc1/2");
   ASSERT(adc[1] > 0);

   adc_start(adc[0]);
   adc_start(adc[1]);

   while(1)
   {
      tmp_value[0] = adc_sample_get(adc[0]);
      tmp_value[1] = adc_sample_get(adc[1]);

      mtx_lock(process_data->pd_lock);
      process_data->adc_value[0] = tmp_value[0];
      process_data->adc_value[1] = tmp_value[1];
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/ADC_SAMPLE_RATE);
   }
}

void write_pwm_out(void * arg)
{
   process_data = (process_data_t *) arg;
   int pwm_out[4];
   int duty_cycle;

   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_per[0] = 0;
   process_data->pwm_out_pul[0] = 0;
   process_data->pwm_out_per[1] = 0;
   process_data->pwm_out_pul[1] = 0;
   mtx_unlock(process_data->pd_lock);

   pwm_out[0] = pwm_open ("/pwm0/0");
   ASSERT(pwm_out[0] > 0);

   pwm_duty_cycle_set(pwm_out[0], 0);
   pwm_start(pwm_out[0]);

   pwm_out[3] = pwm_open ("/pwm0/3");
   ASSERT(pwm_out[3] > 0);

   pwm_duty_cycle_set(pwm_out[3], 0);
   pwm_start(pwm_out[3]);

   while(1)
   {
      mtx_lock(process_data->pd_lock);
      duty_cycle = process_data->pwm_out_pul[0];
      pwm_duty_cycle_set(pwm_out[3], duty_cycle);
      mtx_unlock(process_data->pd_lock);

      mtx_lock(process_data->pd_lock);
      duty_cycle = process_data->pwm_out_pul[1];
      pwm_duty_cycle_set(pwm_out[0], duty_cycle);
      mtx_unlock(process_data->pd_lock);

      task_delay(CFG_TICKS_PER_SECOND/PWM_OUT_WRITE_RATE);
   }
}
