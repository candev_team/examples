

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * servo.h
 * 
 * Description Servo...
 * 
 * Generated from: /candev_model/modules/servo.kfm
 */

#include "ck.h"
#include "servo.h"


// *** Receive handle functions
extern void handleS01s04_pwm(ckd_s01s04_pwm* doc);


// *** Transmit functions
void ckappSend_CKDN_servo_power(ckd_servo_power* doc) {
  ckSend(CKDN_servo_power, (uint8_t*) doc, sizeof(ckd_servo_power), 0);
}

extern void ckappOnIdle(void);
extern bool checkShutdown(void);

void ckappMain(void) {
  ckInit(ckStartListenForDefaultLetter);
  for(;;) {
    ckappOnIdle();
    ckMain();
    if (checkShutdown()) break;
  }
}

void ckappSetActionMode(ckActMode am) {
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
}

void ckappDispatchRTR(uint16_t doc, bool txF) {
}

int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  return 1; // Page not supported
}

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    case CKDN_s01s04_pwm: // S01-S04 PWM pulse widths
      handleS01s04_pwm((ckd_s01s04_pwm*) msgBuf); 
      break;
  } // End of document switch
} // End of ckappDispatch

