

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * servo.h
 * 
 * Description Servo...
 * 
 * Generated from: /candev_model/modules/servo.kfm
 */

#ifndef SERVO
#define SERVO

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// S01-S04 PWM pulse widths. Record number: 10, List number: 2
#define CKDN_s01s04_pwm 10
 
typedef struct {
  uint16_t s01_angle; // Servo 1 angle
  uint16_t s02_angle; // Servo 2 angle
  uint16_t s03_angle; // Servo 3 angle
  uint16_t s04_angle; // Servo 4 angle
  
    
    
    
  
} ckd_s01s04_pwm;


// Servo power. Record number: 11, List number: 1
#define CKDN_servo_power 11
 
typedef struct {
  uint16_t s_voltage; // Servo voltage
  uint16_t s_current; // Servo current
  
    
    
    
  
} ckd_servo_power;



#pragma pack()

// *** Transmit functions

void ckappSend_CKDN_servo_power(ckd_servo_power* doc);

#endif /* SERVO */
