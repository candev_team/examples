

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * motor.h
 * 
 * Fan motor
 * 
 * Generated from: /candev_model/modules/motor.kfm
 */

#include "ck.h"
#include "motor.h"


// *** Receive handle functions
extern void handleMotor_ctrl(ckd_motor_ctrl* doc);


// *** Transmit functions

extern void ckappOnIdle(void);
extern bool checkShutdown(void);

void ckappMain(void) {
  ckInit(ckStartListenForDefaultLetter);
  for(;;) {
    ckappOnIdle();
    ckMain();
    if (checkShutdown()) break;
  }
}

void ckappSetActionMode(ckActMode am) {
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
}

void ckappDispatchRTR(uint16_t doc, bool txF) {
}

int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  return 1; // Page not supported
}

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    case CKDN_motor_ctrl: // Motor control
      handleMotor_ctrl((ckd_motor_ctrl*) msgBuf); 
      break;
  } // End of document switch
} // End of ckappDispatch

