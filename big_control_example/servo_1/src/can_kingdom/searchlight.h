

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * searchlight.h
 * 
 * Moveable light
 * 
 * Generated from: /candev_model/modules/searchlight.kfm
 */

#ifndef S_LIGHT
#define S_LIGHT

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Light control. Record number: 10, List number: 2
#define CKDN_light 10
 
typedef struct {
  uint16_t s_light_h; // Searchlight horizontal
  uint16_t s_light_v; // Searchlight vertical
  uint16_t s_light_lamp; // Searchlight lamp mode
  
    
    
    
  
} ckd_light;



#pragma pack()

// *** Transmit functions


#endif /* S_LIGHT */
