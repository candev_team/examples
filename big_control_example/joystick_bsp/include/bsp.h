/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: bsp.h 2556 2016-05-29 20:01:51Z rtlbjca $
*------------------------------------------------------------------------------
*/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PC4
#define GPIO_UART1_RX   GPIO_PC5

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_ADC1_IN1   GPIO_PA0
#define GPIO_ADC1_IN2   GPIO_PA1
#define GPIO_ADC1_IN3   GPIO_PA2
#define GPIO_ADC1_IN4   GPIO_PA3
#define GPIO_ADC2_IN1   GPIO_PA4
#define GPIO_ADC2_IN2   GPIO_PA5

#define GPIO_IOIN0      GPIO_PB6
#define GPIO_IOIN1      GPIO_PD2
#define GPIO_IOIN2      GPIO_PC3
#define GPIO_IOIN3      GPIO_PC1
#define GPIO_IOIN4      GPIO_PC13
#define GPIO_IOIN5      GPIO_PB7
#define GPIO_IOIN6      GPIO_PC15
#define GPIO_IOIN7      GPIO_PC14
#define GPIO_IOIN8      GPIO_PB5
#define GPIO_IOIN9      GPIO_PC12
#define GPIO_IOIN10     GPIO_PC2
#define GPIO_IOIN11     GPIO_PC0

#define GPIO_VDD_IO     GPIO_PC11

void bsp_reset_peripherals (void);

#endif /* BSP_H */
