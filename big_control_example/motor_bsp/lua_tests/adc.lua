-- Test Lua-ADC integration

-- BSP-specific constanst
valid_channel_name = '/adc1/1'
invalid_channel_name = '/adc1/bad'

-- Basic test (no errors)
channel = adc.open(valid_channel_name)
channel:start()
channel:get_sample()
channel:get_sample()
channel:stop()  -- FIXME: Hangs in stm32_adc_stop() due to bug in ADC driver (rt-labs defect #1060)
channel:close()

-- Test open() error handling
assert(pcall(adc.open, invalid_channel_name) == false) -- 'Could not open channel'

-- Test close() error handling
channel = adc.open(valid_channel_name)
channel:start()
assert(pcall(channel.close, channel) == false) -- 'channel not stopped'
channel:stop()
channel:close()
assert(pcall(channel.close, channel) == false) -- 'channel already closed'

-- Test start() error handling
channel = adc.open(valid_channel_name)
channel:start()
assert(pcall(channel.start, channel) == false) -- 'channel already started'
channel:stop()
channel:close()
assert(pcall(channel.start, channel) == false) -- 'channel not open'

-- Test stop() error handling
channel = adc.open(valid_channel_name)
assert(pcall(channel.stop, channel) == false) -- 'channel not started'
channel:start()
channel:stop()
assert(pcall(channel.stop, channel) == false) -- 'channel not started'
channel:close()
assert(pcall(channel.stop, channel) == false) -- 'channel not open'

-- Test get_sample() error handling
channel = adc.open(valid_channel_name)
assert(pcall(channel.get_sample, channel) == false) -- 'channel not started'
channel:start()
channel:stop()
assert(pcall(channel.get_sample, channel) == false) -- 'channel not started'
channel:close()
assert(pcall(channel.get_sample, channel) == false) -- 'channel not open'

