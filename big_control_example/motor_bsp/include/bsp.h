/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id$
*------------------------------------------------------------------------------
*/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PC4
#define GPIO_UART1_RX   GPIO_PC5

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_PWM_CH_1            GPIO_PC0
#define GPIO_PWM_CH_2            GPIO_PC1
#define GPIO_PWM_CH_3            GPIO_PC2
#define GPIO_PWM_CHN_1           GPIO_PB13
#define GPIO_PWM_CHN_2           GPIO_PB14
#define GPIO_PWM_CHN_3           GPIO_PB15

#define GPIO_COMP1_INP           GPIO_PA1
#define GPIO_COMP2_INP           GPIO_PA7
#define GPIO_COMP4_INP           GPIO_PB0

#define GPIO_ADC1_A_CH1_VOLTAGE  GPIO_PA0
#define GPIO_ADC1_A_CH2_VOLTAGE  GPIO_PA1
#define GPIO_ADC1_A_CH3_VOLTAGE  GPIO_PA2
#define GPIO_ADC1_MOTOR_VOLTAGE  GPIO_PA3

#define GPIO_ADC2_VIN            GPIO_PA4
#define GPIO_ADC2_MOTOR_CURRENT  GPIO_PA5

void bsp_reset_peripherals (void);

#endif /* BSP_H */
