/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2007. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: startup.c 2336 2016-03-24 07:50:39Z rtlfrm $
*------------------------------------------------------------------------------
*/

#include "config.h"
#include <bsp.h>
#include <kern.h>
#include <dev.h>
#include <shell.h>

void startup (void)
{
#ifdef CFG_STARTUP_INIT

#ifdef CFG_STATS_INIT
   stats_calibrate();
#endif  /* CFG_STATS_INIT */

#ifdef CFG_TTOS_INIT
   tt_init();
#endif  /* CFG_TTOS_INIT */

#ifdef CFG_HOTPLUG_INIT
   dev_hotplug_init (CFG_HOTPLUG_PRIORITY, CFG_HOTPLUG_STACK_SIZE,
                     CFG_HOTPLUG_POLL_PERIOD);
#endif  /* CFG_HOTPLUG_INIT */

#ifdef CFG_LWIP_INIT
   bsp_lwip_init();
#endif  /* CFG_LWIP_INIT */

#if defined CFG_USB_INIT
   bsp_usb_init();
#endif  /* CFG_USB_INIT */

#ifndef CFG_HOTPLUG_INIT
   /* Simple probing of all devices at boot time */
   dev_coldplug_init();
#endif

#ifdef CFG_SHELL_INIT
   shell_init (CFG_SHELL_PROMPT);
#endif

#endif  /* CFG_STARTUP_INIT */
}
