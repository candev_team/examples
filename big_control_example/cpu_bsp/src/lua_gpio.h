/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_gpio.h 2530 2016-05-10 13:22:48Z rtlmajo $
*------------------------------------------------------------------------------
*/

#ifndef LUA_GPIO_H
#define LUA_GPIO_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#define LUA_GPIO_LIBNAME "gpio"

/**
 * Create Lua library which extents Lua with a GPIO pin class.
 *
 * Called by interpreter using luaL_requiref().
 *
 * @param L    Lua interpreter.
 * @return     1, always.
 */
int lua_gpio_openlib (lua_State *L);

#ifdef __cplusplus
}
#endif

#endif /* LUA_GPIO_H */
