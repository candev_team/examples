/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_pwm.c 2530 2016-05-10 13:22:48Z rtlmajo $
*------------------------------------------------------------------------------
*/

#include "lua_pwm.h"
#include <lauxlib.h>
#include <pwm/pwm.h>
#include <string.h>

#undef RTK_DEBUG

#ifdef RTK_DEBUG
#define DPRINT(...) rprintp ("lua_pwm: "__VA_ARGS__)
#else
#define DPRINT(...)
#endif  /* RTK_DEBUG */

#define LUA_PWM_CHANNEL LUA_PWM_LIBNAME".channel"

/* PWM channel type. Exposed to Lua as a "userdata" type. */
typedef struct lua_pwm_channel
{
   int fd;
   bool is_opened;
   bool is_started;
} lua_pwm_channel_t;

static void lua_pwm_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Lua: channel = pwm.open('/pwm0/channel_name') */
static int lua_pwm_open (lua_State * L)
{
   /* Get one argument from stack */
   lua_pwm_check_number_of_arguments (L, 1);
   const char * channel_name = luaL_checkstring (L, 1);

   /* Push new class instance on top of stack */
   lua_pwm_channel_t * self = lua_newuserdata (L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable(L, LUA_PWM_CHANNEL); /* Set class for instance */

   self->is_started = false;
   self->fd = pwm_open (channel_name);
   if (self->fd < 0)
   {
      return luaL_error (L, "could not open channel '%s'", channel_name);
   }
   self->is_opened = true;
   DPRINT ("Opened\n");

   /* One result is already on stack */
   return 1;
}

/* Lua: channel.close(channel) OR channel:close() */
static int lua_pwm_close (lua_State * L)
{
   /* Get one argument from stack */
   lua_pwm_check_number_of_arguments (L, 1);
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel already closed");
   luaL_argcheck (L, self->is_started == false, 1, "channel not stopped");
   ASSERT (self->fd >= 0);

   pwm_close (self->fd);
   self->is_opened = false;
   DPRINT ("Closed\n");

   /* Push no results on stack */
   return 0;
}

/* Destructor. Called by garbage collector. */
static int lua_pwm_collect_garbage (lua_State * L)
{
   /* Get one argument from stack */
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   ASSERT (self != NULL);

   if (self->is_opened == false)
   {
      /* Already closed, so nothing to clean up */
      DPRINT ("GC: nothing to do\n");
      return 0; /* Push no results on stack */
   }

   if (self->is_started)
   {
      pwm_stop (self->fd);
      self->is_started = false;
      DPRINT ("GC: stopped\n");
   }

   pwm_close (self->fd);
   self->is_opened = false;
   DPRINT ("GC: closed\n");

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.start(channel) OR channel:start() */
static int lua_pwm_start (lua_State * L)
{
   /* Get one argument from stack */
   lua_pwm_check_number_of_arguments (L, 1);
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started == false, 1, "channel already started");
   ASSERT (self->fd >= 0);

   pwm_start (self->fd);
   self->is_started = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.stop(channel) OR channel:stop() */
static int lua_pwm_stop (lua_State * L)
{
   /* Get one argument from stack */
   lua_pwm_check_number_of_arguments (L, 1);
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   pwm_stop (self->fd);
   self->is_started = false;

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.set_frequency(channel, frequency) OR
 * channel.set_frequency(frequency)
 */
static int lua_pwm_set_frequency (lua_State * L)
{
   /* Get two arguments from stack */
   lua_pwm_check_number_of_arguments (L, 2);
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   int frequency_Hz = luaL_checkinteger (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, frequency_Hz > 0, 2, "frequency needs to be positive");
   ASSERT (self->fd >= 0);

   pwm_frequency_set (self->fd, frequency_Hz);

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.set_duty_cycle(channel, duty_cycle) OR
 * channel.set_duty_cyckle(duty_cycle)
 */
static int lua_pwm_set_duty_cycle (lua_State * L)
{
   /* Get two arguments from stack */
   lua_pwm_check_number_of_arguments (L, 2);
   lua_pwm_channel_t * self = luaL_checkudata (L, 1, LUA_PWM_CHANNEL);
   float percentage = luaL_checknumber (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, percentage >= 0, 2, "duty cycle needs to be >= 0.0 %");
   luaL_argcheck (L, percentage <= 100, 2, "duty cycle needs to be <= 100.0 %");
   ASSERT (self->fd >= 0);

   pwm_duty_cycle_set (self->fd, 100 * percentage);

   /* Push no results on stack */
   return 0;
}

static void lua_pwm_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
         {"close", lua_pwm_close},
         {"start", lua_pwm_start},
         {"stop", lua_pwm_stop},
         {"set_frequency", lua_pwm_set_frequency},
         {"set_duty_cycle", lua_pwm_set_duty_cycle},
         {"__gc", lua_pwm_collect_garbage},
         {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for PWM channel class in the global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_PWM_CHANNEL)
    *      metatable.__index = metatable
    *      metatable.write = ..
    *      metatable.read = .. (and so on)
    */
   luaL_newmetatable (L, LUA_PWM_CHANNEL); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);                  /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");        /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);          /* Add methods to metatable */
   lua_pop (L, 1);                         /* Pop metatable reference from stack */
}

int lua_pwm_openlib (lua_State * L)
{
   lua_pwm_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
       {"open", lua_pwm_open},
       {NULL, NULL}  /* Sentinel */
   };
   luaL_newlib (L, functions);
   return 1;
}
