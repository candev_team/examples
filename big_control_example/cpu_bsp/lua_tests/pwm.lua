-- Test Lua-PWM integration

-- BSP-specific constant
valid_channel_name = '/pwm0/0'

invalid_channel_name = '/pwm0/bad'

-- Basic tests (no errors)
channel = pwm.open(valid_channel_name)
channel:set_frequency(1)
channel:set_frequency(1000000)
channel:set_duty_cycle(0)
channel:set_duty_cycle(50)
channel:set_duty_cycle(100)
channel:set_duty_cycle(0.0)
channel:set_duty_cycle(50.0)
channel:set_duty_cycle(100.0)
channel:start()
channel:set_frequency(1000)
channel:set_duty_cycle(50)
channel:stop()
channel:set_frequency(1000)
channel:set_duty_cycle(50)
channel:start()
channel:set_frequency(1000)
channel:set_duty_cycle(50)
channel:stop()
channel:set_frequency(1000)
channel:set_duty_cycle(50)
channel:close()

-- Test open() error handling
assert(pcall(pwm.open, invalid_channel_name) == false) -- 'could not open channel'

-- Test close() error handling
channel = pwm.open(valid_channel_name)
channel:start()
assert(pcall(channel.close, channel) == false) -- 'channel not stopped'
channel:stop()
channel:close()
assert(pcall(channel.close, channel) == false) -- 'channel already closed'

-- Test start() error handling
channel = pwm.open(valid_channel_name)
channel:start()
assert(pcall(channel.start, channel) == false) -- 'channel already started'
channel:stop()
channel:close()
assert(pcall(channel.start, channel) == false) -- 'channel not opened'

-- Test stop() error handling
channel = pwm.open(valid_channel_name)
assert(pcall(channel.stop, channel) == false) -- 'channel not started'
channel:close()
assert(pcall(channel.stop, channel) == false) -- 'channel not opened'

-- Test set_frequency() error handling
channel = pwm.open(valid_channel_name)
assert(pcall(channel.set_frequency, channel, -1) == false) -- 'needs to be positive'
assert(pcall(channel.set_frequency, channel, 0) == false) -- 'needs to be positive'
assert(pcall(channel.set_frequency, channel, 1000.5) == false) --'integer expected'
channel:close()
assert(pcall(channel.set_frequency, channel, 10) == false) -- 'channel not opened'

-- Test set_duty_cycle() error handling
channel = pwm.open(valid_channel_name)
assert(pcall(channel.set_duty_cycle, channel, -0.1) == false) -- 'needs to be >= 0.0 %'
assert(pcall(channel.set_duty_cycle, channel, 100.1) == false) -- 'needs to be <= 100.0 %'
channel:close()
assert(pcall(channel.set_duty_cycle, channel, 50) == false) -- 'channel not opened'

