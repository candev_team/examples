/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: can_loopback.h 2530 2016-05-10 13:22:48Z rtlmajo $
*------------------------------------------------------------------------------
*/

/**
 * \internal
 *
 * CAN loopback driver used for testing purposes
 * \{
 */

#ifndef CAN_LOOPBACK_H
#define CAN_LOOPBACK_H

#include <drivers/dev.h>

#ifdef __cplusplus
extern "C"
{
#endif

drv_t * can_loopback_init (const char * name);

#ifdef __cplusplus
}
#endif

#endif /* CAN_LOOPBACK_H */

/**
 * \}
 */
