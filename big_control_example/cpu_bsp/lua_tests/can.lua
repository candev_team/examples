-- Test Lua-CAN integration

function print_table(tab)
  for key, value in pairs(tab) do
    print(key, value)
  end
end

-- BSP-specific constant.
-- Test assumes that the fake CAN driver in can_loopback.c
-- has been initialised in BSP with this name
valid_bus_name = '/can0'

-- Constants used in tests
valid_cfg = {baudrate = 500*1000, bs1 = 12, bs2 = 2, sjw = 1, silent = 0}
valid_filter = {id = 0x1234, mask = 0xffff}

-- Basic loopback tests (no errors)
bus = can.open(valid_bus_name)
bus:set_cfg(valid_cfg)
bus:set_filter(valid_filter)
status = bus:get_status()
print_table(status)
bus:on()
bus:transmit(1, '')
bus:transmit(2, '12345678')
id, data = bus:receive()
assert(id == 1 and data == '')
id, data = bus:receive()
assert(id == 2 and data == '12345678')
bus:off()
bus:close()

-- Test open() error handling
assert(pcall(can.open, '/bad') == false) -- 'could not open bus'

-- Test on() error handling
bus = can.open(valid_bus_name)
assert(pcall(bus.on, bus) == false) -- 'not configured'
bus:close()
assert(pcall(bus.on, bus) == false) -- 'bus is closed'

-- Test off() error handling
bus = can.open(valid_bus_name)
bus:close()
assert(pcall(bus.on, bus) == false) -- 'bus is closed'

-- Test set_cfg() min/max values
bus = can.open(valid_bus_name)
bus:set_cfg({baudrate = 1, bs1 = 1, bs2 = 1, sjw = 1, silent = 0})
bus:set_cfg({baudrate = 1000*1000, bs1 = 16, bs2 = 8, sjw = 4, silent = 0})
bus:set_cfg({baudrate = 1, bs1 = 16, bs2 = 1, sjw = 1, silent = 0})
bus:set_cfg({baudrate = 1, bs1 = 1, bs2 = 8, sjw = 1, silent = 0})
bus:set_cfg({baudrate = 1, bs1 = 1, bs2 = 1, sjw = 4, silent = 0})
bus:set_cfg({baudrate = 1, bs1 = 1, bs2 = 1, sjw = 1, silent = 1})
bus:close()

-- Test set_cfg() with out-of-range values
bus = can.open(valid_bus_name)
assert(pcall(bus.set_cfg, bus, {baudrate = 0, bs1 =  1, bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1000001, bs1 = 1, bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  0, bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 = 17, bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  1, bs2 = 0, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  1, bs2 = 9, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  1, bs2 = 1, sjw = 0, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  1, bs2 = 1, sjw = 5, silent = 1}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 =  1, bs2 = 1, sjw = 1, silent = 2}) == false)
bus:close()

-- Test set_cfg() with missing field
bus = can.open(valid_bus_name)
assert(pcall(bus.set_cfg, bus, {              bs1 = 1, bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1,          bs2 = 1, sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 = 1,          sjw = 1, silent = 0}) == false)
assert(pcall(bus.set_cfg, bus, {baudrate = 1, bs1 = 1, bs2 = 1, sjw = 1            }) == false)
bus:close()

-- Test set_cfg() misc error handling
bus = can.open(valid_bus_name)
bus:set_cfg(valid_cfg)
bus:on()
assert(pcall(bus.set_cfg, bus, valid_cfg) == false) -- 'bus is on'
bus:off()
bus:close()
assert(pcall(bus.set_cfg, bus, valid_cfg) == false) -- 'bus is closed'

-- Test set_filter() error handling
bus = can.open(valid_bus_name)
assert(pcall(bus.set_filter, bus, {id = 0x1234}) == false) -- 'missing field' 
assert(pcall(bus.set_filter, bus, {mask = 0xffff}) == false) -- 'missing field'
bus:set_cfg(valid_cfg)
bus:on()
assert(pcall(bus.set_filter, bus, valid_filter) == false) -- 'bus is on'
bus:off()
bus:close()
assert(pcall(bus.set_filter, bus, valid_filter) == false) -- 'bus is closed'

-- Test get_status()
bus = can.open(valid_bus_name)
status = bus:get_status()
assert(status.bus_off)
bus:set_cfg(valid_cfg)
bus:set_filter(valid_filter)
bus:on()
status = bus:get_status()
assert(status.bus_off == false)
bus:off()
bus:close()
assert(pcall(bus.get_status, bus) == false) -- 'bus is closed'

-- Test receive() error handling
bus = can.open(valid_bus_name)
assert(pcall(bus.receive, bus) == false) -- 'bus is not configured'
bus:set_cfg(valid_cfg)
assert(pcall(bus.receive, bus) == false) -- 'no receive filter configured'
bus:set_filter(valid_filter)
assert(pcall(bus.receive, bus) == false) -- 'bus is off'
bus:on()
id, data = bus:receive()
assert(id == nil and data == nil)
bus:off()
bus:close()
assert(pcall(bus.receive, bus) == false) -- 'bus is closed'

-- Test transmit() error handling. Note that fake CAN driver will return
-- error after two transmit()s.
bus = can.open(valid_bus_name)
assert(pcall(bus.transmit, bus, 1, 'data') == false) -- 'bus is not configured'
bus:set_cfg(valid_cfg)
bus:set_filter(valid_filter)
assert(pcall(bus.transmit, bus, 1, 'data') == false) -- 'bus is off'
bus:on()
assert(pcall(bus.transmit, bus, 'data') == false) -- 'wrong number of arguments'
assert(pcall(bus.transmit, bus, 1, 5) == false) -- 'string expected'
assert(pcall(bus.transmit, bus, 1, '123456789') == false) -- 'too much data'
bus:transmit(1, 'data')
bus:transmit(1, 'data')
assert(pcall(bus.transmit, bus, 1, 'data') == false) -- 'failed to transmit'
bus:off()
bus:close()
assert(pcall(bus.transmit, bus, 1, 'data') == false) -- 'bus is closed'

-- Test close() error handling
bus = can.open(valid_bus_name)
bus:set_cfg(valid_cfg)
bus:set_filter(valid_filter)
bus:on()
assert(pcall(can.close, bus) == false) -- 'bus is on'
bus:off()
bus:close()
assert(pcall(can.close, bus) == false) -- 'already closed'
