

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick2.h
 * 
 * 
 * 
 * Generated from: /candev_model/modules/joystick2.kfm
 */

#include "ck.h"
#include "joystick2.h"


// *** Receive handle functions


// *** Transmit functions
void ckappSend_CKDN_joy2_adc1(ckd_joy2_adc1* doc) {
  ckSend(CKDN_joy2_adc1, (uint8_t*) doc, sizeof(ckd_joy2_adc1), 0);
}
void ckappSend_CKDN_joy2_adc2(ckd_joy2_adc2* doc) {
  ckSend(CKDN_joy2_adc2, (uint8_t*) doc, sizeof(ckd_joy2_adc2), 0);
}
void ckappSend_CKDN_joy_io_doc(ckd_joy_io_doc* doc) {
  ckSend(CKDN_joy_io_doc, (uint8_t*) doc, sizeof(ckd_joy_io_doc), 0);
}

extern void ckappOnIdle(void);
extern bool checkShutdown(void);

void ckappMain(void) {
  ckInit(ckStartListenForDefaultLetter);
  for(;;) {
    ckappOnIdle();
    ckMain();
    if (checkShutdown()) break;
  }
}

void ckappSetActionMode(ckActMode am) {
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
}

void ckappDispatchRTR(uint16_t doc, bool txF) {
}

int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  return 1; // Page not supported
}

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
  } // End of document switch
} // End of ckappDispatch

