

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick1.h
 * 
 * 
 * 
 * Generated from: /candev_model/modules/joystick1.kfm
 */

#include "ck.h"
#include "joystick1.h"


// *** Receive handle functions
extern void handleJoy2_adc1(ckd_joy2_adc1* doc);
extern void handleJoy2_adc2(ckd_joy2_adc2* doc);
extern void handleJoy_io_doc(ckd_joy_io_doc* doc);


// *** Transmit functions
void ckappSend_CKDN_s01s04_pwm(ckd_s01s04_pwm* doc) {
  ckSend(CKDN_s01s04_pwm, (uint8_t*) doc, sizeof(ckd_s01s04_pwm), 0);
}
void ckappSend_CKDN_s_light(ckd_s_light* doc) {
  ckSend(CKDN_s_light, (uint8_t*) doc, sizeof(ckd_s_light), 0);
}
void ckappSend_CKDN_motor_ctrl(ckd_motor_ctrl* doc) {
  ckSend(CKDN_motor_ctrl, (uint8_t*) doc, sizeof(ckd_motor_ctrl), 0);
}
void ckappSend_CKDN_lamp_ctrl(ckd_lamp_ctrl* doc) {
  ckSend(CKDN_lamp_ctrl, (uint8_t*) doc, sizeof(ckd_lamp_ctrl), 0);
}

extern void ckappOnIdle(void);
extern bool checkShutdown(void);

void ckappMain(void) {
  ckInit(ckStartListenForDefaultLetter);
  for(;;) {
    ckappOnIdle();
    ckMain();
    if (checkShutdown()) break;
  }
}

void ckappSetActionMode(ckActMode am) {
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
}

void ckappDispatchRTR(uint16_t doc, bool txF) {
}

int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  return 1; // Page not supported
}

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    case CKDN_joy2_adc1: // Joystick 2 ADC values 1
      handleJoy2_adc1((ckd_joy2_adc1*) msgBuf); 
      break;
    case CKDN_joy2_adc2: // Joystick 2 ADC values 2
      handleJoy2_adc2((ckd_joy2_adc2*) msgBuf); 
      break;
    case CKDN_joy_io_doc: // Joystick IO Document
      handleJoy_io_doc((ckd_joy_io_doc*) msgBuf); 
      break;
  } // End of document switch
} // End of ckappDispatch

