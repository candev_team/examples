

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick1.h
 * 
 * 
 * 
 * Generated from: /candev_model/modules/joystick1.kfm
 */

#ifndef JOYSTICK1
#define JOYSTICK1

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Joystick 2 ADC values 1. Record number: 10, List number: 1
#define CKDN_joy2_adc1 10
 
typedef struct {
  uint16_t io_02_adc1; // Joystick 2 ADC value 1
  uint16_t io_02_adc2; // Joystick 2 ADC value 2
  uint16_t io_02_adc3; // Joystick 2 ADC value 3
  uint16_t io_02_adc4; // Joystick 2 ADC value 4
  
    
    
    
  
} ckd_joy2_adc1;


// Joystick 2 ADC values 2. Record number: 11, List number: 1
#define CKDN_joy2_adc2 11
 
typedef struct {
  uint16_t io_02_adc5; // Joystick 2 ADC value 5
  uint16_t io_02_adc6; // Joystick 2 ADC value 6
  
    
    
    
  
} ckd_joy2_adc2;


// Joystick IO Document. Record number: 12, List number: 1
#define CKDN_joy_io_doc 12
 
typedef struct {
  uint8_t sw1_up:1; // Joystick 2 switch 1 up
  uint8_t sw1_down:1; // Joystick 2 switch 1 down
  uint8_t sw2_up:1; // Joystick 2 switch 2 up
  uint8_t sw2_down:1; // Joystick 2 switch 2 down
  uint8_t sw3_up:1; // Joystick 2 switch 3 up
  uint8_t sw3_down:1; // Joystick 2 switch 3 down
  uint8_t sw4_up:1; // Joystick 2 switch 4 up
  uint8_t sw4_down:1; // Joystick 2 switch 4 down
  uint8_t trim_up:1; // Joystick 2 trim up
  uint8_t trim_down:1; // Joystick 2 trim down
  uint8_t trim_left:1; // Joystick 2 trim left
  uint8_t trim_right:1; // Joystick 2 trim right
  
    
    
    
  
} ckd_joy_io_doc;


// S01-S04 PWM pulse widths. Record number: 13, List number: 2
#define CKDN_s01s04_pwm 13
 
typedef struct {
  uint16_t s01_pwm; // S01 PWM pulse width
  uint16_t s02_pwm; // S02 PWM pulse width
  uint16_t s03_pwm; // S03 PWM pulse width
  uint16_t s04_pwm; // S04 PWM pulse width
  
    
    
    
  
} ckd_s01s04_pwm;


// Searchlight control. Record number: 14, List number: 2
#define CKDN_s_light 14
 
typedef struct {
  uint16_t s_light_h; // Searchlight horizontal
  uint16_t s_light_v; // Searchlight vertical
  uint16_t s_light_lamp; // Searchlight lamp mode
  
    
    
    
  
} ckd_s_light;


// Motor control. Record number: 15, List number: 2
#define CKDN_motor_ctrl 15
 
typedef struct {
  uint16_t motor_speed; // Motor speed
  
    
    
    
  
} ckd_motor_ctrl;


// Lamp control. Record number: 16, List number: 2
#define CKDN_lamp_ctrl 16
 
typedef struct {
  uint8_t lamp_a:1; // Lamp A
  uint8_t lamp_b:1; // Lamp B
  
    
    
    
  
} ckd_lamp_ctrl;



#pragma pack()

// *** Transmit functions

void ckappSend_CKDN_s01s04_pwm(ckd_s01s04_pwm* doc);
void ckappSend_CKDN_s_light(ckd_s_light* doc);
void ckappSend_CKDN_motor_ctrl(ckd_motor_ctrl* doc);
void ckappSend_CKDN_lamp_ctrl(ckd_lamp_ctrl* doc);

#endif /* JOYSTICK1 */
