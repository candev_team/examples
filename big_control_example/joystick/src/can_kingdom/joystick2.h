

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * joystick2.h
 * 
 * 
 * 
 * Generated from: /candev_model/modules/joystick2.kfm
 */

#ifndef JOYSTICK2
#define JOYSTICK2

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Joystick 2 ADC values 1-4. Record number: 10, List number: 1
#define CKDN_joy2_adc1 10
 
typedef struct {
  uint16_t adc1; // ADC value 1
  uint16_t adc2; // ADC value 2
  uint16_t adc3; // ADC value 3
  uint16_t adc4; // ADC value 4
  
    
    
    
  
} ckd_joy2_adc1;


// Joystick 2 ADC values 5-6. Record number: 11, List number: 1
#define CKDN_joy2_adc2 11
 
typedef struct {
  uint16_t adc5; // ADC value 5
  uint16_t adc6; // ADC value 6
  
    
    
    
  
} ckd_joy2_adc2;


// Joystick IO Document. Record number: 12, List number: 1
#define CKDN_joy_io_doc 12
 
typedef struct {
  uint8_t sw1_up:1; // Switch 1 up
  uint8_t sw1_down:1; // Switch 1 down
  uint8_t sw2_up:1; // Switch 2 up
  uint8_t sw2_down:1; // Switch 2 down
  uint8_t sw3_up:1; // Switch 3 up
  uint8_t sw3_down:1; // Switch 3 down
  uint8_t sw4_up:1; // Switch 4 up
  uint8_t sw4_down:1; // Switch 4 down
  uint8_t trim_up:1; // Trim up
  uint8_t trim_down:1; // Trim down
  uint8_t trim_left:1; // Trim left
  uint8_t trim_right:1; // Trim right
  
    
    
    
  
} ckd_joy_io_doc;



#pragma pack()

// *** Transmit functions

void ckappSend_CKDN_joy2_adc1(ckd_joy2_adc1* doc);
void ckappSend_CKDN_joy2_adc2(ckd_joy2_adc2* doc);
void ckappSend_CKDN_joy_io_doc(ckd_joy_io_doc* doc);

#endif /* JOYSTICK2 */
