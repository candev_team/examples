/*
 * joystick_example.h
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */


/*! \mainpage Kvaser CANdev IO Module Example
 *
 * \section intro_sec Introduction
 * The Kvaser CANdev IO Module example shows how the different inputs/outputs can be used and
 * how to create a multitask application and reserve data integrity. The example
 * is divided in three block input sampling, process data and output writing.
 *
 * \subsection sampling_sec Input sampling
 * To sample all the inputs on the Kvaser CANdev IO Module, multiple tasks have been created.
 * These tasks can both be prioritized and scheduled individually, e.g.
 * critical inputs are given high priority, slow changing sensor inputs are
 * scheduled to sample at low rate.
 *
 * \dot
 * digraph samping_machine {
 *     node [shape = circle, label="read input data", fontsize=10] read;
 *     node [shape = doublecircle, label="acquire process data lock", fontsize=10] lock;
 *     node [shape = circle, label="write process data", fontsize=10] write;
 *     node [shape = doublecircle, label="release process data lock", fontsize=10] unlock;
 *
 *     node [shape = point ]; qi
 *     qi -> read;
 *     read -> lock  [ label = "read done", fontsize=10];
 *     lock -> write   [ label = "lock acquired", fontsize=10];
 *     write -> unlock  [ label = "write done", fontsize=10];
 *     unlock -> read   [ label = "lock released", fontsize=10];
 * }
 * \enddot
 *
 * \subsection process_sec Process data
 * The process data is a data structure representing the various inputs and
 * outputs on the Kvaser CANdev IO Module, this data structure is used during reading/writing
 * of new output/input.
 *
 * \subsection can_sec CAN Communication
 * An example is given on how to configure a start CAN communication using the
 * init_can function in combination of two tasks handling receiving and
 * transmitting of CAN messages.
 *
 * \subsubsection can_tx_sec CAN transmit messages (IO Module)
 * A CAN transmit task is used to send values of the analog inputs and digital inputs
 * in can messages.
 *
 * id 0x700: dlc=8, adc1 data[0]=LSB,data[1]=MSB,
 * adc2 data[2]=LSB,data[3]=MSB, adc3 data[4]=LSB,data[5]=MSB, adc4 data[6]=LSB,data[7]=MSB
 *
 * id 0x701: dlc=4, adc1 data[0]=LSB,data[1]=MSB,
 * adc2 data[2]=LSB,data[3]=MSB.
 *
 * id 0x702: dlc=2, digital io data[0]:0 = Input0, data[0]:1 = Input1
 * data[0]:2 = Input2, data[0]:3 = Input3, data[0]:4 = Input4, data[0]:5 = Input5,
 * data[0]:6 = Input6, data[0]:7 = Input7, data[1]:0 = Input8, data[1]:1 = Input9,
 * data[1]:2 = Input10, data[1]:3 = Input11.
 *
 * id 0x100, dlc=4, data[0]=LSB,...data[3]=MSB. PWM output calculated from adc[0].
 * Valid values are 0 to 10000 (percent of pulse width multiplied with 100).
 *
 * id 0x300, dlc=5, data[0]=LSB,...data[3]=MSB. PWM output calculated from adc[0].
 * Valid values are 0 to 10000 (percent of pulse width multiplied with 100).
 * data[4]=Input6 (0 = Off, 1 = On).
 *
 * The messages are sent every 100 ms.
 *
 * \subsubsection can_rx_sec CAN receive messages (IO Module)
 * The CAN receive task is not receiving any CAN messages in the IO Module.
 *
 */

/**
 * \defgroup candev_io_example Kvaser CANdev IO Module Example
 * \{
 */

#ifndef SRC_JOYSTICK_EXAMPLE_H
#define SRC_JOYSTICK_EXAMPLE_H

#include <kern.h>
#include <can/can.h>

/** process_data data structure, used to represent process data e.g.  */
typedef struct process_data
{
   mtx_t *  pd_lock;                /**< Process data mutex */
   uint16_t io_01_adc_value[6];     /**< IO_01 Sampled adc values */
   uint16_t io_01_cal_value[2][6];  /**< IO_01 Calibrated min max values */
   int      io_01_gpio_in[12];      /**< IO_01 GPIO input */
   uint16_t io_02_adc_value[6];     /**< IO_02 Sampled adc values */
   uint16_t io_02_cal_value[2][6];  /**< IO_02 Calibrated min max values */
   int      io_02_gpio_in[12];      /**< IO_02 GPIO input */

   int32_t  s01_pwm_out_pul;        /**< S01 PWM pulse width */
   int32_t  s02_pwm_out_pul;        /**< S02 PWM pulse width */
   int32_t  s03_pwm_out_pul;        /**< S03 PWM pulse width */
   int32_t  s04_pwm_out_pul;        /**< S04 PWM pulse width */
   int32_t  s05_pwm_out_pul;        /**< S05 PWM pulse width */
   int32_t  s06_pwm_out_pul;        /**< S06 PWM pulse width */

   int32_t  m01_pwm_out_pul;        /**< M01 PWM pulse width */
   int32_t  m01_pwm_start;          /**< M01 PWM start */

} process_data_t;


#define  ADC_SAMPLE_PRIORITY        10       /**< sample_adc task priority */
#define  ADC_SAMPLE_STACK_SIZE      1024     /**< sample_adc stack size */
#define  ADC_SAMPLE_RATE            100      /**< sample_adc run rate */

#define  GPIO_SAMPLE_PRIORITY        10       /**< sample_adc task priority */
#define  GPIO_SAMPLE_STACK_SIZE      1024     /**< sample_adc stack size */
#define  GPIO_SAMPLE_RATE            10       /**< sample_adc run rate */

#define  CAN_TASK_PRIORITY          10       /**< can_rx_task, can_tx_task task priority */
#define  CAN_TASK_STACK_SIZE        1024     /**< can_rx_task, can_tx_task stack size */
#define  CAN_TASK_TX_RATE           20       /**< can_rx_task, can_tx_task run rate */

#define  CAN_BIT_RATE_125K          0        /**< CAN bitrate 125 Kbit */
#define  CAN_BIT_RATE_250K          1        /**< CAN bitrate 250 Kbit */
#define  CAN_BIT_RATE_500K          2        /**< CAN bitrate 500 Kbit */
#define  CAN_BIT_RATE_1000K         4        /**< CAN bitrate 1000 Kbit */

#define  DUMP_PROCESS_DATA_PRIORITY 10       /**< dump_process_data task priority */
#define  DUMP_PROCESS_DATA_SIZE     1024     /**< dump_process_data stack size */
#define  DUMP_PROCESS_DATA_RATE     2        /**< dump_process_data run rate */

/**
 * can_rx_callback is called by the can driver when a can frame is received,
 * this function is executed in interrupt context and only sets a flag to
 * pass on the actual reading of the can message. The reading of can messages
 * is done in a worker task(can_rx_task).
 *
 * \param arg     Sent as argument setup by can_set_callback.
 * \param event   Current set of callback events.
 *
 */
void can_rx_callback (void * arg, can_event_t event );

/**
 * The function can_rx_task is scheduled as a task and handles all incoming CAN message.
 * It waits for a flag set by can_rx_callback, once the flag is set a CAN
 * message is read from the actual CAN driver and processed.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void can_rx_task (void * arg);

/**
 * The function can_tx_task is scheduled as a task and is used to send CAN message.
 * It sends a number of CAN messages at the rate defined by CAN_TASK_TX_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void can_tx_task (void * arg);

/**
 * The function sample_adc is scheduled as a task, it handles reading all
 * analog inputs inputs. Initially it opens all adc drivers/channels and
 * then enters a loop where the analog values are sampled and stored in a
 * process data structure at a rate given by ADC_SAMPLE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_adc(void * arg);

/**
 * The function sample_gpio is scheduled as a task, it handles reading of
 * a gpio inputs.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_gpio(void * arg);

/**
 * The function dump_process_data is scheduled as a task, it prints the values of the
 * process data structure. The dump_process_data task enters a loop where process data
 * is copied and then printed out on the serial port at a rate given by
 * DUMP_PROCESS_DATA_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void dump_process_data(void * arg);

/**
 * The function init_can responsible for initiating the CAN driver, setting bit rate and
 * filter parameters, set a callback function on CAN message received and go on bus.
 * Once the initiation is complete it spawns the can_rx_task and the can_tx_task.
 *
 * \param process_data     A process_data_t pointer.
 *
 */
void init_can(process_data_t * process_data);

#endif /* SRC_JOYSTICK_EXAMPLE_H */

/**
 * \}
 */

