/*
 * joystick_example.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include <config.h>
#include <adc/adc.h>
#include <can/bxcan.h>
#include <bsp.h>

#include <string.h>
#include "joystick_example.h"

#define PWM_SERVO_MIN      400      /* 4% = 0.8ms */
#define PWM_SERVO_CENTER   750      /* 7.5% = 1.5ms */
#define PWM_SERVO_MAX      1100     /* 11% = 2.2 ms */
#define PWM_FAN_MIN        400      /* 4% = 0.8ms */
#define PWM_FAN_MAX        1000     /* 10% = 2.0ms */
#define PWM_LIGHT_VERT_MIN 635      /* 6.35% = 1.27ms */
#define PWM_LIGHT_VERT_MAX 1095     /* 10.95% = 2.190ms */

#define CAN_MSG_RECEIVED         BIT(0)

typedef struct can_struct
{
   int can_fd;
   flags_t * can_flag;
   process_data_t * process_data;
} can_struct_t;

static process_data_t * process_data;

#if IO_NODE == 1
#include "joystick1.h"

#define JOYSTICK1_SEND_INTERVAL_MS 100

static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

static uint16_t calc_pwm_out (uint16_t adc_val, uint16_t adc_min, uint16_t adc_max, uint16_t pwm_min, uint16_t pwm_max)
{
   uint32_t tmp_calc;

   tmp_calc = adc_max - adc_min;
   tmp_calc = ((pwm_max - pwm_min) * 1000) / tmp_calc;
   tmp_calc = (((adc_val - adc_min) * tmp_calc) / 1000);

   return (uint16_t) tmp_calc + pwm_min;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_s01s04_pwm servos = { PWM_SERVO_CENTER, PWM_SERVO_CENTER, PWM_SERVO_CENTER, PWM_SERVO_CENTER };
   static ckd_s_light s_light = { PWM_SERVO_CENTER, PWM_SERVO_CENTER, PWM_SERVO_MIN };
   static ckd_motor_ctrl motor_ctrl = { PWM_FAN_MIN };
   static ckd_lamp_ctrl lamp;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (JOYSTICK1_SEND_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);

         lamp.lamp_a = process_data->io_02_gpio_in[6];
         lamp.lamp_b = process_data->io_02_gpio_in[4];

         if ((process_data->io_01_gpio_in[0] == 0) && (process_data->io_01_gpio_in[1] == 1))
         {
            if ((process_data->io_01_gpio_in[4] == 1) && (process_data->io_01_gpio_in[5] == 0))
            {
               motor_ctrl.motor_speed = (PWM_FAN_MIN + PWM_FAN_MAX) - calc_pwm_out (process_data->io_02_adc_value[0], process_data->io_02_cal_value[0][0], process_data->io_02_cal_value[1][0], PWM_FAN_MIN, PWM_FAN_MAX);
            }
            else
            {
               motor_ctrl.motor_speed = PWM_FAN_MIN;
            }

            servos.s01_pwm = calc_pwm_out (process_data->io_02_adc_value[1], process_data->io_02_cal_value[0][1], process_data->io_02_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s02_pwm = calc_pwm_out (process_data->io_01_adc_value[0], process_data->io_01_cal_value[0][0], process_data->io_01_cal_value[1][0], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s03_pwm = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s04_pwm = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);

            s_light.s_light_v = (PWM_LIGHT_VERT_MIN + PWM_LIGHT_VERT_MAX) - calc_pwm_out (process_data->io_01_adc_value[0], process_data->io_01_cal_value[0][0], process_data->io_01_cal_value[1][0], PWM_LIGHT_VERT_MIN, PWM_LIGHT_VERT_MAX);
            s_light.s_light_h = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
         }
         else if ((process_data->io_01_gpio_in[0] == 1) && (process_data->io_01_gpio_in[1] == 1))
         {
            s_light.s_light_h = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
            s_light.s_light_v = (PWM_LIGHT_VERT_MIN + PWM_LIGHT_VERT_MAX) - calc_pwm_out (process_data->io_01_adc_value[0],  process_data->io_01_cal_value[0][0], process_data->io_01_cal_value[1][0], PWM_LIGHT_VERT_MIN, PWM_LIGHT_VERT_MAX);
            s_light.s_light_lamp = (PWM_SERVO_MIN + PWM_SERVO_MAX) - calc_pwm_out (process_data->io_02_adc_value[0], process_data->io_02_cal_value[0][0], process_data->io_02_cal_value[1][0], PWM_SERVO_MIN, PWM_SERVO_MAX);
         }
         else if ((process_data->io_01_gpio_in[0] == 1) && (process_data->io_01_gpio_in[1] == 0))
         {
            servos.s01_pwm = calc_pwm_out (process_data->io_01_adc_value[0], process_data->io_01_cal_value[0][0], process_data->io_01_cal_value[1][0], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s02_pwm = calc_pwm_out (process_data->io_01_adc_value[0], process_data->io_01_cal_value[0][0], process_data->io_01_cal_value[1][0], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s03_pwm = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
            servos.s04_pwm = calc_pwm_out (process_data->io_01_adc_value[1], process_data->io_01_cal_value[0][1], process_data->io_01_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);

            s_light.s_light_h = (PWM_SERVO_MIN + PWM_SERVO_MAX) - calc_pwm_out (process_data->io_02_adc_value[1], process_data->io_02_cal_value[0][1], process_data->io_02_cal_value[1][1], PWM_SERVO_MIN, PWM_SERVO_MAX);
            s_light.s_light_v = calc_pwm_out (process_data->io_02_adc_value[0], process_data->io_02_cal_value[0][0], process_data->io_02_cal_value[1][0], PWM_LIGHT_VERT_MIN, PWM_LIGHT_VERT_MAX);
         }

         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_s01s04_pwm (&servos);
         ckappSend_CKDN_s_light (&s_light);
         ckappSend_CKDN_motor_ctrl (&motor_ctrl);
         ckappSend_CKDN_lamp_ctrl (&lamp);
      }
   }
}

bool checkShutdown(void)
{
   return false;
}

void handleJoy2_adc1(ckd_joy2_adc1* doc)
{
   uint32_t i;

   mtx_lock(process_data->pd_lock);

   process_data->io_02_adc_value[0] = doc->io_02_adc1;
   process_data->io_02_adc_value[1] = doc->io_02_adc2;
   process_data->io_02_adc_value[2] = doc->io_02_adc3;
   process_data->io_02_adc_value[3] = doc->io_02_adc4;

   for (i = 0; i < 4; i++)
   {
      if (process_data->io_02_adc_value[i] < process_data->io_02_cal_value[0][i])
      {
         process_data->io_02_cal_value[0][i] = process_data->io_02_adc_value[i];
      }
      if (process_data->io_02_adc_value[i] > process_data->io_02_cal_value[1][i])
      {
         process_data->io_02_cal_value[1][i] = process_data->io_02_adc_value[i];
      }
   }

   mtx_unlock(process_data->pd_lock);
}

void handleJoy2_adc2(ckd_joy2_adc2* doc)
{
   uint32_t i;

   mtx_lock(process_data->pd_lock);

   process_data->io_02_adc_value[4] = doc->io_02_adc5;
   process_data->io_02_adc_value[5] = doc->io_02_adc6;

   for (i = 4; i < 6; i++)
   {
      if (process_data->io_02_adc_value[i] < process_data->io_02_cal_value[0][i])
      {
         process_data->io_02_cal_value[0][i] = process_data->io_02_adc_value[i];
      }
      if (process_data->io_02_adc_value[i] > process_data->io_02_cal_value[1][i])
      {
         process_data->io_02_cal_value[1][i] = process_data->io_02_adc_value[i];
      }
   }

   mtx_unlock(process_data->pd_lock);
}

void handleJoy_io_doc(ckd_joy_io_doc* doc)
{
   mtx_lock(process_data->pd_lock);

   process_data->io_02_gpio_in[0] = doc->sw1_up;
   process_data->io_02_gpio_in[1] = doc->sw1_down;
   process_data->io_02_gpio_in[2] = doc->sw2_up;
   process_data->io_02_gpio_in[3] = doc->sw2_down;
   process_data->io_02_gpio_in[4] = doc->sw3_up;
   process_data->io_02_gpio_in[5] = doc->sw3_down;
   process_data->io_02_gpio_in[6] = doc->sw4_up;
   process_data->io_02_gpio_in[7] = doc->sw4_down;
   process_data->io_02_gpio_in[8] = doc->trim_up;
   process_data->io_02_gpio_in[9] = doc->trim_down;
   process_data->io_02_gpio_in[10] = doc->trim_left;
   process_data->io_02_gpio_in[11] = doc->trim_right;

   mtx_unlock(process_data->pd_lock);
}
#endif


#if IO_NODE == 2
#include "joystick2.h"

#define JOYSTICK2_SEND_INTERVAL_MS 100

static bool send = false;

static void tmr_callback (tmr_t * timer, void * arg)
{
   send = true;
}

void ckappOnIdle(void)
{
   static tmr_t * timer;
   static bool setup = false;
   static ckd_joy2_adc1 joy2_adc1;
   static ckd_joy2_adc2 joy2_adc2;
   static ckd_joy_io_doc joy2_io;

   if (!setup)
   {
      timer = tmr_create (tick_from_ms (JOYSTICK2_SEND_INTERVAL_MS), tmr_callback, NULL, TMR_CYCL);
      tmr_start (timer);

      setup = true;
   }
   else
   {
      if (send)
      {
         send = false;

         mtx_lock(process_data->pd_lock);

         joy2_adc1.adc1 = process_data->io_01_adc_value[0];
         joy2_adc1.adc2 = process_data->io_01_adc_value[1];
         joy2_adc1.adc3 = process_data->io_01_adc_value[2];
         joy2_adc1.adc4 = process_data->io_01_adc_value[3];

         joy2_adc2.adc5 = process_data->io_01_adc_value[4];
         joy2_adc2.adc6 = process_data->io_01_adc_value[5];

         joy2_io.sw1_up = process_data->io_01_gpio_in[0];
         joy2_io.sw1_down = process_data->io_01_gpio_in[1];
         joy2_io.sw2_up = process_data->io_01_gpio_in[2];
         joy2_io.sw2_down = process_data->io_01_gpio_in[3];
         joy2_io.sw3_up = process_data->io_01_gpio_in[4];
         joy2_io.sw3_down = process_data->io_01_gpio_in[5];
         joy2_io.sw4_up = process_data->io_01_gpio_in[6];
         joy2_io.sw4_down = process_data->io_01_gpio_in[7];
         joy2_io.trim_up = process_data->io_01_gpio_in[8];
         joy2_io.trim_down = process_data->io_01_gpio_in[9];
         joy2_io.trim_left = process_data->io_01_gpio_in[10];
         joy2_io.trim_right = process_data->io_01_gpio_in[11];

         mtx_unlock(process_data->pd_lock);

         ckappSend_CKDN_joy2_adc1 (&joy2_adc1);
         ckappSend_CKDN_joy2_adc2 (&joy2_adc2);
         ckappSend_CKDN_joy_io_doc (&joy2_io);
      }
   }
}

bool checkShutdown(void)
{
   return false;
}
#endif

void sample_adc(void * arg)
{
   process_data = (process_data_t *) arg;
   int adc[6];
   uint32_t tmp_value[6];
   int i;

   mtx_lock(process_data->pd_lock);

   for (i = 0; i < 6; i++)
   {
      process_data->io_01_adc_value[i] = 0;
      process_data->io_02_adc_value[i] = 0;
      process_data->io_01_cal_value[0][i] = 0xFFFF;
      process_data->io_01_cal_value[1][i] = 0;
      process_data->io_02_cal_value[0][i] = 0xFFFF;
      process_data->io_02_cal_value[1][i] = 0;
   }

   mtx_unlock(process_data->pd_lock);

   adc[0] = adc_open ("/adc1/1");
   ASSERT(adc[0] > 0);
   adc[1] = adc_open ("/adc1/2");
   ASSERT(adc[1] > 0);
   adc[2] = adc_open ("/adc1/3");
   ASSERT(adc[2] > 0);
   adc[3] = adc_open ("/adc1/4");
   ASSERT(adc[3] > 0);
   adc[4] = adc_open ("/adc2/1");
   ASSERT(adc[4] > 0);
   adc[5] = adc_open ("/adc2/2");
   ASSERT(adc[5] > 0);

   adc_start(adc[0]);
   adc_start(adc[1]);
   adc_start(adc[2]);
   adc_start(adc[3]);
   adc_start(adc[4]);
   adc_start(adc[5]);

   // Delay to allow for first samples to be sampled.
   task_delay (10);

   while(1)
   {
      tmp_value[0] = adc_sample_get(adc[0]);
      tmp_value[1] = adc_sample_get(adc[1]);
      tmp_value[2] = adc_sample_get(adc[2]);
      tmp_value[3] = adc_sample_get(adc[3]);
      tmp_value[4] = adc_sample_get(adc[4]);
      tmp_value[5] = adc_sample_get(adc[5]);


      mtx_lock(process_data->pd_lock);
      for (i = 0; i < 6; i++)
      {
         if (tmp_value[i] < process_data->io_01_cal_value[0][i])
         {
            process_data->io_01_cal_value[0][i] = tmp_value[i];
         }
         if (tmp_value[i] > process_data->io_01_cal_value[1][i])
         {
            process_data->io_01_cal_value[1][i] = tmp_value[i];
         }
      }

      process_data->io_01_adc_value[0] = tmp_value[0];
      process_data->io_01_adc_value[1] = tmp_value[1];
      process_data->io_01_adc_value[2] = tmp_value[2];
      process_data->io_01_adc_value[3] = tmp_value[3];
      process_data->io_01_adc_value[4] = tmp_value[4];
      process_data->io_01_adc_value[5] = tmp_value[5];
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/ADC_SAMPLE_RATE);
   }
}

void sample_gpio(void * arg)
{
   process_data = (process_data_t *) arg;

   mtx_lock(process_data->pd_lock);
   process_data->io_01_gpio_in[0] = 0;
   process_data->io_01_gpio_in[1] = 0;
   process_data->io_01_gpio_in[2] = 0;
   process_data->io_01_gpio_in[3] = 0;
   process_data->io_01_gpio_in[4] = 0;
   process_data->io_01_gpio_in[5] = 0;
   process_data->io_01_gpio_in[6] = 0;
   process_data->io_01_gpio_in[7] = 0;
   process_data->io_01_gpio_in[8] = 0;
   process_data->io_01_gpio_in[9] = 0;
   process_data->io_01_gpio_in[10] = 0;
   process_data->io_01_gpio_in[11] = 0;
   mtx_unlock(process_data->pd_lock);

   while(1)
   {
      mtx_lock(process_data->pd_lock);
      process_data->io_01_gpio_in[0] = gpio_get(GPIO_IOIN0);
      process_data->io_01_gpio_in[1] = gpio_get(GPIO_IOIN1);
      process_data->io_01_gpio_in[2] = gpio_get(GPIO_IOIN2);
      process_data->io_01_gpio_in[3] = gpio_get(GPIO_IOIN3);
      process_data->io_01_gpio_in[4] = gpio_get(GPIO_IOIN4);
      process_data->io_01_gpio_in[5] = gpio_get(GPIO_IOIN5);
      process_data->io_01_gpio_in[6] = gpio_get(GPIO_IOIN6);
      process_data->io_01_gpio_in[7] = gpio_get(GPIO_IOIN7);
      process_data->io_01_gpio_in[8] = gpio_get(GPIO_IOIN8);
      process_data->io_01_gpio_in[9] = gpio_get(GPIO_IOIN9);
      process_data->io_01_gpio_in[10] = gpio_get(GPIO_IOIN10);
      process_data->io_01_gpio_in[11] = gpio_get(GPIO_IOIN11);
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/GPIO_SAMPLE_RATE);
   }
}
