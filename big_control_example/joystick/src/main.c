/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id$
 *------------------------------------------------------------------------------
 */
#include <bsp.h>
#include <shell.h>
#include "joystick_example.h"

#if IO_NODE == 1
#include "joystick1.h"
#elif IO_NODE == 2
#include "joystick2.h"
#endif

#define CK_TASK_PRIO    3
#define CK_TASK_STACK   (2 * 1024)

extern void ckappMain(void);

static void ck_task (void * arg)
{
   (void)arg;

   ckappMain();
}

int main(void)
{
   process_data_t * process_data;

#if IO_NODE == 1
   shell_init ("joy1r>");
#elif IO_NODE == 2
   shell_init ("joy2l>");
#endif

   /* Allocate the process data structure */
   process_data = malloc(sizeof(process_data_t));
   UASSERT (process_data != NULL, EMEM);

   /* Create the process data lock */
   process_data->pd_lock = mtx_create();
   UASSERT(process_data->pd_lock != NULL, EMEM);

   /* Spawn sample tasks */
   task_spawn("sample_adc", sample_adc, ADC_SAMPLE_PRIORITY, ADC_SAMPLE_STACK_SIZE, process_data);
   task_spawn("sample_gpio", sample_gpio, GPIO_SAMPLE_PRIORITY, GPIO_SAMPLE_STACK_SIZE, process_data);

   task_spawn (
         "CAN Kingdom",
         ck_task,
         CK_TASK_PRIO,
         CK_TASK_STACK,
         NULL);

   return 0;
}
