

// *** WARNING: This is generated code! Editing this code might complicate the maintenance of the software.

/* 
 * lamp.h
 * 
 * Static lamps
 * 
 * Generated from: /candev_model/modules/lamp.kfm
 */

#ifndef LAMP
#define LAMP

#include<stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Lamp control. Record number: 10, List number: 2
#define CKDN_lamp 10
 
typedef struct {
  uint8_t lamp_a:1; // Lamp A
  uint8_t lamp_b:1; // Lamp B
  
    
    
    
  
} ckd_lamp;



#pragma pack()

// *** Transmit functions


#endif /* LAMP */
