/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id$
 *------------------------------------------------------------------------------
 */
#include <bsp.h>
#include <shell.h>
#include "lamp_example.h"

#include "lamp.h"

#define CK_TASK_PRIO    3
#define CK_TASK_STACK   (2 * 1024)

extern void ckappMain(void);

static void ck_task (void * arg)
{
   (void)arg;

   ckappMain();
}

int main(void)
{
   shell_init ("lamp>");

   task_spawn (
         "CAN Kingdom",
         ck_task,
         CK_TASK_PRIO,
         CK_TASK_STACK,
         NULL);

   return 0;
}
