/*
 * joystick_example.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include <config.h>
#include <adc/adc.h>
#include <can/bxcan.h>
#include <bsp.h>

#include <string.h>
#include "lamp_example.h"
#include "lamp.h"

void ckappOnIdle(void)
{
}

bool checkShutdown(void)
{
   return false;
}

void handleLamp(ckd_lamp* doc)
{
   static bool lamp_a = false;
   static bool lamp_b = false;

   if (doc->lamp_a != lamp_a)
   {
      lamp_a = doc->lamp_a;
      gpio_set (GPIO_IO_OUT0, 0);
      task_delay (tick_from_ms(100));
      gpio_set (GPIO_IO_OUT0, 1);
   }

   if (doc->lamp_b != lamp_b)
   {
      lamp_b = doc->lamp_b;
      gpio_set (GPIO_IO_OUT1, 0);
      task_delay (tick_from_ms(100));
      gpio_set (GPIO_IO_OUT1, 1);
   }
}
