/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id$
*------------------------------------------------------------------------------
*/

#include <bsp.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int luainterpreter_run_script (const char * file)
{
   int error;

   lua_State* L = luaL_newstate ();
   if (L == NULL)
   {
      const char * msg = "cannot create state: not enough memory";
      lua_writestringerror ("lua: %s\n", msg);
      return 1;
   }
   luaL_openlibs (L);

   error = luaL_dofile (L, file);
   if (error)
   {
      /* Error message was pushed on Lua stack */
      const char * msg = lua_tostring (L, -1);
      lua_writestringerror ("lua: %s\n", msg);
      lua_pop (L, 1);
   }

   lua_close (L);
   return error;
}

