/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id$
*------------------------------------------------------------------------------
*/

#include "lua_adc.h"
#include <lauxlib.h>
#include <adc/adc.h>
#include <string.h>

#undef RTK_DEBUG

#ifdef RTK_DEBUG
#define DPRINT(...) rprintp ("lua_adc: "__VA_ARGS__)
#else
#define DPRINT(...)
#endif  /* RTK_DEBUG */

#define LUA_ADC_CHANNEL LUA_ADC_LIBNAME".channel"

/* ADC channel type. Exposed to Lua as a "userdata" type. */
typedef struct lua_adc_channel
{
   int fd;
   bool is_opened;
   bool is_started;
} lua_adc_channel_t;

static void lua_adc_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Lua: channel = adc.open('/adc0/channel_name') */
static int lua_adc_open (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_check_number_of_arguments (L, 1);
   const char * channel_name = luaL_checkstring (L, 1);

   /* Push new class instance at top of stack */
   lua_adc_channel_t * self = lua_newuserdata(L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable(L, LUA_ADC_CHANNEL); /* Set class for instance */

   self->is_started = false;
   self->fd = adc_open (channel_name);
   if (self->fd < 0)
   {
      return luaL_error (L, "could not open channel '%s'", channel_name);
   }
   self->is_opened = true;
   DPRINT ("Opened\n");

   /* One result is already on stack */
   return 1;
}

/* Lua: channel.close(channel) OR channel:close() */
static int lua_adc_close (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_check_number_of_arguments (L, 1);
   lua_adc_channel_t * self = luaL_checkudata (L, 1, LUA_ADC_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel already closed");
   luaL_argcheck (L, self->is_started == false, 1, "channel not stopped");
   ASSERT (self->fd >= 0);

   adc_close (self->fd);
   self->is_opened = false;
   DPRINT ("Closed\n");

   /* Push no results on stack */
   return 0;
}

/* Destructor. Called by garbage collector. */
static int lua_adc_collect_garbage (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_channel_t * self = luaL_checkudata (L, 1, LUA_ADC_CHANNEL);
   ASSERT (self != NULL);

   if (self->is_opened == false)
   {
      /* Already closed, so nothing to clean up */
      DPRINT ("GC: nothing to do\n");
      return 0; /* Push no results on stack */
   }

   if (self->is_started)
   {
      adc_stop (self->fd);
      self->is_started = false;
      DPRINT ("GC: stopped\n");
   }

   adc_close (self->fd);
   self->is_opened = false;
   DPRINT ("GC: closed\n");

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.start(channel) OR channel:start() */
static int lua_adc_start (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_check_number_of_arguments (L, 1);
   lua_adc_channel_t * self = luaL_checkudata (L, 1, LUA_ADC_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started == false, 1, "channel already started");
   ASSERT (self->fd >= 0);

   adc_start (self->fd);
   self->is_started = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.stop(channel) OR channel:stop() */
static int lua_adc_stop (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_check_number_of_arguments (L, 1);
   lua_adc_channel_t * self = luaL_checkudata (L, 1, LUA_ADC_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   adc_stop (self->fd);
   self->is_started = false;

   /* Push no results on stack */
   return 0;
}

/* Lua: sample = channel.get_sample(channel) OR sample = channel:get_sample() */
static int lua_adc_get_sample (lua_State * L)
{
   /* Get one argument from stack */
   lua_adc_check_number_of_arguments (L, 1);
   lua_adc_channel_t * self = luaL_checkudata (L, 1, LUA_ADC_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   uint32_t sample = adc_sample_get (self->fd);

   /* Push one result on stack */
   lua_pushinteger (L, sample);
   return 1;
}

static void lua_adc_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
         {"close", lua_adc_close},
         {"start", lua_adc_start},
         {"stop", lua_adc_stop},
         {"get_sample", lua_adc_get_sample},
         {"__gc", lua_adc_collect_garbage},
         {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for ADC channel class in the global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_ADC_CHANNEL)
    *      metatable.__index = metatable
    *      metatable.write = ..
    *      metatable.read = .. (and so on)
    */
   luaL_newmetatable (L, LUA_ADC_CHANNEL); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);                  /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");        /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);          /* Add methods to metatable */
   lua_pop (L, 1);                         /* Pop metatable reference from stack */
}

int lua_adc_openlib (lua_State * L)
{
   lua_adc_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
         {"open", lua_adc_open},
         {NULL, NULL}  /* Sentinel */
   };
   luaL_newlib (L, functions);
   return 1;
}
