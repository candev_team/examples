/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id$
*------------------------------------------------------------------------------
*/

#include "lua_gpio.h"
#include <bsp.h>
#include <lauxlib.h>
#include <string.h>

#define LUA_GPIO_PIN    LUA_GPIO_LIBNAME".pin"

typedef struct lua_gpio_pin
{
   const char * name;
   uint32_t id;
} lua_gpio_pin_t;

/* GPIO-pins available. BSP-specific. */
static const lua_gpio_pin_t lua_gpio_pins[] =
{
      {"DEBUG_LED", GPIO_DEBUG_LED},
      {"U_LEVEL0", GPIO_U_LEVEL0},
      {"U_LEVEL1", GPIO_U_LEVEL1},
};

static bool lua_gpio_is_valid (const lua_gpio_pin_t * pin)
{
   unsigned i;

   if (pin == NULL)
   {
      return false;
   }
   for (i = 0; i < NELEMENTS(lua_gpio_pins); i++)
   {
      if (lua_gpio_pins[i].id == pin->id)
      {
         return true;
      }
   }
   return false;
}

static const lua_gpio_pin_t * lua_gpio_find_pin (const char * pin_name)
{
   unsigned i;

   for (i = 0; i < NELEMENTS(lua_gpio_pins); i++)
   {
      if (strcmp (pin_name, lua_gpio_pins[i].name) == 0)
      {
         return &lua_gpio_pins[i];
      }
   }

   return NULL;
}

static void lua_gpio_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Lua: pin = gpio.open('pin_name') */
static int lua_gpio_create (lua_State * L)
{
   /* Get one argument from stack */
   lua_gpio_check_number_of_arguments (L, 1);
   luaL_checktype (L, 1, LUA_TSTRING);
   const char * pin_name = luaL_checkstring (L, 1);

   /* Push new class instance at top of stack */
   lua_gpio_pin_t * self = lua_newuserdata (L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable (L, LUA_GPIO_PIN); /* Set class for instance */

   const lua_gpio_pin_t * found_pin = lua_gpio_find_pin (pin_name);
   if (found_pin == NULL)
   {
      return luaL_argerror (L, 1, "invalid pin name");
   }
   *self = *found_pin;

   /* One result is already on stack */
   return 1;
}

/* Lua: level = pin.get(pin) OR
 *      level = pin:get()
 */
static int lua_gpio_get (lua_State * L)
{
   /* Get one argument from stack */
   lua_gpio_check_number_of_arguments (L, 1);
   lua_gpio_pin_t * self = luaL_checkudata (L, 1, LUA_GPIO_PIN);
   ASSERT (lua_gpio_is_valid (self));

   int level = gpio_get (self->id);

   /* Push one result on stack */
   lua_pushinteger (L, level);
   return 1;
}

/* Lua: pin.set(pin, level) OR
 *      pin:set(level)
 */
static int lua_gpio_set (lua_State * L)
{
   /* Get two arguments from stack */
   lua_gpio_check_number_of_arguments (L, 2);
   lua_gpio_pin_t * self = luaL_checkudata (L, 1, LUA_GPIO_PIN);
   int level = luaL_checkinteger (L, 2);
   ASSERT (lua_gpio_is_valid (self));
   luaL_argcheck (L, level == 0 || level == 1, 2, "invalid level");

   gpio_set (self->id, level);

   /* Push no results on stack */
   return 0;
}

/* Lua: array = gpio.get_pin_names() */
static int lua_gpio_get_pin_names (lua_State * L)
{
   /* Get no arguments from stack */

   /* Push one result on stack */
   unsigned i;
   lua_newtable (L);
   for (i = 0; i < NELEMENTS(lua_gpio_pins); i++)
   {
      /* Lua: array[i] = pin_name */
      lua_pushinteger (L, i + 1);
      lua_pushstring (L, lua_gpio_pins[i].name);
      lua_settable (L, -3);
   }
   return 1;
}

static void lua_gpio_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
         {"get", lua_gpio_get},
         {"set", lua_gpio_set},
         {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for GPIO pin class in the global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_GPIO_PIN)
    *      metatable.__index = metatable
    *      metatable.get = ..
    *      metatable.set = ..
    */
   luaL_newmetatable (L, LUA_GPIO_PIN); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);               /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");     /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);       /* Add methods to metatable */
   lua_pop (L, 1);                      /* Pop metatable reference from stack */
}

int lua_gpio_openlib (lua_State * L)
{
   lua_gpio_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
       {"create", lua_gpio_create},
       {"get_pin_names", lua_gpio_get_pin_names},
       {NULL, NULL}
   };
   luaL_newlib (L, functions);
   return 1;
}
