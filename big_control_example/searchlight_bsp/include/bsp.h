/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id$
*------------------------------------------------------------------------------
*/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PC4
#define GPIO_UART1_RX   GPIO_PC5

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_SERVO_PWM  GPIO_PC3

#define GPIO_ADIN1      GPIO_PA0
#define GPIO_ADIN2      GPIO_PA1

#define GPIO_U_LEVEL0   GPIO_PC13
#define GPIO_U_LEVEL1   GPIO_PC14

#define GPIO_FREQ_1     GPIO_PA6

#define GPIO_DEBUG_LED  GPIO_PA8

#define GPIO_SERVO_PWM_EXTRA  GPIO_PC0


void bsp_reset_peripherals (void);

#endif /* BSP_H */
