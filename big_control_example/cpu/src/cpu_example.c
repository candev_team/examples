/*
 * cpu_example.c
 *
 *  Created on: 26 may 2016
 *      Author: rtlbjca
 */

#include "cpu_example.h"

#include <config.h>
#include <adc/adc.h>
#include <pwm/pwm.h>
#include <can/bxcan.h>
#include <bsp.h>

#include <string.h>


#define CAN_MSG_RECEIVED         BIT(0)

typedef struct can_struct
{
   int can_fd;
   flags_t * can_flag;
   process_data_t * process_data;
} can_struct_t;


void can_rx_callback (void * arg, can_event_t event )
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   flags_set(can_struct->can_flag, CAN_MSG_RECEIVED);
}

void can_rx_task (void * arg)
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   can_frame_t rx_frame;
   flags_value_t flags;
//   int32_t tmp_data;

   while (1)
   {
      flags_wait_any(can_struct->can_flag, CAN_MSG_RECEIVED, &flags);
      flags_clr(can_struct->can_flag, flags);
      can_receive(can_struct->can_fd, &rx_frame);

//      if (rx_frame.id == 0x500)
//      {
//         memcpy(&tmp_data, rx_frame.data, sizeof(tmp_data));
//         mtx_lock(can_struct->process_data->pd_lock);
//         can_struct->process_data->pwm_out_pul = tmp_data;
//         mtx_unlock(can_struct->process_data->pd_lock);
//      }
   }
}

void can_tx_task (void * arg)
{
   can_struct_t * can_struct = (can_struct_t *) arg;
//   can_frame_t tx_frame;
   process_data_t tmp_values;
//   uint16_t tmp_adc;

   while (1)
   {
      mtx_lock(can_struct->process_data->pd_lock);
      memcpy(&tmp_values, can_struct->process_data, sizeof(tmp_values));
      mtx_unlock(can_struct->process_data->pd_lock);

//      tx_frame.id = 0x600;
//      tx_frame.dlc = 2;
//      memset(&tx_frame.data[0], 0, sizeof(tx_frame.data));
//      tmp_adc = (uint16_t) can_struct->process_data->adc_value;
//      memcpy(&tx_frame.data[0], &tmp_adc, 2);
//      can_transmit(can_struct->can_fd, &tx_frame);

      task_delay(CFG_TICKS_PER_SECOND/CAN_TASK_TX_RATE);
   }
}

void sample_adc(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int adc;
   uint32_t tmp_value;

   mtx_lock(process_data->pd_lock);
   process_data->adc_value = 0;
   mtx_unlock(process_data->pd_lock);

   adc = adc_open ("/adc1/1");
   ASSERT(adc > 0);

   adc_start(adc);

   while(1)
   {
      tmp_value = adc_sample_get(adc);

      mtx_lock(process_data->pd_lock);
      process_data->adc_value = tmp_value;
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/ADC_SAMPLE_RATE);
   }
}

void write_pwm_out(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int pwm_out;
   int duty_cycle;

   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_per = 0;
   process_data->pwm_out_pul = 0;
   mtx_unlock(process_data->pd_lock);

   pwm_out = pwm_open ("/pwm0/0");
   ASSERT(pwm_out > 0);

   pwm_duty_cycle_set(pwm_out, 0);
   pwm_start(pwm_out);

   while(1)
   {
      mtx_lock(process_data->pd_lock);
      duty_cycle = process_data->pwm_out_pul;
      if (duty_cycle < 0)
      {
         duty_cycle = 0;
      }
      else if (duty_cycle > 1200)
      {
         duty_cycle = 1200;
      }
      pwm_duty_cycle_set(pwm_out, duty_cycle);
      mtx_unlock(process_data->pd_lock);

      task_delay(CFG_TICKS_PER_SECOND/PWM_OUT_WRITE_RATE);
   }
}

void dump_process_data(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   process_data_t tmp_values;
   int i = 0;
   while(1)
   {
      mtx_lock(process_data->pd_lock);
      memcpy(&tmp_values, process_data, sizeof(tmp_values));
      mtx_unlock(process_data->pd_lock);

      i++;
      rprintf("adc %08d\n\n", tmp_values.adc_value);
      task_delay(CFG_TICKS_PER_SECOND/DUMP_PROCESS_DATA_RATE);
   }
}

void init_can(process_data_t * process_data)
{
   const uint8_t fifo_mode = 1;
   can_cfg_t can_cfg[] = {
         {125000,  13, 2 ,1, 0},
         {250000,   6, 1 ,1, 0},
         {500000,   6, 1 ,1, 0},
         {1000000,  7, 1 ,1, 0},
      };
   can_filter_t filter = {
        .id   = 0x0,
        .mask = 0x0,
      };
   can_struct_t * can_struct;

   can_struct = malloc(sizeof(can_struct_t));
   UASSERT (can_struct != NULL, EMEM);

   can_struct->can_flag = flags_create(0);
   can_struct->can_fd = open("/can0", O_RDWR, 0);
   UASSERT(can_struct->can_fd != -1, EARG);

   can_struct->process_data = process_data;

   can_set_callback (can_struct->can_fd, can_rx_callback, CAN_EVENT_MSG_RECEIVED, can_struct);
   can_filter (can_struct->can_fd, &filter);
   ioctl (can_struct->can_fd, IOCTL_BXCAN_TXFP, &fifo_mode);

   can_bus_off(can_struct->can_fd);
   can_set_cfg(can_struct->can_fd, &can_cfg[CAN_BIT_RATE_125K]);
   can_bus_on(can_struct->can_fd);

   task_spawn ("can_rx_task", can_rx_task, CAN_TASK_PRIORITY, CAN_TASK_STACK_SIZE, can_struct);
   task_spawn ("can_tx_task", can_tx_task, CAN_TASK_PRIORITY, CAN_TASK_STACK_SIZE, can_struct);
}
