/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2008. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: io_nod_ext.h 2336 2016-03-24 07:50:39Z rtlfrm $
*------------------------------------------------------------------------------
*/

#ifndef IO_NOD_EXT_H
#define IO_NOD_EXT_H

#include "config.h"

#include <kern.h>
#include <bsp.h>

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef __cplusplus
}
#endif

#endif /* IO_NOD_EXT_H */
