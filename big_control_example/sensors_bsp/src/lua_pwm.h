/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_pwm.h 2346 2016-03-24 13:04:37Z rtlfrm $
*------------------------------------------------------------------------------
*/

#ifndef LUA_PWM_H
#define LUA_PWM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#define LUA_PWM_LIBNAME "pwm"

/**
 * Create Lua library which extents Lua with a PWM channel class.
 *
 * Called by interpreter using luaL_requiref().
 *
 * @param L    Lua interpreter.
 * @return     1, always.
 */
int lua_pwm_openlib (lua_State * L);

#ifdef __cplusplus
}
#endif

#endif /* LUA_PWM_H */
