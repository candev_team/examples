/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_i2c.h 2357 2016-03-29 12:53:34Z rtlfrm $
*------------------------------------------------------------------------------
*/

#ifndef LUA_I2C_H
#define LUA_I2C_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#define LUA_I2C_LIBNAME "i2c"

/**
 * Create Lua library which extents Lua with a i2c slave class.
 *
 * Called by interpreter using luaL_requiref().
 *
 * @param L    Lua interpreter.
 * @return     1, always.
 */
int lua_i2c_openlib (lua_State * L);

#ifdef __cplusplus
}
#endif

#endif /* LUA_I2C_H */
