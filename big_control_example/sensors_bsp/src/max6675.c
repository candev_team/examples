/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: max6675.c 2336 2016-03-24 07:50:39Z rtlfrm $
 *------------------------------------------------------------------------------
 */

#include <max6675.h>
#include <drivers/spi/spi.h>

float max6675_temperature_in_celsius (int fd)
{
   uint16_t raw_value;
   float celsius;

   UASSERT (fd > 0, EARG);

   spi_select (fd);
   read (fd, &raw_value, 1);
   spi_unselect (fd);

   celsius = (float)(raw_value >> 5); /* Whole degrees */
   celsius += 0.25 * ((raw_value >> 3) & 0x3); /* Fractional part */
   return celsius;
}
