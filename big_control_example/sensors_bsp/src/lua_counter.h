/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_counter.h 2346 2016-03-24 13:04:37Z rtlfrm $
*------------------------------------------------------------------------------
*/

#ifndef LUA_COUNTER_H
#define LUA_COUNTER_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#define LUA_COUNTER_LIBNAME "counter"

/**
 * Create library extending Lua with a counter channel class.
 *
 * Called by interpreter using luaL_requiref().
 *
 * @param L    Lua interpreter.
 * @return     1, always.
 */
int lua_counter_openlib (lua_State * L);

#ifdef __cplusplus
}
#endif

#endif /* LUA_COUNTER_H */
