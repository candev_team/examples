/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: lua_can.c 2346 2016-03-24 13:04:37Z rtlfrm $
*------------------------------------------------------------------------------
*/

/**
 * Lua module for accessing CAN bus.
 *
 * The module represents the CAN bus as instance of a class.
 * We do it this way instead of the simpler approach where the CAN bus is
 * represented by a file descriptor acted on by functions because:
 * - it allows us to close the file descriptor even in case where exception
 *   is thrown (using destructors).
 * - it allows us to keep state the bus, enabling stronger type checking.
 */

#include "lua_can.h"
#include <lauxlib.h>
#include <can/can.h>
#include <fcntl.h>
#include <string.h>

#undef RTK_DEBUG

#ifdef RTK_DEBUG
#define DPRINT(...) rprintp ("lua_can: "__VA_ARGS__)
#else
#define DPRINT(...)
#endif  /* RTK_DEBUG */

#define LUA_CAN_BUS LUA_CAN_LIBNAME".bus"

/* CAN bus type. Exposed to Lua as a "userdata" type. */
typedef struct lua_can_bus
{
   int fd;
   bool is_opened;
   bool is_configured;
   bool is_filtered;
   bool is_on;
} lua_can_bus_t;

/* table.key = integer */
void lua_can_setintegerfield (lua_State * L,
      int table, const char * key, int value)
{
   lua_pushinteger (L, value);   /* Push at top of stack */
   lua_setfield (L, table, key); /* Pop from top of stack */
}

/* table.key = boolean */
void lua_can_setbooleanfield (lua_State * L,
      int table, const char * key, bool value)
{
   lua_pushboolean (L, value);   /* Push at top of stack */
   lua_setfield (L, table, key); /* Pop from top of stack */
}

/* Checks if table.key is an integer and returns it if so.
 * Similar to luaL_check*(), it throws an exception if check fails.
 * It is assumed that Lua has called C with table as argument.
 */
static uint32_t lua_can_checkintegerfield (lua_State * L,
      int table, const char *key)
{
   lua_getfield (L, table, key); /* Push table.key at top of stack */
   luaL_argcheck (L, lua_isinteger (L, -1), table, "missing field");
   uint32_t value = lua_tointeger (L, -1);
   lua_pop (L, 1);               /* Pop table.key from top of stack */
   return value;
}

/* Checks if table is a configuration table and returns it.
 * Similar to luaL_check*(), it throws an exception if check fails.
 * It is assumed that Lua has called C with table as argument.
 */
static can_cfg_t lua_can_checkcfg (lua_State * L, int table)
{
   can_cfg_t cfg;
   luaL_checktype (L, table, LUA_TTABLE);
   memset (&cfg, 0, sizeof(cfg));
   cfg.baudrate = lua_can_checkintegerfield (L, table, "baudrate");
   cfg.bs1 = lua_can_checkintegerfield (L, table, "bs1");
   cfg.bs2 = lua_can_checkintegerfield (L, table, "bs2");
   cfg.sjw = lua_can_checkintegerfield (L, table, "sjw");
   cfg.silent = lua_can_checkintegerfield (L, table, "silent");
   luaL_argcheck (L, cfg.baudrate > 0 && cfg.baudrate <= 1000*1000, table,
         "invalid baudrate");
   luaL_argcheck (L, cfg.bs1 > 0 && cfg.bs1 <= 16, table, "invalid bs1");
   luaL_argcheck (L, cfg.bs2 > 0 && cfg.bs2 <= 8, table, "invalid bs2");
   luaL_argcheck (L, cfg.sjw > 0 && cfg.sjw <= 4, table, "invalid sjw");
   luaL_argcheck (L, cfg.silent <= 1, table, "invalid silent");
   return cfg;
}

/* Checks if table is a filter table and returns it.
 * Similar to luaL_check*(), it throws an exception if check fails.
 * It is assumed that Lua has called C with table as argument.
 */
static can_filter_t lua_can_checkfilter (lua_State * L, int table)
{
   can_filter_t filter;
   luaL_checktype (L, table, LUA_TTABLE);
   memset (&filter, 0, sizeof(filter));
   filter.id = lua_can_checkintegerfield (L, table, "id");
   filter.mask = lua_can_checkintegerfield (L, table, "mask");
   return filter;
}

static void lua_can_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Lua: self = can.open('/can_bus') */
static int lua_can_open (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_check_number_of_arguments (L, 1);
   const char * bus_name = luaL_checkstring (L, 1);

   /* Push new class instance at top of stack */
   lua_can_bus_t * self = lua_newuserdata(L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable(L, LUA_CAN_BUS); /* Set class for instance */

   self->is_on = false;
   self->is_configured = false;
   self->is_filtered = false;
   self->fd = open (bus_name, O_RDWR, 0);
   if (self->fd < 0)
   {
      return luaL_error (L, "could not open bus '%s'", bus_name);
   }
   self->is_opened = true;
   DPRINT ("Opened\n");

   /* One result is already on stack */
   return 1;
}

/* Lua: self.close(self) OR
 *      self:close()
 */
static int lua_can_close (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_check_number_of_arguments (L, 1);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is already closed");
   luaL_argcheck (L, self->is_on == false, 1, "bus is on");
   ASSERT (self->fd >= 0);

   int error = close (self->fd);
   if (error)
   {
      return luaL_error (L, "error closing bus");
   }
   self->is_opened = false;
   DPRINT ("Closed\n");

   /* Push no results on stack */
   return 0;
}

/* Destructor. Called by garbage collector. */
static int lua_can_collect_garbage (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);

   if (self->is_opened == false)
   {
      /* Already closed, so nothing to clean up */
      DPRINT ("GC: nothing to do\n");
      return 0; /* Push no results on stack */
   }

   if (self->is_on)
   {
      DPRINT ("GC: going off bus\n");
      can_bus_off (self->fd);
      self->is_on = false;
   }

   int error = close (self->fd);
   if (error)
   {
      return luaL_error (L, "error closing bus");
   }
   self->is_opened = false;
   DPRINT ("GC: closed\n");

   /* Push no results on stack */
   return 0;
}

/* Lua: self.on(self) OR
 *      self:on()
 */
static int lua_can_on (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_check_number_of_arguments (L, 1);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   luaL_argcheck (L, self->is_configured, 1, "bus is not configured");
   ASSERT (self->fd >= 0);

   can_bus_on (self->fd);
   self->is_on = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: self.off(self) OR
 *      self:off()
 */
static int lua_can_off (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_check_number_of_arguments (L, 1);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   ASSERT (self->fd >= 0);

   can_bus_off (self->fd);
   self->is_on = false;

   /* Push no results on stack */
   return 0;
}

/* Lua: id, data = self.receive(self) OR
 *      id, data = self:receive()
 */
static int lua_can_receive (lua_State * L)
{
   /* Get one argument from stack */
   lua_can_check_number_of_arguments (L, 1);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   luaL_argcheck (L, self->is_configured, 1, "bus is not configured");
   luaL_argcheck (L, self->is_filtered, 1, "no receive filter installed");
   luaL_argcheck (L, self->is_on, 1, "bus is off");
   ASSERT (self->fd >= 0);

   can_frame_t frame;
   int nothing_received;
   nothing_received = can_receive (self->fd, &frame);

   if (nothing_received)
   {
      /* Push no result on stack */
      return 0;
   }
   else
   {
      /* Push two results on stack */
      lua_pushinteger (L, frame.id);
      lua_pushlstring (L, (char *)frame.data, frame.dlc);
      return 2;
   }
}

/* Lua: self.transmit(self, id, data) OR
 *      self:transmit(id, data)
 */
static int lua_can_transmit (lua_State * L)
{
   /* Get three arguments from stack */
   can_frame_t frame;
   size_t size;
   int id;
   int error;
   lua_can_check_number_of_arguments (L, 3);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   id = luaL_checkinteger (L, 2);
   luaL_argcheck (L, lua_type (L, 3) != LUA_TNUMBER, 3, "string expected"); /* Disallow implicit conversion */
   const char * data = luaL_checklstring (L, 3, &size);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   luaL_argcheck (L, self->is_configured, 1, "bus is not configured");
   luaL_argcheck (L, self->is_on, 1, "bus is off");
   luaL_argcheck (L, size <= sizeof(frame.data), 3, "too much data");
   ASSERT (self->fd >= 0);

   memset (&frame, 0, sizeof(frame));
   frame.id = id;
   frame.dlc = size;
   memcpy (frame.data, data, size);
   error = can_transmit (self->fd, &frame);
   if (error)
   {
      return luaL_error (L, "Failed to transmit");
   }

   /* Push no results on stack */
   return 0;
}

/* Lua: self.set_cfg(self, cfg) OR
 *      self:set_cfg(cfg)
 */
static int lua_can_set_cfg (lua_State * L)
{
   /* Get two arguments from stack */
   lua_can_check_number_of_arguments (L, 2);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   can_cfg_t cfg = lua_can_checkcfg (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   luaL_argcheck (L, self->is_on == false, 1, "bus is on");
   ASSERT (self->fd >= 0);

   can_set_cfg (self->fd, &cfg);
   self->is_configured = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: self.set_filter(self, filter) OR
 *      self:set_filter(filter)
 */
static int lua_can_set_filter (lua_State * L)
{
   /* Get two arguments from stack */
   lua_can_check_number_of_arguments (L, 2);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   can_filter_t filter = lua_can_checkfilter (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   luaL_argcheck (L, self->is_on == false, 1, "bus is on");
   ASSERT (self->fd >= 0);

   int error = can_filter (self->fd, &filter);
   if (error)
   {
      return luaL_error (L, "Out of filters");
   }
   self->is_filtered = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: status = self.get_status(self) OR
 *      status = self:get_status()
 */
static int lua_can_get_status (lua_State * L)
{
   /* Get one argument from stack */
   can_status_t status;
   lua_can_check_number_of_arguments (L, 1);
   lua_can_bus_t * self = luaL_checkudata (L, 1, LUA_CAN_BUS);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "bus is closed");
   ASSERT (self->fd >= 0);

   can_get_status (self->fd, &status);

   /* Push one result on stack */
   lua_newtable (L);
   int table = lua_gettop (L);
   uint16_t flags = status.flags;
   lua_can_setintegerfield (L, table, "tx_error", status.tx_error);
   lua_can_setintegerfield (L, table, "rx_error", status.rx_error);
   lua_can_setintegerfield (L, table, "overrun", status.overrun);
   lua_can_setbooleanfield (L, table, "error_passive", flags & CAN_STATUS_ERROR_PASSIVE);
   lua_can_setbooleanfield (L, table, "bus_off", flags & CAN_STATUS_BUS_OFF);
   lua_can_setbooleanfield (L, table, "error_warning", flags & CAN_STATUS_ERROR_WARNING);
   lua_can_setbooleanfield (L, table, "tx_pending", flags & CAN_STATUS_TX_PENDING);
   lua_can_setbooleanfield (L, table, "rx_pending", flags & CAN_STATUS_RX_PENDING);
   lua_can_setbooleanfield (L, table, "tx_err", flags & CAN_STATUS_TX_ERR);
   lua_can_setbooleanfield (L, table, "rx_err", flags & CAN_STATUS_RX_ERR);
   lua_can_setbooleanfield (L, table, "buffer_overflow", flags & CAN_STATUS_BUFFER_OVERFLOW);
   return 1;
}

static void lua_can_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
        {"close", lua_can_close},
        {"on", lua_can_on},
        {"off", lua_can_off},
        {"set_cfg", lua_can_set_cfg},
        {"set_filter", lua_can_set_filter},
        {"get_status", lua_can_get_status},
        {"receive", lua_can_receive},
        {"transmit", lua_can_transmit},
        {"__gc", lua_can_collect_garbage},
        {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for CAN bus class in the global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_CAN_BUS)
    *      metatable.__index = metatable
    *      metatable.transmit = ..
    *      metatable.receive = .. (and so on)
    */
   luaL_newmetatable (L, LUA_CAN_BUS); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);              /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");    /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);      /* Add methods to metatable */
   lua_pop (L, 1);                     /* Pop metatable reference from stack */
}

int lua_can_openlib (lua_State * L)
{
   lua_can_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
       {"open", lua_can_open},
       {NULL, NULL}  /* Sentinel */
   };
   luaL_newlib (L, functions);
   return 1;
}
