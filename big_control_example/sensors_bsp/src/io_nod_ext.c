/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: io_nod_ext.c 2534 2016-05-11 09:21:17Z rtlmajo $
 *------------------------------------------------------------------------------
 */

#include "io_nod_ext.h"

#include <fd.h>
#include <gpio.h>
#include <sio/sio.h>
#include <sio/stm32f3usart.h>
#include <can/bxcan.h>
#include <pwm/stm32_pwm.h>
#include <spi/stm32_spi.h>
#include <nor/stm32f3_efmi.h>
#include <counter/stm32_counter_dual_edge.h>
#include <adc/stm32_adc.h>
#include <nor/spi_nor.h>
#include <fs/wiffs.h>
#include <i2c/stm32f3_i2c.h>
#include <shell.h>
#include <lualib.h>
#include <lauxlib.h>
#include "lua_gpio.h"
#include "lua_adc.h"
#include "lua_spi.h"
#include "lua_pwm.h"
#include "lua_counter.h"
#include "lua_can.h"
#include "lua_i2c.h"

#define TICKS_PER_SAMPLING       10                      /* CFG_TICKS_PER_SECOND = 1000 => 1kHz / 10 = 100Hz */

#ifdef CFG_NOR_INIT
static uint8_t spi_nor_buffer[SPI_NOR_BUFFER_SIZE];
#endif

static const gpio_cfg_t gpio_cfg[] =
{
   /* SERVO POWER */
   { GPIO_U_LEVEL0, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* U_LEVEL0 */
   { GPIO_U_LEVEL1, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* U_LEVEL1 */

   /* PWM */
   { GPIO_PWM1, GPIO_ALTERNATE_2, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* PWM1 */
   { GPIO_PWM2, GPIO_ALTERNATE_2, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* PWM2 */
   { GPIO_PWM3, GPIO_ALTERNATE_2, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* PWM3 */
   { GPIO_PWM4, GPIO_ALTERNATE_2, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* PWM4 */

   /* ADC1 */
   { GPIO_ADC1_IN1, GPIO_ANALOG, GPIO_OPENDRAIN, GPIO_SPEED_LOW, GPIO_NOPULL },     /* ADIN 1 */
   { GPIO_ADC1_IN2, GPIO_ANALOG, GPIO_OPENDRAIN, GPIO_SPEED_LOW, GPIO_NOPULL },     /* ADIN 2 */
   { GPIO_ADC1_IN3, GPIO_ANALOG, GPIO_OPENDRAIN, GPIO_SPEED_LOW, GPIO_NOPULL },     /* ADIN 3 */
   { GPIO_ADC1_IN4, GPIO_ANALOG, GPIO_OPENDRAIN, GPIO_SPEED_LOW, GPIO_NOPULL },     /* ADIN 4 */

   /* ADC2 */
   { GPIO_ADC2_IN1, GPIO_ANALOG, GPIO_OPENDRAIN, GPIO_SPEED_LOW, GPIO_NOPULL },     /* Erroneously called ADIN5 in schematics */

   /* PWM_IN */
   { GPIO_PWM_IN, GPIO_ALTERNATE_1, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* PWM_IN */
   { GPIO_FREQ_0, GPIO_ALTERNATE_1, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* FREQ_0 */
   { GPIO_FREQ_1, GPIO_ALTERNATE_1, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* FREQ_1 */

   /* USART1 */
   { GPIO_UART1_TX, GPIO_ALTERNATE_7, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* TX */
   { GPIO_UART1_RX, GPIO_ALTERNATE_7, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* RX */

   /* SPI2 */
   { GPIO_SPI2_MOSI, GPIO_ALTERNATE_5, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },      /* SPI2_MISO */
   { GPIO_SPI2_MISO, GPIO_ALTERNATE_5, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },      /* SPI2_MOSI */
   { GPIO_SPI2_SCK, GPIO_ALTERNATE_5, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },       /* SPI2_SCK */
   { GPIO_SPI2_NSS, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },            /* SPI2_NSS */

   /* FULE VALVE */
   { GPIO_FULE_VALVE_1, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* FULE_VALVE_0 */
   { GPIO_FULE_VALVE_2, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_NOPULL },     /* FULE_VALVE_1 */

   /* DEBUG LED */
   { GPIO_DEBUG_LED, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_LOW, GPIO_PULLUP },    /* DEBUG LED */

   /* SPI3 */
   { GPIO_SPI3_MOSI, GPIO_ALTERNATE_6, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },      /* SPI3_MISO */
   { GPIO_SPI3_MISO, GPIO_ALTERNATE_6, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },      /* SPI3_MOSI */
   { GPIO_SPI3_SCK, GPIO_ALTERNATE_6, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },       /* SPI3_SCK */
   { GPIO_SPI3_CS, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },             /* SPI3_CS */
   { GPIO_SPI3_K_CS0, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },          /* K_CS0 */
   { GPIO_SPI3_K_CS1, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },          /* K_CS1 */
   { GPIO_SPI3_K_CS2, GPIO_OUTPUT, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },          /* K_CS2 */

   /* I2C */
   { GPIO_I2C_SDA, GPIO_ALTERNATE_4, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },        /* I2C_SDA */
   { GPIO_I2C_SCL, GPIO_ALTERNATE_4, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_PULLUP },        /* I2C_SCL */
   { GPIO_I2C_SMBA, GPIO_ALTERNATE_4, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_PULLUP },       /* I2C_SMBA */

#ifdef CFG_CAN_INIT
   /* CAN1 */
   { GPIO_CAN_TX, GPIO_ALTERNATE_9, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* TX */
   { GPIO_CAN_RX, GPIO_ALTERNATE_9, GPIO_PUSHPULL, GPIO_SPEED_HIGH, GPIO_NOPULL },     /* RX */
#endif
};

/* RCC settings for an external crystal of 12 MHz.
 *
 *  HSE     = 12 MHz
 *  HPRE    = 1
 *  PLL_MUL = 6
 *  PLLCLK  = (HSE / HPRE) * PLL_MULL  => 72 MHz
 *  SYSCLK  = PLLCLK                   => 72 MHz
 *  HCLK    = SYSCLK / 1               => 72 MHz
 *  PCLK1   = HCLK / 2                 => 36 MHz
 *  PCLK2   = HCLK / 1                 => 72 MHz
 */
static const rcc_cfg_t rcc_cfg =
{
   .pll_mul     = 4,       /* PLL multiplier */
   .wait_states = 2        /* FLASH wait-states */
};

const os_cfg_t os_cfg =
{
   .stack_err_limit   = CFG_STACK_ERR_LIMIT,
   .ticks_per_second  = CFG_TICKS_PER_SECOND,
   .main_stack_size   = CFG_MAIN_STACK_SIZE,
   .main_priority     = CFG_MAIN_PRIORITY,
   .idle_stack_size   = CFG_IDLE_STACK_SIZE,
   .reaper_stack_size = CFG_REAPER_STACK_SIZE,
   .sig_pool_size     = CFG_SIG_POOL_SIZE,
   .sig_pool_blocks   = CFG_SIG_POOL_BLOCKS,
};

#ifdef CFG_SHELL_INIT
SHELL_CMD (cmd_help);
SHELL_CMD (cmd_history);
SHELL_CMD (cmd_peek);
SHELL_CMD (cmd_poke);
SHELL_CMD (cmd_task_show);
SHELL_CMD (cmd_dev_show);
SHELL_CMD (cmd_fd_show);
SHELL_CMD (cmd_ls);
SHELL_CMD (cmd_mv);
SHELL_CMD (cmd_cat);
SHELL_CMD (cmd_mkdir);
SHELL_CMD (cmd_rmdir);
SHELL_CMD (cmd_rm);
SHELL_CMD (cmd_yrecv);
SHELL_CMD (cmd_lua);
#endif

static void peripheral_init (void)
{
   RCC_AHBENR     = AHB_DMA1 | AHB_DMA2 | AHB_IOPA | AHB_IOPB | AHB_IOPC | AHB_IOPD | AHB_IOPE | AHB_IOPF | AHB_IOPG | AHB_ADC12;
   RCC_APB1ENR    = APB1_TIM2 | APB1_SPI2 | APB1_SPI3 | APB1_CAN | APB1_I2C1;
   RCC_APB2ENR    = APB2_TIM1 | APB2_USART1 | APB2_TIM16 | APB2_TIM17 | APB2_SYSCFG;
   RCC_CFGR2      = CFGR2_ADC12PRES(0x10);
   SYSCFG_CFGR1   |= CFGR1_TIM16_DMA_RMP | CFGR1_TIM17_DMA_RMP | CFGR1_ADC2_DMA_RMP;
}

void bsp_early_init (void)
{
   extern vector_table_t vectors;

   rcc_init (&rcc_cfg);
   peripheral_init ();
   gpio_configure (gpio_cfg, NELEMENTS (gpio_cfg));

   /* Initialise NVIC */
   nvic_init ();
   nvic_vector_table_set ((uint32_t)&vectors);

   /* Initialise heap allocator */
#if defined(CFG_HEAP_TYPE_STATIC)
   heap_static_init();
#elif defined(CFG_HEAP_TYPE_DYNAMIC)
   heap_dynamic_init();
#endif
}

static void spi_cs_set_flash (const void * arg, int level)
{
   gpio_set (GPIO_SPI2_NSS, level);
}

static void spi_cs_set_temp0 (const void * arg, int level)
{
   gpio_set (GPIO_SPI3_K_CS0, level);
}

static void spi_cs_set_temp1 (const void * arg, int level)
{
   gpio_set (GPIO_SPI3_K_CS1, level);
}

static void spi_cs_set_temp2 (const void * arg, int level)
{
   gpio_set (GPIO_SPI3_K_CS2, level);
}

void bsp_late_init (void)
{
   drv_t * drv;
#ifdef CFG_WIFFS_INIT
   int error;
#endif
   /* Setup TIM1 for PWM1-4 */
   static const stm32_pwm_cfg_t stm32_pwm_cfg =
   {
      .base       = TIM1_BASE,
      .clock      = CFG_PCLK2_FREQUENCY,
      .prescale   = 22,
      .frequency  = 50,
      .duty_cycle = {0,0,0,0},
      .adv_timer  = true,
   };
   /* Setup TIM2 for PWM_IN */
   static const stm32_counter_dual_edge_cfg_t stm32_counter_cfg =
   {
      .base         = TIM2_BASE,
      .clock        = CFG_PCLK2_FREQUENCY,
      .max_width_ns = 80 * 1000 * 1000,       // 80ms
      .tick_time_ns = 1 * 1000 * 1000 * 1000 / CFG_TICKS_PER_SECOND,
      .type         = STM32_COUNTER_ADVANCED_TIMER,
   };
#ifdef CFG_FREQ_INIT
   /* Setup TIM17 for FREQ_0 */
   static const stm32_counter_dual_edge_cfg_t stm32_freq_0_cfg =
   {
      .base         = TIM17_BASE,
      .clock        = CFG_PCLK2_FREQUENCY,
      .max_width_ns = 80 * 1000 * 1000,       // 80ms
      .tick_time_ns = 1 * 1000 * 1000 * 1000 / CFG_TICKS_PER_SECOND,
      .type         = STM32_COUNTER_SIMPLE_TIMER,
      .dma_ch       = DMA1_CHANNEL7,
   };
   /* Setup TIM16 for FREQ_1 */
   static const stm32_counter_dual_edge_cfg_t stm32_freq_1_cfg =
   {
      .base         = TIM16_BASE,
      .clock        = CFG_PCLK2_FREQUENCY,
      .max_width_ns = 80 * 1000 * 1000,       // 80ms
      .tick_time_ns = 1 * 1000 * 1000 * 1000 / CFG_TICKS_PER_SECOND,
      .type         = STM32_COUNTER_SIMPLE_TIMER,
      .dma_ch       = DMA1_CHANNEL6,
   };
#endif
   static const stm32f3usart_cfg_t stm32f3usart_cfg =
   {
      .base    = USART1_BASE,
      .irq     = IRQ_USART1,
      .clock   = CFG_PCLK2_FREQUENCY,
      .tx_size = 32,
      .rx_size = 32,
   };
   static const sio_cfg_t sio_cfg =
   {
      .baudrate = 115200,
      .databits = 8,
      .parity   = None,
      .stopbits = 1,
   };
#ifdef CFG_I2C_INIT
   static const stm32f3_i2c_cfg_t i2c1_cfg =
   {
      .base = I2C1_BASE,
      .irq_event = IRQ_I2C1_EV,
      .module_clock_Hz = CFG_SYSCLK_FREQUENCY,
      .serial_clock_Hz = 400 * 1000,
      .address_bits = 7,
      .rx_dma_ch = DMA1_CHANNEL7, /* CONFLICT with FREQ_0 */
      .tx_dma_ch = DMA1_CHANNEL6, /* CONFLICT with FREQ_1 */
   };
#endif
#ifdef CFG_CAN_INIT
   static const bxcan_cfg_t bxcan_cfg =
   {
      .base = BXCAN1_BASE,
      .irq_tx = IRQ_CAN1_TX,
      .irq_rx0 = IRQ_CAN1_RX0,
      .irq_rx1 = IRQ_CAN1_RX1,
      .irq_sce = IRQ_CAN1_SCE,
      .clock = CFG_PCLK1_FREQUENCY,
   };
#endif
#ifdef CFG_INOR_INIT
   static const stm32f3_efmi_cfg_t efmi_cfg =
   {
         .base = FLASH_BASE,
         .start_address = 0x08020000,
         .start_sector = 64,
         .num_sectors = 192,
         .sector_size = 0x0800,
   };
#endif
   static const stm32_adc_cfg_t adc1_cfg =
   {
         .base = ADC1_BASE,
         .dma_ch = DMA1_CHANNEL1,
         .irq = IRQ_DMA1_CHANNEL1,
         .resolution = RESOLUTION_12BIT,
         .time[0] = SAMPLING_TIME_19_5,   /* CH1, GPIO_ADIN1. */
         .time[1] = SAMPLING_TIME_19_5,   /* CH2, GPIO_ADIN2. */
         .time[2] = SAMPLING_TIME_19_5,   /* CH3, GPIO_ADIN3. */
         .time[3] = SAMPLING_TIME_19_5,   /* CH4, GPIO_ADIN4. */
         //.time[15] = SAMPLING_TIME_19_5,  /* CH16, internal temperature. */
         //.time[16] = SAMPLING_TIME_19_5,  /* CH17, Vbat/2. */
         //.time[17] = SAMPLING_TIME_19_5,  /* CH18, internal reference voltage. */
         .sampling_period_ticks = TICKS_PER_SAMPLING,
   };
   static const stm32_adc_cfg_t adc2_cfg =
   {
         .base = ADC2_BASE,
         .dma_ch = DMA2_CHANNEL3, /* Remapped in SYSCFG_CFGR1 */
         .irq = IRQ_DMA2_CHANNEL3,
         .resolution = RESOLUTION_12BIT,
         .time[0] = SAMPLING_TIME_19_5,   /* CH1. */
         .sampling_period_ticks = TICKS_PER_SAMPLING,
   };
   static const spi_slave_cfg_t spi2_slave_cfg[] =
   {
      {
         .name     = "flash",
         .cs_id    = 0,
         .baudrate = 36000000,  /* 36 MHz */
         .databits = 8,
         .mode     = SPI_MODE_3,
         .cs_set   = spi_cs_set_flash,
      },
   };
   static const stm32_spi_cfg_t stm32_spi2_cfg =
   {
         .variant  = STM32_SPI_DATA_SIZE_VARIABLE,
         .base     = SPI2_BASE,
         .irq      = IRQ_SPI2,
         .clock    = CFG_PCLK1_FREQUENCY,
         .slaves   = spi2_slave_cfg,
         .n_slaves = NELEMENTS (spi2_slave_cfg),
         .rx_dma_ch = DMA1_CHANNEL4,
         .tx_dma_ch = DMA1_CHANNEL5,
   };
#ifdef CFG_NOR_INIT
   static const spi_nor_cfg_t spi_nor_cfg =
   {
      .chip_type              = SPI_CHIP_NUMONYX,
      .page_size              = 256,  /* page size:       256B */
      .pages_per_sub_sector   = 16,   /* subsector size:   4kB */
      .sub_sectors_per_sector = 16,   /* sector size:     64kB */
      .number_of_sectors      = 32,   /* memory size:      2MB */
      .manufacturer_id        = 0x01, /* JEDEC */
      .device_id              = 0x40, /* JEDEC */
      .sub_device_id          = 0x15, /* JEDEC */
      .buffer                 = spi_nor_buffer
   };
#endif
   static const spi_slave_cfg_t spi3_slave_cfg[] =
   {
      {
         .name = "k-temp0",
         .cs_id = GPIO_SPI3_K_CS0,
         .baudrate = 4 * 1000 * 1000,
         .databits = 16,
         .mode     = SPI_MODE_1,
         .cs_set   = spi_cs_set_temp0,
      },
      {
         .name = "k-temp1",
         .cs_id = GPIO_SPI3_K_CS1,
         .baudrate = 4 * 1000 * 1000,
         .databits = 16,
         .mode     = SPI_MODE_1,
         .cs_set   = spi_cs_set_temp1,
      },
      {
         .name = "k-temp2",
         .cs_id = GPIO_SPI3_K_CS2,
         .baudrate = 4 * 1000 * 1000,
         .databits = 16,
         .mode     = SPI_MODE_1,
         .cs_set   = spi_cs_set_temp2,
      },
   };
   static const stm32_spi_cfg_t stm32_spi3_cfg =
   {
      .variant  = STM32_SPI_DATA_SIZE_VARIABLE,
      .base     = SPI3_BASE,
      .irq      = IRQ_SPI3,
      .clock    = CFG_PCLK1_FREQUENCY,
      .slaves   = spi3_slave_cfg,
      .n_slaves = NELEMENTS (spi3_slave_cfg),
      .rx_dma_ch = DMA2_CHANNEL1,
      .tx_dma_ch = DMA2_CHANNEL2,
   };

   uint32_t clk = CFG_SYSCLK_FREQUENCY / 8;
   uint32_t load = clk / CFG_TICKS_PER_SECOND - 1;

   systick_init (load);

   /* Initialise device layer */
   dev_init (CFG_NUM_DEVICES, CFG_NUM_FILES);

   /* Initialise DMA */
   dma_init();

   /* Install PWM drivers */
   drv = stm32_pwm_init ("/pwm0", &stm32_pwm_cfg);
   ASSERT (drv != NULL);

   /* Install PWM_IN drivers */
   drv = stm32_counter_dual_edge_init ("/counter0", &stm32_counter_cfg);
   ASSERT (drv != NULL);
#ifdef CFG_FREQ_INIT
   /* Install FREQ_0 drivers */
   drv = stm32_counter_dual_edge_init ("/freq0", &stm32_freq_0_cfg);
   ASSERT (drv != NULL);
   /* Install FREQ_1 drivers */
   drv = stm32_counter_dual_edge_init ("/freq1", &stm32_freq_1_cfg);
   ASSERT (drv != NULL);
#endif

   /* Install UART driver */
   drv = stm32f3usart_init ("/sio0", &stm32f3usart_cfg);
   ASSERT (drv != NULL);
   sio_set_cfg (drv, &sio_cfg);
   sio_set_console (drv);

   /* Initialize ADC1 driver */
   drv = stm32_adc_init ("/adc1", &adc1_cfg);
   ASSERT (drv != NULL);

   /* Initialize ADC2 driver */
   drv = stm32_adc_init ("/adc2", &adc2_cfg);
   ASSERT (drv != NULL);

   /* Install SPI2 driver */
   spi_cs_set_flash (NULL, 1);
   drv = stm32_spi_init("/spi2", &stm32_spi2_cfg);
   ASSERT (drv != NULL);

#ifdef CFG_NOR_INIT
   /* Install NOR flash driver */
   gpio_set (GPIO_SPI2_NSS, 1);
   drv = spi_nor_init ("/nor0", "/spi2/flash", &spi_nor_cfg);
   ASSERT (drv != NULL);

#ifdef CFG_WIFFS_INIT
   /* Install Spiffs filesystem wrapper */
   drv = wiffs_init ("/disk1", CFG_NUM_FILES);
   ASSERT (drv != NULL);
   error = wiffs_mount ("/disk1", "/nor0", 0, 0);
   if (error)
   {
      error = wiffs_format("/disk1", "/nor0");
      ASSERT(error == 0);
      error = wiffs_mount ("/disk1", "/nor0", 0, 0);
      ASSERT(error == 0);
   }
#endif

#endif

   /* Install SPI3 driver */
   spi_cs_set_temp0 (NULL, 1);
   spi_cs_set_temp1 (NULL, 1);
   spi_cs_set_temp2 (NULL, 1);
   drv = stm32_spi_init("/spi3", &stm32_spi3_cfg);
   ASSERT (drv != NULL);

#ifdef CFG_I2C_INIT
#ifdef CFG_FREQ_INIT
#error "Can't use both i2c and FREQ as they use the same DMA channels"
#endif
   /* Install i2c driver */
   drv = stm32f3_i2c_init ("/i2c1", &i2c1_cfg);
   ASSERT (drv != NULL);
#endif

#ifdef CFG_CAN_INIT
   /* Install CAN driver */
   drv = bxcan_init("/can0", &bxcan_cfg);
   ASSERT (drv != NULL);
#endif

#ifdef CFG_INOR_INIT
   /* Install internal flash driver */
   drv = stm32f3_efmi_drv_init(CFG_INOR_DEV_NAME, &efmi_cfg);
   ASSERT (drv != NULL);
#endif

}

void idle (void * arg)
{
   extern vuint32_t os_idle_counter;

   while (1)
   {
      os_idle_counter++;

#ifdef CFG_IDLE_WFI

      /* Reduce power consumption by sleeping the CPU until an
         interrupt occurs. This may make debugging more difficult */
      asm volatile ("wfi;");

#ifdef CFG_STATS_INIT
#warning CPU load will be incorrectly measured when using WFI instruction
#endif

#endif  /* CFG_IDLE_WFI */
   }
}

void bsp_reset_peripherals (void)
{
   *(volatile uint32_t *)SYSTICK_BASE = ~(BIT(1) | BIT(0));

   RCC_AHBRSTR |= AHB_DMA1 | AHB_DMA2 | AHB_IOPA | AHB_IOPB | AHB_IOPC | AHB_IOPD | AHB_IOPE | AHB_IOPF | AHB_IOPG | AHB_ADC12;
   RCC_APB1RSTR |= APB1_TIM2 | APB1_SPI2 | APB1_SPI3 | APB1_CAN | APB1_I2C1;
   RCC_APB2RSTR |= APB2_TIM1 | APB2_USART1 | APB2_TIM16 | APB2_TIM17 | APB2_SYSCFG;

   RCC_AHBRSTR &= ~(AHB_DMA1 | AHB_DMA2 | AHB_IOPA | AHB_IOPB | AHB_IOPC | AHB_IOPD | AHB_IOPE | AHB_IOPF | AHB_IOPG | AHB_ADC12);
   RCC_APB1RSTR &= ~(APB1_TIM2 | APB1_SPI2 | APB1_SPI3 | APB1_CAN | APB1_I2C1);
   RCC_APB2RSTR &= ~(APB2_TIM1 | APB2_USART1 | APB2_TIM16 | APB2_TIM17 | APB2_SYSCFG);

   SYSCFG_CFGR1   &= ~(CFGR1_TIM16_DMA_RMP | CFGR1_TIM17_DMA_RMP);
}

/* Override default implementation in lua/src/linit.c.
 * luaL_openlibs() will call this one instead of the default one.
 *
 * Note that the function will not run if its name is changed.
 */
void bsp_lua_openlibs (lua_State * L)
{
   static const luaL_Reg libraries[] =
   {
      {LUA_GPIO_LIBNAME, lua_gpio_openlib},
      {LUA_ADC_LIBNAME, lua_adc_openlib},
      {LUA_SPI_LIBNAME, lua_spi_openlib},
      {LUA_PWM_LIBNAME, lua_pwm_openlib},
      {LUA_COUNTER_LIBNAME, lua_counter_openlib},
      {LUA_CAN_LIBNAME, lua_can_openlib},
      {LUA_I2C_LIBNAME, lua_i2c_openlib},
      {NULL, NULL} /* Sentinel */
   };
   const luaL_Reg * lib;

   /* "require" functions from array of libs and set results to global table */
   for (lib = libraries; lib->func; lib++)
   {
      luaL_requiref (L, lib->name, lib->func, 1);
      lua_pop (L, 1);  /* Remove lib on Lua stack */
   }
}

void uerror(err_t err)
{
   vuint32_t i;

   rprintp("UERROR: %d", err);
   task_show();
   fd_show();
   log_show();

   /* Fatal error - flash LED forever */
   for (;;)
   {
      for (i = 0; i < 10000000; i++);
      gpio_set(GPIO_DEBUG_LED, 1);
      for (i = 0; i < 10000000; i++);
      gpio_set(GPIO_DEBUG_LED, 0);
   }
}

/* This function is called from the exception handlers. The function
 * must not return.
 */
void exception_logger (uint32_t sp, uint32_t ipsr)
{
   vuint32_t i;
   uint32_t pc;
   uint32_t * p;
   static const char * const fault[] =
   {
      "HardFault",
      "MemManage",
      "BusFault",
      "UsageFault",
   };

   pc = *(uint32_t *)(sp + 0x18);
   rprintp ("Exception: %s @ pc = 0x%X\n\n", fault[ipsr - EXC_HARD], pc);

   /* Dump stack */
   p = (uint32_t *)sp;
   for (i = 0; i < 8; i++)
   {
      rprintp ("0x%08x: %08x %08x %08x %08x\n", p, p[0], p[1], p[2], p[3]);
      p += 4;
   }
   rprintp ("\n");

   task_show();
   log_show();

   /* Fatal error - flash LED forever */
   for (;;)
   {
      for (i = 0; i < 10000000; i++);
      gpio_set(GPIO_DEBUG_LED, 1);
      for (i = 0; i < 10000000; i++);
      gpio_set(GPIO_DEBUG_LED, 0);
   }
}
