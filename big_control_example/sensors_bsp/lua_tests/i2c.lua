-- Test Lua-i2c integration.

-- A fake i2c driver with loopback slave is assumed to be initialised with
-- this name
slave_name = '/i2c1/42'

invalid_slave_name = '/i2c1/128'

-- Send/receive single byte buffer (no errors)
slave = i2c.open(slave_name)
sent = '1'
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:close()

-- Send/receive buffer with special characters (no errors)
slave = i2c.open(slave_name)
sent = '\x00\a\b\f\n\r\t\v\\\x00\xff'
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:close()

-- Send/receive large buffer (no errors)
slave = i2c.open(slave_name)
sent = string.rep('\x42', 6000)
slave:write(sent)
received = slave:read(sent:len())
assert(received == sent)
slave:close()

-- Test open() error handling
assert(pcall(i2c.open, invalid_slave_name) == false) -- 'could not open slave'
assert(pcall(i2c.open, slave_name, 'extra') == false) -- 'wrong number of arguments'
assert(pcall(i2c.open) == false) -- 'wrong number of arguments'

-- Test close() error handling
slave = i2c.open(slave_name)
assert(pcall(i2c.close, slave, 'extra') == false) -- 'wrong number of arguments'
assert(pcall(i2c.close) == false) -- 'wrong number of arguments'
assert(pcall(i2c.close, slave) == false) -- 'wrong number of arguments'
slave:close()

-- Test read() error handling
slave = i2c.open(slave_name)
assert(pcall(slave.read, slave, 0) == false)  -- 'size needs to be positive'
assert(pcall(slave.read, slave, -1) == false)  -- 'size needs to be positive'
slave:close()

-- Test write() error handling
slave = i2c.open(slave_name)
assert(pcall(slave.write, slave, '') == false)  -- 'size needs to be positive'
assert(pcall(slave.write, slave, 5) == false)  -- 'string expected'
assert(pcall(slave.write, slave, 5.0) == false)  -- 'string expected'
slave:close()

