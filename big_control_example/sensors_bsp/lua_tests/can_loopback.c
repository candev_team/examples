/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2006. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: can_loopback.c 2352 2016-03-29 08:42:15Z rtlfrm $
*------------------------------------------------------------------------------
*/

#include "can_loopback.h"
#include <can/fake_can.h>
#include <string.h>
#include <errno.h>

#define CAN_LOOBBACK_FRAMES   2

static mbox_t * received_frames;
static mbox_t * unused_frames;

static int can_loopback_transmit (const can_frame_t * frame)
{
   int timeout;
   can_frame_t * received;

   ASSERT (frame != NULL);
   ASSERT (unused_frames != NULL);
   ASSERT (received_frames != NULL);
   timeout = mbox_fetch_tmo (unused_frames, (void **)&received, 0);
   if (timeout)
   {
      return -1; /* No frame buffer available */
   }
   ASSERT (received != NULL);
   *received = *frame;
   timeout = mbox_post_tmo (received_frames, received, 0);
   ASSERT (timeout == false);

   return 0;
}

static int can_loopback_receive (can_frame_t * frame)
{
   int timeout;
   can_frame_t * received;

   ASSERT (frame != NULL);
   ASSERT (unused_frames != NULL);
   ASSERT (received_frames != NULL);
   timeout = mbox_fetch_tmo (received_frames, (void **)&received, 0);
   if (timeout)
   {
      return ENODATA; /* No frame buffer available */
   }
   ASSERT (received != NULL);
   *frame = *received;
   timeout = mbox_post_tmo (unused_frames, received, 0);
   ASSERT (timeout == false);

   return 0;
}

static void can_loopback_bus_off (void)
{
   can_frame_t frame;
   int error;

   /* Move all frames to unused queue */
   do
   {
      error = can_loopback_receive (&frame);
   } while (error == false);
}

drv_t * can_loopback_init (const char * name)
{
   static const fake_can_callbacks_t callbacks =
   {
         .transmit = can_loopback_transmit,
         .receive = can_loopback_receive,
         .bus_off = can_loopback_bus_off,
   };
   unsigned i;
   static can_frame_t frames[CAN_LOOBBACK_FRAMES];

   unused_frames = mbox_create (NELEMENTS(frames));
   received_frames = mbox_create (NELEMENTS(frames));
   for (i = 0; i < NELEMENTS(frames); i++)
   {
      mbox_post (unused_frames, &frames[i]);
   }

   return fake_can_init (name, &callbacks);
}
