-- Test Lua-counter integration

-- BSP-specific constant
valid_channel_name = '/counter0/0'

-- Constant used in tests
invalid_channel_name = '/counter0/bad'

-- Test open/start/stop/close (no errors)
channel = counter.open(valid_channel_name)
channel:start('PULSE')
channel:stop()
channel:start('PERIOD')
channel:stop()
channel:close()

-- Test open() error handling.
assert(pcall(counter.open, invalid_channel_name) == false) -- 'could not open'

-- Test close() error handling.
channel = counter.open(valid_channel_name)
channel:start('PERIOD')
assert(pcall(channel.close, channel) == false) -- 'not stopped'
channel:stop()
channel:close()
assert(pcall(channel.close, channel) == false) -- 'already closed'

-- Test start() error handling.
channel = counter.open(valid_channel_name)
assert(pcall(channel.start, channel, 3) == false) -- 'string expected'
assert(pcall(channel.start, channel, 'bad') == false) -- 'invalid mode'
channel:start('PULSE')
assert(pcall(channel.start, channel, 'PULSE') == false) -- 'already started'
channel:stop()
channel:close()
assert(pcall(channel.start, channel, 'PULSE') == false) -- 'not opened'

-- Test stop() error handling.
channel = counter.open(valid_channel_name)
assert(pcall(channel.stop, channel) == false) -- 'not started'
channel:start('PULSE')
channel:stop()
assert(pcall(channel.stop, channel) == false) -- 'not started'
channel:close()
assert(pcall(channel.stop, channel) == false) -- 'not opened'

-- Test read_count/time error handling.
channel = counter.open(valid_channel_name)
assert(pcall(channel.read_count, channel) == false) -- 'not started'
assert(pcall(channel.read_time, channel) == false) -- 'not started'
channel:start('PULSE')
assert(channel:read_count() == nil)
assert(channel:read_time() == nil)
channel:stop()
channel:start('PERIOD')
assert(channel:read_count() == nil)
assert(channel:read_time() == nil)
channel:stop()
channel:close()
assert(pcall(channel.read_count, channel) == false) -- 'not opened'
assert(pcall(channel.read_time, channel) == false) -- 'not opened'
