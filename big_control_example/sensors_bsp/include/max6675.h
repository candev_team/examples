/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: max6675.h 2336 2016-03-24 07:50:39Z rtlfrm $
 *------------------------------------------------------------------------------
 */

/**
 * MAX6675 temperature sensor
 *
 * Example usage:
 * \code
 * #include <max6675.h>
 * int max6675 = open ("/spi3/k-temp0", O_RDONLY, 0);
 * float temperature = max6675_temperature_in_celsius (max6675);
 * \endcode
 */

#ifndef MAX6675_H
#define MAX6675_H

#include <fcntl.h>

/**
 * Read current temperature from sensor.
 *
 * @param fd      File descriptor returned from open().
 * @return        Temperature in Celsius. Range: 0.0� - 1024.0�.
 *                Resolution: 0.25�C.
 */
float max6675_temperature_in_celsius (int fd);

#endif /* MAX6675_H */
