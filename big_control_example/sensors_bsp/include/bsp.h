/******************************************************************************
*                *          ***                    ***
*              ***          ***                    ***
* ***  ****  **********     ***        *****       ***  ****          *****
* *********  **********     ***      *********     ************     *********
* ****         ***          ***              ***   ***       ****   ***
* ***          ***  ******  ***      ***********   ***        ****   *****
* ***          ***  ******  ***    *************   ***        ****      *****
* ***          ****         ****   ***       ***   ***       ****          ***
* ***           *******      ***** **************  *************    *********
* ***             *****        ***   *******   **  **  ******         *****
*                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
*
* http://www.rt-labs.com
* Copyright (C) 2009. rt-labs AB, Sweden. All rights reserved.
*------------------------------------------------------------------------------
* $Id: bsp.h 2336 2016-03-24 07:50:39Z rtlfrm $
*------------------------------------------------------------------------------
*/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_U_LEVEL0   GPIO_PC13
#define GPIO_U_LEVEL1   GPIO_PC14

#define GPIO_PWM1       GPIO_PC0
#define GPIO_PWM2       GPIO_PC1
#define GPIO_PWM3       GPIO_PC2
#define GPIO_PWM4       GPIO_PC3

#define GPIO_ADC1_IN1   GPIO_PA0
#define GPIO_ADC1_IN2   GPIO_PA1
#define GPIO_ADC1_IN3   GPIO_PA2
#define GPIO_ADC1_IN4   GPIO_PA3
#define GPIO_ADC2_IN1   GPIO_PA4

#define GPIO_PWM_IN     GPIO_PA5

#define GPIO_FREQ_0     GPIO_PA7
#define GPIO_FREQ_1     GPIO_PA6

#define GPIO_UART1_TX   GPIO_PC4
#define GPIO_UART1_RX   GPIO_PC5

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_FULE_VALVE_1     GPIO_PC8
#define GPIO_FULE_VALVE_2     GPIO_PC9

#define GPIO_DEBUG_LED        GPIO_PA8

#define GPIO_SPI3_MOSI  GPIO_PC12
#define GPIO_SPI3_MISO  GPIO_PC11
#define GPIO_SPI3_SCK   GPIO_PC10
#define GPIO_SPI3_CS    GPIO_PD2
#define GPIO_SPI3_K_CS0 GPIO_PA12
#define GPIO_SPI3_K_CS1 GPIO_PA11
#define GPIO_SPI3_K_CS2 GPIO_PA10

#define GPIO_I2C_SDA    GPIO_PB7
#define GPIO_I2C_SCL    GPIO_PB6
#define GPIO_I2C_SMBA   GPIO_PB5

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8



void bsp_reset_peripherals (void);

#endif /* BSP_H */
