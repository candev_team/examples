/*---------------------------------------------------------------------------*\

   Joystick - CAN Node Joystick Controller

Note: This module relies on the "Joystick BSP". However, as a long-term effort
code should rely less on this specialized BSP and eventually use a generic one.
For now, the only change to Joystick BSP is that start_process_data() call in
file "io_nod.c" is decommented (as process_data is no longer used).

   2019-02-17  Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include  "Joystick.h"
#include  "ckJoystick.h"     /* CAN Kingdom documents */
#include  <adc/adc.h>        /* ADC driver */
#include  <gpio.h>           /* GPIO driver */


#define  ADC_MAX  4095     /* ADC converter is assumed to be 12-bit */
#define  POS_MAX   511     /* Position signal is assumed to be 10-bit */

#define  TRIM_POS  0x1     /* Trim signals bit encodings */
#define  TRIM_NEG  0x2

#define  SAVE_DLY_MS    2000     /* Calibration save delay {ms} */

#define  TX_PERIOD_MS     50     /* CAN transmission period {ms} */
#define  TX_TASK_PRIO      3     /* Joystick channel task priority */
#define  TX_TASK_STACK  1024     /* Joystick channel task stack size */


/*--- PCB connector mapping to ADC and GPIO hardware ------------------------*/


/* Note: Apparently RT Kernel must have file names as string literals.
Keeping them in RAM doesn't seem to work, even if they are constant. */

static const char *adcDev[] = {     /* ADC device names */
  "/adc1/1", "/adc1/2", "/adc1/3", "/adc1/4" };

static const char *calFile[] = {     /* Calibration data file names */
  "/disk1/an0.cal", "/disk1/an1.cal", "/disk1/an2.cal", "/disk1/an3.cal" };

static const int mapSW[] = {     /* Trim connections (negative logic) */
  GPIO_PB10, GPIO_PB11, GPIO_PC15, GPIO_PC6,
  GPIO_PA4,  GPIO_PA5,  GPIO_PA6,  GPIO_PC13 };


#if 0
/* I/O and PWRON not used for now, move enum's to header when need arise. */
typedef enum  IOx { IO0=0, IO1, IO2, IO3 } IOx;
typedef enum  PWRONx { PWRON0=0, PWRON1, PWRON2, PWRON3 } PWRONx;

static const int mapIO[] = {
  GPIO_PC0,  GPIO_PC1,  GPIO_PC2,  GPIO_PC3 };

static const int mapPWRON[] = {
  GPIO_PC4,  GPIO_PC5,  GPIO_PB0,  GPIO_PB1 };
#endif


/*--- Static functions ------------------------------------------------------*/


/* Convert ADC raw data to signed position signal. */
static int16_t  adcToPos( JoyChan *p, uint16_t adc )
{
  int16_t  pos;

  /* Two linear interpolations on each side of potentiometer mid position */
  if (adc >= p->mid) {
    pos = ((int32_t)POS_MAX*(adc - p->mid))     /* Cast is crucial */
        / (p->max - p->mid);
  }else{
    pos = -((int32_t)POS_MAX*(p->mid - adc))     /* Cast is crucial */
        / (p->mid - p->min);
  }

  if (pos >  POS_MAX) pos =  POS_MAX;     /* Limit to not overflow signal */
  if (pos < -POS_MAX) pos = -POS_MAX;

  return  pos;
}



/* Joystick calibration, only executed if trim was active during power-on. */
static void  calibrate( JoyChan *p, uint16_t adc )
{
  /* Low-pass filtered ADC reading (max 14-bit) for noise suppression */
  p->lpAdc = (3*p->lpAdc + adc + 2)/4;   /* 2 is to round, rather than trunc */

  if (p->reset) {          /* While trim is active during power-on */
    p->min = (2*p->lpAdc + 0)/3;         /* Min is initially -1/3 downwards */
    p->mid = p->lpAdc;                   /* Middle position */
    p->max = (2*p->lpAdc + ADC_MAX)/3;   /* Max is initially +1/3 upwards */
  }else if (p->lpAdc < p->min) {
    p->min = p->lpAdc;               /* Later lo/hi watermark as joystick */
  }else if (p->lpAdc > p->max) {     /*   is moved throughout its range.  */
    p->max = p->lpAdc;
  }else{
    return;     /* No calibration adjustment, don't restart timer */
  }

  /* Start new save delay period after calibration adjustment */
  tmr_stop( p->saveTimer );
  tmr_set( p->saveTimer, tick_from_ms(SAVE_DLY_MS) );
  tmr_start( p->saveTimer );
}



/* Restore joystick calibration from file, return non-zero on first. */
static int  restoreCalibration( JoyChan *p )
{
  int file;  struct stat status;

  if (stat( p->fileName, &status ) != 0) return 1;   /* File not found? */

  file = open( p->fileName, O_RDONLY, 0 );
  read( file, &p->min, sizeof p->min );
  read( file, &p->mid, sizeof p->mid );
  read( file, &p->max, sizeof p->max );
  close( file );
  return 0;
}



/* Save joystick calibration to file. */
static void  saveCalibration( JoyChan *p )
{
  int  file;

  file = open( p->fileName, O_CREAT|O_WRONLY, 0 );
  write( file, &p->min, sizeof p->min );
  write( file, &p->mid, sizeof p->mid );
  write( file, &p->max, sizeof p->max );
  close( file );
}



/* Save delayed period expired. */
static void  onSaveTimer( tmr_t *timer, void *arg )
{
  JoyChan  *p = arg;     /* Our type */

  tmr_stop( timer );    /* Let periodic tx task do the actual save. We're in */
  p->save = 1;          /*   interrupt context, free stack may be small.     */
}



/* Time to transmit. */
static void  onTxTimer( tmr_t *timer, void *arg )
{
  JoyChan  *p = arg;         /* Our type */

  task_start( p->txTask );
}



/* Periodic CAN transmission. */
static void  txTaskFunc( void *arg )
{
  JoyChan  *p = arg;      /* Our type */
  uint16_t adc;           /* ADC raw data */
  int16_t  pos, trim;     /* Position (10-bit) and trim (2-bit) signals */

  for (;;) {
    task_stop();     /* Sync (wait for timer to start task) */

    /* Read trim buttons (negative logic) */
    trim  = (!gpio_get( p->gpioPos )) ? TRIM_POS : 0;
    trim |= (!gpio_get( p->gpioNeg )) ? TRIM_NEG : 0;

    /* Force servo mid reset on calibration power-on */
    if (p->reset && trim != 0) {
      trim = TRIM_POS|TRIM_NEG;     /* Force servo mid pos reset */
    }else{
      p->reset = 0;     /* Trim button no longer pressed */
    }

    /* Read potentiometer position */
    adc = adc_sample_get( p->adc );

    /* Perform mid and endpoint calibrations */
    if (p->cal) calibrate( p, adc );

    /* Convert ADC data to position signal */
    pos = adcToPos( p, adc );

    /* Populate and send CAN message. CAN Kingdom has unique documents for
    everything, even when signal layout is identical. We therefore need to
    switch, if we want to be type safe. */
    switch (p->ckDoc) {
    case CKDN_JoyXoutDoc: {
      JoyXoutDoc page;     /* X signal document */
      page.x = pos;
      page.xTrim = trim;
      sendJoyXoutDoc( &page );
      } break;

    case CKDN_JoyYoutDoc: {
      JoyYoutDoc page;     /* Y signal document */
      page.y = pos;
      page.yTrim = trim;
      sendJoyYoutDoc( &page );
      } break;

    case CKDN_JoyX1outDoc: {
      JoyX1outDoc page;     /* Optional X signal document */
      page.x = pos;
      page.xTrim = trim;
      sendJoyX1outDoc( &page );
      } break;

    case CKDN_JoyY1outDoc: {
      JoyY1outDoc page;     /* Optional Y signal document */
      page.y = pos;
      page.yTrim = trim;
      sendJoyY1outDoc( &page );
      } break;
    }

    /* Save calibration if delay has elapsed */
    if (p->save) {
      p->save = 0;
      saveCalibration( p );
    }

    // RS-232 terminal debug trace example
    //rprintf( "adc%5d   trim%2d   pos%5d   min%5d    mid%5d    max%5d\n",
    //          adc,     trim,     pos,     p->min,   p->mid,   p->max );
  }
}


/*--- Initialization --------------------------------------------------------*/


void  joyInit( JoyChan *p, ANx pot, SWx posTrim, SWx negTrim, int ckDoc )
{
  /* Set VDD_IO_LEVEL to 3.3 volt, as this is the ADC ref voltage. However, be
  aware the PCB doesn't actually put ref voltage on ANx sockets. Don't expect
  stellar precision with ratiometric sensors. */
  gpio_set( GPIO_PB2, 0 );      /* GPIO_PB2 values, 0: 3.3 V, 1: 5.0 V */

  p->adc = adc_open( adcDev[pot] );     /* Init, start continuous conversion */
  ASSERT( p->adc > 0 );
  adc_start( p->adc );                  /* Pin already configured in BSP */

  p->gpioPos = mapSW[posTrim];     /* Trim buttons connection */
  p->gpioNeg = mapSW[negTrim];     /* Pin and GPIO already configured in BSP */

  p->fileName = calFile[pot];      /* Calibration data flash file */
  p->cal = !gpio_get(p->gpioPos)        /* Perform min/mid/max calibration    */
        || !gpio_get(p->gpioNeg)        /*   if trim pressed (negative logic) */
        || restoreCalibration( p );     /*   or calibration file not read.    */
  p->reset = p->cal;                 /* High while power-on trim pressed */
  p->save = 0;                     /* Don't save calibration data just yet */

  p->lpAdc = ADC_MAX/2;     /* Start guess for short start-up transient */

  p->ckDoc = ckDoc;     /* CAN Kingdom document number */

  /* Delayed calibration save timer */
  p->saveTimer = tmr_create( tick_from_ms(SAVE_DLY_MS),
                             onSaveTimer, p, TMR_ONCE );

  /* CAN transmission task driven by cyclic timer */
  p->txTask  = task_spawn( "CAN Tx", txTaskFunc,
                           TX_TASK_PRIO, TX_TASK_STACK, p );
  p->txTimer = tmr_create( tick_from_ms(TX_PERIOD_MS),
                           onTxTimer, p, TMR_CYCL );
  tmr_start( p->txTimer );                             /* Go! */
}


/*--- Functions -------------------------------------------------------------*/


/* None */
