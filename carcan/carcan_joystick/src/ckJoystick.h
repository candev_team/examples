/* 
 * Header file for module Joystick.
 * 
  * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 * Module description: 
 * Joystick with trim controls (buttons or monostable slider).
 * X and Y is sent using separate documents. Optionally a
 * second joystick can be supported as well, for a total of 4
 * channels.
 * 
 * Generation source file: /_car_model/ckJoystick.kfm
 */

#ifndef CKJOYSTICK
#define CKJOYSTICK

#include <stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Joystick X Output. Record number: 11, List number: 1
#define CKDN_JoyXoutDoc 11
 
typedef struct {
  int16_t x:10; // X output
  uint8_t xTrim:2; // X trim
} JoyXoutDoc;

// Joystick Y Output. Record number: 12, List number: 1
#define CKDN_JoyYoutDoc 12
 
typedef struct {
  int16_t y:10; // Y output
  uint8_t yTrim:2; // Y trim
} JoyYoutDoc;

// Joystick X1 Output. Record number: 13, List number: 1
#define CKDN_JoyX1outDoc 13
 
typedef struct {
  int16_t x:10; // X output
  uint8_t xTrim:2; // X trim
} JoyX1outDoc;

// Joystick Y1 Output. Record number: 14, List number: 1
#define CKDN_JoyY1outDoc 14
 
typedef struct {
  int16_t y:10; // Y output
  uint8_t yTrim:2; // Y trim
} JoyY1outDoc;

#pragma pack()


// *** Transmit functions

void sendJoyXoutDoc(JoyXoutDoc *doc);
void sendJoyYoutDoc(JoyYoutDoc *doc);
void sendJoyX1outDoc(JoyX1outDoc *doc);
void sendJoyY1outDoc(JoyY1outDoc *doc);

#endif /* CKJOYSTICK */
