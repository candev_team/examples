/*---------------------------------------------------------------------------*\

   Sample CAN Node Joystick Controller

See "Joystick.h" for details.

   2019-01-27  Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include <shell.h>        /* shell_prompt_set() */
#include "Joystick.h"     /* CAN Node Joystick Controller */


#define  CK_TASK_PRIO      3     /* CAN Kingdom task priority */
#define  CK_TASK_STACK  2048     /* CAN Kingdom task stack size */


/*--- Object allocations ----------------------------------------------------*/


JoyChan  joyX,  joyY;     /* Joystick channels for x/y */
//JoyChan  joyX1, joyY1;    /* Optional joystick channels */
task_t   *ckTask;         /* CAN Kingdom task */


/*--- Detailed functions to complete auto generated code --------------------*/


void  ckappMain( void );     /* Should have been declared in "ckJoystick.h" */



/* Proxy to run CAN Kingdom from RT-Kernel task signature */
static void  ckTaskFunc( void *arg )
{
  ckappMain();   /* CAN Kingdom is Singleton (no args, assumes 1 interface) */
}


/*--- Main function ---------------------------------------------------------*/


int  main( void )
{
  shell_prompt_set( "joystick> " );     /* RS-232 terminal prompt (115 kbps) */

  /* Configure X/Y joystick channels by defining how they are connected to
  hardware and CAN Kingdom documents */
  joyInit( &joyX, AN0, SW0, SW1, CKDN_JoyXoutDoc );
  joyInit( &joyY, AN1, SW2, SW3, CKDN_JoyYoutDoc );

  /* These are for an optional second joystick */
  //joyInit( &joyX1, AN2, SW4, SW5, CKDN_JoyX1outDoc );
  //joyInit( &joyY1, AN3, SW6, SW7, CKDN_JoyY1outDoc );

  /* Fire up CAN Kingdom task */
  ckTask = task_spawn( "CAN Kingdom", ckTaskFunc,
                       CK_TASK_PRIO, CK_TASK_STACK, NULL );

  /* Question: Can we halt the processor here? It actually runs quite hot. */

  return  0;
}
