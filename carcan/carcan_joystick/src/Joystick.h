#ifndef  _JOYSTICK_H
#define  _JOYSTICK_H
/*---------------------------------------------------------------------------*\

   Joystick - CAN Node Joystick Controller

This node reads two joysticks with trims, and sends CAN signals accordingly.
Each direction (x/y) is separate utilizing; one ADC channel, two GPIO trim
buttons and one CK document. Intended hardware is I/O-node with F3 processor.

Potentiometer is coded as signed signal, exercising full range (-511..511 with
10-bit). Right/up should be wired for increasing ADC voltage. Trims are coded
as 2-bit signal, [+]=0x1, [-]=0x2. Conversion and transmission period is 50 ms.

Joystick calibration is initialized by activating any trim button at power-on.
Button must be activated for ~3 sec after power-on, while joystick should be in
middle position. Then operator shall exercise available full stroke (slowly) in
order to disclose min/max. Finally, joystick should be left alone (for ~3 sec),
in order to store calibration data in flash memory. As a final step, operator
can enforce no further calibration, by initialing a new (normal) power cycle.

If 2 (or 4) channels are instantiated, one trim on each needs to be activated
in order for them to participate in calibration (channels are independent).

   2019-02-17 Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include  "ckJoystick.h"    /* CAN Kingdom documents */
#include  <tmr.h>           /* RT-Kernel timer (tmr_t) */
#include  <kern.h>          /* RT-Kernel task (task_t) */

#ifdef __cplusplus
extern "C" {
#endif


/* Hardware connector symbols. Note: ANx/SWx etc follows schematics, not PCB.
(BrVoss forgot to update PCB print when schematics was updated.) */
typedef enum  ANx { AN0=0, AN1, AN2, AN3 } ANx;
typedef enum  SWx { SW0=0, SW1, SW2, SW3, SW4, SW5, SW6, SW7 } SWx;



typedef  struct JoyChan {     /* Private data*/
  int        adc;                /* ADC handle for potentiometer */
  int        gpioPos, gpioNeg;   /* GPIO for trim buttons */
  const char *fileName;          /* Calibration data flash file */
  int        cal, reset, save;   /* Calibration, reset and delay save (bool) */
  uint16_t   min, mid, max;      /* ADC reading for mid pos and endstops */
  uint16_t   lpAdc;              /* LP-filtered ADC for noise suppression */
  int        ckDoc;              /* CAN Kingdom document number */
  tmr_t      *saveTimer;         /* Timer for delayed calibration save */
  task_t     *txTask;            /* CAN transmission task */
  tmr_t      *txTimer;           /* Timer for periodic transmission */
} JoyChan;



/* Initialize. Pass ANx for potentiometer, SWx for trim buttons (negative
logic) and CAN Kingdom document number. */
void  joyInit( JoyChan*, ANx pot, SWx posTrim, SWx negTrim, int ckDoc );


/* CAN receive dispatchers. */
/* None */


#ifdef __cplusplus
}
#endif
#endif   /* _JOYSTICK_H */
