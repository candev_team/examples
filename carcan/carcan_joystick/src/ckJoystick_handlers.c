/* 
 * Handler implementation for module Joystick. 
 * 
 * TODO: This file contains skeleton code for the application. The application
 * developer should implement the missing pieces. 
 *
 * NOTE: This file is not meant to be re-generated. It should be copied to a
 * location where it is not overwritten when code is re-generated. 
 * 
 * File contents:
 * - Message handler functions
 * - Functions called from the application main loop
 * - CAN Kingdom stack callback functions
 * 
 * Generation source file: /_car_model/ckJoystick.kfm
 */

#include "ck.h"
#include "ckHal.h"
#include "ckJoystick.h"


void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  // TODO: This function is optional and should be implemented if the module
  // is supposed to support any messages other that those that are handled in
  // ckappDispatch.
  
  rprintp("Unknown document received: %d\n", doc);
}


// *** Functions called in the main loop

void ckappOnIdle(void) {
  // TODO: This function probably should be implemented.
  //
  // This is the place for code that should be run continually when the
  // application has not received any messages.

  ckhalDelay(0);  /* Give other tasks a chance to execute */
}


bool checkShutdown(void) {
  // TODO: This function probably should be implemented.
  //
  // This is the place for code that checks if the 
  // application should terminate.
  
  return 0;
}


// *** Callback functions from the CAN Kingdom stack

void ckappSetActionMode(ckActMode actMode) {
  // TODO: This function maybe should be implemented.
  // 
  // This function is called by the CAN Kingdom stack when the application
  // should change action mode.
  //
  // actMode can be either ckRunning or ckFreeze.  
}


void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  // TODO: This function maybe should be implemented.
  //
  // This function is called by the CAN Kingdom stack when a message is
  // received while the application is in action mode 'freeze'.
}


int ckappMayorsPage(uint8_t page, uint8_t *buf) {
  // TODO: This function is optional and should be implemented if the module 
  // is supposed to support any application specific Mayor's pages. 

  return 1;    // 1 means that the page is not supported
}


void ckappDispatchRTR(uint16_t doc, bool txF) {
  // This function only has to be implemented if the application is supposed to
  // support RTR messages.
}


/*
 * This function is called by the CAN Kingdom stack implementation when the 
 * application starts. It should not return until the application exists.
 */
void ckappMain(void) {
  // This CAN Kingdom function initializes the stack
  ckInit(ckStartListenForDefaultLetter);
  for (;;) {
    // This gives the application a change to run code when there 
    // are no received messages to process
    ckappOnIdle();

    // This calls the CAN Kingdom stack, which checks for 
    // messages and reports them with ckappDispatch
    ckMain();
    
    // This gives the application a chance to check for application shutdown
    if (checkShutdown()) break;
  }
}
