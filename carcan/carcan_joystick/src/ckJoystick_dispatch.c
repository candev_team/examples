/* 
 * Message dispatch functionality for module Joystick.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software. 
 *  
 * File contents:
 * - Code for dispatching received messages to user defined handler functions.
 * - Message transmit functions for the documents in the module.
 *
 * Generation source file: /_car_model/ckJoystick.kfm
 */

#include "ck.h"
#include "ckJoystick.h"


extern void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen);

// *** Functions for sending messages

void sendJoyXoutDoc(JoyXoutDoc *doc) {
  ckSend(CKDN_JoyXoutDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendJoyYoutDoc(JoyYoutDoc *doc) {
  ckSend(CKDN_JoyYoutDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendJoyX1outDoc(JoyX1outDoc *doc) {
  ckSend(CKDN_JoyX1outDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendJoyY1outDoc(JoyY1outDoc *doc) {
  ckSend(CKDN_JoyY1outDoc, (uint8_t*) doc, sizeof *doc, 0);
}



void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    default:
      onOtherMessage(doc, msgBuf, msgLen);
  }
}
