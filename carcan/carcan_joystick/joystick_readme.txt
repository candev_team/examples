Car CAN Joystick
================
 
The project contains source code that implements the Joystick module of
the Car CAN example.

The joystick module reads input from a joystick device and transmits the
resulting data on the CAN bus. It also handles calibration of the joystick
and saves the calibration result in persistent flash memory.

More technical details can be found in the documentation in the file
`Joystick.h`.
