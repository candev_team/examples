Car CAN Servo
=============

The project contains source code that implements the Servo module of the
Car CAN example.

The servo module software is used to control both the steering and the throttle
of the model car.

The servo module receives control messages on the CAN bus. It uses a PWM unit
to regulate its controlled device. It also receives trim messages.

More technical details can be found in the documentation in the file
`Servo.h`.
