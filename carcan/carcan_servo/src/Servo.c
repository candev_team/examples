/*---------------------------------------------------------------------------*\

   Servo - CAN Node Servo

Note: This module relies on the "Servo BSP". However, as a long-term effort
code should rely less on this specialized BSP and eventually use a generic one.
For now, the only change to Servo BSP is that start_process_data() call in
file "servo_nod.c" is decommented (as process_data is no longer used).

   2019-02-22  Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include  "Servo.h"
#include  "ckServo.h"     /* CAN Kingdom documents */
#include  <adc/adc.h>     /* ADC driver */
#include  <pwm/pwm.h>     /* PWM driver */
#include  <bsp.h>


#define  ADC_SVO_CUR    "/adc1/3"       /* ADC devices in use */
#define  ADC_BAT_VOLT   "/adc1/4"
#define  ADC_SVO_VOLT   "/adc2/1"
#define  PWM_DEV        "/pwm0/3"       /* PWM device for servo pulse */
#define  TRIM_FILE  "/disk1/trim.cal"   /* Flash memory for trim/dir */

#define  ADC_REF   3300     /* ADC reference voltage {mV} */
#define  ADC_MAX   4095     /* ADC converter is assumed to be 12-bit */

#define  DUTY_MAX  1100     /* PWM duty (11%) for 2.2 ms pulse */
#define  DUTY_MIN   400     /* PWM duty ( 4%) for 0.8 ms pulse */
#define  POS_MAX    511     /* Position signal is assumed to be 10-bit */

#define  TRIM_POS   0x1     /* Trim signals bit encodings */
#define  TRIM_NEG   0x2

#define  TRIM_DUR_MS  10000     /* Duration {ms} to trim full position range */
#define  ALIVE_DLY_MS   200     /* Input timeout {ms} before forced mid pos */
#define  SAVE_DLY_MS   2000     /* Calibration save delay {ms} after trim */

#define  TX_PERIOD_MS    50     /* CAN transmission period {ms} */
#define  TX_TASK_PRIO     3     /* Servo task priority */
#define  TX_TASK_STACK 1024     /* Servo task stack size */


/*--- Static functions ------------------------------------------------------*/


/* Convert ADC raw data to battery voltage {mV}. */
static uint16_t  adcToUbat( uint16_t adc )
{
  /* 47k/6k2 voltage divider */
  const uint32_t mul = ADC_REF * (47000L+6200L)>>10;     /* max 20 bit */
  const uint32_t div = ADC_MAX *      6200L    >>10;     /* max 32 bit */

  return (uint16_t)((mul*adc)/div);
}



/* Convert ADC raw data to servo voltage {mV}. */
static uint16_t  adcToVolt( uint16_t adc )
{
  /* 39k/15k voltage divider */
  const uint32_t mul = ADC_REF * (39000L+15000L)>>10;     /* max 20 bit */
  const uint32_t div = ADC_MAX *     15000L     >>10;     /* max 32 bit */

  return (uint16_t)((mul*adc)/div);
}



/* Convert ADC raw data to servo current {mA}. */
static uint16_t  adcToCur( uint16_t adc )
{
  /* LT6106: I = Vadc/Rsense*Rin/Rout,. Rsense = 0.002, Rin = 51, Rout = 5k1 */
  const uint32_t mul = ADC_REF * 1000L * 51L   >>10;     /* max 20 bit */
  const uint32_t div = ADC_MAX *   2L  * 5100L >>10;     /* max 32 bit */

  return (uint16_t)((mul*adc)/div);
}



/* Convert position signal to PWM duty. */
static uint16_t  posToDuty( int16_t pos )
{
  const int32_t mul = DUTY_MAX - DUTY_MIN;         /* Duty range */
  const int32_t div = 2*POS_MAX;                   /* Position range */
  const int32_t mid = DUTY_MAX/2 + DUTY_MIN/2;     /* Mid position duty */
  uint16_t  duty;

  duty = (uint16_t)((mul*pos)/div + mid);

  if (duty > DUTY_MAX) duty = DUTY_MAX;    /* Limit to not exceed duty range */
  if (duty < DUTY_MIN) duty = DUTY_MIN;

  return duty;
}



/* Start new time-out period. */
static void  keepAlive( Servo *p )
{
  p->stop = 0;                 /* Release forced mid pos, if active */
  tmr_stop( p->aliveTimer );
  tmr_set( p->aliveTimer, tick_from_ms(ALIVE_DLY_MS) );
  tmr_start( p->aliveTimer );
}



/* Force servo pulse corresponding to zero input. */
static void  onAliveTimer( tmr_t *timer, void *arg )
{
  Servo  *p = arg;     /* Our type */

  tmr_stop( timer );   /* Let periodic tx task do force mid pos. We're in */
  p->stop = 1;         /*   interrupt context, free stack may be small.  */
}



/* Save delayed period expired. */
static void  onSaveTimer( tmr_t *timer, void *arg )
{
  Servo  *p = arg;     /* Our type */

  tmr_stop( timer );   /* Let periodic tx task do the actual save. We're in */
  p->save = 1;         /*   interrupt context, free stack may be small.     */
}



/* Time to transmit. */
static void  onTxTimer( tmr_t *timer, void *arg )
{
  Servo  *p = arg;     /* Our type */

  task_start( p->txTask );
}



/* Trim mid position or change direction, 'dt' is reception period {ms} */
void  trim( Servo *p, uint16_t trim, int16_t input, uint16_t dt )
{
  int16_t  d;

  if (-POS_MAX/4 <= input     /* Input close to zero? (mid position trim) */
  &&  input < POS_MAX/4) {
    d = ((int32_t)(2*POS_MAX)*dt)/TRIM_DUR_MS;     /* Trim amount */
    if (trim == TRIM_POS) {                      /* [+] trim? */
      p->trim += d;
      if (p->trim > POS_MAX) p->trim = POS_MAX;     /* Limit */
    }else if (trim == TRIM_NEG) {                 /* [-] trim? */
      p->trim -= d;
      if (p->trim < -POS_MAX) p->trim = -POS_MAX;     /* Limit */
    }else if (trim == (TRIM_POS|TRIM_NEG)) {          /* Reset trim? */
      p->trim = 0;
    }else{
      return;     /* No 'trim' input, no change/save */
    }

  }else if ((3*POS_MAX)/4 <= input) {     /* 'input' close to max? */
    if (trim == TRIM_POS) p->dir =  1;      /* [+] set normal direction */
    if (trim == TRIM_NEG) p->dir = -1;      /* [-]  "  reverse    "     */

  }else if (input < -(3*POS_MAX)/4) {     /* 'input' close to min? */
    if (trim == TRIM_NEG) p->dir =  1;      /* [-] set normal direction */
    if (trim == TRIM_POS) p->dir = -1;      /* [+]  "  reverse    "     */

  }else{
    return;     /* 'input' in intermediate interval, no change/save */
  }

  /* Start new save delay period. */
  tmr_stop( p->saveTimer );
  tmr_set( p->saveTimer, tick_from_ms(SAVE_DLY_MS) );
  tmr_start( p->saveTimer );
}



/* Restore servo trim/direction from file, return non-zero on first.  */
static int  restoreTrim( Servo *p )
{
  int file;  struct stat status;

  if (stat( TRIM_FILE, &status ) != 0) return 1;     /* File not found? */

  file = open( TRIM_FILE, O_RDONLY, 0 );
  read( file, &p->trim, sizeof p->trim );
  read( file, &p->dir, sizeof p->dir );
  close( file );
  return 0;
}



/* Save servo trim/direction to file. */
static void  saveTrim( Servo *p )
{
  int  file;

  file = open( TRIM_FILE, O_CREAT|O_WRONLY, 0 );
  write( file, &p->trim, sizeof p->trim );
  write( file, &p->dir, sizeof p->dir );
  close( file );
}



/* Periodic CAN transmission. */
static void  txTaskFunc( void *arg )
{
  Servo  *p = arg;                               /* Our type */
  uint16_t  adcBatVolt, adcSvoVolt, adcSvoCur;   /* ADC raw data */
  SvoSupplyDoc  supplyPage;                  /* CAN tx documents */
  SvoPowerDoc   powerPage;

  for (;;) {
    task_stop();     /* Sync (wait for timer to start task) */

    /* Sample ADC channels (almost) concurrently */
    adcBatVolt = adc_sample_get( p->adcBatVolt );
    adcSvoVolt = adc_sample_get( p->adcSvoVolt );
    adcSvoCur  = adc_sample_get( p->adcSvoCur  );

    /* Convert ADC data to physical units, populate and send CAN messages */
    supplyPage.uBat  = adcToUbat( adcBatVolt );
    powerPage.uServo = adcToVolt( adcSvoVolt );
    powerPage.iServo = adcToCur( adcSvoCur );
    sendSvoSupplyDoc( &supplyPage );
    sendSvoPowerDoc( &powerPage );

    /* Save calibration if delay has elapsed */
    if (p->save) {
      p->save = 0;
      saveTrim( p );
    }

    /* Force mid position if CAN input time-out has elapsed */
    if (p->stop) {
      pwm_duty_cycle_set( p->pwm, posToDuty( p->trim ) );
    }

    // RS-232 terminal debug trace example
    //rprintf( "uBat%6d mV   uServo%6d mV   iServo%5d mA\n",
    //  supplyPage.uBat, powerPage.uServo, powerPage.iServo );
  }
}


/*--- Initialization --------------------------------------------------------*/


void  svoInit( Servo *p )
{
  /*
   * Always measure VCC_SERVO and VDD_SENSOR before connecting any peripherals.
   * Values are depends on Vin and must be verified for your voltage.
   * Change the values (and move setup to application) to get correct voltage
   * for your connected peripherals.
   */
  set_i2c_pot_vcc_servo(0x57);

  p->adcBatVolt = adc_open( ADC_BAT_VOLT );
  ASSERT( p->adcBatVolt > 0 );
  p->adcSvoVolt = adc_open( ADC_SVO_VOLT );
  ASSERT( p->adcSvoVolt > 0 );
  p->adcSvoCur  = adc_open( ADC_SVO_CUR  );
  ASSERT( p->adcSvoCur  > 0 );
  adc_start( p->adcBatVolt );     /* Init, start continuous conversion */
  adc_start( p->adcSvoVolt );
  adc_start( p->adcSvoCur  );

  if (restoreTrim( p )) {     /* Try read trim/dir,              */
    p->trim = 0;              /*   use default if file not read. */
    p->dir = 1;
  }
  p->save = 0;     /* Don't save trim/dir just yet */

  p->stop = 1;                     /* Force mid pos until CAN input arrives */
  p->pwm  = pwm_open( PWM_DEV );     /* Activate PWM output for servo pulse */
  ASSERT( p->pwm > 0 );
  pwm_start( p->pwm );     /* PWM period already set to 20 ms in BSP */
  pwm_duty_cycle_set( p->pwm, posToDuty( p->trim ) );

  /* Timer to force mid pos (stop controlled machine) when CAN input ceases */
  p->aliveTimer = tmr_create( tick_from_ms(ALIVE_DLY_MS),
                              onAliveTimer, p, TMR_ONCE );

  /* Delayed trim/dir save timer */
  p->saveTimer  = tmr_create( tick_from_ms(SAVE_DLY_MS),
                              onSaveTimer, p, TMR_ONCE );

  /* CAN transmission task driven by cyclic timer */
  p->txTask  = task_spawn( "CAN Tx", txTaskFunc,
                           TX_TASK_PRIO, TX_TASK_STACK, p );
  p->txTimer = tmr_create( tick_from_ms(TX_PERIOD_MS),
                           onTxTimer, p, TMR_CYCL );
  tmr_start( p->txTimer );                             /* Go! */
}


/*--- Functions -------------------------------------------------------------*/


void handleSvoInpDoc( Servo *p, SvoInpDoc *msg )
{
  tick_t tick;  uint16_t dt, duty;

  /* Start new time-out period */
  keepAlive( p );

  /* Calculate time dt {ms} since last rx */
  tick = tick_get();
  dt = tick_to_ms( tick - p->lastTick );
  p->lastTick = tick;

  /* Adjust mid pos or direction, if 'trim' is active */
  if (msg->trim != 0) trim( p, msg->trim, msg->input, dt );

  /* Calculate duty, output to servo */
  duty = posToDuty( p->dir*msg->input + p->trim );
  pwm_duty_cycle_set( p->pwm, duty );

  // RS-232 terminal debug trace example
  rprintf( "input%5d   trim%4d   dir%3d   duty%5d   dt%3d ms\n",
           msg->input, p->trim,  p->dir,  duty,     dt );
}
