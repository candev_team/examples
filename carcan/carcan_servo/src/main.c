/*---------------------------------------------------------------------------*\

   Sample CAN Node Servo

See "Servo.h" for details.

   2019-01-27  Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include  <shell.h>     /* shell_prompt_set() */
#include "Servo.h"      /* CAN Node Servo */


#define  CK_TASK_PRIO      3     /* CAN Kingdom task priority */
#define  CK_TASK_STACK  2048     /* CAN Kingdom task stack size */


/*--- Object allocations ----------------------------------------------------*/


Servo   servo;           /* CAN Node Servo */
task_t  *ckTask;         /* CAN Kingdom task */


/*--- Detailed functions to complete auto generated code --------------------*/


void  ckappMain( void );



void  onSvoInpDoc( SvoInpDoc *page )
{
  handleSvoInpDoc( &servo, page );
}



/* Proxy to run CAN Kingdom from RT-Kernel task signature. */
static void  ckTaskFunc( void *arg )
{
  ckappMain();   /* CAN Kingdom is Singleton (no args, assumes 1 interface) */
}


/*--- Main function ---------------------------------------------------------*/


int  main( void )
{
  shell_prompt_set( "servo> " );     /* RS-232 terminal prompt (115 kbps) */

  svoInit( &servo );

  /* Change values in bsp setup when calling set_i2c_pot_vcc_servo(wra_value)
   * and set_i2c_pot_vcc_sensor(wrb_value) to get correct voltage
   * (depends on input voltage) for the peripherals to be connected.
   * Or call from application if more than one application use the same bsp
   * and need to have different voltages.
   */

  /* Fire up CAN Kingdom task */
  ckTask = task_spawn( "CAN Kingdom", ckTaskFunc,
                       CK_TASK_PRIO, CK_TASK_STACK, NULL );

  /* Question: Can we halt the processor here? It actually runs quite hot. */

  return  0;
}
