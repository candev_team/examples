#ifndef  _SERVO_H
#define  _SERVO_H
/*---------------------------------------------------------------------------*\

   Servo - CAN Node Servo

This node receives servo 'input' signal on CAN and outputs R/C-servo pulses
accordingly. 'input' is signed and converted like so (10-bit example):

  'input'  Pulse
  ---------------
    511    2.2 ms
     0     1.5 ms     Pulse period is constant 20 ms
   -511    0.8 ms

A 'trim' signal (2-bit) is also received and feed to an integrator, which is
added to 'input' in order to adjust servo mid position. If 'trim' is received
with 'input' close to min/max, servo direction can be changed:

  'input'  'trim'          Action
  ----------------------------------------
   ~max     01b     Set normal direction
   ~max     10b     Set reverse direction
    ~0      01b     Trim [+] mid position
    ~0      11b     Reset mid position
    ~0      10b     Trim [-] mid position
   ~min     01b     Set reverse direction
   ~min     10b     Set normal direction

An active 'trim' will traverse the whole pulse range in 10 seconds. Integrator
state and direction is preserved in flash memory between power cycles.

A safety feature will output pulses corresponding to 'input' 0, in case 'input'
signal has been absent for more than 200 ms.

The node also has voltage control for the R/C-servo supply and the ability to
measure (and publish on CAN); supply voltage, R/C-servo voltage and current.

   2019-02-22  Bengt Cyren for Kvaser AB, Sweden

\*---------------------------------------------------------------------------*/

#include  "ckServo.h"     /* CAN Kingdom documents */
#include  <tmr.h>         /* tmr_t */
#include  <kern.h>        /* task_t */

#ifdef __cplusplus
extern "C" {
#endif


typedef  struct Servo {     /* Private data */
  int      adcBatVolt,    /* ADC handles */
           adcSvoVolt,
           adcSvoCur;
  int      pwm;           /* PWM handle */
  int16_t  trim, dir;     /* Mid pos trim integrator, direction (-1/1) */
  int      save, stop;    /* Delayed save and time-out stop (bool) */
  tick_t   lastTick;      /* Tick for last input rx (to measure rx period) */
  tmr_t    *aliveTimer;   /* Timer for auto mid-point on input disappearance */
  tmr_t    *saveTimer;    /* Timer for delayed calibration save */
  task_t   *txTask;       /* CAN transmission task */
  tmr_t    *txTimer;      /* Timer for periodic transmission */
} Servo;



/* Initialize servo. */
void  svoInit( Servo* );


/* CAN receive dispatchers. */
void  handleSvoInpDoc( Servo*, SvoInpDoc* );


#ifdef __cplusplus
}
#endif
#endif   /* _SERVO_H */
