/* 
 * Message dispatch functionality for module Servo.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software. 
 *  
 * File contents:
 * - Code for dispatching received messages to user defined handler functions.
 * - Message transmit functions for the documents in the module.
 *
 * Generation source file: /_car_model/ckServo.kfm
 */

#include "ck.h"
#include "ckServo.h"


// *** Functions for handling received messages

extern void onSvoInpDoc(SvoInpDoc *doc);


extern void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen);

// *** Functions for sending messages

void sendSvoSupplyDoc(SvoSupplyDoc *doc) {
  ckSend(CKDN_SvoSupplyDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendSvoPowerDoc(SvoPowerDoc *doc) {
  ckSend(CKDN_SvoPowerDoc, (uint8_t*) doc, sizeof *doc, 0);
}



void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    case CKDN_SvoInpDoc:    // Servo Input
      onSvoInpDoc((SvoInpDoc*) msgBuf); 
      break;
    default:
      onOtherMessage(doc, msgBuf, msgLen);
  }
}
