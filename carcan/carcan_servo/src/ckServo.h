/* 
 * Header file for module Servo.
 * 
  * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 * Module description: 
 * Servo driver with R/C pulse output. Input controls position.
 * Trim signal adjust middle position. Trim can also control
 * direction in conjunction with input. Servo also measures and
 * sends a few voltage and currents.
 * 
 * Generation source file: /_car_model/ckServo.kfm
 */

#ifndef CKSERVO
#define CKSERVO

#include <stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Servo Input. Record number: 11, List number: 2
#define CKDN_SvoInpDoc 11
 
typedef struct {
  int16_t input:10; // Input
  uint8_t trim:2; // Trim
} SvoInpDoc;

// Servo Supply. Record number: 12, List number: 2
#define CKDN_SvoSupplyDoc 12
 
typedef struct {
  uint16_t uBat; // Battery Voltage
} SvoSupplyDoc;

// Servo Power. Record number: 13, List number: 2
#define CKDN_SvoPowerDoc 13
 
typedef struct {
  uint16_t uServo; // Servo Voltage
  uint16_t iServo; // Servo Current
} SvoPowerDoc;

#pragma pack()


// *** Transmit functions

void sendSvoSupplyDoc(SvoSupplyDoc *doc);
void sendSvoPowerDoc(SvoPowerDoc *doc);

#endif /* CKSERVO */
