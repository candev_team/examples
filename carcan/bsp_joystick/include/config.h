/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#ifndef CONFIG_H
#define CONFIG_H

/* Enable the startup routine if defined. The startup routine
 * implements a recommended driver and middleware initialisation
 * sequence.
 */
#define CFG_STARTUP_INIT

/* Enable the shell if defined */
#define CFG_SHELL_INIT
#define CFG_SHELL_PROMPT        "io_nod> "

/* Enable the statistics functionality if defined. */
#undef CFG_STATS_INIT

/* Configure the max number of devices. This determines the size of the
 * device table.
 */
#define CFG_NUM_DEVICES         12

/* Configure the max number of open files. This determines the size of
 * the file table.
 */
#define CFG_NUM_FILES           20

/* Enable hotplug functionality if defined */
#undef CFG_HOTPLUG_INIT
#if defined (CFG_HOTPLUG_INIT)
# define CFG_HOTPLUG_PRIORITY    2
# define CFG_HOTPLUG_STACK_SIZE  1280
# define CFG_HOTPLUG_POLL_PERIOD (1 * CFG_TICKS_PER_SECOND)
#endif

/* Configure the size of the IRQ stack. The stack must be large enough
 * to hold the worst case number of nested interrupt contexts.
 */
#define CFG_IRQ_STACK_SIZE      512

/* Configure the % percentage of the task stack size used in stack control.
 * If task stack usage exceed stack_size % CFG_STACK_ERR_LIMIT an error
 * ESTACK will be reported.
 */
#define CFG_STACK_ERR_LIMIT     90

/* Configure the size of the main task stack. This task must be
 * implemented in the user application and is the task that will be
 * started immediately after the kernel has finished initialising.
 */
#define CFG_MAIN_STACK_SIZE     1280

/* Configure the priority of the main task. */
#define CFG_MAIN_PRIORITY       10

/* Configure the size of the idle task stack. The idle task must
 * always be present. It runs at the lowest priority, when no other
 * task is ready to execute.
 */
#define CFG_IDLE_STACK_SIZE     128

/* Configure the size of the reaper task stack. The reaper task
 * reclaims the resources allocated by tasks that have finished. The
 * reaper task is not needed if the system contains no task that
 * finishes its main loop, and will not be created if the stack size
 * is set to 0.
 */
#define CFG_REAPER_STACK_SIZE   256

/* Configure the type of the heap. Currently supported types are:
 *
 *   CFG_HEAP_TYPE_STATIC       Static allocation. Memory can not be
 *                              freed. No objects should be destroyed.
 *
 *   CFG_HEAP_TYPE_DYNAMIC      Dynamic allocation. Memory is allocated
 *                              and freed.
 *
 * Only one type can be defined.
 */
#undef  CFG_HEAP_TYPE_STATIC
#define CFG_HEAP_TYPE_DYNAMIC

/* Configure the size of signal pool */
#define CFG_SIG_POOL_SIZE       (2 * 1024)

/* Configure the available block sizes in the signal pool */
#define CFG_SIG_POOL_BLOCKS     { 20, 32, 48, 64, 100, 300, 1000, 1500 }

/* Configure the size of the event log. This is only needed for
 * debugging the system. If the size is set to zero then event logging
 * is disabled.
 */
#define CFG_EVENT_LOG_SIZE      (1 * 512)

/* Set the CPU clk frequency. Must match PLL settings.
 */
#define CFG_SYSCLK_FREQUENCY    72000000
#define CFG_HCLK_FREQUENCY      72000000
#define CFG_PCLK1_FREQUENCY     (CFG_HCLK_FREQUENCY / 2)
#define CFG_PCLK2_FREQUENCY     CFG_HCLK_FREQUENCY

/* Configure the number of ticks per second (Hz) */
#define CFG_TICKS_PER_SECOND    1000

/* Reduce power consumption by using the WFI instruction in idle loop */
#undef CFG_IDLE_WFI

/* Initialise external NOR flash driver if defined */
#define CFG_NOR_INIT

/* Enable NOR flash filesystem if defined */
#define CFG_WIFFS_INIT

/* Initialise internal NOR flash driver if defined */
#undef CFG_INOR_INIT
#define CFG_INOR_DEV_NAME        "/efmi0"

/* Enable CAN functionality if defined */
#define CFG_CAN_INIT

/* Enable i2c functionality if defined. */
#undef CFG_I2C_INIT

#undef CFG_SPI3_INIT

/* Enable Lua functionality if defined */
#undef CFG_LUA_INIT

#endif /* CONFIG_H */
