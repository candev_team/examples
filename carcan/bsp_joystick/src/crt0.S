/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#include "config.h"

        .section ".start", "ax"
        .syntax unified
        .thumb
        .thumb_func

        .global start

start:
        @ Disable interrupts
        mov     r0, #0x10
        msr     basepri, r0

        @ Enable FPU
        ldr     r0, =0xE000ED88
        ldr     r1, [r0]
        orr     r1, r1, #(0xF << 20)
        str     r1, [r0]

        @ Copy data to ram

        ldr     r0, =data_start_rom
        ldr     r1, =data_start_ram
        ldr     r2, =data_end_ram

1:
        cmp     r1, r2
        itt     ne
        ldrne   r3, [r0], #4
        strne   r3, [r1], #4
        bne     1b

        @ Clear BSS

        ldr     r0, =bss_start
        ldr     r1, =bss_end
        mov     r2, #0
2:
        cmp     r0, r1
        it      ne
        strne   r2, [r0], #4
        bne     2b

        @ Jump to kernel init
        b       __init

        @@ Define some sections needed by rt-kernel

        @ IRQ stack

        .section ".irqstack", "wa", "nobits"
        .balign 8
irq_stack:
        .space CFG_IRQ_STACK_SIZE

        @ Event log

#if CFG_EVENT_LOG_SIZE != 0
        .section ".event", "wa", "nobits"
event_log:
        .space CFG_EVENT_LOG_SIZE
#endif
