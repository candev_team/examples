Car CAN Example Project
=======================

This is an example project for the CANdev system. It uses a model of a simple
remote controlled car to demonstrate how the difference aspects of the CANdev
system work.

The example consists of the following parts:

* A Kingdom Founder model of the Car CAN network. The model is used for the
	following things:
  - to specify and document the design of the network
  - to verify the consistency of the design
  - to generate C source code for sending and receiving messages by interface
    with the CAN Kingdom runtime.

* C source code for a joystick and a servo module.

* BSP:s (Board Support Packages) for the joystick and servo hardware.

* The CAN Kingdom runtime implementation.

The car network in the example consists of two servos that control the
steering and throttle. The servos are controlled by a joystick on a remote
control device. An Air Bridge device is used to send wireless CAN data from
the joystick to the servos.
