/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

/**
 * \internal
 *
 * CAN loopback driver used for testing purposes
 * \{
 */

#ifndef CAN_LOOPBACK_H
#define CAN_LOOPBACK_H

#include <drivers/dev.h>

#ifdef __cplusplus
extern "C"
{
#endif

drv_t * can_loopback_init (const char * name);

#ifdef __cplusplus
}
#endif

#endif /* CAN_LOOPBACK_H */

/**
 * \}
 */
