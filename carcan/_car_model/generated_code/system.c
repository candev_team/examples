

/*
 * King implementation for the system Car CAN.
 * 
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * Generation source file: /_car_model/system.kfs
 */

#include "ckHal.h"
#include "kCk.h"
#include "system.h"

#define BASE_NR 123
#define CK_CITY_COUNT 3
uint8_t ckCityNumbers[CK_CITY_COUNT] = { 5, 1, 6 };

void ckKingCheck(uint32_t baseNo, uint16_t moduleCount, uint8_t *cities) { 
  // ckKP1(0, baseNo);
  // ...
}

void ckKingMain(void) {
  canSetup(&defdocCfg);
  ckKingDefineFolders();
  ckKP0(0, amRun, cmCommunicate);
  for(;;) {
    ckKingCheck(BASE_NR, CK_CITY_COUNT, ckCityNumbers);
//  if (checkShutdown())
//    break;
  }
}


void ckKingDefineFolders(void) { 
  // Connection: Steering
  // Producer: Joystick X Output @ Joystick
  ckDefineFolder(CKCityNo_joystick, 2, 2, CKDN_JoyXoutDoc, 2, ckTx | ckEnable);
  // Consumer: Servo Input @ Steering
  ckDefineFolder(CKCityNo_steering, 3, 2, CKDN_SvoInpDoc, 2, ckRcv | ckEnable);

  // Connection: Motor Control
  // Producer: Joystick Y Output @ Joystick
  ckDefineFolder(CKCityNo_joystick, 4, 4, CKDN_JoyYoutDoc, 2, ckTx | ckEnable);
  // Consumer: Servo Input @ Motor
  ckDefineFolder(CKCityNo_motor, 3, 4, CKDN_SvoInpDoc, 2, ckRcv | ckEnable);

  // Connection: Battery Voltage
  // Producer: Servo Supply @ Steering
  ckDefineFolder(CKCityNo_steering, 5, 12, CKDN_SvoSupplyDoc, 2, ckTx | ckEnable);

  // Connection: Servo Power
  // Producer: Servo Power @ Steering
  ckDefineFolder(CKCityNo_steering, 6, 13, CKDN_SvoPowerDoc, 4, ckTx | ckEnable);

  // Connection: Optional Josytick X
  // Producer: Joystick X1 Output @ Joystick
  ckDefineFolder(CKCityNo_joystick, 7, 6, CKDN_JoyX1outDoc, 2, ckTx | ckEnable);

  // Connection: Optional Joystick Y
  // Producer: Joystick Y1 Output @ Joystick
  ckDefineFolder(CKCityNo_joystick, 8, 8, CKDN_JoyY1outDoc, 2, ckTx | ckEnable);

}
