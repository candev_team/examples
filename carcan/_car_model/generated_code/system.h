/* 
 * Header file for Car CAN.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * R/C car CAN Kingdom Founder example.
 * 
 * Generation source file: /_car_model/system.kfs
 */

#ifndef SYSTEM
#define SYSTEM

#include <stdint.h>

// --- City numbers
#define CKCityNo_steering 5
#define CKCityNo_joystick 1
#define CKCityNo_motor 6

// --- Servo
#define CKDN_SvoInpDoc 11    // Servo Input
#define CKDN_SvoSupplyDoc 12    // Servo Supply
#define CKDN_SvoPowerDoc 13    // Servo Power

// --- Joystick
#define CKDN_JoyXoutDoc 11    // Joystick X Output
#define CKDN_JoyYoutDoc 12    // Joystick Y Output
#define CKDN_JoyX1outDoc 13    // Joystick X1 Output
#define CKDN_JoyY1outDoc 14    // Joystick Y1 Output

#if __cplusplus
extern "C" {
#endif

void ckKingDefineFolders(void);
void ckKingMain(void);

#if __cplusplus
}
#endif

#endif /* SYSTEM */
