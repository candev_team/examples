/* ckHal.h
 *
 * Mats �kerblom   2000-09-19
 * Last changed    2000-09-19
 *
 * Platform specific functions.
 */

#ifndef __CKHAL_H
#define __CKHAL_H

#include "ck.h"
#include <can/can.h>

#ifndef _CANSTAT_H_
typedef enum {
  canOK                  = 0,
  canERR_PARAM           = -1,     // Error in parameter
  canERR_NOMSG           = -2,     // No messages available
  canERR_NOTFOUND        = -3,     // Specified hw not found
  canERR_TIMEOUT         = -7,     // Timeout ocurred
  canERR_NOTINITIALIZED  = -8,     // Lib not initialized
  canERR_TXBUFOFL        = -13,    // Transmit buffer overflow
  canERR_HARDWARE        = -15     // Some hardware error has occurred
} canStatus;
#endif

#define envInvalid 0xffffffff // Marks that the envelope isn't defined
#define envMask 0x1fffffff // Masks the id bits only
#define envExtended 0x80000000L // Bit set in extended CAN id's
#define envRTR 0x40000000L // Bit set in the ID of an RTR message id.
#define dlcInvalid 255


extern int canHalChannelNo; // Assign this variable before the mayor is started to select CAN channel

#ifdef __cplusplus
extern "C" {
#endif

void ckhalGetDefaultConfig(ckConfig *cfg);
int ckhalGetConfig(ckConfig *cfg);
void ckhalSaveConfig(ckConfig *cfg);

int ckhalLoadFolders(void);
int ckhalSaveFolders(void);

uint8_t *ckhalGetEAN(void);
uint8_t *ckhalGetSerial(void);

// Make a hardware reset of the module. Should never return.
void ckhalReset(void);

#define HAL_TIMER_FREQ CFG_TICKS_PER_SECOND  // Timer frequency in Hz
// Returns the current value of a 32 bit timer; it should wrap at 0xffffffff.
uint32_t ckhalReadTimer(void);

// Give up the current time slice if running in a multi tasking environment.
void ckhalDoze(void);

// Sleep for the time n (given in units of 1/HAL_TIMER_FREQ).
void ckhalDelay(uint32_t n);

// If ckMain is called in the ISR, these function must be implemented.
// Otherwise, they can be empty functions doing nothing.
// They might be called in ckMain (and thus possibly in the ISR).
void ckhalDisableCANInterrupts(void);
void ckhalEnableCANInterrupts(void);

// initialize the CAN hardware
canStatus canSetup(can_cfg_t * can_cfg);

// set the CAN baudrate
canStatus canSetBaudrate(can_cfg_t * can_cfg);

// Reads a CAN message from the bus. If it is an extended CAN id, id:31 is set.
canStatus canReadMessage(uint32_t *id, uint8_t *msg, uint8_t *dlc);

// Writes a CAN message to the bus. If id:31 is set, it is an extended CAN id.
canStatus canWriteMessage(uint32_t id, const uint8_t *msg, uint8_t dlc);

// Set communication mode. Depending on hardware, some modes may not be supported, in 
// which case a higher mode is selected.
void canSetCommMode(ckCommMode commMode);

// Shut down the CAN communication
void canShutdown(void);

// Convert btr to can_cfg_t
void ckhal_btr_to_can_cfg (uint16_t btr, can_cfg_t * can_cfg);

#ifdef __cplusplus
}
#endif

#endif // __CKHAL_H
