CAN Kingdom Runtime Read-me
===========================

This projects contains the CAN Kingdom runtime implementation for mayors and
kings (controllers for embedded units and network masters in CAN Kingdom
terminology).

This is a library project to which application projects can link to use the
CAN Kingdom functionality.

The project consists of two parts:

CAN Kingdom Mayor Component 
---------------------------

It provides the following functionality to CAN Kingdom mayors (controller for
embedded units in CAN Kingdom terminology):

* Send Mayor's documents
* Receive King's documents
* Manage configuration for message ID:s and node ID:s (folder and city number
  in CAN Kingdom terminology).

CAN Kingdom King Component
--------------------------

It provides the following functionality for CAN Kingdom kings (network master
controllers in CAN Kingdom terminology):

Send king's documents do embedded units, to configure and control them.

A king can be either another embedded unit or for example a PC connected to
the CAN network.

Building
--------

The runtime is an Eclipse project. Applications that use it should declare the
runtime project as a dependency in Eclipse.

Dependencies
------------

* The Hardware Interface Layer depends on rt-kernel. rt-kernel headers files
  and binaries must be availible to build it. (They are installed as parts of
  the CANdev Workbench.)
* The rest of the project only depends on the C standard library.   
