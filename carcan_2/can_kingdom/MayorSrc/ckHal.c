/* ckHal.c
 *
 * Mats Akerblom   2000-09-19
 * Last changed    2000-09-25
 *
 * Platform specific functions.
 *
 * This is an implementation for Windows NT/95/98 and the canlib
 */

#include <string.h>
#include <tick.h>
#include <task.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <can/can.h>
#include <stdio.h>
#include <stm32.h>
#include "ck.h"
#include "ckHal.h"

#define CK_CFG_FILE           "/disk1/ck.cfg"
#define CK_FOLDERS_FILE       "/disk1/folders.cfg"
#define CK_FOLDERS_TEMP_FILE  "/disk1/folders.tmp"

// =============================================================================
/* The config functions should return data about node numbers etc that
* CAN Kingdom needs to know in order to operate.
* This structure will possibly also contain the folder setup in the future.
*/
/* Return the default configuration.
* This functions must always succeed.
*/
void ckhalGetDefaultConfig (ckConfig *cfg)
{
   cfg->baseNumber = envInvalid;
   cfg->nodeNumber = 255;
   cfg->btr = 0x2307; // 125 kBit/s for a 82527
}

/* Return the saved configuration. If there is no NVRAM, or it is uninitialized,
* return 1, return 0 on success.
*/

int ckhalGetConfig (ckConfig *cfg)
{
   struct stat file_status;

   if (stat (CK_CFG_FILE, &file_status) == 0)
   {
      int file = open (CK_CFG_FILE, O_RDONLY, 0);

      read (file, cfg, sizeof(ckConfig));

      close (file);
   }
   else
   {
      ckhalGetDefaultConfig (cfg);

      ckhalSaveConfig (cfg);
   }

   return 0;
}

/* Called when configuration data that should be kept after rebooting
* the module.
* No return value as we can do nothing if it fails or isn't supported.
*/

void ckhalSaveConfig(ckConfig *cfg)
{
   int file = open (CK_CFG_FILE, O_CREAT | O_WRONLY, 0);

   write (file, cfg, sizeof(ckConfig));

   close (file);
}

/* Load the folders from NVRAM. Returns non-zero if error.
 */
int ckhalLoadFolders (void)
{
   ckFolderT temp_folder;
   struct stat file_status;

   if (stat (CK_FOLDERS_FILE, &file_status) != 0)
   {
      return -1;
   }

   int file = open (CK_FOLDERS_FILE, O_RDONLY, 0);

   // Delete all slots except Kings/Mayors page
   ckClearFolders (1);

   // Insert the folders from NVRAM.
   // The position which are unused are filled with zeroes, making the
   // ffInUse-flag cleared.
   while (read (file, (void*)&temp_folder, sizeof(ckFolderT)) == sizeof(ckFolderT))
   {
      ckDefineLocalFolder (temp_folder.folderNo,
                           temp_folder.doc,
                           temp_folder.envelope,
                           (temp_folder.flags & ffDLC) ? temp_folder.dlc : dlcInvalid,
                           temp_folder.flags & ffEnabled,
                           temp_folder.flags & ffTx,
                           temp_folder.flags & ffRTR,
                           temp_folder.flags & ffExt);
   }

   close (file);

   return 0;
}

/* Write the folder configuration to NVRAM. We save at most slotsMaxSaved
 * folders and not folder 0 or 1.
 */
int ckhalSaveFolders (void)
{
   int err;
   uint32_t i;
   struct stat file_status;

   if (stat (CK_FOLDERS_TEMP_FILE, &file_status) == 0)
   {
      remove (CK_FOLDERS_TEMP_FILE);
   }

   int file = open (CK_FOLDERS_TEMP_FILE, O_CREAT | O_WRONLY, 0);

   // Write the folders
   for (i = 0; i < folderMaxCount; i++)
   {
      if ((ckSlots[i].flags & ffInUse) && ckSlots[i].doc != rdocKing
            && ckSlots[i].doc != tdocMayor)
      {
         if ((err = write (file, (void*)&ckSlots[i], sizeof (ckFolderT))) < 0)
         {
            close (file);
            return err;
         }
      }
   }

   close (file);

   if (stat (CK_FOLDERS_FILE, &file_status) == 0)
   {
      remove (CK_FOLDERS_FILE);
   }

   rename (CK_FOLDERS_TEMP_FILE, CK_FOLDERS_FILE);

   return 0;
}

/* Return an EAN code that will be included in the
* Mayors' page 0. A pointer to a static 5 character string
* should be returned (the value is 40-bit).
* This is usually a constant for a certain implementation.
*/
uint8_t *ckhalGetEAN(void) {
  return (uint8_t*)"\1\2\3\4\5";
}

/* Return the serial number. It is 40-bit, LSB first.
* This is usually read from NVRAM.
*/
uint8_t *ckhalGetSerial(void) {
  return (uint8_t*)"\1\0\0\0\0";
}

// =============================================================================
void ckhalReset (void)
{
   scb_soft_reset ();
}

// Returns the current value of a 32 bit timer; it should wrap at 0xffffffff.
// Should for some reason it be impossible to implement a timer,
// return the value of a running counter or startup with listening for
// a default letter will not work at all (ckInit() will never return).
uint32_t ckhalReadTimer(void) {
  return tick_get();
}
// Give up the current time slice if running in a multi tasking environment.
// The delay should not be long, maybe one ms or one clock tick.
// It is OK if the function doesn nothing, but certain ck rountines may then use up all
// CPU time.
void ckhalDoze(void) {
   task_delay (1);
}

// Sleep for the time n (given in units of 1/HAL_TIMER_FREQ).
void ckhalDelay(uint32_t n) {
   task_delay (n);
}
// If ckMain is called in the ISR, these function must be implemented.
// Otherwise, they can be empty functions doing nothing.
// They might be called in ckMain (and thus possibly in the ISR).
void ckhalDisableCANInterrupts(void) {
}
void ckhalEnableCANInterrupts(void) {
}


// =============================================================================
int canHalChannelNo = 0; // Assign this variable before the mayor is started to select CAN channel
static int can_fd;
canStatus canSetup(can_cfg_t * can_cfg) {
  canStatus stat;

  can_filter_t filter = {
       .id   = 0x0,
       .mask = 0x0,
     };

  can_fd = open("/can0", O_RDWR, 0);
  UASSERT(can_fd != -1, EARG);

  //can_set_callback (can_struct->can_fd, can_rx_callback, CAN_EVENT_MSG_RECEIVED, can_struct);
  can_filter (can_fd, &filter);

  stat = canSetBaudrate(can_cfg);
  if (stat < 0) {
    rprintf("Bus params failed, stat=%d\n", stat);
    return stat;
  }

  canSetCommMode(ckCommunicate);

  return canOK;
}

canStatus canSetBaudrate (can_cfg_t * can_cfg)
{
   canSetCommMode (ckSilent);
   can_set_cfg (can_fd, can_cfg);

   return canOK;
}

/* In an embedded system, canShutdown is usually not called.
* But it is nedded to enable restart when run under Windows.
*/
void canShutdown(void) {
   if (can_fd != 0)
   {
      close (can_fd);
   }
}
/*
uint8_t canGetTxBusy(void) {
  unsigned long flags;
  canReadStatus(cHandle, &flags);
  return (flags & canSTAT_TX_PENDING) != 0;
}
*/

// Reads a CAN message from the bus.
canStatus canReadMessage (uint32_t *id, uint8_t *msg, uint8_t *dlc)
{
   can_frame_t frame;
   canStatus status = canERR_NOMSG;

   if (can_receive (can_fd, &frame) == 0)
   {

      *id = frame.id;
      *dlc = frame.dlc;
      memcpy (msg, &frame.data, frame.dlc);
      status = canOK;
   }

   return status;
}

// Writes a CAN message to the bus.
canStatus canWriteMessage (uint32_t id, const uint8_t *msg, uint8_t dlc)
{
   can_frame_t frame;
   canStatus status = canERR_NOMSG;

   frame.id = id;
   frame.dlc = dlc;
   memcpy (&frame.data, msg, dlc);

   if (can_transmit (can_fd, &frame) == 0)
   {
      status = canOK;
   }
   return status;
}

// Set communication mode. Depending on hardware, some modes may not be supported, in 
// which case a higher mode is selected.
void canSetCommMode(ckCommMode commMode) {
  switch(commMode) {
    case ckSilent:      // The output is disabled, not even acks's are transmitted.
                        // In this case, nothing is received either since CAN transceiver doesn't support this.
      can_bus_off(can_fd);
      break;
    case ckListenOnly:  // We listen but respond only to Kings Letters.
    case ckCommunicate: // Normal communication.
      can_bus_on(can_fd);
      break;
    default:
      break;
  }
}

void ckhal_btr_to_can_cfg (uint16_t btr, can_cfg_t * can_cfg)
{
   uint16_t brp = 1 + (btr & 0x3f);

   can_cfg->sjw = 1 + ((btr >> 6) & 0x03);
   can_cfg->bs1 = 1 + ((btr >> 8) & 0x0f);
   can_cfg->bs2 = 1 + ((btr >> 12) & 0x07);
   can_cfg->silent = 0;
   // We assume a 8 MHz clock frequency
   can_cfg->baudrate = 8000000L / (brp * (1 + can_cfg->bs1 + can_cfg->bs2));
}
