/* kCk.c
 *
 * CAN Kingdom support.
 *
 * (c) 1996-2000 Kvaser
 *
 * Mats Akerblom  1996-03-28
 * Last changed   2000-11-19
 */


#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdarg.h>
#include <assert.h>
#include <math.h>

#ifdef _Windows
  #include <windows.h>
#endif
#include "ckHal.h"
#include "kCk.h"


#define kingsEnv 0

static void fillBuf(uint8_t *buf, uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, uint8_t b4, uint8_t b5, uint8_t b6, uint8_t b7) {
  buf[0] = b0;
  buf[1] = b1;
  buf[2] = b2;
  buf[3] = b3;
  buf[4] = b4;
  buf[5] = b5;
  buf[6] = b6;
  buf[7] = b7;
}

// Kings page 0
void ckKP0(uint8_t city, actModeT am, commModeT cm, uint8_t cityMode) {
  uint8_t buf[8];
  fillBuf(buf,
     (uint8_t)city, 0, (uint8_t)am, (uint8_t)cm,
     cityMode,
     0,0,0);
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 0. Supports only standard id's.
void ckKP1(uint8_t city, long baseNo, uint8_t respPage, bool extF) {
  uint8_t buf[8]; 
  fillBuf(buf,
    (uint8_t)city, 1,
    respPage, // Ask the mayor to respond with this page
    0,
    LSB(baseNo), MSB(baseNo), (uint8_t)((baseNo >> 16) & 0xff),
    (uint8_t)((baseNo >> 24) & 0xff));
  if (extF)
    buf[7] |= 0x80;
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 2. Always enabled.
void ckKP2(uint8_t city, uint32_t env, uint8_t folder, bool extF /* = 0 */) {
  uint8_t buf[8];

  buf[0] = (uint8_t)city;
  buf[1] = 2; // Page 2
  buf[2] = (uint8_t)(env & 255);
  buf[3] = (uint8_t)((env >> 8) & 255);
  buf[4] = (uint8_t)((env >> 16) & 255);
  buf[5] = (uint8_t)((env >> 24) & 31);
  if (extF)
    buf[5] |= 0x80; // No compressed envelopes.
  buf[6] = (uint8_t)folder;
  buf[7] = 3; // Enable the folder, assign the new envelope.

  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 3. Assigning a City to Groups
void ckKP3(uint8_t city, uint8_t gr1, uint8_t gr2, uint8_t gr3, uint8_t gr4, uint8_t gr5, uint8_t gr6) {
  uint8_t buf[8];
  fillBuf(buf,
    (uint8_t)city, 3, (uint8_t)gr1, (uint8_t)gr2, (uint8_t)gr3, (uint8_t)gr4, (uint8_t)gr5, (uint8_t)gr6);
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 4. Removing a City from Groups
void ckKP4(uint8_t city, uint8_t gr1, uint8_t gr2, uint8_t gr3, uint8_t gr4, uint8_t gr5, uint8_t gr6) {
  uint8_t buf[8];
  fillBuf(buf,
    (uint8_t)city, 4, (uint8_t)gr1, (uint8_t)gr2, (uint8_t)gr3, (uint8_t)gr4, (uint8_t)gr5, (uint8_t)gr6);
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 8. Change the baudrate.
void ckKP8(uint8_t city, int brChip, uint8_t btr0, uint8_t btr1) {
  uint8_t buf[8];
  fillBuf(buf,
    (uint8_t)city, 8, (uint8_t)brChip, 0, (uint8_t)btr0, (uint8_t)btr1, 0,0);
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 9. Change of City physical address
void ckKP9(uint8_t city, int newCity, uint8_t respPage /* = 255 */) {
  uint8_t buf[8];
  fillBuf(buf,
    (uint8_t)city, 9,
    respPage,  // Mayor's response page
    (uint8_t)newCity,
    0, 0, 0, 0);
  canWriteMessage(kingsEnv, buf, 8);
}

// Kings page 20. Set filters.
void ckKP20(uint8_t city, uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, bool extF, uint8_t fPrimitiveNo, uint8_t fNo) {
  uint8_t buf[8];
  b3 &= 0x1f;
  if (extF)
    b3 |= 0x80;
  fillBuf(buf,
     (uint8_t)city, 20,
     b0,b1,b2,b3,
     (uint8_t)fPrimitiveNo, (uint8_t)fNo);
  canWriteMessage(kingsEnv, buf, 8);
}


// Kings page 16
void ckKP16(uint8_t city, uint8_t folder, uint16_t doc, uint8_t dlc, bool txF, bool rtrF,
            int enableF) {
  uint8_t buf[8];

  buf[0] = (uint8_t)city;
  buf[1] = 16; // Page 16
  buf[2] = (uint8_t)folder;
  buf[3] = 0x80 + dlc;
  if (rtrF)
    buf[3] |= 0x40;
  buf[4] = (uint8_t)(0xb0 | (enableF ? 64 : 0) | (txF ? 1 : 0));  // Enable, remove old, insert new document
  buf[5] = (uint8_t)(doc >> 8);
  buf[6] = (uint8_t)(doc & 255);
  buf[7] = 0;

  canWriteMessage(kingsEnv, buf, 8);
}


void ckDefineFolder(uint8_t city, uint8_t folder, uint32_t env, uint16_t doc, uint8_t dlc,
                    uint8_t flags) {
  ckKP2(city, env, folder, flags & ckExt);
  ckKP16(city, folder, doc, dlc, flags & ckTx, flags & ckRTR, flags & ckEnable);
}


