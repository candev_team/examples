/* kCk.h
 *
 * CAN Kingdom support.
 *
 * Mats Akerblom  1996-03-28
 * Last changed   2000-09-22
 */

#ifndef __KCK_H
#define __KCK_H

#include "ckCommon.h"

typedef enum {amKeep = 0, amRun = 1, amFreeze = 2, amReset = 3} actModeT;
typedef enum {cmKeep = 0, cmSilent = 1, cmListenOnly = 2, cmCommunicate = 3} commModeT;

/* Flags for folder definition and envelope assignment */
#define ckRTR 1      /* RTR bit */
#define ckTx 2       /* Tx bit, else Rx */
#define ckEnable 4   /* Enable bit */
#define ckExt 8      /* Extended frame bit */

#if __cplusplus
extern "C" {
#endif

void ckKP0(uint8_t city, actModeT am, commModeT cm, uint8_t cityMode);
void ckKP1(uint8_t city, long baseNo, uint8_t respPage, bool extF);
void ckKP2(uint8_t city, uint32_t env, uint8_t folder, bool extF);
void ckKP3(uint8_t city, uint8_t gr1, uint8_t gr2, uint8_t gr3, uint8_t gr4, uint8_t gr5, uint8_t gr6);
void ckKP4(uint8_t city, uint8_t gr1, uint8_t gr2, uint8_t gr3, uint8_t gr4, uint8_t gr5, uint8_t gr6);
void ckKP8(uint8_t city, int brChip, uint8_t btr0, uint8_t btr1);
void ckKP9(uint8_t city, int newCity, uint8_t respPage);
void ckKP16(uint8_t city, uint8_t folder, uint16_t doc, uint8_t dlc, bool txF, bool rtrF,
            int enableF);
void ckKP20(uint8_t city, uint8_t b0, uint8_t b1, uint8_t b2, uint8_t b3, bool extF, uint8_t fPrimitiveNo, uint8_t fNo);

void ckDefineFolder(uint8_t city, uint8_t folder, uint32_t env, uint16_t doc, uint8_t dlc,
                    uint8_t flags);

#if __cplusplus
}
#endif

#endif // __KCK_H
