/* 
 * Handler implementation for module ckJoystick. 
 * 
 * File contents:
 * - Message handler functions
 * - Functions called from the application main loop
 * - CAN Kingdom stack callback functions
 * 
 * Generation source file: /car_model/ckJoystick.kfm
 * This file has been edited since generation.
 */

/**
 *  @file joystick/src/ckJoystick_handlers.c
 *  @brief Functions handling incoming letters and the CanKingdom stack.
 */


#include "ck.h"
#include "ckJoystick.h"
#include "init.h"
#include "timers.h"
#include "tasks.h"

/*--- Object allocations ----------------------------------------------------*/

joystick js;        /* Joystick struct */
joyAxis joyX, joyY; /* Joystick axes used in the joystick struct */

/*---------------------------------------------------------------------------*/

void onOtherMessage (uint16_t doc, uint8_t *msgBuf, uint8_t msgLen)
{
  // TODO: This function is optional and should be implemented if the module 
  // is supposed to support any messages other that those that are handled in
  // ckappDispatch.

  rprintp ("Unknown document received: %d\n", doc);
}

// *** Functions called in the main loop
// This is the place for code that should be run continually when the
// application has not received any messages.
void ckappOnIdle (void)
{
  task_delay (0);   /* Giver others (application) chance to execute */
}

bool checkShutdown (void)
{
  // TODO: This function probably should be implemented.
  //
  // This is the place for code that checks if the 
  // application should terminate.
  return 0;
}

/**
 * @brief Change joystick mode.
 *
 * When called all timers are stopped, destroyed and tasks are ended.
 * Lastly new tasks and are created and started.
 *
 * @param js Joystick to do the operation on.
 */
void changeJoystick(joystick *js)
{
  if (js->running)  /* Timers not started */
  {
    stopTimers(js);
  }
  destroyTimers(js);
  js->stopTasks = 1;
  runTasksOnce(js); /* stops tasks since stopTasks == 1 */
  js->stopTasks = 0;
  taskInit(js);
  if (js->running)
  {
    startTimers(js);
  }
}

// *** Callback functions from the CAN Kingdom stack

/**
 * @brief Handles changing city mode.
 *
 * Only needs to be implemented if city has several modes.
 *
 * @param mode sent by the capital.
 */
void ckappSetCityMode(uint8_t mode)
{
  /* Sets joystick based on mode. Mode == 1 is left, mode == 2 is right. */
  switch (mode)
  {
    case 1:
      if (js.joySel != LJoystick)
      {
        js.joySel = LJoystick;
        changeJoystick(&js);      
      }
      break;
    case 2:
      if (js.joySel != RJoystick)
      {
        js.joySel = RJoystick;
        changeJoystick(&js);
      }
      break;
  }
}

/**
 * @brief Starts or stops applications.
 *
 * Starts timers if these are not running and stops them if they are.
 * When the timers are stopped no messages are sent and
 * it is also safe to destroy them.
 *
 * @param actMode sent by the capital.
 */
void ckappSetActionMode(ckActMode actMode)
{
//  rprintp("SetActionMode \n");
  if (actMode)
  {
    if (!js.running && actMode == ckRunning)
    {
      js.running = 1;
      startTimers(&js); 
    }
    else if (js.running && actMode == ckFreeze)
    {
      js.running = 0;
      stopTimers(&js);
    }
  }
}

void ckappDispatchFreeze (uint16_t doc, uint8_t *msgBuf, uint8_t msgLen)
{
  // TODO: This function maybe should be implemented.
  //
  // This function is called by the CAN Kingdom stack when a message is
  // received while the application is in action mode 'freeze'.

   // Do nothing?
}

int ckappMayorsPage (uint8_t page, uint8_t *buf)
{
  // TODO: This function is optional and should be implemented if the module 
  // is supposed to support any application specific Mayor's pages. 

  return 1; // 1 means that the page is not supported
}

void ckappDispatchRTR (uint16_t doc, bool txF)
{
  // This function only has to be implemented if the application is supposed to
  // support RTR messages.
}

/*
 * This function is called by the CAN Kingdom stack implementation when the
 * application starts. It should not return until the application exists.
 */
/**
 * @brief The main task of the system.
 *
 * Initializes the servo and runs the CanKingdom stack.
 */
void ckappMain (void)
{
  /* Init joystick */
  js.joyX = &joyX;
  js.joyY = &joyY;

  joyInit(&js, SW4, SW5);

  /* Calibrate if calibration activated after init */
  while(js.joyX->cal || js.joyY->cal)
  {
    task_delay(0);
  }

  /* Init tasks */
  taskInit(&js);

  task_delay(tick_from_ms(1000)); /* Give air bridge time to start */

  /* This CAN Kingdom function initializes the stack */
  ckInit(ckStartWithKing);

  for (;;)
  {
    // This gives the application a change to run code when there 
    // are no received messages to process
    ckappOnIdle();

    // This calls the CAN Kingdom stack, which checks for
    // messages and reports them with ckappDispatch
    ckMain();

    // This gives the application a chance to check for application shutdown
    if (checkShutdown())
      break;
  }
}
