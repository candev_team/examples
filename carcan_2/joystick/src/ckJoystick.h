/* 
 * Header file for module ckJoystick.
 * 
  * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 *Generation source file: /car_model/ckJoystick.kfm
 */
 /**
 * @file ckJoystick.h
 * @brief Joystick city.
 * Module description: 
 * Joystick city that reads one joystick with two trim buttons and sends CAN signals accordingly.
 * Each axis (x/y) is separate utilizing one ADC channel and two GPIO trim buttons.
 *
 * Potentiometer is coded as an unsigned signal, exercising full range (0 to 4096 with
 * 12-bit resolution). The trim buttons and the switch are coded as 2-bit signals, [+]=0x1, [-]=0x2.
 *
 * Hyperperiod of the system is 200ms.
 * joyAxis calibration is initialized by activating the trim in either direction at power-on.
 * Trim must be activated for ~2 s after power-on, while the joystick should be in
 * middle position. Then operator shall slowly move the joystick to the corners and along the sides
 * in order to disclose min/max.
 * Finally, the joystick should be left alone (for ~5 s), in order to store calibration data in flash memory.
 * After calibration the node will start CanKingdom as normal and wait for the capital.
 *
 * Both axes are initialized as well as both trims. The capital will later on decide what should be sent and how.
 * Modes will decide what tasks will run and these can be set by the capital.
 *
 * Joystick axes are sent in one message each.
 * Trim buttons are combined into one message.
 * Switch is sent in one message.
 *
 * Intended hardware is I/O-node with F3 processor.
 * 
 * @authors Hashem Hashem & Andre' Idoffsson.
 * @date 2019-07
 */

#ifndef CKJOYSTICK
#define CKJOYSTICK

#include <stdint.h>
#include <tmr.h>  /* RT-Kernel timer (tmr_t) */
#include <kern.h> /* RT-Kernel task (task_t) */
#include "ck.h"

#ifdef __cplusplus
extern "C" {
#endif

#define ADC_MAX 4095  /* ADC converter is assumed to be 12-bit */
#define ADC_MID 2047
#define POS_MAX 4095  /* POS max 12-bit*/
#define POS_MID 2047

/**
 *  @brief Enum for different joystick modes.
 */
typedef enum {LJoystick=0, RJoystick} joySelector;

/* Hardware connector symbols. Note: ANx/SWx etc follows schematics, not PCB.
(BrVoss forgot to update PCB print when schematics was updated.) */

/**
 *  @brief Enum for different potentiometer ports.
 */
typedef enum ANx {AN0=0, AN1, AN2, AN3} ANx;

/**
 *  @brief Enum for different Switch ports.
 *
 *  One Switch port is actually both SW0 and SW1 and so on.
 */
typedef enum SWx {SW0=0, SW1, SW2, SW3, SW4, SW5, SW6, SW7} SWx;

/**
 *  @brief Enum for different IO ports.
 */
typedef enum IOx {IO0=0, IO1, IO2, IO3} IOx;

/**
 * @brief Struct for joystick axes.
 *
 * This struct is used to store variables,
 * timer handles and the calibration task handle.
 */
typedef struct {
  int        adc;                   /**< ADC handle for potentiometer */
  int        gpioPos, gpioNeg;      /**< GPIO for trim buttons */
  const char *fileName;             /**< Calibration data flash file */
  int        cal, reset, save;      /**< Calibration, reset and delay save (bool) */
  uint16_t   min, mid, max;         /**< ADC reading for mid pos and endstops */
  uint16_t   lpAdc;                 /**< LP-filtered ADC for noise suppression */
  tmr_t      *saveTimer;            /**< Timers for delayed calibration save */
  tmr_t      *calibrationInterval;  /**< Timer for cyclic calibration */
  task_t     *calibrationTa;        /**< Calibration task */
} joyAxis;

/**
 * @brief Struct for a complete joystick.
 *
 * This struct is used to store variables,
 * timer arrays and task arrays. Contains two joyAxis,
 * one for the x-axis and one for the y-axis.
 * Also contains switch GPIO.
 */
typedef struct {
  uint8_t     numTasks;         /**< Number of tasks */
  uint8_t     stopTasks;        /**< Stop task flag */
  uint8_t     running;          /**< Is the joystick communicating? off/on 0/1 */
  joySelector joySel;           /**< Determines L/R joystick */
  uint8_t     sw1Up, sw1Down;   /**< GPIO for one 2-position switch */
  joyAxis     *joyX;            /**< Joystick x-axis */
  joyAxis     *joyY;            /**< Joystick y-axis */
  task_t*     tasks[3];         /**< Task array */
  tmr_t*      timers[3];        /**< Timer array */
  uint32_t    offsets[3];       /**< Offsets between timers, used to schedule correctly. */
} joystick;

#pragma pack(1)

// *** Document constants and structs

//! Joystick X Output. Record number: 10, List number: 1
#define CKDN_joyXOutDoc 10
 
typedef struct {
  uint16_t xOut:12; // X Output
} joyXOutDoc;

//! Joystick Y Output. Record number: 11, List number: 1
#define CKDN_joyYOutDoc 11
 
typedef struct {
  uint16_t yOut:12; // Y Output
} joyYOutDoc;

//! Trim Buttons. Record number: 12, List number: 1
#define CKDN_trimDoc 12
 
typedef struct {
  uint8_t xTrim:2; // X Trim
  uint8_t yTrim:2; // Y Trim
} trimDoc;

//! Switch outputs. Record number: 13, List number: 1
#define CKDN_switchDoc 13
 
typedef struct {
  uint8_t switch3w:2; // Three-way Switch
} switchDoc;

#pragma pack()

// *** Transmit functions
/**
 * @brief Sends an X position document.
 * @param doc The document that is to be sent.
 */
void sendJoyXOutDoc(joyXOutDoc *doc);

/**
 * @brief Sends a Y position document.
 * @param doc The document that is to be sent.
 */
void sendJoyYOutDoc(joyYOutDoc *doc);

/**
 * @brief Sends a trim button document.
 * @param doc The document that is to be sent.
 */
void sendTrimDoc(trimDoc *doc);

/**
 * @brief Sends a switch position document.
 * @param doc The document that is to be sent.
 */
void sendSwitchDoc(switchDoc *doc);

// *** CanKingdom functions
/**
 * @brief Giver other tasks (application) a chance to execute.
 */
void ckappOnIdle(void);

/**
 * @brief The main task of the system.
 *
 * Initializes the joystick.
 * Waits if calibration is in progress before continuing.
 * Starts the tasks needed based on joystick mode.
 * Runs the CanKingdom stack.
 */
void ckappMain(void);

#endif /* CKJOYSTICK */
