/**
 *  @file joystick/src/tasks.h
 *  @brief Contains functions related to running tasks.
 */

#ifndef _TASKS_H
#define _TASKS_H

#include "ckJoystick.h"     /* CAN Kingdom documents */

#define CK_TASK_PRIO     3  /* CAN Kingdom task priority */
#define CK_TASK_STACK 2048  /* CAN Kingdom task stack size */

#define TX_TASK_PRIO     3  /* Joystick channel task priority */
#define TX_TASK_STACK 1024  /* Joystick channel task stack size */

/**
 * @brief Gets the joystick's X output and sends it in a letter.
 * @param arg Joystick that is sending the letter.
 */
void sendXOut(void* arg);

/** @ingroup tx
 * @brief Gets the joystick's Y output and sends it in a letter.
 * @param arg Joystick that is sending the letter.
 */
void sendYOut(void* arg);

/** @ingroup tx
 * @brief Gets the joystick's trim outputs and sends them in a letter.
 * @param arg Joystick that is sending the letter.
 */
void sendTrim(void* arg);

/** @ingroup tx
 * @brief Gets the switch output and sends it in a letter.
 * @param arg Joystick that is sending the letter.
 */
void sendSwitch(void* arg);


/**
 * @brief Runs all active tasks once, independent of schedule.
 *
 * Used when changing joystick mode by calling this function
 * with the stopTasks flag set.
 *
 * @param js Joystick to do the operation on.
 */
void runTasksOnce(joystick *js);

/**
 * @brief Task for handling joystick calibration.
 *
 * @param arg Joystick axis to do the operation on.
 */
void calibrations(void *arg);

#endif /* TASKS_H */
