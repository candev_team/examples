#include "cal.h"
#include "init.h"
#include <adc/adc.h>    /* ADC driver */

/* Joystick calibration, only executed if trim was active during power-on. */
void calibrate(joyAxis *ja)
{
  uint16_t adc, adc1, adc2, adc3;

  adc1 = adc_sample_get(ja->adc);
  adc2 = adc_sample_get(ja->adc);
  adc3 = adc_sample_get(ja->adc);
  adc = (adc1+adc2+adc3)/3; /* Middle position */

  if (ja->reset) {
    ja->mid = adc;
    ja->lpAdc = ja->mid;
  }
  /* Low-pass filtered ADC reading (max 14-bit) for noise suppression */
  ja->lpAdc = (2 * ja->lpAdc + adc + 2) / 3; /* 2 is to round, rather than trunc */

  /* While trim is active during power-on */
  if (ja->reset)
  {
    ja->min = ja->mid - 200;        /* Min is initially mid -200 */
    ja->max = ja->mid + 200;        /* Max is initially mid +200 */
    ja->reset = 0;
  }
  /* Later lo/hi watermark as joystick
     is moved throughout its range. */
  else if (ja->lpAdc < ja->min)
  {
    ja->min = ja->lpAdc;
  }
   else if (ja->lpAdc > ja->max)
  {
    ja->max = ja->lpAdc;
  }
  /* No calibration adjustment, don't restart timer */
  else
  {
    return;
  }
  rprintp("min = %d \n", ja->min);
  rprintp("mid = %d \n", ja->mid);
  rprintp("max = %d \n", ja->max);
  /* Start new save delay period after calibration adjustment */
  tmr_stop(ja->saveTimer);
  tmr_set(ja->saveTimer, tick_from_ms (SAVE_DLY_MS));
  tmr_start(ja->saveTimer);
}

/* Restore joystick calibration from file, return non-zero on first. */
int restoreCalibration(joyAxis *ja)
{
  int file;
  struct stat status;
  if (stat(ja->fileName, &status) != 0)
     return 1; /* File not found? */

  file = open(ja->fileName, O_RDONLY, 0);
  read(file, &ja->min, sizeof ja->min);
  read(file, &ja->mid, sizeof ja->mid);
  read(file, &ja->max, sizeof ja->max);
  close(file);
  return 0;
}

/* Save joystick calibration to file. */
void saveCalibration(joyAxis *ja)
{
  int file;
  file = open(ja->fileName, O_CREAT | O_WRONLY, 0);
  write(file, &ja->min, sizeof ja->min);
  write(file, &ja->mid, sizeof ja->mid);
  write(file, &ja->max, sizeof ja->max);
  close(file);
}
