/**
 *  @file joystick/src/cal.h
 *  @brief Calibration functions.
 */

#ifndef _CAL_H
#define _CAL_H

#include "ckJoystick.h"

#define SAVE_DLY_MS  5000  /* Calibration save delay {ms} */

/**
 * @brief Used in order to Calibrate the reach of each axis.
 *
 * Uses the sampled mean of the ADC in combination with a simple low-pass filter
 * in order to give new values to the min, mid and max of the joystick.
 *
 * @param ja Joystick axis to do the operation on.
 */
void calibrate(joyAxis *ja);

/**
 * @brief Restore joystick calibration from file.
 *
 * @param ja Joystick axis to do the operation on.
 * @return Non-zero if no calibration file found.
 */
int restoreCalibration(joyAxis *ja);

/**
 * @brief Saves the calibration to file.
 *
 * @param ja Joystick axis to do the operation on.
 */
void saveCalibration(joyAxis *ja);

#endif /* _CAL_H */
