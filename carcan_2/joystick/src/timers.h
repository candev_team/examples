/**
 *  @file joystick/src/timers.h
 *  @brief Functions relating to timers.
 */

#ifndef _TIMERS_H
#define _TIMERS_H

/*--- Timer callback --------------------------------------------------------*/

/**
 * @brief Callback function. Starts a task for sending joystick position.
 * @param timer Calling timer handle.
 * @param arg Joystick axis to do the operation on.
 */
void sendPosTask(tmr_t *timer, void *arg);

/**
 * @brief Callback function. Starts a task for sending trim button positions.
 * @param timer Calling timer handle.
 * @param arg Joystick axis to do the operation on.
 */
void sendTrimTask(tmr_t *timer, void *arg);

/**
 * @brief Callback function. Starts a task for sending switch position.
 * @param timer Calling timer handle.
 * @param arg Joystick axis to do the operation on.
 */
void sendSwTask(tmr_t *timer, void *arg);

/**
 * @brief Callback function. Starts a task for the calibration procedure.
 * @param timer Calling timer handle.
 * @param arg Joystick axis to do the operation on.
 */
void calibrationInterval(tmr_t*timer, void *arg);

/**
 * @brief Callback function. Starts a task for saving calibrations.
 * @param timer Calling timer handle.
 * @param arg Joystick axis to do the operation on.
 */
void onSaveTimer(tmr_t *timer, void *arg);

/*--- Timer functions -------------------------------------------------------*/
/**
 * @brief Starts all timers that can trigger tasks.
 *
 * Starts all timers that can trigger tasks in the correct order
 * with the correct delays in between the timers.
 * This startup phase always ends after one hyperperiod.
 *
 * @warning Calling this while there are timers already
 * running in the system could cause problems.
 *
 * @param js Joystick to do the operation on.
 */
void startTimers(joystick *js);

/**
 * @brief Stops all timers that can trigger tasks.
 *
 * @warning Calling this while there are timers already
 * running in the system could cause problems.
 *
 * @param js Joystick to do the operation on.
 */
void stopTimers(joystick *js);

/**
 * @brief Destroys all timers that can trigger tasks.
 *
 * @warning This is an unsafe operation if timers are not stopped!
 *
 * @param js Joystick to do the operation on.
 */
void destroyTimers(joystick *js);


#endif /* TIMERS_H */
