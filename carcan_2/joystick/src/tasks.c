/**
 *  @file joystick/src/tasks.c
 *  @brief Functions used internally by task callbacks.
 */

#include "tasks.h"
#include "ckJoystick.h" /* CAN Kingdom documents */
#include "cal.h"
#include <stdint.h>
#include <tmr.h>        /* RT-Kernel timer (tmr_t) */
#include <kern.h>       /* RT-Kernel task (task_t) */
#include <adc/adc.h>    /* ADC driver */
#include <gpio.h>       /* GPIO driver */

#define TRIM_POS 0x1    /* Trim signals bit encodings */
#define TRIM_NEG 0x2

#define SWITCH_POS 0x1  /* Switch signals bit encodings */
#define SWITCH_NEG 0x0

/**
 * @brief Converts ADC raw data to unsigned 12-bit position signal.
 *
 * Converts the raw sampled ADC data by running it through a low-pass filter
 * and then scaling it to the correct position value.
 *
 * @warning Function will not work properly if calibrations is done poorly.
 * @param ja Joystick axis to do the operation on.
 * @param adc Sampled value to use.
 * @return 16-bit unsigned position value in the range of 0 to POS_MAX.
 */
static uint16_t adcToPos(joyAxis *ja, uint16_t adc)
{
  int32_t  pos;
  ja->lpAdc = (2 * ja->lpAdc + adc + 2) / 3; /* 2 is to round, rather than trunc */

  /* Is sampled value between min and mid? */
  if (ja->lpAdc <= ja->mid && ja->lpAdc >= ja->min)
  {
    pos = ((double)((ja->lpAdc-ja->min)*ADC_MID)/(ja->mid-ja->min));
  }
  /* Is sampled value between mid and max? */
  else if (ja->lpAdc > ja->mid && ja->lpAdc <= ja->max)
  {
    pos = ((double)((ja->lpAdc-ja->mid)*(ADC_MAX-ADC_MID))/(ja->max-ja->mid)+ADC_MID);
  }
  /* If sampled value is out of range of calibration, set the calibrated edge values */
  else if (ja->lpAdc < ja->min)
  {
    ja->lpAdc = ja->min;
  }
  else
  {
    ja->lpAdc = ja->max;
  }

  /* Limit to not overflow signal */
  if (pos >  POS_MAX) 
    pos = POS_MAX;
  if (pos < 0) 
    pos = 1;

  return (uint16_t)pos; /* Return unsigned */
}

void sendXOut(void* arg)
{
  joystick *js = arg;
  joyAxis *x = js->joyX; 
  uint16_t adc, adc1, adc2, adc3;
  uint16_t pos;
  for (;;)
  {
    task_stop(); /* Sync (wait for timer to start task) */
    if (js->stopTasks)
    {
      return;
    }

    /* Sample three times, then calculate mean */
    adc1 = adc_sample_get(x->adc);
    adc2 = adc_sample_get(x->adc);
    adc3 = adc_sample_get(x->adc);
    adc = (adc1+adc2+adc3)/3;

    pos = adcToPos(x, adc);

    /* Construct document and send */
    joyXOutDoc page;
    page.xOut = pos;
    sendJoyXOutDoc(&page);
  }
}

void sendYOut(void* arg)
{
  joystick *js = arg;
  joyAxis *y = js->joyY;
  uint16_t adc, adc1, adc2, adc3;
  uint16_t pos;
  for (;;)
  {
    task_stop(); /* Sync (wait for timer to start task) */
    if (js->stopTasks)
    {
      return;
    }

    /* Sample three times, then calculate mean */
    adc1 = adc_sample_get(y->adc);
    adc2 = adc_sample_get(y->adc);
    adc3 = adc_sample_get(y->adc);
    adc = (adc1+adc2+adc3)/3;

    pos = adcToPos(y, adc);

    /* Construct document and send */
    joyYOutDoc page;
    page.yOut = pos;
    sendJoyYOutDoc(&page);
  }
}

void sendTrim(void* arg)
{
  joystick *js = arg;

  uint8_t xTrim;
  uint8_t yTrim;
  for (;;)
  {
    task_stop(); /* Sync (wait for timer to start task) */
    if (js->stopTasks)
    {
      return;
    }

    /* Read trim buttons (negative logic) */
    xTrim = (!gpio_get(js->joyX->gpioPos)) ? TRIM_POS : 0;
    xTrim |= (!gpio_get(js->joyX->gpioNeg)) ? TRIM_NEG : 0;

    yTrim = (!gpio_get(js->joyY->gpioPos)) ? TRIM_POS : 0;
    yTrim |= (!gpio_get(js->joyY->gpioNeg)) ? TRIM_NEG : 0;

    /* Force servo mid reset on calibration power-on */
    if (js->joyX->reset && xTrim != 0)
    {
      xTrim = TRIM_POS | TRIM_NEG; /* Force servo mid pos reset */
    }
    else
    {
      js->joyX->reset = 0; /* Trim button no longer pressed */
    }

    if (js->joyY->reset && yTrim != 0)
    {
      yTrim = TRIM_POS | TRIM_NEG; /* Force servo mid pos reset */
    }
    else
    {
      js->joyY->reset = 0; /* Trim button no longer pressed */
    }

    /* Construct document and send */
    trimDoc page;
    page.xTrim = xTrim;
    page.yTrim = yTrim;
    sendTrimDoc(&page);
  }
}

void sendSwitch(void* arg)
{
  joystick *js = arg;
  uint32_t pressed; /* If switch is pressed */
  for (;;)
  {
    task_stop(); /* Sync (wait for timer to start task) */
    if (js->stopTasks)
    {
      return;
    }

    pressed = (!gpio_get(js->sw1Up)) ? SWITCH_POS : 0;
    pressed |= (!gpio_get(js->sw1Down)) ? SWITCH_NEG : 0;

    /* Construct document and send */
    switchDoc page;
    page.switch3w = pressed;
    sendSwitchDoc(&page);
  }
}

void runTasksOnce(joystick *js) 
{
  uint8_t i;
  for (i = 0; i < js->numTasks; i++)
  {
    task_start(js->tasks[i]);
  }
}

void calibrations(void *arg){
  joyAxis *ja =arg;
  for(;;){
    task_stop();

    /* Save calibration if delay has elapsed */
    if (ja->save){
      saveCalibration(ja);
      ja->save = 0;
      ja->cal = 0;
      tmr_stop(ja->calibrationInterval);
      tmr_destroy(ja->calibrationInterval);
      return;
    }

    if (ja->cal){
      calibrate(ja);
    }
  }
}
