/**
 *  @file joystick/src/init.h
 *  @brief Initialization functinons.
 */

#ifndef _INIT_H
#define _INIT_H

#include "ckJoystick.h"

#define HYPERPERIOD       200  /* Hyperperiod of the system. */
#define TX_PERIOD_POS       5  /* CAN transmission period {ms} position msg */
#define TX_PERIOD_TRIM    200  /* CAN transmission period {ms} trim msg */
#define TX_PERIOD_SW      200  /* CAN transmission period {ms} switch msg */
#define TX_OFFSET_POS_J1    0
#define TX_OFFSET_TRIM_J1  32  /* CAN transmission offset {ms} trim joystick 1 */
#define TX_OFFSET_SW_J1   120  /* CAN transmission offset {ms} switch joystick 1 */
#define TX_OFFSET_POS_J2    1  /* CAN transmission offset {ms} pos joystick 2 */
#define TX_OFFSET_TRIM_J2  81  /* CAN transmission offset {ms} trim joystick 2 */

/**
 * @brief Initializes a joystick with two axes.
 *
 * @param js      Joystick to do the operation on.
 * @param swUp    Switch connection.
 * @param swDown  Switch connection.
 */
void joyInit(joystick *js, SWx swUp, SWx swDown);

/**
 * @brief Initializes all tasks needed for operation.
 *
 * @param js Joystick to do the operation on.
 */
void taskInit(joystick *js);

#endif /* _INIT_H */
