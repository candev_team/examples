/**
 *  @file joystick/src/init.c
 *  @brief Functions used internally by init functions.
 */


#include "ckJoystick.h"
#include "tasks.h"
#include "init.h"
#include "timers.h"
#include "cal.h"

#include <gpio.h>       /* GPIO driver */
#include <adc/adc.h>    /* ADC driver */

/*! ADC device names */
static const char *adcDev[] = {
  "/adc1/1", "/adc1/2", "/adc1/3", "/adc1/4"
};

/*! Calibration data file names */
static const char *calFile[] = { 
  "/disk1/an0.cal", "/disk1/an1.cal", "/disk1/an2.cal", "/disk1/an3.cal"
};

/*! Trim connections (negative logic) */
static const int mapSW[] = { 
  GPIO_PB10, GPIO_PB11, GPIO_PC15, GPIO_PC6, 
  GPIO_PA4, GPIO_PA5, GPIO_PA6, GPIO_PC13
};

/* Stub: Just to get around the ttos bsp linker error */
void bsp_tt_init()
{
}

/*--- Initialization --------------------------------------------------------*/

/**
 * @brief Initializes a joystick axis.
 *
 * Two connections are needed for one trim button.
 *
 * @param ja  Joystick axis to do the operation on.
 * @param pot Potentiometer to assign to the joyAxis.
 * @param posTrim Trim button connection to assign to this joyAxis.
 * @param negTrim Trim button connection to assign to this joyAxis.
 */
static void joyInitAxis(joyAxis *ja, ANx pot, SWx posTrim, SWx negTrim)
{
  /* Set VDD_IO_LEVEL to 3.3 volt, as this is the ADC ref voltage. However, be
   aware the PCB doesn't actually put ref voltage on ANx sockets. Don't expect
   stellar precision with ratiometric sensors. */
  gpio_set(GPIO_PB2, 0); /* GPIO_PB2 values, 0: 3.3 V, 1: 5.0 V */

  ja->adc = adc_open(adcDev[pot]);  /* Init, start continuous conversion */
  ASSERT(ja->adc > 0);
  adc_start(ja->adc);               /* Pin already configured in BSP */

  ja->gpioPos = mapSW[posTrim];     /* Trim buttons connection */
  ja->gpioNeg = mapSW[negTrim];     /* Pin and GPIO already configured in BSP */
  ja->fileName = calFile[pot];      /* Calibration data flash file */
  ja->cal = !gpio_get(ja->gpioPos)  /* Perform min/mid/max calibration */
  || !gpio_get(ja->gpioNeg)         /* if trim pressed (negative logic) */
  || restoreCalibration(ja);        /* or calibration file not read. */
  ja->reset = ja->cal;              /* High while power-on trim pressed */
  rprintf("Calibrate @1 = %d \n" , ja->reset);
  if (!ja->cal)
  {
    ja->lpAdc = ja->mid;
  }
  else
  {
    ja->save = 0; /* Don't save calibration data just yet */
    /* Init calibration task and timer */
    ja->calibrationTa = task_spawn("Cal Task", calibrations,
          TX_TASK_PRIO, TX_TASK_STACK, ja);
    ja->calibrationInterval = tmr_create(tick_from_ms (100), calibrationInterval, ja,
        TMR_CYCL);
    /* Delayed calibration save timer */
    ja->saveTimer = tmr_create(tick_from_ms (SAVE_DLY_MS), onSaveTimer, ja,
      TMR_ONCE);

    /* Start calibration task and timer */
    tmr_start(ja->calibrationInterval);
    tmr_start(ja->saveTimer);
    }
}
void joyInit(joystick *js, SWx swUp, SWx swDown)
{
  js->stopTasks = 0;
  js->running = 0;                    /* Default to off */
  js->joySel = LJoystick;             /* Default to left joystick */
  js->numTasks = 0;
  /* Init joystick channels */
  joyInitAxis(js->joyX, AN0, SW0, SW1);
  joyInitAxis(js->joyY, AN1, SW2, SW3);
  js->sw1Up = mapSW[swUp];            /* Switch connection */
  js->sw1Down = mapSW[swDown];
}

/* Init transmission tasks */
void taskInit(joystick *js)
{

  if (js->joySel == LJoystick) 
  {
    js->numTasks = 3;

    /* set tasks & timers in system */
    js->tasks[0] = task_spawn("CAN Tx Main", sendXOut,
      TX_TASK_PRIO, TX_TASK_STACK, js);
    js->timers[0] = tmr_create(tick_from_ms (TX_PERIOD_POS), sendPosTask, js,
      TMR_CYCL);
    js->offsets[0] = TX_OFFSET_POS_J1;

    js->tasks[1] = task_spawn("CAN Tx Second", sendTrim,
      TX_TASK_PRIO, TX_TASK_STACK, js);
    js->timers[1] = tmr_create(tick_from_ms (TX_PERIOD_TRIM), sendTrimTask, js,
      TMR_CYCL);
    js->offsets[1] = TX_OFFSET_TRIM_J1;

    js->tasks[2] = task_spawn("CAN Tx Third", sendSwitch,
      TX_TASK_PRIO, TX_TASK_STACK, js);
    js->timers[2] = tmr_create(tick_from_ms(TX_PERIOD_SW),
      sendSwTask, js, TMR_CYCL);
    js->offsets[2] = TX_OFFSET_SW_J1;
  }
  else
  {
    js->numTasks = 2;

    /* set tasks & timers in system */
    js->tasks[0] = task_spawn("CAN Tx Main", sendYOut,
      TX_TASK_PRIO, TX_TASK_STACK, js);
    js->timers[0] = tmr_create(tick_from_ms(TX_PERIOD_POS),
      sendPosTask, js, TMR_CYCL);
    js->offsets[0] = TX_OFFSET_POS_J2;

    js->tasks[1] = task_spawn("CAN Tx Second", sendTrim,
      TX_TASK_PRIO, TX_TASK_STACK, js);
    js->timers[1] = tmr_create(tick_from_ms(TX_PERIOD_TRIM),
      sendTrimTask, js, TMR_CYCL);
    js->offsets[1] = TX_OFFSET_TRIM_J2;
  }
}
