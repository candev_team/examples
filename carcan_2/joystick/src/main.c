/*---------------------------------------------------------------------------*
 Sample dual CAN Node Joystick Controller

 See "xxxx" for details.

 2019-01-27  André Idoffsson & Hashem Hashem for Kvaser AB, Sweden

 \*---------------------------------------------------------------------------*/

#include <shell.h>      /* shell_prompt_set() */
#include "ckJoystick.h"
#include "tasks.h"
#include <gpio.h>       /* GPIO driver */
#include <adc/adc.h>    /* ADC driver */

/* Proxy to run CAN Kingdom from RT-Kernel task signature */
static void ckTaskFunc(void *arg)
{
  ckappMain(); /* CAN Kingdom is Singleton (no args, assumes 1 interface) */
}

/*--- Main function ---------------------------------------------------------*/
/* rprintp() for time critical prints else rprintf()*/
int main (void)
{
  //shell_prompt_set("joystick> \n"); /* RS-232 terminal prompt (115 kbps) */
  //task_delay(tick_from_ms (20));

  /* Fire up CAN Kingdom task */
  task_spawn("CAN Kingdom", ckTaskFunc,
    CK_TASK_PRIO, CK_TASK_STACK, NULL);

  return 0;

}
