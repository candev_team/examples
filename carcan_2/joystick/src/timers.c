#include "tasks.h"
#include "timers.h"
#include "init.h"

/* Timer callbacks */
void sendPosTask(tmr_t *timer, void *arg)
{
  joystick *js = arg; /* Our type */
  task_start(js->tasks[0]);
}

void sendTrimTask(tmr_t *timer, void *arg)
{
  joystick *js = arg; /* Our type */
  task_start(js->tasks[1]);
}

void sendSwTask(tmr_t *timer, void *arg)
{
  joystick *js = arg; /* Our type */
  task_start(js->tasks[2]);
}

void calibrationInterval(tmr_t*timer, void *arg)
{
  joyAxis *ja = arg;
  task_start(ja->calibrationTa);
}

/* Save delayed period expired. */
void onSaveTimer(tmr_t *timer, void *arg)
{
  joyAxis *ja = arg;
  tmr_stop (timer); /* Let periodic tx task do the actual save. We're in */
  ja->save = 1; /*   interrupt context, free stack may be small.     */
  rprintp("save \n");
  rprintp("min = %d \n", ja->min);
  rprintp("mid = %d \n", ja->mid);
  rprintp("max = %d \n", ja->max);
}


/* Start all timers for a joystick */
void startTimers(joystick *js)
{
  uint8_t i;
  uint8_t delay = HYPERPERIOD;
  for (i = 0; i < js->numTasks; i++)
  {
    delay -= js->offsets[i];

    if (js->offsets[i])
      task_delay(tick_from_ms(js->offsets[i]));
    tmr_start(js->timers[i]);
  }
  task_delay(tick_from_ms(delay));
}

/* Stop all timers for a joystick */
void stopTimers(joystick *js)
{
  uint8_t i;
    for (i = 0; i < js->numTasks; i++)
    {
      tmr_stop(js->timers[i]);
    }
}

void destroyTimers(joystick *js)
{
  uint8_t i;
    for (i = 0; i < js->numTasks; i++)
    {
      tmr_destroy(js->timers[i]);
    }
}
