-- Test Lua-gpio integration

function print_array(array)
  for i, _ in ipairs(array) do
    print(array[i])
  end
end

-- Basic tests (no errors). Uses first two pins in GPIO table
pin_names = gpio.get_pin_names()
print_array(pin_names)
pin1 = gpio.create(pin_names[1])
pin2 = gpio.create(pin_names[2])
pin1:set(1)
pin2:set(0)
assert(pin1:get() == 1)
assert(pin2:get() == 0)
pin1:set(0)
pin2:set(1)
assert(pin1:get() == 0)
assert(pin2:get() == 1)

-- Test create() error handling
assert(pcall(gpio.create, 3) == false) -- 'string expected'
assert(pcall(gpio.create, 'BAD') == false) -- 'invalid pin name'

-- Test get() error handling
assert(pcall(pin1.get, pin1, 1) == false) -- 'wrong number of arguments'
assert(pcall(pin1.get, nil) == false) -- 'gpio.pin extected'

-- Test set() error handling
assert(pcall(pin1.set, pin1) == false) -- 'wrong number of arguments'
assert(pcall(pin1.set, nil, 1) == false) -- 'gpio.pin expected'
assert(pcall(pin1.set, pin1, 2) == false) -- 'invalid level'
assert(pcall(pin1.set, pin1, -1) == false) -- 'invalid level'
