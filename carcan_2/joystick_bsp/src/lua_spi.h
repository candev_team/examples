/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#ifndef LUA_SPI_H
#define LUA_SPI_H

#ifdef __cplusplus
extern "C"
{
#endif

#include <lua.h>

#define LUA_SPI_LIBNAME "spi"

/**
 * Create Lua library which extents Lua with a SPI slave class.
 *
 * Called by interpreter using luaL_requiref().
 *
 * @param L    Lua interpreter.
 * @return     1, always.
 */
int lua_spi_openlib (lua_State * L);

#ifdef __cplusplus
}
#endif

#endif /* LUA_SPI_H */
