/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#include <bsp.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int luainterpreter_run_script (const char * file)
{
   int error;

   lua_State* L = luaL_newstate ();
   if (L == NULL)
   {
      const char * msg = "cannot create state: not enough memory";
      lua_writestringerror ("lua: %s\n", msg);
      return 1;
   }
   luaL_openlibs (L);

   error = luaL_dofile (L, file);
   if (error)
   {
      /* Error message was pushed on Lua stack */
      const char * msg = lua_tostring (L, -1);
      lua_writestringerror ("lua: %s\n", msg);
      lua_pop (L, 1);
   }

   lua_close (L);
   return error;
}

