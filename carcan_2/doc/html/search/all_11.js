var searchData=
[
  ['rdocking',['rdocKing',['../ck_8h.html#a20e92fb87532bb61e396c1db7e577a70',1,'ck.h']]],
  ['reset',['reset',['../structjoyAxis.html#adb7a760512715e86f925f8a8d4a15ba8',1,'joyAxis']]],
  ['respenv',['respEnv',['../ck_8c.html#afc30a42e91650482ac04bfb55794054d',1,'ck.c']]],
  ['restorecalibration',['restoreCalibration',['../cal_8c.html#a182ad4bfc270184029b12db05c605763',1,'restoreCalibration(joyAxis *ja):&#160;cal.c'],['../cal_8h.html#a182ad4bfc270184029b12db05c605763',1,'restoreCalibration(joyAxis *ja):&#160;cal.c']]],
  ['rjoystick',['RJoystick',['../joystick_2src_2ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195ca97250b1bc73c8f3552e17785854f2641',1,'ckJoystick.h']]],
  ['rjoystick_5fmode',['RJoystick_mode',['../capital_2src_2system_8h.html#a41cb5f9bd5d227266ea81e5df4f32f36',1,'system.h']]],
  ['rmotor',['RMotor',['../servo_2src_2ckServo_8h.html#ade36793e70b096c3178a5eac1635bc71ad1b50a315eb4ebc53d274fc9074dbc23',1,'ckServo.h']]],
  ['rmotor_5fmode',['RMotor_mode',['../capital_2src_2system_8h.html#ad2841b3203832eb96ad7e9528ab9fdae',1,'system.h']]],
  ['running',['running',['../structjoystick.html#af77f8244799e85284b8b438289f5f689',1,'joystick']]],
  ['runtasksonce',['runTasksOnce',['../joystick_2src_2tasks_8c.html#a17be9637240961a23c77ddea2a303ee0',1,'runTasksOnce(joystick *js):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a17be9637240961a23c77ddea2a303ee0',1,'runTasksOnce(joystick *js):&#160;tasks.c']]],
  ['rx_5ftask_5fprio',['RX_TASK_PRIO',['../servo_2src_2ckServo_8h.html#a496f884f882bd072890388d019511ffc',1,'ckServo.h']]],
  ['rx_5ftask_5fstack',['RX_TASK_STACK',['../servo_2src_2ckServo_8h.html#a3d759ee2a95245d9a197f8b331647cd4',1,'ckServo.h']]]
];
