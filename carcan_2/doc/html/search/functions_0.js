var searchData=
[
  ['calibrate',['calibrate',['../cal_8h.html#a3739116f7eacd77d2f0e7c2fa9016bdc',1,'cal.c']]],
  ['calibrationinterval',['calibrationInterval',['../timers_8h.html#ab2e9db087e89a8c144033cfcacb867c4',1,'timers.c']]],
  ['calibrations',['calibrations',['../joystick_2src_2tasks_8c.html#a0a2927adad61a38cfe9909d40633c2c6',1,'calibrations(void *arg):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a0a2927adad61a38cfe9909d40633c2c6',1,'calibrations(void *arg):&#160;tasks.c']]],
  ['changejoystick',['changeJoystick',['../ckJoystick__handlers_8c.html#a03d5d2e208cf272a4bdb154c7a33f4cb',1,'ckJoystick_handlers.c']]],
  ['checkshutdown',['checkShutDown',['../system_8c.html#a8da27ac29e8184c2e2c1e32888188a80',1,'system.c']]],
  ['ckappmain',['ckappMain',['../ckJoystick_8h.html#ab11ea6d13c83be83783fde46004c1f5f',1,'ckappMain(void):&#160;ckJoystick_handlers.c'],['../ckJoystick__handlers_8c.html#ab11ea6d13c83be83783fde46004c1f5f',1,'ckappMain(void):&#160;ckJoystick_handlers.c'],['../ckServo_8h.html#ab11ea6d13c83be83783fde46004c1f5f',1,'ckappMain(void):&#160;ckJoystick_handlers.c'],['../ckServo__handlers_8c.html#ab11ea6d13c83be83783fde46004c1f5f',1,'ckappMain(void):&#160;ckServo_handlers.c']]],
  ['ckapponidle',['ckappOnIdle',['../ckJoystick_8h.html#a6a705966f68158f66b39fa5531c1e5af',1,'ckappOnIdle(void):&#160;ckJoystick_handlers.c'],['../ckJoystick__handlers_8c.html#a6a705966f68158f66b39fa5531c1e5af',1,'ckappOnIdle(void):&#160;ckJoystick_handlers.c'],['../ckServo__handlers_8c.html#a6a705966f68158f66b39fa5531c1e5af',1,'ckappOnIdle(void):&#160;ckServo_handlers.c']]],
  ['ckappsetactionmode',['ckappSetActionMode',['../ckJoystick__handlers_8c.html#abe63903cd5436ba28aae3307f6a0ba19',1,'ckJoystick_handlers.c']]],
  ['ckappsetcitymode',['ckappSetCityMode',['../ckJoystick__handlers_8c.html#a95b5567807de5df61007aa3802df26d0',1,'ckappSetCityMode(uint8_t mode):&#160;ckJoystick_handlers.c'],['../ckServo__handlers_8c.html#a95b5567807de5df61007aa3802df26d0',1,'ckappSetCityMode(uint8_t mode):&#160;ckServo_handlers.c']]],
  ['ckkingcheck',['ckKingCheck',['../system_8c.html#a5fff6586ed2707b1e22525563e4ec92e',1,'system.c']]],
  ['ckkingdefinefolders',['ckKingDefineFolders',['../system_8c.html#a8e9aae3a74f0ceb736878d98778f973c',1,'system.c']]],
  ['ckkingmain',['ckKingMain',['../system_8c.html#a07241efc71d28eab18927a0501e80925',1,'system.c']]]
];
