var searchData=
[
  ['savecalibration',['saveCalibration',['../cal_8h.html#ad6b8c90b33de3512862053f2cac2bdbb',1,'cal.c']]],
  ['senddefaultletter',['sendDefaultLetter',['../system_8c.html#acf5f7d5deb5d1db7bcc8cf025732220a',1,'system.c']]],
  ['sendjoyxoutdoc',['sendJoyXOutDoc',['../ckJoystick_8h.html#a781347c824f64c716005769a2e9b47e8',1,'ckJoystick_dispatch.c']]],
  ['sendjoyyoutdoc',['sendJoyYOutDoc',['../ckJoystick_8h.html#aac24eff2e145eef6eb0fa66094e2afb2',1,'ckJoystick_dispatch.c']]],
  ['sendpostask',['sendPosTask',['../timers_8h.html#a2dfec9104ebb3b13ab7f478b90dc9c23',1,'timers.c']]],
  ['sendswitch',['sendSwitch',['../joystick_2src_2tasks_8c.html#a3f69284b13c3cdb78aaf32f24a391bff',1,'sendSwitch(void *arg):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a3f69284b13c3cdb78aaf32f24a391bff',1,'sendSwitch(void *arg):&#160;tasks.c']]],
  ['sendswitchdoc',['sendSwitchDoc',['../ckJoystick_8h.html#a092f14d94deb2843b569b4f8a6070f23',1,'ckJoystick_dispatch.c']]],
  ['sendswtask',['sendSwTask',['../timers_8h.html#a460b5f5a45839913365e4eddecece5dc',1,'timers.c']]],
  ['sendtrim',['sendTrim',['../joystick_2src_2tasks_8c.html#a21dd849478cd294a2037b00d7e695d48',1,'sendTrim(void *arg):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a21dd849478cd294a2037b00d7e695d48',1,'sendTrim(void *arg):&#160;tasks.c']]],
  ['sendtrimdoc',['sendTrimDoc',['../ckJoystick_8h.html#a14a66b0b2034d3f6462acf5a42a1ac5e',1,'ckJoystick_dispatch.c']]],
  ['sendtrimtask',['sendTrimTask',['../timers_8h.html#a614a60baa6a29fe9764af47d15c7cb7c',1,'timers.c']]],
  ['sendxout',['sendXOut',['../joystick_2src_2tasks_8c.html#a9b34b1a0d9347c8fe25917c0915cf873',1,'sendXOut(void *arg):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a9b34b1a0d9347c8fe25917c0915cf873',1,'sendXOut(void *arg):&#160;tasks.c']]],
  ['sendyout',['sendYOut',['../joystick_2src_2tasks_8c.html#a51b3e21ed733337cfae1a0b26854583e',1,'sendYOut(void *arg):&#160;tasks.c'],['../joystick_2src_2tasks_8h.html#a51b3e21ed733337cfae1a0b26854583e',1,'sendYOut(void *arg):&#160;tasks.c']]],
  ['starttimers',['startTimers',['../timers_8h.html#aacc27e3b5bdb701a5f679eff2c6d4827',1,'timers.c']]],
  ['stoptimers',['stopTimers',['../timers_8h.html#a2329ff3db24b5439bc6c7b7795134554',1,'timers.c']]],
  ['svoinit',['svoInit',['../ckServo_8h.html#aaae63c305790b833d0f3411cc2e29e9f',1,'svoInit(Servo *s):&#160;ckServo_handlers.c'],['../ckServo__handlers_8c.html#aaae63c305790b833d0f3411cc2e29e9f',1,'svoInit(Servo *s):&#160;ckServo_handlers.c']]]
];
