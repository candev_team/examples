var searchData=
[
  ['save',['save',['../structjoyAxis.html#abeb8baadb5d54f3e7a43fff7051f519f',1,'joyAxis']]],
  ['savetimer',['saveTimer',['../structjoyAxis.html#a0602ab53e47c4f19cb02cad169656b7e',1,'joyAxis']]],
  ['servo',['servo',['../servo_2src_2ckServo__dispatch_8c.html#a79efceea669fb85a732c30f47cf7e59c',1,'servo():&#160;ckServo_handlers.c'],['../servo_2src_2ckServo__handlers_8c.html#a79efceea669fb85a732c30f47cf7e59c',1,'servo():&#160;ckServo_handlers.c']]],
  ['stop',['stop',['../structServo.html#a6c0af9f2e97842405fb15ed952ef2976',1,'Servo']]],
  ['stoptasks',['stopTasks',['../structjoystick.html#acd1602f87ac1ac56488cc5c7f37b6d1c',1,'joystick']]],
  ['stoptimer',['stopTimer',['../structServo.html#ab959758347cbf365af111a05fb35cbf7',1,'Servo']]],
  ['sw1down',['sw1Down',['../structjoystick.html#ac01d49c0517fcfd58b5bdc775ca727ab',1,'joystick']]],
  ['sw1up',['sw1Up',['../structjoystick.html#ad8b1246b4daee2c6c3fb6ecbf423cad4',1,'joystick']]],
  ['switch3w',['switch3w',['../structswitchDoc.html#ade10aa389bbc94c56db2417764d6a3e3',1,'switchDoc']]],
  ['switch_5fval',['switch_val',['../structswitchDoc.html#a2142c77633e3a63192c86095017dd1a5',1,'switchDoc']]]
];
