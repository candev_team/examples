var searchData=
[
  ['joyaxis',['joyAxis',['../structjoyAxis.html',1,'']]],
  ['joyinit',['joyInit',['../init_8c.html#a11e133126c2eec5b6f9ddf91a0306401',1,'joyInit(joystick *js, SWx swUp, SWx swDown):&#160;init.c'],['../init_8h.html#a11e133126c2eec5b6f9ddf91a0306401',1,'joyInit(joystick *js, SWx swUp, SWx swDown):&#160;init.c']]],
  ['joysel',['joySel',['../structjoystick.html#ad867a2ebb8c5bc540cad4b6cfb3c80b8',1,'joystick']]],
  ['joyselector',['joySelector',['../ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195c',1,'ckJoystick.h']]],
  ['joystick',['joystick',['../structjoystick.html',1,'']]],
  ['joyx',['joyX',['../structjoystick.html#ac7c59fd17eb96b1c20fd5c25f12325c1',1,'joystick']]],
  ['joyy',['joyY',['../structjoystick.html#a1aaf596094a545facaf93d41085815d8',1,'joystick']]]
];
