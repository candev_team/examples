var searchData=
[
  ['taskinit',['taskInit',['../init_8c.html#a2d23a4220b61cc37db1425090b0b9abd',1,'taskInit(joystick *js):&#160;init.c'],['../init_8h.html#a2d23a4220b61cc37db1425090b0b9abd',1,'taskInit(joystick *js):&#160;init.c']]],
  ['tasks',['tasks',['../structjoystick.html#a3729781d9e2d5ad42432002e34810a67',1,'joystick::tasks()'],['../structServo.html#adca97067697491f60be1b66366ccdf27',1,'Servo::tasks()']]],
  ['tasks_2ec',['tasks.c',['../joystick_2src_2tasks_8c.html',1,'']]],
  ['tasks_2eh',['tasks.h',['../joystick_2src_2tasks_8h.html',1,'(Global Namespace)'],['../servo_2src_2tasks_8h.html',1,'(Global Namespace)']]],
  ['timers',['timers',['../structjoystick.html#ab090a5b3ca986f4054777f3962f89d9b',1,'joystick']]],
  ['timers_2eh',['timers.h',['../timers_8h.html',1,'']]],
  ['type',['type',['../structServo.html#a2696e560f6434009cc17f3216cd0056e',1,'Servo']]]
];
