var structServo =
[
    [ "aliveTimer", "structServo.html#a338f5ab771a97260969a3fb381879991", null ],
    [ "dir", "structServo.html#acafc3c424be4cc2cba6579b7954e45a3", null ],
    [ "lastSwitch", "structServo.html#a7f86928f406171c212cf339beca5cc42", null ],
    [ "lastTrimTick", "structServo.html#ae1adc498c4b4fc89b3587cd6a145d995", null ],
    [ "lastXPos", "structServo.html#a45c0083c8b6ec1b44a85a99e96872c49", null ],
    [ "lastXTrim", "structServo.html#a93b439f250632449aca82e78ea4fb2e5", null ],
    [ "lastYPos", "structServo.html#a938aadba06a6485e45608709710296e9", null ],
    [ "lastYTrim", "structServo.html#a027a46dfc13dd2726bf13a37c6cdebc3", null ],
    [ "stop", "structServo.html#a6c0af9f2e97842405fb15ed952ef2976", null ],
    [ "stopTimer", "structServo.html#ab959758347cbf365af111a05fb35cbf7", null ],
    [ "tasks", "structServo.html#adca97067697491f60be1b66366ccdf27", null ],
    [ "type", "structServo.html#a2696e560f6434009cc17f3216cd0056e", null ]
];