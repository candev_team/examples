var structjoyAxis =
[
    [ "adc", "structjoyAxis.html#ae5d88649401f796b100a8ffb867b5507", null ],
    [ "calibrationInterval", "structjoyAxis.html#a3cae54c0237f738ddf708354b80ae88e", null ],
    [ "calibrationTa", "structjoyAxis.html#aa20cfc11d7d5eeea813abc43b73de070", null ],
    [ "fileName", "structjoyAxis.html#a1862cb72b37ffe98140c1fcf06a28969", null ],
    [ "gpioNeg", "structjoyAxis.html#ab1afe8ed791829f410700e5a277b977d", null ],
    [ "lpAdc", "structjoyAxis.html#ad94834937778e76b2a957f2ecb44a464", null ],
    [ "max", "structjoyAxis.html#ac66b569507cc273bbf83ce5dd5f70e84", null ],
    [ "save", "structjoyAxis.html#abeb8baadb5d54f3e7a43fff7051f519f", null ],
    [ "saveTimer", "structjoyAxis.html#a0602ab53e47c4f19cb02cad169656b7e", null ]
];