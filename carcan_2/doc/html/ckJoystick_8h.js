var ckJoystick_8h =
[
    [ "joyAxis", "structjoyAxis.html", "structjoyAxis" ],
    [ "joystick", "structjoystick.html", "structjoystick" ],
    [ "CKDN_joyXOutDoc", "ckJoystick_8h.html#a626a8cb5fd17b5ae5621aed531c9dc6e", null ],
    [ "CKDN_joyYOutDoc", "ckJoystick_8h.html#ac957633dde3286a410ab72900aebbb68", null ],
    [ "CKDN_switchDoc", "ckJoystick_8h.html#a9fdcd8383e94d76d5622bba39a690652", null ],
    [ "CKDN_trimDoc", "ckJoystick_8h.html#ae706d90360fd1d810be13ef14d1c6a23", null ],
    [ "ANx", "ckJoystick_8h.html#ad6ed457253192a246fdf309eb964b9c6", null ],
    [ "IOx", "ckJoystick_8h.html#a855959eda89debb006f1ad9104c5d6c4", null ],
    [ "SWx", "ckJoystick_8h.html#a572f6b6f306f033e26b1704de1a6dc1b", null ],
    [ "ANx", "ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81", null ],
    [ "IOx", "ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1", null ],
    [ "joySelector", "ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195c", null ],
    [ "SWx", "ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6", null ],
    [ "ckappMain", "ckJoystick_8h.html#ab11ea6d13c83be83783fde46004c1f5f", null ],
    [ "ckappOnIdle", "ckJoystick_8h.html#a6a705966f68158f66b39fa5531c1e5af", null ],
    [ "sendJoyXOutDoc", "ckJoystick_8h.html#a781347c824f64c716005769a2e9b47e8", null ],
    [ "sendJoyYOutDoc", "ckJoystick_8h.html#aac24eff2e145eef6eb0fa66094e2afb2", null ],
    [ "sendSwitchDoc", "ckJoystick_8h.html#a092f14d94deb2843b569b4f8a6070f23", null ],
    [ "sendTrimDoc", "ckJoystick_8h.html#a14a66b0b2034d3f6462acf5a42a1ac5e", null ]
];