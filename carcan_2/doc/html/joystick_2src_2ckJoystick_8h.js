var joystick_2src_2ckJoystick_8h =
[
    [ "joyAxis", "structjoyAxis.html", "structjoyAxis" ],
    [ "joystick", "structjoystick.html", "structjoystick" ],
    [ "joyXOutDoc", "structjoyXOutDoc.html", "structjoyXOutDoc" ],
    [ "joyYOutDoc", "structjoyYOutDoc.html", "structjoyYOutDoc" ],
    [ "trimDoc", "structtrimDoc.html", "structtrimDoc" ],
    [ "switchDoc", "structswitchDoc.html", "structswitchDoc" ],
    [ "ADC_MAX", "joystick_2src_2ckJoystick_8h.html#a555a695bf58df062dc03f0e892d95cd7", null ],
    [ "ADC_MID", "joystick_2src_2ckJoystick_8h.html#aacfd21cdefe4d8baa7d6f434875adff7", null ],
    [ "CKDN_joyXOutDoc", "joystick_2src_2ckJoystick_8h.html#a626a8cb5fd17b5ae5621aed531c9dc6e", null ],
    [ "CKDN_joyYOutDoc", "joystick_2src_2ckJoystick_8h.html#ac957633dde3286a410ab72900aebbb68", null ],
    [ "CKDN_switchDoc", "joystick_2src_2ckJoystick_8h.html#a9fdcd8383e94d76d5622bba39a690652", null ],
    [ "CKDN_trimDoc", "joystick_2src_2ckJoystick_8h.html#ae706d90360fd1d810be13ef14d1c6a23", null ],
    [ "POS_MAX", "joystick_2src_2ckJoystick_8h.html#a45863e8ead88875c75036aea10d15810", null ],
    [ "POS_MID", "joystick_2src_2ckJoystick_8h.html#a9ce3b38fbbd82513999d4bebab972cc7", null ],
    [ "ANx", "joystick_2src_2ckJoystick_8h.html#ad6ed457253192a246fdf309eb964b9c6", null ],
    [ "IOx", "joystick_2src_2ckJoystick_8h.html#a855959eda89debb006f1ad9104c5d6c4", null ],
    [ "SWx", "joystick_2src_2ckJoystick_8h.html#a572f6b6f306f033e26b1704de1a6dc1b", null ],
    [ "ANx", "joystick_2src_2ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81", [
      [ "AN0", "joystick_2src_2ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81a5d9fc74a0bc7b2a12d3f91ed729ca6f6", null ],
      [ "AN1", "joystick_2src_2ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81a9b629f136ed9175ebbdd76980f404477", null ],
      [ "AN2", "joystick_2src_2ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81a489040565659333e391a144b2f61f901", null ],
      [ "AN3", "joystick_2src_2ckJoystick_8h.html#a5451ba995784eb3cef911db912039b81ac14b38a472fd5569f809017973f9d1e0", null ]
    ] ],
    [ "IOx", "joystick_2src_2ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1", [
      [ "IO0", "joystick_2src_2ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1ab40d49882f65d9f96ab78e03cd794136", null ],
      [ "IO1", "joystick_2src_2ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1a17a52bdf6be8a8aa8f28cc5ed2ad65fc", null ],
      [ "IO2", "joystick_2src_2ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1abeda2b531fd3396af9bd354b5ef020a8", null ],
      [ "IO3", "joystick_2src_2ckJoystick_8h.html#ac58a9fd07eb48710222d99abf34868e1a407bc4408d49d67d52a2d128126cb600", null ]
    ] ],
    [ "joySelector", "joystick_2src_2ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195c", [
      [ "LJoystick", "joystick_2src_2ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195cae5d8e2fa1c849d70800e064f7fd7ac11", null ],
      [ "RJoystick", "joystick_2src_2ckJoystick_8h.html#a4d9989590e47921b63c19c1bfa9b195ca97250b1bc73c8f3552e17785854f2641", null ]
    ] ],
    [ "SWx", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6", [
      [ "SW0", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6ae687b2007a08227ccbaa479af150163d", null ],
      [ "SW1", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6ae2b4f431c9f9f89d2ce3c9d4561b3345", null ],
      [ "SW2", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6a2d2a1277b6d4e00a23812eba450647ea", null ],
      [ "SW3", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6a74a947b9c744abf2bfeea79e4345adc7", null ],
      [ "SW4", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6a6322d04739d928cb14415f55d6678449", null ],
      [ "SW5", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6adbde5b52293bf239b79e2f80d2485402", null ],
      [ "SW6", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6a8899fa13fe9a5ff5d3e9cdd986e0f371", null ],
      [ "SW7", "joystick_2src_2ckJoystick_8h.html#a73fc0d7213171e652f127098e73fd2c6a31b8eb90784c7e92ebf9d39e99998df8", null ]
    ] ],
    [ "ckappMain", "joystick_2src_2ckJoystick_8h.html#ab11ea6d13c83be83783fde46004c1f5f", null ],
    [ "ckappOnIdle", "joystick_2src_2ckJoystick_8h.html#a6a705966f68158f66b39fa5531c1e5af", null ],
    [ "sendJoyXOutDoc", "group__Document.html#ga781347c824f64c716005769a2e9b47e8", null ],
    [ "sendJoyYOutDoc", "group__Document.html#gaac24eff2e145eef6eb0fa66094e2afb2", null ],
    [ "sendSwitchDoc", "group__Document.html#ga092f14d94deb2843b569b4f8a6070f23", null ],
    [ "sendTrimDoc", "group__Document.html#ga14a66b0b2034d3f6462acf5a42a1ac5e", null ]
];