var structjoystick =
[
    [ "joySel", "structjoystick.html#ad867a2ebb8c5bc540cad4b6cfb3c80b8", null ],
    [ "joyX", "structjoystick.html#ac7c59fd17eb96b1c20fd5c25f12325c1", null ],
    [ "joyY", "structjoystick.html#a1aaf596094a545facaf93d41085815d8", null ],
    [ "numTasks", "structjoystick.html#a64e3321b9cb1ae5aeaa45c69bd5a79fa", null ],
    [ "offsets", "structjoystick.html#a45c6f4e3d9bd5e93bb3215b29557c80b", null ],
    [ "running", "structjoystick.html#af77f8244799e85284b8b438289f5f689", null ],
    [ "stopTasks", "structjoystick.html#acd1602f87ac1ac56488cc5c7f37b6d1c", null ],
    [ "sw1Down", "structjoystick.html#ac01d49c0517fcfd58b5bdc775ca727ab", null ],
    [ "tasks", "structjoystick.html#a3729781d9e2d5ad42432002e34810a67", null ],
    [ "timers", "structjoystick.html#ab090a5b3ca986f4054777f3962f89d9b", null ]
];