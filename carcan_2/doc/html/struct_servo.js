var struct_servo =
[
    [ "aliveTimer", "struct_servo.html#a338f5ab771a97260969a3fb381879991", null ],
    [ "dir", "struct_servo.html#acafc3c424be4cc2cba6579b7954e45a3", null ],
    [ "lastSwitch", "struct_servo.html#a7f86928f406171c212cf339beca5cc42", null ],
    [ "lastTrimTick", "struct_servo.html#ae1adc498c4b4fc89b3587cd6a145d995", null ],
    [ "lastXPos", "struct_servo.html#a45c0083c8b6ec1b44a85a99e96872c49", null ],
    [ "lastXTrim", "struct_servo.html#a93b439f250632449aca82e78ea4fb2e5", null ],
    [ "lastYPos", "struct_servo.html#a938aadba06a6485e45608709710296e9", null ],
    [ "lastYTrim", "struct_servo.html#a027a46dfc13dd2726bf13a37c6cdebc3", null ],
    [ "pwm", "struct_servo.html#a490a9e9e2b50ef1774453ae170fc60a1", null ],
    [ "stop", "struct_servo.html#a6c0af9f2e97842405fb15ed952ef2976", null ],
    [ "stopTimer", "struct_servo.html#ab959758347cbf365af111a05fb35cbf7", null ],
    [ "tasks", "struct_servo.html#adca97067697491f60be1b66366ccdf27", null ],
    [ "trim", "struct_servo.html#ae4efec01bcb64ad168cb228d82025fc1", null ],
    [ "type", "struct_servo.html#a2696e560f6434009cc17f3216cd0056e", null ]
];