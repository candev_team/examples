var structjoy_axis =
[
    [ "adc", "structjoy_axis.html#ae5d88649401f796b100a8ffb867b5507", null ],
    [ "cal", "structjoy_axis.html#afb3817c3f83cf5275206594f812d391a", null ],
    [ "calibrationInterval", "structjoy_axis.html#a3cae54c0237f738ddf708354b80ae88e", null ],
    [ "calibrationTa", "structjoy_axis.html#aa20cfc11d7d5eeea813abc43b73de070", null ],
    [ "fileName", "structjoy_axis.html#a1862cb72b37ffe98140c1fcf06a28969", null ],
    [ "gpioNeg", "structjoy_axis.html#ab1afe8ed791829f410700e5a277b977d", null ],
    [ "gpioPos", "structjoy_axis.html#a574b8a3f91c98da86c2ccae5af07c0f5", null ],
    [ "lpAdc", "structjoy_axis.html#ad94834937778e76b2a957f2ecb44a464", null ],
    [ "max", "structjoy_axis.html#ac66b569507cc273bbf83ce5dd5f70e84", null ],
    [ "mid", "structjoy_axis.html#aa217e155b3f8d951b61a7396ecd20239", null ],
    [ "min", "structjoy_axis.html#a1a1f4624f66ab0b2eb0b98316514c369", null ],
    [ "reset", "structjoy_axis.html#adb7a760512715e86f925f8a8d4a15ba8", null ],
    [ "save", "structjoy_axis.html#abeb8baadb5d54f3e7a43fff7051f519f", null ],
    [ "saveTimer", "structjoy_axis.html#a0602ab53e47c4f19cb02cad169656b7e", null ]
];