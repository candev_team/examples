var joystick_2src_2ck_joystick__handlers_8c =
[
    [ "changeJoystick", "joystick_2src_2ck_joystick__handlers_8c.html#a03d5d2e208cf272a4bdb154c7a33f4cb", null ],
    [ "checkShutdown", "joystick_2src_2ck_joystick__handlers_8c.html#a64ccd89da304a25cfa294ddacb0caf74", null ],
    [ "ckappDispatchFreeze", "joystick_2src_2ck_joystick__handlers_8c.html#a8e2804394c9b1cd35296f680918df483", null ],
    [ "ckappDispatchRTR", "joystick_2src_2ck_joystick__handlers_8c.html#a60e03efe50af978977431eec47e954fb", null ],
    [ "ckappMain", "joystick_2src_2ck_joystick__handlers_8c.html#ab11ea6d13c83be83783fde46004c1f5f", null ],
    [ "ckappMayorsPage", "joystick_2src_2ck_joystick__handlers_8c.html#a9f8aa8579e1193d272da8d1b1e7091fb", null ],
    [ "ckappOnIdle", "joystick_2src_2ck_joystick__handlers_8c.html#a6a705966f68158f66b39fa5531c1e5af", null ],
    [ "ckappSetActionMode", "joystick_2src_2ck_joystick__handlers_8c.html#abe63903cd5436ba28aae3307f6a0ba19", null ],
    [ "ckappSetCityMode", "joystick_2src_2ck_joystick__handlers_8c.html#a95b5567807de5df61007aa3802df26d0", null ],
    [ "onOtherMessage", "joystick_2src_2ck_joystick__handlers_8c.html#a8bdf02e6cf189a1440faffd6d6686230", null ],
    [ "joyX", "joystick_2src_2ck_joystick__handlers_8c.html#a8986a27456ac9e874baf9094d70eb353", null ],
    [ "joyY", "joystick_2src_2ck_joystick__handlers_8c.html#a33d8bac95a7c4c86b192a392c7782181", null ],
    [ "js", "joystick_2src_2ck_joystick__handlers_8c.html#a5ced57569fa1af29c97add4e76580cfb", null ]
];