var timers_8c =
[
    [ "calibrationInterval", "group__Timer.html#gab2e9db087e89a8c144033cfcacb867c4", null ],
    [ "destroyTimers", "timers_8c.html#a38ded447bc079e82d30274c54fa364f1", null ],
    [ "onSaveTimer", "group__Timer.html#ga152a93e9a2c3eda4bd9830186e7483ed", null ],
    [ "sendPosTask", "group__Timer.html#ga2dfec9104ebb3b13ab7f478b90dc9c23", null ],
    [ "sendSwTask", "group__Timer.html#ga460b5f5a45839913365e4eddecece5dc", null ],
    [ "sendTrimTask", "group__Timer.html#ga614a60baa6a29fe9764af47d15c7cb7c", null ],
    [ "startTimers", "timers_8c.html#aacc27e3b5bdb701a5f679eff2c6d4827", null ],
    [ "stopTimers", "timers_8c.html#a2329ff3db24b5439bc6c7b7795134554", null ]
];