var capital__bsp_2include_2config_8h =
[
    [ "CFG_CAN_INIT", "capital__bsp_2include_2config_8h.html#a8c9e536eedd94dd9dbe16337bd2e08a9", null ],
    [ "CFG_EVENT_LOG_SIZE", "capital__bsp_2include_2config_8h.html#ad2010de0a7e2c45dc24dff00d95cd9c3", null ],
    [ "CFG_HCLK_FREQUENCY", "capital__bsp_2include_2config_8h.html#aacd2dc976145019d57f73cdb773c1ade", null ],
    [ "CFG_HEAP_TYPE_DYNAMIC", "capital__bsp_2include_2config_8h.html#a921fea07f74e1074c6ba46cc35cc8e93", null ],
    [ "CFG_IDLE_STACK_SIZE", "capital__bsp_2include_2config_8h.html#a85714aa0d707ffe89dec3cd2f8371f65", null ],
    [ "CFG_INOR_DEV_NAME", "capital__bsp_2include_2config_8h.html#a6e1b23975e4d15cc48f6143f3db8fc4c", null ],
    [ "CFG_IRQ_STACK_SIZE", "capital__bsp_2include_2config_8h.html#ac946bddeeb2a41e65228cae4bc188c93", null ],
    [ "CFG_MAIN_PRIORITY", "capital__bsp_2include_2config_8h.html#a1cfaad96c23cbf596181a0ab30863196", null ],
    [ "CFG_MAIN_STACK_SIZE", "capital__bsp_2include_2config_8h.html#a7fa136ac3c6f64d3f1d68a1e3c51922e", null ],
    [ "CFG_NOR_INIT", "capital__bsp_2include_2config_8h.html#a20705678959fc2981e0ba9880f86abf3", null ],
    [ "CFG_NUM_DEVICES", "capital__bsp_2include_2config_8h.html#a6c9b105638ae8dc64ec81b85e7be10aa", null ],
    [ "CFG_NUM_FILES", "capital__bsp_2include_2config_8h.html#a210737f253f36f3bcaaadc86de76d3f8", null ],
    [ "CFG_PCLK1_FREQUENCY", "capital__bsp_2include_2config_8h.html#acc1f71a9904ad7d5a1a7788f2b813b1a", null ],
    [ "CFG_PCLK2_FREQUENCY", "capital__bsp_2include_2config_8h.html#a8a5041c50028afa287963bf07491c259", null ],
    [ "CFG_REAPER_STACK_SIZE", "capital__bsp_2include_2config_8h.html#a6871f0949e672f47c7b8dc8e0abc2bc2", null ],
    [ "CFG_SHELL_INIT", "capital__bsp_2include_2config_8h.html#a8c13ab5bd06a45a7b3bc007a11e32c70", null ],
    [ "CFG_SHELL_PROMPT", "capital__bsp_2include_2config_8h.html#abefa5ebf53d0caf00f1181d600ad4981", null ],
    [ "CFG_SIG_POOL_BLOCKS", "capital__bsp_2include_2config_8h.html#a1ca4e4a01151079dba30f8b403bfa810", null ],
    [ "CFG_SIG_POOL_SIZE", "capital__bsp_2include_2config_8h.html#ac4964a7227b587fb4ab8605b9612ae3a", null ],
    [ "CFG_STACK_ERR_LIMIT", "capital__bsp_2include_2config_8h.html#a96f1c7d69b174f6b84dd11f4980b77b1", null ],
    [ "CFG_STARTUP_INIT", "capital__bsp_2include_2config_8h.html#af54c8b3c6024d4431b9efe9bf8d282e9", null ],
    [ "CFG_SYSCLK_FREQUENCY", "capital__bsp_2include_2config_8h.html#a1eaa19c6faff4cba4da0118fc9e8be3d", null ],
    [ "CFG_TICKS_PER_SECOND", "capital__bsp_2include_2config_8h.html#a370453bd049c61c4bab4b18d8c6a8e97", null ],
    [ "CFG_WIFFS_INIT", "capital__bsp_2include_2config_8h.html#a3b4fcb2a86bc4cea84ec3aa7329f7bf5", null ]
];