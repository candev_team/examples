/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#include "lua_counter.h"
#include <lauxlib.h>
#include <counter/counter.h>
#include <string.h>

#undef RTK_DEBUG

#ifdef RTK_DEBUG
#define DPRINT(...) rprintp ("lua_counter: "__VA_ARGS__)
#else
#define DPRINT(...)
#endif  /* RTK_DEBUG */

#define LUA_COUNTER_CHANNEL LUA_COUNTER_LIBNAME".channel"

/* Counter channel type. Exposed to Lua as a "userdata" type. */
typedef struct lua_counter_channel
{
   int fd;
   bool is_opened;
   bool is_started;
} lua_counter_channel_t;

typedef struct lua_counter_mode
{
   const char * name;
   counter_dual_edge_modes_t  id;
} lua_counter_mode_t;

static void lua_counter_check_number_of_arguments (lua_State * L, int expected)
{
   int actual = lua_gettop (L);

   if (actual != expected)
   {
      luaL_error (L, "wrong number of arguments");
   }
}

/* Similar to luaL_checkstring(), but also checks that mode is valid. */
static counter_dual_edge_modes_t lua_counter_checkmode (lua_State * L, int arg)
{
   unsigned i;
   static const lua_counter_mode_t modes[] =
   {
         {"PULSE",      COUNTER_DUAL_EDGE_MEASURE_PULSE},
         {"PERIOD",     COUNTER_DUAL_EDGE_MEASURE_PERIOD},
   };
   luaL_checktype (L, arg, LUA_TSTRING);
   const char * mode_name = luaL_checkstring (L, arg);

   for (i = 0; i < NELEMENTS(modes); i++)
   {
      if (strcmp (mode_name, modes[i].name) == 0)
      {
         return modes[i].id;
      }
   }

   return luaL_argerror (L, arg, "invalid mode");
}

/* Lua: channel = counter.open('/counter0/channel_name') */
static int lua_counter_open (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_check_number_of_arguments (L, 1);
   const char * channel_name = luaL_checkstring (L, 1);

   /* Push new class instance at top of stack */
   lua_counter_channel_t * self = lua_newuserdata(L, sizeof(*self));
   ASSERT (self != NULL);
   memset (self, 0x00, sizeof(*self));
   luaL_setmetatable(L, LUA_COUNTER_CHANNEL); /* Set class for instance */

   self->is_started = false;
   self->fd = counter_open (channel_name);
   if (self->fd < 0)
   {
      return luaL_error (L, "could not open channel '%s'", channel_name);
   }
   self->is_opened = true;
   DPRINT ("Opened\n");

   /* One result is already on stack */
   return 1;
}

/* Lua: channel.close(channel) OR
 *      channel:close()
 */
static int lua_counter_close (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_check_number_of_arguments (L, 1);
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel already closed");
   luaL_argcheck (L, self->is_started == false, 1, "channel not stopped");
   ASSERT (self->fd >= 0);

   counter_close (self->fd);
   self->is_opened = false;
   DPRINT ("Closed\n");

   /* Push no results on stack */
   return 0;
}

/* Destructor. Called by garbage collector. */
static int lua_counter_collect_garbage (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   ASSERT (self != NULL);

   if (self->is_opened == false)
   {
      /* Already closed, so nothing to clean up */
      DPRINT ("GC: nothing to do\n");
      return 0; /* Push no results on stack */
   }

   if (self->is_started)
   {
      counter_stop (self->fd);
      self->is_started = false;
      DPRINT ("GC: stopped\n");
   }

   counter_close (self->fd);
   self->is_opened = false;
   DPRINT ("GC: closed\n");

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.start(channel, mode) OR
 *      channel:start(mode)
 */
static int lua_counter_start (lua_State * L)
{
   /* Get two arguments from stack */
   lua_counter_check_number_of_arguments (L, 2);
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   counter_dual_edge_modes_t mode = lua_counter_checkmode (L, 2);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started == false, 1, "channel already started");
   ASSERT (self->fd >= 0);

   counter_start (self->fd, mode);
   self->is_started = true;

   /* Push no results on stack */
   return 0;
}

/* Lua: channel.stop(channel) OR
 *      channel:stop()
 */
static int lua_counter_stop (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_check_number_of_arguments (L, 1);
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   counter_stop (self->fd);
   self->is_started = false;

   /* Push no results on stack */
   return 0;
}

/* Lua: raw_count = channel.read_count(channel) OR
 *      raw_count = channel:read_count()
 */
static int lua_counter_read_count (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_check_number_of_arguments (L, 1);
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   int32_t raw_count = counter_read_count (self->fd);

   /* Push one result on stack */
   if (raw_count < 0)
   {
      DPRINT ("Error: Count = %d\n", raw_count);
      lua_pushnil (L);
   }
   else
   {
      lua_pushinteger (L, raw_count);
   }
   return 1;
}

/* Lua: time_ns = channel.read_time(channel) OR
 *      time_ns = channel:read_time()
 */
static int lua_counter_read_time (lua_State * L)
{
   /* Get one argument from stack */
   lua_counter_check_number_of_arguments (L, 1);
   lua_counter_channel_t * self = luaL_checkudata (L, 1, LUA_COUNTER_CHANNEL);
   ASSERT (self != NULL);
   luaL_argcheck (L, self->is_opened, 1, "channel not opened");
   luaL_argcheck (L, self->is_started, 1, "channel not started");
   ASSERT (self->fd >= 0);

   int32_t time_ns = counter_read_time (self->fd);

   /* Push one result on stack */
   if (time_ns < 0)
   {
      DPRINT ("Error: Time = %d\n", time_ns);
      lua_pushnil (L);
   }
   else
   {
      lua_pushinteger (L, time_ns);
   }
   return 1;
}

static void lua_counter_register_methods (lua_State *L)
{
   static const luaL_Reg methods[] =
   {
         {"close", lua_counter_close},
         {"start", lua_counter_start},
         {"stop", lua_counter_stop},
         {"read_count", lua_counter_read_count},
         {"read_time", lua_counter_read_time},
         {"__gc", lua_counter_collect_garbage},
         {NULL, NULL}, /* Sentinel */
   };

   /* Create and register metatable for counter channel class in the
    * global Registry.
    *
    * Leaves stack unchanged.
    *
    * Lua: metatable = newmetatable(LUA_COUNTER_CHANNEL)
    *      metatable.__index = metatable
    *      metatable.start = ..
    *      metatable.stop = .. (and so on)
    */
   luaL_newmetatable (L, LUA_COUNTER_CHANNEL); /* Push metatable reference on stack */
   lua_pushvalue (L, -1);                      /* Push metatable reference on stack */
   lua_setfield (L, -2, "__index");            /* Pop metatable reference from stack */
   luaL_setfuncs (L, methods, 0);              /* Add methods to metatable */
   lua_pop (L, 1);                             /* Pop metatable reference from stack */
}

int lua_counter_openlib (lua_State * L)
{
   lua_counter_register_methods (L);

   /* Push one result on stack (the library) */
   static const luaL_Reg functions[] =
   {
       {"open", lua_counter_open},
       {NULL, NULL} /* Sentinel */
   };
   luaL_newlib (L, functions);
   return 1;
}
