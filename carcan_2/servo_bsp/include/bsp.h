/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2017 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#ifndef BSP_H
#define BSP_H

#include <stm32.h>

#define GPIO_UART1_TX   GPIO_PA9
#define GPIO_UART1_RX   GPIO_PA10

#define GPIO_CAN_TX     GPIO_PB9
#define GPIO_CAN_RX     GPIO_PB8

#define GPIO_SPI2_MOSI  GPIO_PB15
#define GPIO_SPI2_MISO  GPIO_PB14
#define GPIO_SPI2_SCK   GPIO_PB13
#define GPIO_SPI2_NSS   GPIO_PB12

#define GPIO_SERVO_PWM  GPIO_PC3

#define GPIO_ADC1_IN2   GPIO_PA1
#define GPIO_ADC1_IN3   GPIO_PA2
#define GPIO_ADC1_IN4   GPIO_PA3
#define GPIO_ADC2_IN1   GPIO_PA4

#define GPIO_FREQ_1     GPIO_PA6

#define GPIO_I2C_SDA    GPIO_PB7
#define GPIO_I2C_SCL    GPIO_PB6

#define GPIO_DEBUG_LED  GPIO_PA8

void bsp_reset_peripherals (void);

/**
 * Always measure VCC_SERVO before connecting any servo.
 * Value are depends on Vin and must be verified,
 * call this function to set correct value for your voltage.
 */
int set_i2c_pot_vcc_servo (uint8_t wra_value);

/**
 * Always measure VDD_SENSOR before connecting any peripheral.
 * Value are depends on Vin and must be verified,
 * call this function to set correct value for your voltage.
 */
int set_i2c_pot_vdd_sensor (uint8_t wrb_value);

#endif /* BSP_H */
