/*********************************************************************
 *        _       _         _
 *  _ __ | |_  _ | |  __ _ | |__   ___
 * | '__|| __|(_)| | / _` || '_ \ / __|
 * | |   | |_  _ | || (_| || |_) |\__ \
 * |_|    \__|(_)|_| \__,_||_.__/ |___/
 *
 * http://www.rt-labs.com
 * Copyright 2007 rt-labs AB, Sweden.
 * See LICENSE file in the project root for full license information.
 ********************************************************************/

#include "config.h"
#include <bsp.h>
#include <kern.h>
#include <dev.h>
#include <shell.h>
#include <net.h>

#ifndef CFG_LWIP_IPADDR
#define CFG_LWIP_IPADDR()     IP4_ADDR (&ipaddr, 0, 0, 0, 0)
#endif

#ifndef CFG_LWIP_NETMASK
#define CFG_LWIP_NETMASK()    IP4_ADDR (&netmask, 0, 0, 0, 0)
#endif

#ifndef CFG_LWIP_GATEWAY
#define CFG_LWIP_GATEWAY()    IP4_ADDR (&gw, 0, 0, 0, 0)
#endif

#ifndef CFG_LWIP_NAMESERVER
#define CFG_LWIP_NAMESERVER() IP4_ADDR (&nameserver, 0, 0, 0, 0)
#endif

#ifndef CFG_LWIP_HOSTNAME
#define CFG_LWIP_HOSTNAME       NULL
#endif

#ifdef CFG_LWIP_INIT
static void startup_lwip (void)
{
   ip_addr_t ipaddr;
   ip_addr_t netmask;
   ip_addr_t gw;
   ip_addr_t nameserver;

   /* Initialise parameters */
   CFG_LWIP_IPADDR();
   CFG_LWIP_NETMASK();
   CFG_LWIP_GATEWAY();
   CFG_LWIP_NAMESERVER();

   net_init("/net", bsp_eth_init);
   net_configure (NULL, &ipaddr, &netmask, &gw, &nameserver,
                  CFG_LWIP_HOSTNAME);

#ifndef CFG_HOTPLUG_INIT
   /* Give PHY some time to establish link before coldplugging */
   task_delay (tick_from_ms (2 * 1000));
#endif
}
#endif  /* CFG_LWIP_INIT */

void startup (void)
{
#ifdef CFG_STARTUP_INIT

#ifdef CFG_STATS_INIT
   stats_calibrate();
#endif  /* CFG_STATS_INIT */

#ifdef CFG_TTOS_INIT
   tt_init();
#endif  /* CFG_TTOS_INIT */

#ifdef CFG_HOTPLUG_INIT
   dev_hotplug_init (CFG_HOTPLUG_PRIORITY, CFG_HOTPLUG_STACK_SIZE,
                     CFG_HOTPLUG_POLL_PERIOD);
#endif  /* CFG_HOTPLUG_INIT */

#ifdef CFG_LWIP_INIT
   startup_lwip();
#endif  /* CFG_LWIP_INIT */

#ifdef CFG_PTPD_INIT
   bsp_ptpd_init();
#endif  /* CFG_LWIP_INIT */

#if defined CFG_USB_INIT
   bsp_usb_init();
#endif  /* CFG_USB_INIT */

#ifndef CFG_HOTPLUG_INIT
   /* Simple probing of all devices at boot time */
   dev_coldplug_init();
#endif

#ifdef CFG_SHELL_INIT
   shell_init (CFG_SHELL_PROMPT);
#endif

#endif  /* CFG_STARTUP_INIT */
}
