
#include "system.h"
#include <kern.h>

#define TASK_PRIO  3
#define TASK_STACK 2048

void kingTaskFunc(void *arg)
{
   ckKingMain();
}

int main(void)
{
   task_spawn("King", kingTaskFunc,
     TASK_PRIO, TASK_STACK, NULL);
   return 0;
}
