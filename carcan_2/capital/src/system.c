/*
 * King implementation for the system System.
 * 
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * Generation source file: /car_model/system.kfs
 */
/**
 * @file system.c
 * @brief Capital implementation.
 *
 * This implementation supports two slightly different systems:
 * one car with two motors and one steering axle
 * and another car with one motor and one steering axle.
 *
 * @authors Hashem Hashem & Andre' Idoffsson.
 * @date 2019-07
 */
#include "ckHal.h"
#include "kCk.h"
#include "system.h"
#include <can/can.h>

#define BASE_NO 100

#define ONE_MOTOR // Car with one motor. Comment this line for car with two motors.

#ifdef ONE_MOTOR
#define CK_CITY_COUNT 4
uint8_t ckCityNumbers[CK_CITY_COUNT] = { 1, 2, 3, 12 };
#else
#define CK_CITY_COUNT 5
uint8_t ckCityNumbers[CK_CITY_COUNT] = { 1, 2, 10, 11, 12 };
#endif

uint8_t activeCities[CK_CITY_COUNT];

can_cfg_t defDocCfg = {125000, 13, 2, 1, 0};
#define defDocEnv 2031L
#define defDocData "\xaa\xaa\xaa\xaa\xaa\xaa\xaa\xaa"
#define defDocDLC 8
#define kingsEnv 0

/**
 * @brief Sends default letter.
 */
void sendDefaultLetter(void)
{
   canWriteMessage(defDocEnv, (uint8_t*)defDocData, defDocDLC);
}

/**
 * @brief Identify cities in the kingdom and give them instructions.
 *
 * King sends out default letter and KP1 and waits for
 * cities to answer with correct envelope.
 *
 * If correct envelope is received then the King will set up the city,
 * unless it already has been done.
 */
void ckKingCheck(void) {
  sendDefaultLetter();

  int i;
  for (i = 0; i < CK_CITY_COUNT; i++)
  {
    /* If city already active try next city */
    if (activeCities[i])
    {
      continue;
    }

    uint32_t id;
    uint8_t dlc;
    uint8_t buf[8];

    ckKP1(ckCityNumbers[i], BASE_NO, 0, 0);
    task_delay(tick_from_ms(5)); /* Give city time to respond */
    /* Check answer to KP1 */
    if (canReadMessage(&id, buf, &dlc) != canOK)
    {
      continue;
    }

    /* Checks if received msg is mayor's page 0 */
    if (id == (BASE_NO + ckCityNumbers[i]) && dlc == 8)
    {
      ckKingDefineFolders(ckCityNumbers[i]);  /* Define folders */
      activeCities[i] = 1;   /* Remember activation */
    }
  }
}

/**
 * @brief Shut down if all cities have been activated.
 * @return Non-zero when all cities have been activated.
 */
int checkShutDown(void)
{
  int i;
  for (i = 0; i < CK_CITY_COUNT; i++)
  {
    if (!activeCities[i])
      return 0;
  }
  return 1;
}

/**
 * @brief Main function of the system.
 *
 * Sets up CAN bus and runs the CanKingdom startup phase.
 */
void ckKingMain(void)
{
  canSetup(&defDocCfg);
  ckKP0(0, amReset, cmKeep, 0); // reset nodes
  task_delay(tick_from_ms(100));
  for (;;) {
    ckKingCheck();
    task_delay(tick_from_ms(5)); /* don't spam */
    if (checkShutDown())
    {
      ckKP0(0, amRun, cmCommunicate, 0); /* Start the system! */
      break;
    }
  }
}

/* System specific King's page that sets duty range for servo cities.
 *  King's page 150, sets duty range
 *  Line 0 = city address
 *  Line 1 = page no
 *  Line 2 = min duty LSB
 *  Line 3 = min duty MSB
 *  Line 4 = max duty LSB
 *  Line 5 = max duty MSB
 *  1000 == 10 %, 400 == 4%, and so on
 */
static void ckKP150(uint8_t city, uint16_t minDuty, uint16_t maxDuty)
{
  uint8_t buf[8] = {city, 150,
                    LSB(minDuty), MSB(minDuty),
                    LSB(maxDuty), MSB(maxDuty),
                    0, 0};
  canWriteMessage(kingsEnv, buf, 8);
}

/* King's page 151, sets mix values in percent
 * Line 2 = value for mixValues[0]
 * Line 3 = value for mixValues[1]
 * Values are 0 to 255 but capped on receiver side at 100 for 100%
 */
static void ckKP151(uint8_t city, uint8_t mixVal1, uint8_t mixVal2)
{
  uint8_t buf[8] = {city, 151,
                    mixVal1, mixVal2,
                    0, 0, 0, 0};
  canWriteMessage(kingsEnv, buf, 8);
}

/* King's page 152, sets rates values in percent
 * Line 2 = value for rates[0]
 * Line 3 = value for rates[1]
 * Values are 0 to 255 but capped at 100 for 100%
 */
static void ckKP152(uint8_t city, uint8_t rate1, uint8_t rate2)
{
  uint8_t buf[8] = {city, 152,
                    rate1, rate2,
                    0, 0, 0, 0};
  canWriteMessage(kingsEnv, buf, 8);
}

/* King's page 153, reverses direction
 * Has no content, city reverses direction upon reception.
 * Meant to be sent once, and then
 * save trim to keep direction.
 */
static void ckKP153(uint8_t city)
{
  uint8_t buf[8] = {city, 153, 0, 0, 0, 0, 0, 0};
  canWriteMessage(kingsEnv, buf, 8);
}


/**
 * @brief Defines folders in the cities and assigns envelopes.
 *
 * Sends commands to set envelopes and folders for the system. @p cityNo is the
 * affected city number, where 1 to 5 are the different cities in the system.
 * Based on auto-generated code from Kingdom Founder.
 *
 * @param cityNo City to set up.
 */
void ckKingDefineFolders(int cityNo)
{
  switch (cityNo)
  {
    // Left Joystick
    case 1:
      // Producer: Joystick X Output @ Left Joystick
      ckDefineFolder(CKCityNo_lJoystick, 2, 10, CKDN_joyXOutDoc, 2, ckTx | ckEnable);
      // Producer: Trim Buttons @ Left Joystick
      ckDefineFolder(CKCityNo_lJoystick, 6, 12, CKDN_trimOutDoc, 1, ckTx | ckEnable);
      // Producer: Switch outputs @ Left Joystick
      ckDefineFolder(CKCityNo_lJoystick, 8, 14, CKDN_stfSwitchOutDoc, 1, ckTx | ckEnable);
      /* Set city mode to left joystick */
      ckKP0(CKCityNo_lJoystick, amKeep, cmKeep, CLJoystick_mode);
      break;

    // Right Joystick
    case 2:
      // Producer: Joystick Y Output @ Right Joystick
      ckDefineFolder(CKCityNo_rJoystick, 4, 11, CKDN_joyYOutDoc, 2, ckTx | ckEnable);
      // Producer: Trim Buttons @ Right Joystick
      ckDefineFolder(CKCityNo_rJoystick, 6, 13, CKDN_trimOutDoc, 1, ckTx | ckEnable);
      /* Set city mode to right joystick */
      ckKP0(CKCityNo_rJoystick, amKeep, cmKeep, CRJoystick_mode);
      break;

    // main motor
    case 3:
      // Consumer: Throttle Position @ Left Motor
      ckDefineFolder(CKCityNo_motor, 5, 11, CKDN_throttleDoc, 2, ckEnable);
      // Consumer: Trim @ Left Motor
      ckDefineFolder(CKCityNo_motor, 7, 13, CKDN_trimDoc, 1, ckEnable);
      // Consumer: Switch @ Left Motor
      ckDefineFolder(CKCityNo_motor, 9, 14, CKDN_stfSwitchDoc, 1, ckEnable);
      /* Set city mode to left motor */
      ckKP0(CKCityNo_motor, amKeep, cmKeep, Motor_mode);
      /* Set duty range */
      ckKP150(CKCityNo_motor, DUTY_MIN_MOTOR, DUTY_MAX_MOTOR);
      break;

    // Left motor
    case 10:
      // Consumer: Steering Position @ Right motor
      ckDefineFolder(CKCityNo_lMotor, 3, 10, CKDN_yawDoc, 2, ckEnable);
      // Consumer: Throttle Position @ Left Motor
      ckDefineFolder(CKCityNo_lMotor, 5, 11, CKDN_throttleDoc, 2, ckEnable);
      // Consumer: Trim @ Left Motor
      ckDefineFolder(CKCityNo_lMotor, 7, 13, CKDN_trimDoc, 1, ckEnable);
      // Consumer: Switch @ Left Motor
      ckDefineFolder(CKCityNo_lMotor, 9, 14, CKDN_stfSwitchDoc, 1, ckEnable);
      /* Set city mode to left motor */
      ckKP0(CKCityNo_lMotor, amKeep, cmKeep, LMotor_mode);
      /* Set duty range */
      ckKP150(CKCityNo_lMotor, DUTY_MIN_MOTOR, DUTY_MAX_MOTOR);
      break;

    // Right Motor
    case 11:
      // Consumer: Steering Position @ Right motor
      ckDefineFolder(CKCityNo_rMotor, 3, 10, CKDN_yawDoc, 2, ckEnable);
      // Consumer: Throttle Position @ Right motor
      ckDefineFolder(CKCityNo_rMotor, 5, 11, CKDN_throttleDoc, 2, ckEnable);
      // Consumer: Trim @ Right motor
      ckDefineFolder(CKCityNo_rMotor, 7, 13, CKDN_trimDoc, 1, ckEnable);
      // Consumer: Switch @ Right motor
      ckDefineFolder(CKCityNo_rMotor, 9, 14, CKDN_stfSwitchDoc, 1, ckEnable);
      /* Set city mode to right motor */
      ckKP0(CKCityNo_rMotor, amKeep, cmKeep, RMotor_mode);
      /* Set duty range */
      ckKP150(CKCityNo_rMotor, DUTY_MIN_MOTOR, DUTY_MAX_MOTOR);
      break;


    // Steering
    case 12:
      // Consumer: Steering Position @ Steering
      ckDefineFolder(CKCityNo_steering, 3, 10, CKDN_yawDoc, 2, ckEnable);
      // Consumer: Trim @ Steering
      ckDefineFolder(CKCityNo_steering, 7, 12, CKDN_trimDoc, 1, ckEnable);
      // Consumer: Switch @ Steering
      ckDefineFolder(CKCityNo_steering, 9, 14, CKDN_stfSwitchDoc, 1, ckEnable);
      /* Set city mode to steering */
      ckKP0(CKCityNo_steering, amKeep, cmKeep, Steering_mode);
      /* Set duty range */
      ckKP150(CKCityNo_steering, DUTY_MIN_STEERING, DUTY_MAX_STEERING);
      break;
  }
}
