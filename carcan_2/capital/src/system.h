/* 
 * Header file for System.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * 
 * 
 * Generation source file: /car_model/system.kfs
 */

#ifndef SYSTEM
#define SYSTEM

#include <stdint.h>

// --- City numbers
#define CKCityNo_lJoystick 1
#define CKCityNo_rJoystick 2
#define CKCityNo_motor 3
#define CKCityNo_lAileron 4
#define CKCityNo_lFlap 5
#define CKCityNo_rAileron 6
#define CKCityNo_rFlap 7
#define CKCityNo_elevator 8
#define CKCityNo_rudder 9
#define CKCityNo_lMotor 10
#define CKCityNo_rMotor 11
#define CKCityNo_steering 12

// --- ckJoystick
#define CKDN_joyXOutDoc 10    // Joystick X Output
#define CKDN_joyYOutDoc 11    // Joystick Y Output
#define CKDN_trimOutDoc 12    // Trim Buttons
#define CKDN_stfSwitchOutDoc 13    // Save Trim and Flap Switches
#define CKDN_rmSwitchOutDoc 14    // Rates and Mixing Switches

/* City modes for joystick nodes */
#define CLJoystick_mode 1 /* Car left joystick mode */
#define CRJoystick_mode 2 /* Car right joystick mode */
#define COJoystick_mode 3 /* Car one joystick mode */
#define PLJoystick_mode 4 /* Plane left joystick mode */
#define PRJoystick_mode 5 /* Plane right joystick mode */

// --- ckServo
#define CKDN_yawDoc 10    // Yaw Position
#define CKDN_throttleDoc 11    // Throttle Position
#define CKDN_pitchDoc 12    // Pitch Position
#define CKDN_rollDoc 13    // Roll Position
#define CKDN_trimDoc 14    // Trims
#define CKDN_stfSwitchDoc 15    // Save Trim and Flap Switches
#define CKDN_rmSwitchDoc 16    // Rates and Mixing Switch

/* City modes for servo nodes */
#define Motor_mode 1
#define Rudder_mode 2
#define LAileron_mode 3
#define RAileron_mode 4
#define LFlap_mode 5
#define RFlap_mode 6
#define Elevator_mode 7
#define LMotor_mode 8
#define RMotor_mode 9
#define Steering_mode 10

/* Duty ranges
 * The duty_cycle percentage is specified as an integer with two
 * decimals. E.g. 50.01% is specified as the decimal value 5001.
 * */
#define DUTY_MIN_MOTOR 400
#define DUTY_MAX_MOTOR 1100

#define DUTY_MIN_LAILERON 650
#define DUTY_MAX_LAILERON 950

#define DUTY_MIN_LFLAP 400
#define DUTY_MAX_LFLAP 900

#define DUTY_MIN_RAILERON 575
#define DUTY_MAX_RAILERON 875

#define DUTY_MIN_RFLAP 525
#define DUTY_MAX_RFLAP 1025

#define DUTY_MIN_ELEVATOR 500
#define DUTY_MAX_ELEVATOR 1000

#define DUTY_MIN_RUDDER 575
#define DUTY_MAX_RUDDER 875

#define DUTY_MAX_STEERING 950
#define DUTY_MIN_STEERING 550


/* Elevator mix values on flap lowering.
 * First value is % to lower elevator on flap mid pos
 * and second value is % to lower on flap fully lowered pos
 */
#define ELEVATOR_MIX_1 10
#define ELEVATOR_MIX_2 20

/* Rudder to aileron mixing, value is rudder movement % on roll */
#define RUDDER_MIX 20

/* Rates, values are percentages */
/* Values taken from "Avios Grand Tundra" manual
 * which contains recommended rates for that specific plane.
 */
#define AILERON_RATES_1 82
#define AILERON_RATES_2 64

#define ELEVATOR_RATES_1 74
#define ELEVATOR_RATES_2 48

#define RUDDER_RATES_1 80
#define RUDDER_RATES_2 60

#if __cplusplus
extern "C" {
#endif

void ckKingDefineFolders(int);
void ckKingMain(void);

#if __cplusplus
}
#endif

#endif /* SYSTEM */
