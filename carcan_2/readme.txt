Car CAN 2
=========

This project is Car CAN 2, which is an evolved version of the of the original Car CAN example project.

It was developed by Hashem Hashem and André Idoffsson for their master's thesis project.

Car CAN 2 is more complete and capable, but it is also more complicated and might be harder for new users to understand.

The project has improvements both for controlling the car and to the CAN Kingdom runtime.
