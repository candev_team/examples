/* 
 * Header file for System.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * 
 * 
 * Generation source file: /car_model/system.kfs
 */

#ifndef SYSTEM
#define SYSTEM

#include <stdint.h>

// --- City numbers
#define CKCityNo_left_joystick 1
#define CKCityNo_right_joystick 2
#define CKCityNo_motor 3
#define CKCityNo_right_motor 11
#define CKCityNo_steering 12
#define CKCityNo_left_motor 10

// --- ckJoystick
#define CKDN_joyXOutDoc 10    // Joystick X Output
#define CKDN_joyYOutDoc 11    // Joystick Y Output
#define CKDN_trimDoc 12    // Trim Buttons
#define CKDN_switchDoc 13    // Switch outputs

// --- ckServo
#define CKDN_steeringPosDoc 10    // Steering Position
#define CKDN_trimDoc 12    // 2Trim
#define CKDN_switchDoc 13    // Switch
#define CKDN_throttlePosDoc 11    // Throttle Position

// --- ckServo
#define CKDN_steeringPosDoc 10    // Steering Position
#define CKDN_trimDoc 12    // 2Trim
#define CKDN_switchDoc 13    // Switch
#define CKDN_throttlePosDoc 11    // Throttle Position

#if __cplusplus
extern "C" {
#endif

void ckKingDefineFolders(void);
void ckKingMain(void);

#if __cplusplus
}
#endif

#endif /* SYSTEM */
