

/*
 * King implementation for the system System.
 * 
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * Generation source file: /car_model/system.kfs
 */

#include "ckHal.h"
#include "kCk.h"
#include "system.h"

#define BASE_NR 100
#define CK_CITY_COUNT 6
uint8_t ckCityNumbers[CK_CITY_COUNT] = { 1, 2, 3, 11, 12, 10 };

void ckKingCheck(uint32_t baseNo, uint16_t moduleCount, uint8_t *cities) { 
  // ckKP1(0, baseNo);
  // ...
}

void ckKingMain(void) {
  canSetup(defdocBtr);
  ckKingDefineFolders();
  ckKP0(0, amRun, cmCommunicate);
  for(;;) {
    ckKingCheck(BASE_NUMBER, CK_MODULE_COUNT, ckCityNumbers);
//  if (checkShutdown())
//    break;
  }
}


void ckKingDefineFolders(void) { 
  // Connection: Steering
  // Producer: Joystick X Output @ Left Joystick
  ckDefineFolder(CKCityNo_left_joystick, 2, 10, CKDN_joyXOutDoc, ckTx | ckEnable);
  // Consumer: Steering Position @ Right motor
  ckDefineFolder(CKCityNo_right_motor, 3, 10, CKDN_steeringPosDoc, ckRcv | ckEnable);
  // Consumer: Steering Position @ Steering
  ckDefineFolder(CKCityNo_steering, 3, 10, CKDN_steeringPosDoc, ckRcv | ckEnable);
  // Consumer: Steering Position @ Left Motor
  ckDefineFolder(CKCityNo_left_motor, 4, 10, CKDN_steeringPosDoc, ckRcv | ckEnable);

  // Connection: Throttle
  // Producer: Joystick Y Output @ Right Joystick
  ckDefineFolder(CKCityNo_right_joystick, 5, 11, CKDN_joyYOutDoc, ckTx | ckEnable);
  // Consumer: Throttle Position @ Motor
  ckDefineFolder(CKCityNo_motor, 6, 11, CKDN_throttlePosDoc, ckRcv | ckEnable);
  // Consumer: Throttle Position @ Right motor
  ckDefineFolder(CKCityNo_right_motor, 6, 11, CKDN_throttlePosDoc, ckRcv | ckEnable);
  // Consumer: Throttle Position @ Left Motor
  ckDefineFolder(CKCityNo_left_motor, 7, 11, CKDN_throttlePosDoc, ckRcv | ckEnable);

  // Connection: Steering Trim
  // Producer: Trim Buttons @ Left Joystick
  ckDefineFolder(CKCityNo_left_joystick, 8, 12, CKDN_trimDoc, ckTx | ckEnable);
  // Consumer: 2Trim @ Steering
  ckDefineFolder(CKCityNo_steering, 9, 12, CKDN_trimDoc, ckRcv | ckEnable);

  // Connection: Throttle Trim
  // Producer: Trim Buttons @ Right Joystick
  ckDefineFolder(CKCityNo_right_joystick, 8, 13, CKDN_trimDoc, ckTx | ckEnable);
  // Consumer: 2Trim @ Right motor
  ckDefineFolder(CKCityNo_right_motor, 9, 13, CKDN_trimDoc, ckRcv | ckEnable);
  // Consumer: 2Trim @ Left Motor
  ckDefineFolder(CKCityNo_left_motor, 10, 13, CKDN_trimDoc, ckRcv | ckEnable);

  // Connection: Switch
  // Producer: Switch outputs @ Left Joystick
  ckDefineFolder(CKCityNo_left_joystick, 11, 14, CKDN_switchDoc, ckTx | ckEnable);
  // Consumer: Switch @ Motor
  ckDefineFolder(CKCityNo_motor, 12, 14, CKDN_switchDoc, ckRcv | ckEnable);
  // Consumer: Switch @ Right motor
  ckDefineFolder(CKCityNo_right_motor, 12, 14, CKDN_switchDoc, ckRcv | ckEnable);
  // Consumer: Switch @ Steering
  ckDefineFolder(CKCityNo_steering, 12, 14, CKDN_switchDoc, ckRcv | ckEnable);
  // Consumer: Switch @ Left Motor
  ckDefineFolder(CKCityNo_left_motor, 13, 14, CKDN_switchDoc, ckRcv | ckEnable);

}
