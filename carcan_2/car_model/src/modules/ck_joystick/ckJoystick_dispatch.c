/* 
 * Message dispatch functionality for module ckJoystick.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software. 
 *  
 * File contents:
 * - Code for dispatching received messages to user defined handler functions.
 * - Message transmit functions for the documents in the module.
 *
 * Generation source file: /car_model/ckJoystick.kfm
 */

#include "ck.h"
#include "ckJoystick.h"


extern void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen);

// *** Functions for sending messages

void sendJoyXOutDoc(joyXOutDoc *doc) {
  ckSend(CKDN_joyXOutDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendJoyYOutDoc(joyYOutDoc *doc) {
  ckSend(CKDN_joyYOutDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendTrimDoc(trimDoc *doc) {
  ckSend(CKDN_trimDoc, (uint8_t*) doc, sizeof *doc, 0);
}


void sendSwitchDoc(switchDoc *doc) {
  ckSend(CKDN_switchDoc, (uint8_t*) doc, sizeof *doc, 0);
}



void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    default:
      onOtherMessage(doc, msgBuf, msgLen);
  }
}
