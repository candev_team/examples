/* 
 * Header file for module ckJoystick.
 * 
  * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 * Module description: 
 * Joystick with 2 trim buttons and switches.
 * 
 * Generation source file: /car_model/ckJoystick.kfm
 */

#ifndef CKJOYSTICK
#define CKJOYSTICK

#include <stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Joystick X Output. Record number: 10, List number: 1
#define CKDN_joyXOutDoc 10
 
typedef struct {
  uint16_t xOut:12; // X Output
} joyXOutDoc;

// Joystick Y Output. Record number: 11, List number: 1
#define CKDN_joyYOutDoc 11
 
typedef struct {
  uint16_t yOut:12; // Y Output
} joyYOutDoc;

// Trim Buttons. Record number: 12, List number: 1
#define CKDN_trimDoc 12
 
typedef struct {
  uint8_t xTrim:2; // X Trim
  uint8_t yTrim:2; // Y Trim
} trimDoc;

// Switch outputs. Record number: 13, List number: 1
#define CKDN_switchDoc 13
 
typedef struct {
  uint8_t switch3w:2; // Three-way Switch
} switchDoc;

#pragma pack()


// *** Transmit functions

void sendJoyXOutDoc(joyXOutDoc *doc);
void sendJoyYOutDoc(joyYOutDoc *doc);
void sendTrimDoc(trimDoc *doc);
void sendSwitchDoc(switchDoc *doc);

#endif /* CKJOYSTICK */
