/* 
 * Message dispatch functionality for module ckServo.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software. 
 *  
 * File contents:
 * - Code for dispatching received messages to user defined handler functions.
 * - Message transmit functions for the documents in the module.
 *
 * Generation source file: /car_model/ckServo.kfm
 */

#include "ck.h"
#include "ckServo.h"


// *** Functions for handling received messages

extern void onSteeringPosDoc(steeringPosDoc *doc);


extern void onTrimDoc(trimDoc *doc);


extern void onSwitchDoc(switchDoc *doc);


extern void onThrottlePosDoc(throttlePosDoc *doc);


extern void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen);


void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen) {
  switch (doc) {
    case CKDN_steeringPosDoc:    // Steering Position
      onSteeringPosDoc((steeringPosDoc*) msgBuf); 
      break;
    case CKDN_trimDoc:    // 2Trim
      onTrimDoc((trimDoc*) msgBuf); 
      break;
    case CKDN_switchDoc:    // Switch
      onSwitchDoc((switchDoc*) msgBuf); 
      break;
    case CKDN_throttlePosDoc:    // Throttle Position
      onThrottlePosDoc((throttlePosDoc*) msgBuf); 
      break;
    default:
      onOtherMessage(doc, msgBuf, msgLen);
  }
}
