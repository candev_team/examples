/* 
 * Header file for module ckServo.
 * 
  * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 * Module description: 
 * Servo node that sends PWM signal to servo or ESC.
 * 
 * Generation source file: /car_model/ckServo.kfm
 */

#ifndef CKSERVO
#define CKSERVO

#include <stdint.h>

#pragma pack(1)

// *** Document constants and structs

// Steering Position. Record number: 10, List number: 2
#define CKDN_steeringPosDoc 10
 
typedef struct {
  uint16_t pos:12; // Position
} steeringPosDoc;

// 2Trim. Record number: 12, List number: 2
#define CKDN_trimDoc 12
 
typedef struct {
  uint8_t xTrim:2; // X Trim
  uint8_t yTrim:2; // Y Trim
} trimDoc;

// Switch. Record number: 13, List number: 2
#define CKDN_switchDoc 13
 
typedef struct {
  uint8_t switch_val:2; // Switch
} switchDoc;

// Throttle Position. Record number: 11, List number: 2
#define CKDN_throttlePosDoc 11
 
typedef struct {
  uint16_t pos:12; // Position
} throttlePosDoc;

#pragma pack()


// *** Transmit functions


#endif /* CKSERVO */
