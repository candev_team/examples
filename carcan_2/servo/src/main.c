#include "ckServo.h"
#include <shell.h>

/* Proxy to run CAN Kingdom from RT-Kernel task signature */
static void ckTaskFunc(void *arg)
{
  ckappMain(); /* CAN Kingdom is Singleton (no args, assumes 1 interface) */
}

int main (void)
{
  //shell_prompt_set ("servo \n"); /* RS-232 terminal prompt (115 kbps) */
  task_delay(tick_from_ms (25)); // Give king time to start

  /* Fire up CAN Kingdom task */
  task_spawn("CAN Kingdom", ckTaskFunc, CK_TASK_PRIO, CK_TASK_STACK, NULL);

  return 0;
}
