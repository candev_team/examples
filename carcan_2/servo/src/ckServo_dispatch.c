/* 
 * Message dispatch functionality for module ckServo.
 *
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software. 
 *  
 * File contents:
 * - Code for dispatching received messages to user defined handler functions.
 * - Message transmit functions for the documents in the module.
 *
 * Generation source file: /airplane_model/ckServo.kfm
 */

#include "ck.h"
#include "ckServo.h"


// *** Functions for handling received messages

extern void onYawDoc(yawDoc *doc);
extern void onThrottleDoc(throttleDoc *doc);
extern void onPitchDoc(pitchDoc *doc);
extern void onRollDoc(rollDoc *doc);
extern void onTrimDoc(trimDoc *doc);
extern void onStfSwitchDoc(stfSwitchDoc *doc);
extern void onRmSwitchDoc(rmSwitchDoc *doc);
extern void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen);

void ckappDispatch(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen)
{
  switch (doc)
  {
  case CKDN_yawDoc: // Yaw Position
    onYawDoc((yawDoc *)msgBuf);
    break;
  case CKDN_throttleDoc: // Throttle Position
    onThrottleDoc((throttleDoc *)msgBuf);
    break;
  case CKDN_pitchDoc: // Pitch Position
    onPitchDoc((pitchDoc *)msgBuf);
    break;
  case CKDN_rollDoc: // Roll Position
    onRollDoc((rollDoc *)msgBuf);
    break;
  case CKDN_trimDoc: // Trims
    onTrimDoc((trimDoc *)msgBuf);
    break;
  case CKDN_stfSwitchDoc: // Save Trim and Flap Switches
    onStfSwitchDoc((stfSwitchDoc *)msgBuf);
    break;
  case CKDN_rmSwitchDoc: // Rates and Mixing Switch
    onRmSwitchDoc((rmSwitchDoc *)msgBuf);
    break;
  default:
    onOtherMessage(doc, msgBuf, msgLen);
  }
}

void ckappDispatchFreeze(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen)
{
  if (doc == rdocKing)
    onOtherMessage(doc, msgBuf, msgLen);
}