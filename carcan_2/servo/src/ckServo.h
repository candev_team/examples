/*
 * Header file for module ckServo.
 * 
 * WARNING: This is generated code! Editing this file might complicate the
 * maintenance of the software.
 *
 * File contents:
 * - Structs and constants for the documents of the module
 * - Declaration of message transmit functions
 *
 * Generation source file: /car_model/ckServo.kfm
 */
 /**
 * @file ckServo.h
 * @brief Servo city.
 * Module description: 
 * Servo city that sends PWM signal to servo or ESC. The capital can select mode of
 * operation 1 (Left motor), 2 (Right motor), 3 (Steering), 4 (Single motor).
 * City takes 'input' signal on CAN and outputs RC-servo pulses accordingly.
 * 'input' is unsigned 12-bit and converted like so (12-bit example):
 *
 * 'input'  Pulse
 * ---------------
 *  4095    2.2 ms
 *  2046    1.5 ms     Pulse period is constant 20 ms
 *     0    0.8 ms
 * 
 * A 'trim' signal (2-bit) is also received and fed to an integrator, which is
 * added to 'input' in order to adjust servo mid position. If 'trim' is received
 * with 'input' close to min/max, servo direction can be changed:
 *
 * 'input'  'trim'          Action
 * ----------------------------------------
 * ~max      01b     Set normal direction
 * ~max      10b     Set reverse direction
 * ~middle   01b     Trim [+] mid position
 * ~middle   11b     Reset mid position
 * ~middle   10b     Trim [-] mid position
 * ~min      01b     Unused in this edition
 * ~min      10b     Unused in this edition
 *
 * An active 'trim' will traverse the whole pulse range in 10 seconds.
 * Integrator state and direction is preserved in flash memory between
 * power cycles if and only when switch save has been received.
 *
 * A safety feature will output pulses corresponding to 'input' middle, in case 'input'
 * signal has been absent for more than 200 ms.
 *
 * Intended hardware is servo node with STM32F3 MCU.
 *
 * @authors Hashem Hashem & Andre' Idoffsson.
 * @date 2019-07
 */

#ifndef CKSERVO
#define CKSERVO

#include <stdint.h>
#include <kern.h> /* task_t */
#include "tasks.h"

#ifdef __cplusplus
extern "C" {
#endif

/*--- Defines ---------------------------------------------------------------*/
#define RX_TASK_PRIO      3   /* RX task priority */
#define RX_TASK_STACK  2048   /* RX task stack size */
#define CK_TASK_PRIO      3   /* CAN Kingdom task priority */
#define CK_TASK_STACK  2048   /* CAN Kingdom task stack size */

// #define ADC_SVO_CUR    "/adc1/3"       /* ADC devices in use */
// #define ADC_BAT_VOLT   "/adc1/4"
// #define ADC_SVO_VOLT   "/adc2/1"

#define TRIM_FILE  "/disk1/trim.cal"   /* Flash memory for trim/dir */

#define POS_MAX 4095 /* Position signal is assumed to be 12-bit. */

/**
 * @brief Enum for different city modes.
 * Steering, LMotor, RMotor are car specific modes.
 */
typedef enum {Motor = 0, LAileron, RAileron, LFlap, RFlap,
              Elevator, Rudder, Steering, LMotor, RMotor} nodeType;

/**
 * @brief Struct for servo
 *
 * This struct is used to store variables, latest received inputs,
 * timer handles and task handles.
 */
typedef struct {
  nodeType  type;         /**< Defines behavior of servo node */
  task_t    *tasks[8];    /**< Tasks in system */
  tmr_t     *aliveTimer;  /**< Timer for auto mid-point on input disappearance */
  tmr_t     *stopTimer;   /**< Timer for periodically sending mid signal on loss of connection */
  tick_t    lastTrimTick; /**< Tick for last Trim input */
  int       pwm, stop;    /**< PWM handle */
  int16_t   trim;         /**< Trim integrator */
  uint16_t  minDuty, maxDuty; /**< Duty range */
  uint16_t  xPos, yPos;   /**< Last received inputs */
  uint16_t  rollPos;      /**< Used in rudder servo cause xPos is used by rudder */
  uint8_t   xTrim, yTrim, saveSw, flapSw, ratesSw, mixSw; /**< Last received inputs */
  uint8_t   reverse;      /**< Reverse direction flag (0/1). 1 is reversed dir, 0 no change. */
  float     ratesScaler;  /**< Duty scaling ratio */
  float     mixScaler;    /**< Scaling ratio used when mixing */
  float     mixValues[2]; /**< Values set by the king and used when mixing */
  float     rates[2];     /**< Values set by the king and used as rates */
} Servo;

#pragma pack(1)

// *** Document constants and structs

// Yaw Position. Record number: 10, List number: 1
#define CKDN_yawDoc 10

typedef struct {
  uint16_t pos:12; // Position
} yawDoc;

// Throttle Position. Record number: 11, List number: 1
#define CKDN_throttleDoc 11

typedef struct {
  uint16_t pos:12; // Position
} throttleDoc;

// Pitch Position. Record number: 12, List number: 1
#define CKDN_pitchDoc 12

typedef struct {
  uint16_t pos:12; // Position
} pitchDoc;

// Roll Position. Record number: 13, List number: 1
#define CKDN_rollDoc 13
 
typedef struct {
  uint16_t pos:12; // Position
} rollDoc;

// Trims. Record number: 14, List number: 1
#define CKDN_trimDoc 14
 
typedef struct {
  uint8_t xTrim:2; // X Trim
  uint8_t yTrim:2; // Y Trim
} trimDoc;

// Save Trim and Flap Switches. Record number: 15, List number: 1
#define CKDN_stfSwitchDoc 15
 
typedef struct {
  uint8_t saveSwitch:1; // Save Trim Switch
  uint8_t flapSwitch:2; // Flap Control Switch
} stfSwitchDoc;

// Rates and Mixing Switch. Record number: 16, List number: 1
#define CKDN_rmSwitchDoc 16
 
typedef struct {
  uint8_t ratesSwitch:2; // Rates Switch
  uint8_t mixSwitch:2; // Mixing Switch
} rmSwitchDoc;

#pragma pack()

void  ckappMain( void );

#endif /* CKSERVO */
