/**
 * @file servo/src/tasks.h
 * @brief This file contains functions relating to tasks running in the system.
 */

#ifndef  _TASKS_H
#define  _TASKS_H

#include <stdint.h>

/* Trim signals bit encodings */
#define TRIM_POS 0x1
#define TRIM_NEG 0x2

#define TRIM_DUR_MS 10000 /* Duration {ms} to trim full position range */

/* Switch signals bit encodings for 2 position switch*/
#define SWITCH2P_OFF 0x0
#define SWITCH2P_ON 0x1

/* Switch encodings 3 position switch */
#define SWITCH3P_UP 0
#define SWITCH3P_MID 1
#define SWITCH3P_DOWN 2

/**
 *  @brief Convert position signal to PWM duty.
 *
 *  This function takes in @p pos and converts it into a PWM signal.
 *  Offset is added to the input according to the set trim and can even
 *  invert the input given depending on the trim values.
 *
 *  In order to become a duty signal the input is scaled and limited to DUTY_MIN & DUTY_MAX
 *
 *  @param  pos    12-bit unsigned value.
 *  @param  arg    Arg is of the type Servo.
 *  @return duty
 */
uint16_t posToDuty(uint16_t pos, void *arg);

void onYaw(void* arg);
void onPitch(void* arg);
void onRoll(void* arg);
void onStfSwitch(void* arg);
void onRmSwitch(void* arg);

/**
 * @brief Function called when throttle letter is received.
 * @param arg servo to do the operation on.
 */
void onThrottle(void* arg);

/**
 * @brief Function called when trim letter is received.
 * @param arg servo to do the operation on.
 */
void onTrim(void* arg);

#endif /* TASKS_H */
