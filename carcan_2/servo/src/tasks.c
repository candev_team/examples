#include "tasks.h"
#include "ck.h"
#include "ckServo.h"
#include <kern.h>
#include <pwm/pwm.h> /* PWM driver */

/*--- Helper Functions-------------------------------------------------------*/

/* Convert position signal to PWM duty. */
uint16_t posToDuty(uint16_t pos, void *arg)
{
  Servo *p = arg;
  int16_t tmp = pos + p->trim;
  int32_t dutyRange;
  int16_t duty;
  int16_t midPoint, newMinDuty;

  /* Scale duty range according to rates value and get new duty min value */
  midPoint = p->minDuty + (p->maxDuty - p->minDuty) / 2;
  dutyRange = (int32_t)(p->ratesScaler * (p->maxDuty - p->minDuty));
  newMinDuty = midPoint - dutyRange / 2;

  /* Is negative direction needed? */
  if (p->reverse)
    tmp = (uint16_t)POS_MAX - tmp; /* Invert position */

  duty = dutyRange * tmp / POS_MAX + newMinDuty;

  /* Limit to not exceed duty range */
  if (duty > p->maxDuty)
    duty = p->maxDuty; 
  if (duty < p->minDuty)
    duty = p->minDuty;

  return (uint16_t)duty;
}

/* Save servo trim/direction to file. */
static void saveTrim(Servo *p)
{
  int file;

  file = open(TRIM_FILE, O_CREAT | O_WRONLY, 0);
  write(file, &p->trim, sizeof p->trim);
  write(file, &p->reverse, sizeof p->reverse);
  close(file);
}

/* Trim mid position or change direction, 'dt' is reception period {ms} */
static void trim(Servo *p, uint16_t trim, uint16_t dt)
{
  int16_t d;
  uint16_t modifier;
  uint8_t skipTrim = 0;

  switch (p->type)
  {
  /* X Trim */
  case LAileron:
  case RAileron:
    skipTrim = 1; /* Don't trim later, only change dir if needed */
    /* no break */
  case Steering:
  case Rudder:
    modifier = p->xPos;
    break;

  /* Y Trim */
  case LMotor:
  case RMotor:
  case Motor:
  case Elevator:
    modifier = p->yPos;
    break;

  /* No trim for other types! */
  default:
    break;
  }

  if (modifier <= POS_MAX - 500 && !skipTrim) /* Input NOT close to max? (mid position trim) */
  {
    d = (int32_t)(2 * POS_MAX) * dt / TRIM_DUR_MS; /* Trim amount */

    /* [+] trim? */
    if (trim == TRIM_POS)
    {
      p->trim += d;
      if (p->trim > POS_MAX / 2)
        p->trim = POS_MAX / 2; /* Limit */
    }
    /* [-] trim? */
    else if (trim == TRIM_NEG)
    {
      p->trim -= d;
      if (p->trim < -POS_MAX / 2)
        p->trim = -POS_MAX / 2; /* Limit */
    }
    /* Reset trim? */
    else if (trim == (TRIM_POS | TRIM_NEG))
    {
      p->trim = 0;
    }
    else
    {
      return; /* No 'trim' input, no change/save */
    }
  }
  /* 'modifier' close to max? */
  else if (POS_MAX - 500 < modifier)
  {
    if (trim == TRIM_POS)
      p->reverse = 0; /* [+] set normal direction */
    if (trim == TRIM_NEG)
      p->reverse = 1; /* [-] "reverse" */
  }
  else
  {
    return; /* No 'trim' input, no change/save */
  }
}

/* Trim mid position in relation to each other i.e. one [+] and the other [-],*/
/* 'dt' is reception period {ms} */
static void trimDifferentiate(Servo *p, uint16_t trim, uint16_t dt)
{
  int16_t d;
  d = (int32_t)(2 * POS_MAX) * dt / TRIM_DUR_MS; /* Trim amount */

  /* Left aileron, left motor */
  if (p->type == LAileron || p->type == LMotor)
  {
    /* [+] trim? */
    if (trim == TRIM_POS)
    {
      p->trim += d;
      if (p->trim > POS_MAX / 2)
        p->trim = POS_MAX / 2; /* Limit */
    }
    /* [-] trim? */
    else if (trim == TRIM_NEG)
    {
      p->trim -= d;
      if (p->trim < -POS_MAX / 2)
        p->trim = -POS_MAX / 2; /* Limit */
    }
    /* Reset trim? */
    else if (trim == (TRIM_POS | TRIM_NEG))
    {
      p->trim = 0;
    }
  }
  /* Right aileron, right motor */
  else
  {
    /* [-] trim? */
    if (trim == TRIM_POS)
    {
      p->trim -= d;
      if (p->trim < -POS_MAX / 2)
        p->trim = -POS_MAX / 2; /* Limit */
    }
    /* [+] trim? */
    else if (trim == TRIM_NEG)
    {
      p->trim += d;
      if (p->trim > POS_MAX / 2)
        p->trim = POS_MAX / 2; /* Limit */
    }
    /* Reset trim? */
    else if (trim == (TRIM_POS | TRIM_NEG))
    {
      p->trim = 0;
    }
  }
}

/*--- Tasks -----------------------------------------------------------------*/

void onYaw(void *arg)
{
  Servo *p = arg;
  uint16_t duty;
  uint16_t pos;
  uint16_t midPos = POS_MAX / 2;

  for (;;)
  {
    task_stop();
    pos = p->xPos;

    /* Rudder support on aileron movement */
    if (p->mixSw == SWITCH3P_DOWN
        && ((pos > midPos && pos < midPos * (1.0 + p->mixScaler))
        || (pos < midPos && pos > midPos * (1.0 - p->mixScaler))))
    {
      /* If right roll, add right yaw else add left yaw */
      /* Right roll */
      /* TODO: Right now this assumes that the right joystick is inverted, ie 0
       * is the rightmost position on the joystick while POS_MAX is to the left.
       * Earlier we have assumed that POS_MAX is to the right, but the rudder doesn't
       * listen to trims from the right joystick and cannot know if the ailerons are reversed or not.
       * This needs a change in the modules in airplane_model to be implemented.
       */
      if (p->rollPos < midPos - 300)
        pos = (uint16_t)(midPos * (1.0 + p->mixScaler));
      /* Left roll */
      else if (p->rollPos > midPos + 300)
        pos = (uint16_t)(midPos * (1.0 - p->mixScaler));
      /* No roll */
      else
        pos = midPos;
    }

    /* Calculate duty, output to servo */
    duty = posToDuty(pos, p);
    pwm_duty_cycle_set(p->pwm, duty);
  }
}

void onThrottle(void *arg)
{
  Servo *p = arg;
  uint16_t duty;
  uint16_t pos;
  double scaler = 1.0; /* No scaling factor */

  for (;;)
  {
    task_stop();

    pos = p->yPos;

    /* If left motor and value on lastxPos below mid then scale pos */
    if (p->type == LMotor &&
      p->xPos < (POS_MAX / 2 - 100) && p->yPos > (POS_MAX / 2 + 100))
    {
      /* Generates value between 0.9 and 1.0 */
      scaler = ((double)(p->xPos)/(POS_MAX/2))*0.2+0.8;
      pos = (uint16_t)(pos * scaler); /* Generates value between 0 and POS_MAX/2 */
    }
    /* If right motor and value on lastxPos above mid then scale pos */
    else if (p->type == RMotor &&
      p->xPos > (POS_MAX / 2 + 100) && p->yPos > (POS_MAX / 2 + 100))
    {
      /* Generates value between 0.9 and 1.0*/
      scaler = ((double)((p->xPos)/POS_MAX)-1)*0.2+0.8;
      pos = (uint16_t)(pos * scaler); /* Generates value between POS_MAX/2 and POS_MAX*/
    }
    /* Calculate duty, output to servo */
    duty = posToDuty(pos, p);
    pwm_duty_cycle_set(p->pwm, duty);
  }
}

void onPitch(void *arg)
{
  Servo *p = arg;
  int16_t pos;
  uint16_t duty;

  for (;;)
  {
    task_stop();
    pos = p->yPos;

    /* Elevator compensation on flap lowering */
    if (p->mixSw == SWITCH3P_MID)
    {
      pos = (int16_t)(p->yPos - p->mixScaler * p->yPos);
      pos = pos < 0 ? 0 : pos; /* Limit because we need it unsigned later */
    }

    /* Calculate duty, output to servo */
    duty = posToDuty((uint16_t)pos, p);
    pwm_duty_cycle_set(p->pwm, duty);
  }
}

void onRoll(void *arg)
{
  Servo *p = arg;
  uint16_t duty;
  int16_t pos;

  for (;;)
  {
    task_stop();
    /* We assume a joystick x-axis gives minimum output when moved to the left edge
     * and maximum output when moved to the right. That is,
     * on max right roll pos == POS_MAX (4095), while on max left it's 0.
     * To roll right, right aileron is lifted while left aileron is lowered.
     * To roll left, the reverse applies. Therefore, we give a pos that generates
     * minimum duty for pos == POS_MAX and maximum duty for pos == 0 on left aileron.
     * right aileron is unchanged.
     */
    pos = (p->type == LAileron) ? (POS_MAX - p->xPos) : p->xPos;

    /* Calculate duty, output to servo */
    duty = posToDuty(pos, p);
    pwm_duty_cycle_set(p->pwm, duty);
  }
}

void onTrim(void *arg)
{
  Servo *p = arg;
  tick_t tick;
  uint16_t dt;

  for (;;)
  {
    task_stop();
    /* Calculate time dt {ms} since last trim rx */
    tick = tick_get();
    dt = tick_to_ms(tick - p->lastTrimTick);
    p->lastTrimTick = tick;

    switch (p->type)
    {
    case Motor:
    case Elevator:
      if (p->yTrim)
        trim(p, p->yTrim, dt);
      break;
    case LAileron:
    case RAileron:
      if (p->xTrim)
      {
        trim(p, p->xTrim, dt);
        trimDifferentiate(p, p->xTrim, dt);
      }
      break;
    case Steering:
    case Rudder:
      if (p->xTrim)
        trim(p, p->xTrim, dt);
      break;
    case LMotor:
    case RMotor:
      if (p->yTrim)
        trim(p, p->yTrim, dt);
      if (p->xTrim)
        trimDifferentiate(p, p->xTrim, dt);
      break;
    default:
      break;
    }
  }
}

void onStfSwitch(void *arg)
{
  Servo *p = arg;
  uint16_t duty, min, mid, max;

  for (;;)
  {
    task_stop();
    if (p->saveSw == SWITCH2P_ON)
      saveTrim(p);

    /* Flap positions. min is cruise position (straight flaps), mid is mid pos
     * and max is fully lowered or "landing" position.
     */
    if (p->reverse) {
      min = POS_MAX;
      mid = POS_MAX / 2;
      max = 0;
    }
    else
    {
      min = 0;
      mid = POS_MAX / 2;
      max = POS_MAX;
    }
    /* Flaps */
    if (p->type == LFlap || p->type == RFlap)
    {
      switch (p->flapSw)
      {
      case SWITCH3P_UP:
        duty = posToDuty(min, p);
        pwm_duty_cycle_set(p->pwm, duty);
        break;

      case SWITCH3P_MID:
        duty = posToDuty(mid, p);
        pwm_duty_cycle_set(p->pwm, duty);
        break;

      case SWITCH3P_DOWN:
        duty = posToDuty(max, p);
        pwm_duty_cycle_set(p->pwm, duty);
      }
    }
  }
}

void onRmSwitch(void *arg)
{
  Servo *p = arg;

  for (;;)
  {
    task_stop();
    /* Scale duty depending on rates switch */
    switch (p->ratesSw)
    {
    /* High rates */
    case SWITCH3P_UP:
      p->ratesScaler = 1.0;
      break;

    /* Mid rates */
    case SWITCH3P_MID:
      p->ratesScaler = p->rates[0];
      break;

    /* Low rates  */
    case SWITCH3P_DOWN:
      p->ratesScaler = p->rates[1];
      break;
    }

    switch (p->mixSw)
    {
    /* No mixing */
    case SWITCH3P_UP:
      p->mixScaler = 0;
      break;

    /* Elevator compensation on flap lowering */
    case SWITCH3P_MID:
      if (p->type == Elevator) /* Only update scaler for elevator */
      {
        switch (p->flapSw) /* Check flap positions */
        {
        case SWITCH3P_UP: /* Fully raised */
          p->mixScaler = 0; /* No mixing */
          break;
        case SWITCH3P_MID:
          p->mixScaler = p->mixValues[0];  // Percentage to lower elevator;
          break;
        case SWITCH3P_DOWN: /* Fully lowered */
          p->mixScaler = p->mixValues[1];
          break;
        }
      }
      break;

    /* Rudder support on aileron movement */
    case SWITCH3P_DOWN:
      p->mixScaler = p->mixValues[0]; /* Move rudder x % of aileron position per direction */
      break;
    }
  }
}
