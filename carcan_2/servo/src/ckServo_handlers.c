/* 
 * Handler implementation for module ckServo. 
 * 
 *
 * NOTE: This file is not meant to be re-generated. It should be copied to a
 * location where it is not overwritten when code is re-generated. 
 * 
 * File contents:
 * - Message handler functions
 * - Functions called from the application main loop
 * - CAN Kingdom stack callback functions
 * 
 * Generation source file: /car_model/ckServo.kfm
 */

/**
 * @file ckServo_handlers.c
 * @brief This file contains functions for handling reception of messages.
 *
 * It also has initialization functions, timer callbacks and the timeout check.
 */
#include "ck.h"
#include "ckServo.h"
#include "tasks.h"
#include <kern.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <bsp.h>
#include <adc/adc.h>
#include <pwm/pwm.h> /* PWM driver */

#define PWM_DEV "/pwm0/3" /* PWM device for servo pulse */

#define ALIVE_DLY_MS 100 /* Input timeout {ms} before forced mid pos */
#define STOP_DLY_MS 50   /* Interval for sending mid signal */

// #define  DUTY_MAX_STEERING 1000 /* PWM duty (10%) for 2 ms pulse */
// #define  DUTY_MIN_STEERING 500  /* PWM duty ( 5%) for 1 ms pulse */
#define DUTY_MAX_MOTOR 1100 /* PWM duty (11%) for 2.2 ms pulse */
#define DUTY_MIN_MOTOR 400  /* PWM duty ( 4%) for 0.8 ms pulse */

/*--- Object allocations ----------------------------------------------------*/

Servo s; /* CAN Node Servo */

/*--- Static functions ------------------------------------------------------*/

/**
 * @brief Refreshes the keepAlive timer.
 * @param s Servo to do the operation on.
 */
static void keepAlive(Servo *s)
{
  s->stop = 0; /* Release forced mid pos if active */
  tmr_stop(s->aliveTimer);
  tmr_set(s->aliveTimer, tick_from_ms(ALIVE_DLY_MS));
  tmr_start(s->aliveTimer);
}

/**
 * @brief Timer callback for aliveTimer.
 *
 * Force servo pulse corresponding to neutral input if timer reaches its end.
 *
 * @param timer Calling timer handle.
 * @param arg Servo to do the operation on.
 */
static void onAliveTimer(tmr_t *timer, void *arg)
{
  Servo *s = arg;
  s->stop = 1;
  tmr_stop(timer); /* Stop timer */
  tmr_start(s->stopTimer);
}

/**
 * @brief Timer callback for stopTimer.
 *
 *
 * @param timer Calling timer handle.
 * @param arg Servo to do the operation on.
 */
static void onStopTimer(tmr_t *timer, void *arg)
{
  Servo *s = arg;
  if (s->stop)
  {
    task_start(s->tasks[7]);
    return;
  }
  tmr_stop(s->stopTimer);
}

/**
 * @brief Restore trim/direction from file.
 *
 * @param s Servo to do the operation on.
 * @return Non-zero if file not found.
 */
static int restoreTrim(Servo *s)
{
  int file;
  struct stat status;

  if (stat(TRIM_FILE, &status) != 0)
    return 1; /* File not found? */

  file = open(TRIM_FILE, O_RDONLY, 0);
  read(file, &s->trim, sizeof s->trim);
  read(file, &s->reverse, sizeof s->reverse);
  close(file);
  return 0;
}

/* Makes plane stop throttle, lowers flaps as if landing
 * and gives a little upward pitch so that the plane
 * glides down and lands. This happens when the plane
 * goes out of range of the transmitter.
 *
 * This is npt enough since the plane can go out of range
 * in many different positions and this assumes straight flight. */
static void onStop(void *arg)
{
  Servo *s = arg;
  uint16_t pos;

  for (;;)
  {
    task_stop();
    tmr_stop(s->stopTimer);
    tmr_set(s->stopTimer, tick_from_ms(ALIVE_DLY_MS));
    tmr_start(s->stopTimer);

    switch(s->type)
    {
    case LFlap:
    case RFlap:
      pos = s->reverse == 1 ? 0 : POS_MAX;
      pwm_duty_cycle_set(s->pwm, posToDuty(pos, s)); /* Lower flaps */
      break;

    case Elevator:
      pos = (uint16_t)(0.80 * (POS_MAX / 2));
      pwm_duty_cycle_set(s->pwm, posToDuty(pos, s)); /* Give a little upward elevator */
      break;
    default:
      pwm_duty_cycle_set(s->pwm, posToDuty(POS_MAX / 2, s)); /* Send mid value for neutral */
    }
  }
}

/*--- Initialization --------------------------------------------------------*/
/**
 * @brief Function to initialize a servo.
 *
 * @param s Arg of the type Servo.
 */
static void svoInit(Servo *s)
{
  s->type = Motor; /* Default to motor node */
  s->minDuty = DUTY_MIN_MOTOR;
  s->maxDuty = DUTY_MAX_MOTOR;
  /* Default to no rates no matter switch position */
  s->ratesScaler = 1.0f;
  s->rates[0] = 1.0f;
  s->rates[1] = 1.0f;
  /* Default to no mixing no matter switch position */
  s->mixScaler = 0.0f;
  s->mixValues[0] = 0.0f;
  s->mixValues[1] = 0.0f;
  /*
   * Always measure VCC_SERVO and VDD_SENSOR before connecting any peripherals.
   * Values are depends on Vin and must be verified for your voltage.
   * Change the values (and move setup to application) to get correct voltage
   * for your connected peripherals.
   */
  set_i2c_pot_vcc_servo(0x57);

  if (restoreTrim(s))
  {              /* Try read trim/dir, */
    s->trim = 0; /* use default if file not read. */
    s->reverse = 0;
  }

  s->pwm = pwm_open(PWM_DEV); /* Activate PWM output for servo pulse */
  ASSERT(s->pwm > 0);
  //pwm_frequency_set(s->pwm, 200); /* Set PWM freq. */
  pwm_start(s->pwm);                                       /* PWM period already set to 20 ms in BSP (50 Hz) */
  pwm_duty_cycle_set(s->pwm, posToDuty((POS_MAX / 2), s)); /* Force mid pos until CAN input arrives */

  /* Spawn rx Tasks */
  s->tasks[0] = task_spawn("Yaw handler task", onYaw, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[1] = task_spawn("Throttle handler task", onThrottle, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[2] = task_spawn("Pitch handler task", onPitch, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[3] = task_spawn("Roll handler task", onRoll, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[4] = task_spawn("Trim handler task", onTrim, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[5] = task_spawn("STF Switches handler task", onStfSwitch, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[6] = task_spawn("RM Switches handler task", onRmSwitch, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));
  s->tasks[7] = task_spawn("CAN Rx Stop", onStop, RX_TASK_PRIO, RX_TASK_STACK, s);
  task_delay(tick_from_ms(2));

  /* Timer to force mid pos (stop controlled machine) when CAN input ceases */
  s->aliveTimer = tmr_create(tick_from_ms(ALIVE_DLY_MS), onAliveTimer, s, TMR_ONCE);
  s->stopTimer = tmr_create(tick_from_ms(STOP_DLY_MS), onStopTimer, s, TMR_ONCE);
}

// *** Functions for handling received messages
/* Also acts as steering doc for car */
void onYawDoc(yawDoc *msg)
{
  /* Start new time-out period */
  keepAlive(&s);
  /* Store this position for trim modifications */
  s.xPos = msg->pos;
  if (s.type == Steering)
    task_start(s.tasks[3]); // Roll task
  else if (s.type == Rudder)
    task_start(s.tasks[0]); // Yaw task
}

void onThrottleDoc(throttleDoc *msg)
{
  keepAlive(&s);
  s.yPos = msg->pos;
  task_start(s.tasks[1]);
}

void onPitchDoc(pitchDoc *msg)
{
  keepAlive(&s);
  s.yPos = msg->pos;
  task_start(s.tasks[2]);
}

void onRollDoc(rollDoc *msg)
{
  keepAlive(&s);
  if (s.type == Rudder)
  {
    s.rollPos = msg->pos;
  }
  else
  {
    s.xPos = msg->pos;
    task_start(s.tasks[3]);
  }
}

void onTrimDoc(trimDoc *msg)
{
  keepAlive(&s);
  s.xTrim = msg->xTrim;
  s.yTrim = msg->yTrim;
  task_start(s.tasks[4]);
}

void onStfSwitchDoc(stfSwitchDoc *msg)
{
  keepAlive(&s);
  s.saveSw = msg->saveSwitch;
  s.flapSw = msg->flapSwitch;
  task_start(s.tasks[5]);
}

void onRmSwitchDoc(rmSwitchDoc *msg)
{
  keepAlive(&s);
  s.ratesSw = msg->ratesSwitch;
  s.mixSw = msg->mixSwitch;
  task_start(s.tasks[6]);
}

void onOtherMessage(uint16_t doc, uint8_t *msgBuf, uint8_t msgLen)
{
  /* King's page 150, sets duty range 
  *  Line 2 = min duty LSB
  *  Line 3 = min duty MSB
  *  Line 4 = max duty LSB
  *  Line 5 = max duty MSB
  *  1000 == 10 %, 400 == 4%, and so on
  */
  if (doc == rdocKing && msgBuf[1] == 150 && msgLen == 8)
  {
    uint16_t minDuty = (msgBuf[3] << 8) | msgBuf[2];
    uint16_t maxDuty = (msgBuf[5] << 8) | msgBuf[4];
    s.minDuty = minDuty;
    s.maxDuty = maxDuty;
  }
  /* King's page 151, sets mix values in percent
   * Line 2 = value for mixValues[0]
   * Line 3 = value for mixValues[1]
   * Values are 0 to 255 but capped at 100 for 100%
   */
  if (doc == rdocKing && msgBuf[1] == 151 && msgLen == 8)
  {
    float val1 = msgBuf[2] > 100 ? 1.0 : msgBuf[2] / 100.0;
    float val2 = msgBuf[3] > 100 ? 1.0 : msgBuf[3] / 100.0;
    s.mixValues[0] = val1;
    s.mixValues[1] = val2;
  }
  /* King's page 152, sets rates values in percent
   * Line 2 = value for rates[0]
   * Line 3 = value for rates[1]
   * Values are 0 to 255 but capped at 100 for 100%
   */
  if (doc == rdocKing && msgBuf[1] == 152 && msgLen == 8)
  {
    float val1 = msgBuf[2] > 100 ? 1.0 : msgBuf[2] / 100.0;
    float val2 = msgBuf[3] > 100 ? 1.0 : msgBuf[3] / 100.0;
    s.rates[0] = val1;
    s.rates[1] = val2;
  }
  /* King's page 153, reverses direction
   * Has no content, city reverses direction upon reception.
   * Meant to be sent once from a CAN interface or such, and then
   * save trim to keep direction.
   */
  if (doc == rdocKing && msgBuf[1] == 153 && msgLen == 8)
  {
    s.reverse = s.reverse == 0 ? 1 : 0;
  }

}

// *** Functions called in the main loop
/**
 * @brief Gives other tasks (application) a chance to execute.
 */
void ckappOnIdle(void)
{
  task_delay(0); /* Giver others (application) chance to execute */
}

bool checkShutdown(void)
{
  // TODO: This function probably should be implemented.
  //
  // This is the place for code that checks if the
  // application should terminate.

  return 0;
}

// *** Callback functions from the CAN Kingdom stack

/**
 * @brief Handles changing city mode.
 *
 * Only needs to be implemented if city has several modes.
 *
 * @param mode Sent by the capital.
 */
void ckappSetCityMode(uint8_t mode)
{
  switch (mode)
  {
  case 1:
    s.type = Motor;
    break;
  case 2:
    s.type = Rudder;
    break;
  case 3:
    s.type = LAileron;
    break;
  case 4:
    s.type = RAileron;
    break;
  case 5:
    s.type = LFlap;
    break;
  case 6:
    s.type = RFlap;
    break;
  case 7:
    s.type = Elevator;
    break;
  case 8:
    s.type = LMotor;
    break;
  case 9:
    s.type = RMotor;
    break;
  case 10 :
    s.type = Steering;
    break;
  }
}

void ckappSetActionMode(ckActMode actMode)
{
  // TODO: This function maybe should be implemented.
  //
  // This function is called by the CAN Kingdom stack when the application
  // should change action mode.
  //
  // actMode can be either ckRunning or ckFreeze.
}

int ckappMayorsPage(uint8_t page, uint8_t *buf)
{
  // TODO: This function is optional and should be implemented if the module
  // is supposed to support any application specific Mayor's pages.

  return 1; // 1 means that the page is not supported
}

void ckappDispatchRTR(uint16_t doc, bool txF)
{
  // This function only has to be implemented if the application is supposed to
  // support RTR messages.
}

/*
 * This function is called by the CAN Kingdom stack implementation when the 
 * application starts. It should not return until the application exists.
 */
/**
 * @brief The main task of the system.
 *
 * Initializes the servo and runs the CanKingdom stack.
 */
void ckappMain(void)
{
  svoInit(&s);
  // This CAN Kingdom function initializes the stack
  ckInit(ckStartWithKing);

  for (;;)
  {
    // This gives the application a change to run code when there
    // are no received messages to process
    ckappOnIdle();

    // This calls the CAN Kingdom stack, which checks for
    // messages and reports them with ckappDispatch
    ckMain();

    // This gives the application a chance to check for application shutdown
    if (checkShutdown())
      break;
  }
}
