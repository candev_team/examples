/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: main.c 5635 2015-06-12 13:41:07Z rtlbjca $
 *------------------------------------------------------------------------------
 */

/*! \mainpage Kvaser CANdev I/O Module A HS example
 *
 * \section intro_sec Introduction
 * The Kvaser CANdev I/O Module A HS example shows how the different inputs/outputs can be used and
 * how to create a multitask application and reserve data integrity.The example
 * is divided in three block input sampling, process data and output writing.
 *
 * \subsection sampling_sec Input sampling
 * To sample all the inputs on the Kvaser CANdev I/O Module A HS, multiple tasks have been created.
 * These tasks can both be prioritized and scheduled individually, e.g.
 * critical inputs are give high priority, slow changing sensor inputs are
 * scheduled to sample at low rate.
 *
 * \dot
 * digraph samping_machine {
 *     node [shape = circle, label="read input data", fontsize=10] read;
 *     node [shape = doublecircle, label="acquire process data lock", fontsize=10] lock;
 *     node [shape = circle, label="write process data", fontsize=10] write;
 *     node [shape = doublecircle, label="release process data lock", fontsize=10] unlock;
 *
 *     node [shape = point ]; qi
 *     qi -> read;
 *     read -> lock  [ label = "read done", fontsize=10];
 *     lock -> write   [ label = "lock acquired", fontsize=10];
 *     write -> unlock  [ label = "write done", fontsize=10];
 *     unlock -> read   [ label = "lock released", fontsize=10];
 * }
 * \enddot
 *
 * \subsection process_sec Process data
 * The process data is a data structure representing the various inputs and
 * outputs on the Kvaser CANdev I/O Module A HS, this data structure is used during reading/writing
 * of new output/input.
 *
 * \subsection write_sec Writing to outputs
 * To write all the outputs on the Kvaser CANdev I/O Module A HS, multiple tasks have been created.
 * These tasks can both be prioritized and scheduled individually, e.g.
 * critical outputs are give high priority, slow changing actuator outputs are
 * scheduled to be written at low rate.
 *
 * \dot
 * digraph output_machine {
 *     node [shape = doublecircle, label="acquire process data lock", fontsize=10] lock;
 *     node [shape = circle, label="read process data", fontsize=10] read;
 *     node [shape = doublecircle, label="release process data lock", fontsize=10] unlock;
 *     node [shape = circle, label="write output data", fontsize=10] write;
 *
 *     node [shape = point ]; qi
 *     qi -> lock;
 *     lock -> read  [ label = "lock acquired", fontsize=10];
 *     read -> unlock   [ label = "read done", fontsize=10];
 *     unlock -> write  [ label = "lock released", fontsize=10];
 *     write -> lock   [ label = "write done", fontsize=10];
 * }
 * \enddot
 *
 * \subsection can_sec CAN Communication
 * An example is given on how to configure a start CAN communication using the
 * init_can function in combination of two tasks handling receiving and
 * transmitting of CAN messages.
 *
 * \subsubsection can_tx_sec CAN transmit messages
 * A CAN transmit task is used to send values of the four
 * analog inputs in can messages id 0x100-0x103, dlc=4,
 * data[0]=LSB,...data[3]=MSB. The messages are sent every 100 ms.
 *
 * \subsubsection can_rx_sec CAN receive messages
 * A CAN receive task is used to receive pulse width values of the four
 * PWM outputs in can messages id 0x200-0x203, dlc=4,
 * data[0]=LSB,...data[3]=MSB. Valid values are 0 to 10000
 * (percent of pulse width multiplied with 100).
 *
 */

/**
 * \defgroup canio_a_hs_example Kvaser CANdev I/O Module A HS Example
 * \{
 */

#ifndef SRC_CANIO_A_HS_EXAMPLE_H
#define SRC_CANIO_A_HS_EXAMPLE_H

#include <kern.h>
#include <can/can.h>

/** process_data data structure, used to represent process data e.g.  */
typedef struct process_data
{
   mtx_t *  pd_lock;          /**< Process data mutex */
   uint32_t adc_value[5];     /**< Sampled adc values */
   int32_t  pwm_in_per;       /**< Sampled pwm period */
   int32_t  pwm_in_pul;       /**< Sampled pwm pulse */
   int32_t  freq_in[2];       /**< Sampled period */
   uint32_t k_elem[3];        /**< Sampled temperature */
   int32_t  pwm_out_per[4];   /**< PWM period output */
   int32_t  pwm_out_pul[4];   /**< PWM pulse output */
} process_data_t;


#define  ADC_SAMPLE_PRIORITY        10       /**< sample_adc task priority */
#define  ADC_SAMPLE_STACK_SIZE      1024     /**< sample_adc stack size */
#define  ADC_SAMPLE_RATE            10       /**< sample_adc run rate */

#define  PWM_IN_SAMPLE_PRIORITY     10       /**< sample_pwm_in task priority */
#define  PWM_IN_SAMPLE_STACK_SIZE   1024     /**< sample_pwm_in stack size */
#define  PWM_IN_SAMPLE_RATE         20       /**< sample_pwm_in run rate */

#define  FREQ_IN_SAMPLE_PRIORITY    10       /**< sample_freq_in task priority */
#define  FREQ_IN_SAMPLE_STACK_SIZE  1024     /**< sample_freq_in stack size */
#define  FREQ_IN_SAMPLE_RATE        20       /**< sample_freq_in run rate */

#define  K_ELEM_SAMPLE_PRIORITY     10       /**< sample_k_elem task priority */
#define  K_ELEM_SAMPLE_STACK_SIZE   1024     /**< sample_k_elem stack size */
#define  K_ELEM_SAMPLE_RATE         2        /**< sample_k_elem run rate */

#define  PWM_OUT_WRITE_PRIORITY     10       /**< write_pwm_out task priority */
#define  PWM_OUT_WRITE_STACK_SIZE   1024     /**< write_pwm_out stack size */
#define  PWM_OUT_WRITE_RATE         10       /**< write_pwm_out run rate */

#define  CAN_TASK_PRIORITY          10       /**< can_rx_task, can_tx_task task priority */
#define  CAN_TASK_STACK_SIZE        1024     /**< can_rx_task, can_tx_task stack size */
#define  CAN_TASK_TX_RATE           100      /**< can_rx_task, can_tx_task run rate */

#define  CAN_BIT_RATE_125K          0        /**< CAN bitrate 125 Kbit */
#define  CAN_BIT_RATE_250K          1        /**< CAN bitrate 250 Kbit */
#define  CAN_BIT_RATE_500K          2        /**< CAN bitrate 500 Kbit */
#define  CAN_BIT_RATE_1000K         4        /**< CAN bitrate 1000 Kbit */

#define  DUMP_PROCESS_DATA_PRIORITY 10       /**< dump_process_data task priority */
#define  DUMP_PROCESS_DATA_SIZE     1024     /**< dump_process_data stack size */
#define  DUMP_PROCESS_DATA_RATE     2        /**< dump_process_data run rate */

/**
 * can_rx_callback is called by the can driver when a can frame is received,
 * this function is executed in interrupt context and only sets a flag to
 * pass on the actual reading of the can message. The reading of can messages
 * is done in a worker task(can_rx_task).
 *
 * \param arg     Sent as argument setup by can_set_callback.
 * \param event   Current set of callback events.
 *
 */
void can_rx_callback (void * arg, can_event_t event );

/**
 * The function can_rx_task is scheduled as a task and handles all incoming CAN message.
 * It waits for a flag set by can_rx_callback, once the flag is set a CAN
 * message is read from the acctual CAN driver and processed.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void can_rx_task (void * arg);

/**
 * The function can_tx_task is scheduled as a task and is used ro send CAN message.
 * It sends a number of CAN messages at the rate defined by CAN_TASK_TX_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void can_tx_task (void * arg);

/**
 * The function sample_adc is scheduled as a task, it handles reading all
 * analog inputs inputs. Initially it opens all adc drivers/channels and
 * then enters a loop where the analog values are sampled and stored in a
 * process data structure at a rate given by ADC_SAMPLE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_adc(void * arg);

/**
 * The function sample_pwm_in is scheduled as a task, it handles reading of
 * a pulse width modulated signal. Initially it opens the counter driver and
 * then enters a loop where the period and pulse values are sampled and stored in a
 * process data structure at a rate given by PWM_IN_SAMPLE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_pwm_in(void * arg);

/**
 * The function sample_freq_in is scheduled as a task, it handles sampling of
 * frequency. Initially it opens two counter drivers and then enters a loop
 * where the period values are sampled and stored in a process data structure
 * at a rate given by FREQ_IN_SAMPLE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_freq_in(void * arg);

/**
 * The function sample_k_elem is scheduled as a task, it handles reading values
 * from the three MAX6675 Cold-Junction-Compensated K-Thermocouple-to-Digital
 * Converter chips. Initially it opens the three SPI slave chip drivers and
 * then enters a loop where the temperature values are sampled and stored in
 * a process data structure at a rate given by K_ELEM_SAMPLE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void sample_k_elem(void * arg);

/**
 * The function write_pwm_out is scheduled as a task, it handles writing PWM
 * output values taken from the process data structure. Initially it opens
 * all the PWM drivers and then enters a loop where a new pulse width is set
 * to each PWM output at a rate given by PWM_OUT_WRITE_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void write_pwm_out(void * arg);

/**
 * The function dump_process_data is scheduled as a task, it prints the values of the
 * process data structure. The dump_process_data task enters a loop where process data
 * is copied and then printed out on the serial port at a rate given by
 * DUMP_PROCESS_DATA_RATE.
 *
 * \param arg     Void pointer, in this case a process_data_t pointer.
 *
 */
void dump_process_data(void * arg);

/**
 * The function init_can responsible for initiating the CAN driver, setting bit rate and
 * filter parameters, set a callback function on CAN message received and go on bus.
 * Once the initiation is complete it spawns the can_rx_task and the can_tx_task.
 *
 * \param process_data     A process_data_t pointer.
 *
 */
void init_can(process_data_t * process_data);

#endif /* SRC_CANIO_A_HS_EXAMPLE_H */

/**
 * \}
 */

