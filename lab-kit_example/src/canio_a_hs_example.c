/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: main.c 5635 2015-06-12 13:41:07Z rtlbjca $
 *------------------------------------------------------------------------------
 */

#include "canio_a_hs_example.h"
#include <config.h>
#include <adc/adc.h>
#include <counter/counter.h>
#include <spi/spi.h>
#include <pwm/pwm.h>
#include <can/bxcan.h>
#include <bsp.h>

#include <string.h>


#define CAN_MSG_RECEIVED         BIT(0)

typedef struct can_struct
{
   int can_fd;
   flags_t * can_flag;
   process_data_t * process_data;
} can_struct_t;


void can_rx_callback (void * arg, can_event_t event )
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   flags_set(can_struct->can_flag, CAN_MSG_RECEIVED);
}

void can_rx_task (void * arg)
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   can_frame_t rx_frame;
   flags_value_t flags;
   int32_t tmp_data;

   while (1)
   {
      flags_wait_any(can_struct->can_flag, CAN_MSG_RECEIVED, &flags);
      flags_clr(can_struct->can_flag, flags);
      can_receive(can_struct->can_fd, &rx_frame);

      if ((rx_frame.id & 0xFFFFFFFC) == 0x200)
      {
         memcpy(&tmp_data, rx_frame.data, sizeof(tmp_data));
         mtx_lock(can_struct->process_data->pd_lock);
         can_struct->process_data->pwm_out_pul[(rx_frame.id & 0x03)] = tmp_data;
         mtx_unlock(can_struct->process_data->pd_lock);
      }
   }
}

void can_tx_task (void * arg)
{
   can_struct_t * can_struct = (can_struct_t *) arg;
   can_frame_t tx_frame;
   process_data_t tmp_values;

   while (1)
   {

      mtx_lock(can_struct->process_data->pd_lock);
      memcpy(&tmp_values, can_struct->process_data, sizeof(tmp_values));
      mtx_unlock(can_struct->process_data->pd_lock);

      tx_frame.id = 0x100;
      tx_frame.dlc = 4;
      memcpy(tx_frame.data, &can_struct->process_data->adc_value[0], tx_frame.dlc);
      can_transmit(can_struct->can_fd, &tx_frame);

      tx_frame.id = 0x101;
      tx_frame.dlc = 4;
      memcpy(tx_frame.data, &can_struct->process_data->adc_value[1], tx_frame.dlc);
      can_transmit(can_struct->can_fd, &tx_frame);

      tx_frame.id = 0x102;
      tx_frame.dlc = 4;
      memcpy(tx_frame.data, &can_struct->process_data->adc_value[2], tx_frame.dlc);
      can_transmit(can_struct->can_fd, &tx_frame);

      tx_frame.id = 0x103;
      tx_frame.dlc = 4;
      memcpy(tx_frame.data, &can_struct->process_data->adc_value[3], tx_frame.dlc);
      can_transmit(can_struct->can_fd, &tx_frame);

      task_delay(CFG_TICKS_PER_SECOND/CAN_TASK_TX_RATE);
   }
}

void sample_adc(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int adc[5];
   uint32_t tmp_value[5];

   mtx_lock(process_data->pd_lock);
   process_data->adc_value[0] = 0;
   process_data->adc_value[1] = 0;
   process_data->adc_value[2] = 0;
   process_data->adc_value[3] = 0;
   process_data->adc_value[4] = 0;
   mtx_unlock(process_data->pd_lock);

   adc[0] = adc_open ("/adc1/1");
   ASSERT(adc[0] > 0);
   adc[1] = adc_open ("/adc1/2");
   ASSERT(adc[1] > 0);
   adc[2] = adc_open ("/adc1/3");
   ASSERT(adc[2] > 0);
   adc[3] = adc_open ("/adc1/4");
   ASSERT(adc[3] > 0);
   adc[4] = adc_open ("/adc1/5");
   ASSERT(adc[4] > 0);

   adc_start(adc[0]);
   adc_start(adc[1]);
   adc_start(adc[2]);
   adc_start(adc[3]);
   adc_start(adc[4]);

   while(1)
   {
      tmp_value[0] = adc_sample_get(adc[0]);
      tmp_value[1] = adc_sample_get(adc[1]);
      tmp_value[2] = adc_sample_get(adc[2]);
      tmp_value[3] = adc_sample_get(adc[3]);
      tmp_value[4] = adc_sample_get(adc[4]);

      mtx_lock(process_data->pd_lock);
      process_data->adc_value[0] = tmp_value[0];
      process_data->adc_value[1] = tmp_value[1];
      process_data->adc_value[2] = tmp_value[2];
      process_data->adc_value[3] = tmp_value[3];
      process_data->adc_value[4] = tmp_value[4];
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/ADC_SAMPLE_RATE);
   }
}

void sample_pwm_in(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int pwm_in_per;
   int pwm_in_pul;
   int32_t tmp_value[2];

   mtx_lock(process_data->pd_lock);
   process_data->pwm_in_per = 0;
   process_data->pwm_in_pul = 0;
   mtx_unlock(process_data->pd_lock);

   pwm_in_per = counter_open("/counter0/0");
   ASSERT(pwm_in_per > 0);
   pwm_in_pul = counter_open("/counter0/1");
   ASSERT(pwm_in_pul > 0);

   counter_start(pwm_in_per, COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   counter_start(pwm_in_pul, COUNTER_DUAL_EDGE_MEASURE_PULSE);

   /* Dummy read to clear timeout in counter */
   counter_read_time(pwm_in_per);
   counter_read_time(pwm_in_pul);
   task_delay(CFG_TICKS_PER_SECOND/PWM_IN_SAMPLE_RATE);

   while(1)
   {
      tmp_value[0] = counter_read_time(pwm_in_per);
      tmp_value[1] = counter_read_time(pwm_in_pul);

      mtx_lock(process_data->pd_lock);
      process_data->pwm_in_per = tmp_value[0];
      process_data->pwm_in_pul = tmp_value[1];
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/PWM_IN_SAMPLE_RATE);
   }
}

void sample_freq_in(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int freq_in[2];
   int32_t tmp_value[2];

   mtx_lock(process_data->pd_lock);
   process_data->freq_in[0] = 0;
   process_data->freq_in[1] = 0;
   mtx_unlock(process_data->pd_lock);

   freq_in[0] = counter_open("/freq0/0");
   ASSERT(freq_in[0] > 0);
   freq_in[1] = counter_open("/freq1/0");
   ASSERT(freq_in[1] > 0);

   counter_start(freq_in[0], COUNTER_DUAL_EDGE_MEASURE_PERIOD);
   counter_start(freq_in[1], COUNTER_DUAL_EDGE_MEASURE_PERIOD);

   /* Dummy read to clear timeout in counter */
   counter_read_time(freq_in[0]);
   counter_read_time(freq_in[1]);
   task_delay(CFG_TICKS_PER_SECOND/FREQ_IN_SAMPLE_RATE);

   while(1)
   {
      tmp_value[0] = counter_read_time(freq_in[0]);
      tmp_value[1] = counter_read_time(freq_in[1]);

      mtx_lock(process_data->pd_lock);
      process_data->freq_in[0] = tmp_value[0];
      process_data->freq_in[1] = tmp_value[1];
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/FREQ_IN_SAMPLE_RATE);
   }
}

void sample_k_elem(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   char * spi_path[] = {"/spi3/k-temp0", "/spi3/k-temp1", "/spi3/k-temp2"};
   int k_elem[3];
   int32_t tmp_value[3];
   int i;

   mtx_lock(process_data->pd_lock);
   process_data->k_elem[0] = 0;
   process_data->k_elem[1] = 0;
   process_data->k_elem[2] = 0;
   mtx_unlock(process_data->pd_lock);

   k_elem[0] = open (spi_path[0], O_RDONLY, 0);
   ASSERT(k_elem[0] > 0);
   k_elem[1] = open (spi_path[1], O_RDONLY, 0);
   ASSERT(k_elem[1] > 0);
   k_elem[2] = open (spi_path[2], O_RDONLY, 0);
   ASSERT(k_elem[2] > 0);

   while(1)
   {
      for (i = 0; i < 3; i++)
      {
         spi_select (k_elem[i]);
         read (k_elem[i], &tmp_value[i], 1);
         spi_unselect(k_elem[i]);
         tmp_value[i] = (tmp_value[i] >> 3) & 0xFFF;
      }

      mtx_lock(process_data->pd_lock);
      for (i = 0; i < 3; i++)
      {
         process_data->k_elem[i] = tmp_value[i];
      }
      mtx_unlock(process_data->pd_lock);
      task_delay(CFG_TICKS_PER_SECOND/K_ELEM_SAMPLE_RATE);
   }
}

void write_pwm_out(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   int pwm_out[4];
   int duty_cycle[4];
   int i;

   mtx_lock(process_data->pd_lock);
   process_data->pwm_out_per[0] = 0;
   process_data->pwm_out_pul[0] = 0;
   process_data->pwm_out_per[1] = 0;
   process_data->pwm_out_pul[1] = 0;
   process_data->pwm_out_per[2] = 0;
   process_data->pwm_out_pul[2] = 0;
   process_data->pwm_out_per[3] = 0;
   process_data->pwm_out_pul[3] = 0;
   mtx_unlock(process_data->pd_lock);


   pwm_out[0] = pwm_open("/pwm0/0");
   ASSERT(pwm_out[0] > 0);

   pwm_out[1] = pwm_open("/pwm0/1");
   ASSERT(pwm_out[1] > 0);

   pwm_out[2] = pwm_open("/pwm0/2");
   ASSERT(pwm_out[2] > 0);

   pwm_out[3] = pwm_open("/pwm0/3");
   ASSERT(pwm_out[3] > 0);

   for (i = 0; i < 4; i++)
   {
      pwm_frequency_set(pwm_out[i], 50);
      pwm_duty_cycle_set(pwm_out[i], 0);
      pwm_start(pwm_out[i]);
   }

   while(1)
   {
      mtx_lock(process_data->pd_lock);
      for (i = 0; i < 4; i++)
      {
         duty_cycle[i] = process_data->pwm_out_pul[i];
      }

      mtx_unlock(process_data->pd_lock);

      for (i = 0; i < 4; i++)
      {
         pwm_duty_cycle_set(pwm_out[i], duty_cycle[i]);
      }
      task_delay(CFG_TICKS_PER_SECOND/PWM_OUT_WRITE_RATE);
   }
}


void dump_process_data(void * arg)
{
   process_data_t * process_data = (process_data_t *) arg;
   process_data_t tmp_values;
   int i = 0;
   while(1)
   {
      mtx_lock(process_data->pd_lock);
      memcpy(&tmp_values, process_data, sizeof(tmp_values));
      mtx_unlock(process_data->pd_lock);

      gpio_set(GPIO_DEBUG_LED, i%2);
      i++;
      rprintf("adc[0] %08d\nadc[1] %08d\nadc[2] %08d\nadc[3] %08d\nadc[4] %08d   \n"
            "pwm_in_per %08dns\npwm_in_pul %08dns\nfreq_in[0] %08dns\nfreq_in[1] %08dns\n"
            "k_elem[0] %04d.%02d\nk_elem[1] %04d.%02d\nk_elem[2] %04d.%02d\n"
            "pwm_out_pul[0] %08d%%\npwm_out_pul[1] %08d%%\npwm_out_pul[2] %08d%%\npwm_out_pul[3] %08d%%\n",
            tmp_values.adc_value[0], tmp_values.adc_value[1], tmp_values.adc_value[2], tmp_values.adc_value[3], tmp_values.adc_value[4],
            tmp_values.pwm_in_per, tmp_values.pwm_in_pul, tmp_values.freq_in[0], tmp_values.freq_in[1],
            (tmp_values.k_elem[0]/4), (tmp_values.k_elem[0]%4) * 25, (tmp_values.k_elem[1]/4), (tmp_values.k_elem[1]%4) * 25, (tmp_values.k_elem[2]/4), (tmp_values.k_elem[2]%4) * 25,
            tmp_values.pwm_out_pul[0], tmp_values.pwm_out_pul[1], tmp_values.pwm_out_pul[2], tmp_values.pwm_out_pul[3]);
      task_delay(CFG_TICKS_PER_SECOND/DUMP_PROCESS_DATA_RATE);
   }
}

void init_can(process_data_t * process_data)
{
   const uint8_t fifo_mode = 1;
   can_cfg_t can_cfg[] = {
         {125000,  13, 2 ,1, 0},
         {250000,   6, 1 ,1, 0},
         {500000,   6, 1 ,1, 0},
         {1000000,  7, 1 ,1, 0},
      };
   can_filter_t filter = {
        .id   = 0x0,
        .mask = 0x0,
      };
   can_struct_t * can_struct;

   can_struct = malloc(sizeof(can_struct_t));
   UASSERT (can_struct != NULL, EMEM);

   can_struct->can_flag = flags_create(0);
   can_struct->can_fd = open("/can0", O_RDWR, 0);
   UASSERT(can_struct->can_fd != -1, EARG);

   can_struct->process_data = process_data;

   can_set_callback (can_struct->can_fd, can_rx_callback, CAN_EVENT_MSG_RECEIVED, can_struct);
   can_filter (can_struct->can_fd, &filter);
   ioctl (can_struct->can_fd, IOCTL_BXCAN_TXFP, &fifo_mode);

   can_bus_off(can_struct->can_fd);
   can_set_cfg(can_struct->can_fd, &can_cfg[CAN_BIT_RATE_125K]);
   can_bus_on(can_struct->can_fd);

   task_spawn ("can_rx_task", can_rx_task, CAN_TASK_PRIORITY, CAN_TASK_STACK_SIZE, can_struct);
   task_spawn ("can_tx_task", can_tx_task, CAN_TASK_PRIORITY, CAN_TASK_STACK_SIZE, can_struct);
}
