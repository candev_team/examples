/******************************************************************************
 *                *          ***                    ***
 *              ***          ***                    ***
 * ***  ****  **********     ***        *****       ***  ****          *****
 * *********  **********     ***      *********     ************     *********
 * ****         ***          ***              ***   ***       ****   ***
 * ***          ***  ******  ***      ***********   ***        ****   *****
 * ***          ***  ******  ***    *************   ***        ****      *****
 * ***          ****         ****   ***       ***   ***       ****          ***
 * ***           *******      ***** **************  *************    *********
 * ***             *****        ***   *******   **  **  ******         *****
 *                           t h e  r e a l t i m e  t a r g e t  e x p e r t s
 *
 * http://www.rt-labs.com
 * Copyright (C) 2015. rt-labs AB, Sweden. All rights reserved.
 *------------------------------------------------------------------------------
 * $Id: main.c 5635 2015-06-12 13:41:07Z rtlbjca $
 *------------------------------------------------------------------------------
 */
#include "canio_a_hs_example.h"

int main(void)
{
   process_data_t * process_data;

   /* Allocate the process data structure */
   process_data = malloc(sizeof(process_data_t));
   UASSERT (process_data != NULL, EMEM);

   /* Create the process data lock */
   process_data->pd_lock = mtx_create();
   UASSERT(process_data->pd_lock != NULL, EMEM);

   /* Spawn sample tasks */
   task_spawn("sample_adc", sample_adc, ADC_SAMPLE_PRIORITY, ADC_SAMPLE_STACK_SIZE, process_data);
   task_spawn("sample_pwm_in", sample_pwm_in, PWM_IN_SAMPLE_PRIORITY, PWM_IN_SAMPLE_STACK_SIZE, process_data);
   task_spawn("sample_freq_in", sample_freq_in, FREQ_IN_SAMPLE_PRIORITY, FREQ_IN_SAMPLE_STACK_SIZE, process_data);
   task_spawn("sample_k_elem", sample_k_elem, K_ELEM_SAMPLE_PRIORITY, K_ELEM_SAMPLE_STACK_SIZE, process_data);

   /* Spawn write tasks */
   task_spawn("write_pwm_out", write_pwm_out, PWM_OUT_WRITE_PRIORITY, PWM_OUT_WRITE_STACK_SIZE, process_data);

   /* Initiate CAN and CAN rx/tx tasks */
   init_can(process_data);

   /* Initiate CAN and CAN rx/tx tasks */
   task_spawn("dump_process_data", dump_process_data, DUMP_PROCESS_DATA_PRIORITY, DUMP_PROCESS_DATA_SIZE, process_data);

   return 0;
}
